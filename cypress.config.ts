import { defineConfig } from "cypress";
import dotenv from "dotenv";
import { existsSync, rmSync } from "fs";

dotenv.config();

export default defineConfig({
  video: false,
  retries: {
    runMode: 1,
    openMode: 0,
  },
  e2e: {
    baseUrl: process.env.CYPRESS_URL,
    experimentalModifyObstructiveThirdPartyCode: true,
    setupNodeEvents(on) {
      on(
        "after:spec",
        (_spec: Cypress.Spec, results: CypressCommandLine.RunResult) => {
          if (results && results.video) {
            // Do we have failures for any retry attempts?
            const failures = results.tests.some((test) =>
              test.attempts.some((attempt) => attempt.state === "failed"),
            );
            if (!failures && results.video) {
              // delete the video if the spec passed and no tests retried
              existsSync(results.video) && rmSync(results.video);
            }
          }
        },
      );
    },
  },
  env: {
    loginEmail: process.env.CYPRESS_MAIL,
    loginPassword: process.env.CYPRESS_PASSWORD,
    username: process.env.CYPRESS_USERNAME,
    baseUrl: process.env.CYPRESS_URL,
    apiUrl: process.env.CYPRESS_API_URL,
  },
});
