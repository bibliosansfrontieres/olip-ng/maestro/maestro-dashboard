describe("The Home Page", () => {
  it("successfully loads", () => {
    cy.visit("/"); // base Url in cypress.config.ts
    cy.getByDataTestId("loginTitle").should("be.visible");
  });
});
