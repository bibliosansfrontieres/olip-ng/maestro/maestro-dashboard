describe("The Project Page", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.loginToAAD(Cypress.env("loginEmail"), Cypress.env("loginPassword"));
    cy.intercept(`${Cypress.env("apiUrl")}/projects/88040`, {
      fixture: "project/project",
    });
    cy.visit("/projects/88040");
  });
  it("verify project page", () => {
    cy.getByDataTestId("projectInformation").should("be.visible");
    //TODO: find a way to verify the count of device
    cy.getByDataTestId("projectPageDeviceModalButton").should("be.visible");
    cy.getByDataTestId("refreshProjectButton").should("be.visible");
    cy.fixture("project/project").then((project) => {
      cy.getByDataTestId("projectPageVmModalButton").should(
        "contain",
        project.virtualMachines.length,
      );

      cy.getByDataTestId("projectTitle").should(
        "contain.text",
        `${project.title} - ${project.id}`,
      );
      cy.getByDataTestId("projectDescription").should(
        "contain.text",
        project.description,
      );

      cy.getByDataTestId("redirectButtonLink-omekaProject")
        .should(
          "contain.text",
          `http://omeka.tm.bsf-intranet.org/admin/items/show/${project.id}`,
        )
        .should("have.attr", "href")
        .and(
          "include",
          `http://omeka.tm.bsf-intranet.org/admin/items/show/${project.id}`,
        );

      cy.getByDataTestId("projectStartDate").should(
        "contain.text",
        project.startDate,
      );
      cy.getByDataTestId("projectEndDate").should(
        "contain.text",
        project.endDate,
      );
      cy.getByDataTestId("projectLanguages").should(
        "contain.text",
        project?.languages.replace(",", " "),
      );
      cy.getByDataTestId("projectLocation").should(
        "contain.text",
        project.location,
      );
      cy.getByDataTestId("projectNumberDevice").should(
        "contain.text",
        project.device,
      );
      cy.getByDataTestId("projectProjectManager").should(
        "contain.text",
        project?.projectManager.replace("\r\n", " "),
      );
      cy.getByDataTestId("projectPartners").should(
        "contain.text",
        project?.partners.replace("\r\n", " "),
      );
    });
  });
  it("verify project error", () => {
    cy.fixture("project/project").then((project) => {
      cy.getByDataTestId("errorTitle")
        .should("be.visible")
        .should("contain", project.projectErrors.length);

      cy.getByDataTestId("errorCell")
        .should("have.length", project.projectErrors.length)
        .each(($errorCell, index) => {
          cy.wrap($errorCell).within(() => {
            cy.getByDataTestId("errorText").should(
              "contain.text",
              `${Cypress._.upperFirst(project.projectErrors[index].entity.toLowerCase())} #${project.projectErrors[index].entityId} : ${project.projectErrors[index].message}`,
            );

            cy.getByDataTestId("errorLink")
              .should("have.attr", "href")
              .and(
                "include",
                `https://omeka.tm.bsf-intranet.org/admin/${project.projectErrors[index].entity === "PACKAGE" ? "package-manager/index/show/id" : "items/show"}/${project.projectErrors[index].entityId}`,
              );

            cy.getByDataTestId("errorIcon").should("be.visible");
          });
        });
    });
  });
});
