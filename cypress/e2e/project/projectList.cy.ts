describe("The Project List", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.loginToAAD(Cypress.env("loginEmail"), Cypress.env("loginPassword"));
    cy.intercept(`${Cypress.env("apiUrl")}/projects`, {
      fixture: "project/projectList",
    });
    cy.visit("/projects");
  });
  it("verify project list", () => {
    cy.getByDataTestId("projectsCard").should("be.visible");
    cy.fixture("project/projectList").then(({ data }) => {
      cy.getByDataTestId("projectId")
        .should("contain", data[0].id)
        .should("have.attr", "href")
        .and("include", `/projects/${data[0].id}`);
      cy.getByDataTestId("tooltipCell-title").should("contain", data[0].title);
      cy.getByDataTestId("tooltipCell-description").should(
        "contain",
        data[0].description,
      );
      cy.getByDataTestId("updatedDate").should("be.visible");
      cy.getByDataTestId("badgeClickableTable").should("be.visible");
      cy.getByDataTestId("projectPlaylistsCount").should(
        "contain",
        data[0].playlistsCount,
      );
      cy.getByDataTestId("projectItemsCount").should(
        "contain",
        data[0].itemsCount,
      );
      cy.getByDataTestId("projectListVmModalButton").should(
        "contain",
        data[0].virtualMachines.length,
      );
      //TODO: find a way to verify the count of device
      cy.getByDataTestId("projectListDeviceModalButton")
        .scrollIntoView()
        .should("be.visible");
      cy.getByDataTestId("redirectButtonLink-projectId")
        .should("have.attr", "href")
        .and("include", `/projects/${data[0].id}`);
      cy.getByDataTestId("redirectButtonLink-omekaProject")
        .should("have.attr", "href")
        .and(
          "include",
          `http://omeka.tm.bsf-intranet.org/admin/items/show/${data[0].id}`,
        );
      cy.getByDataTestId("refreshProjectButton")
        .scrollIntoView()
        .should("be.visible");
    });
  });
});
