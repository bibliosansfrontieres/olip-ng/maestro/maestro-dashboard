describe("The Navbar", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.loginToAAD(Cypress.env("loginEmail"), Cypress.env("loginPassword"));
    cy.visit("/");
  });
  it("verify navbar", () => {
    cy.getByDataTestId("navbar").should("be.visible");
    cy.getByDataTestId("routeName").should("be.visible");
    cy.getByDataTestId("badgeMenu").should("be.visible");
    cy.getByDataTestId("notificationsButton").should("be.visible");
    cy.getByDataTestId("darkMode").should("be.visible");
    cy.getByDataTestId("language").should("be.visible");
    cy.getByDataTestId("userDropdownButton").should("be.visible");
  });
  it("verify notifications", () => {
    cy.getByDataTestId("notificationsButton").click();
    // assert toast when clicking read all notifications
    cy.getByDataTestId("readAllNotificationsButton").click();
    cy.get(".Toastify__toast-body").should("be.visible");
    // assert redirecting to notifications list page
    // force true because of toast
    cy.getByDataTestId("notificationsButton").click({ force: true });
    cy.getByDataTestId("seeAllNotificationsButton").click();
    cy.url().should("contain", "notifications");
  });
  it("verify dark mode", () => {
    // to ensure the localStorage darkMode is set
    cy.getByDataTestId("darkMode").click();
    cy.getAllLocalStorage().then((localStorages) => {
      // get the darkMode local storage before clicking
      const beforeDarkMode = localStorages[Cypress.env("baseUrl")].darkMode;
      cy.getByDataTestId("darkMode").click();
      cy.getAllLocalStorage().then((afterLocalStorages) => {
        // get the darkMode local storage after clicking
        const afterDarkMode =
          afterLocalStorages[Cypress.env("baseUrl")].darkMode;
        // assert both are string of boolean
        expect(afterDarkMode).to.be.oneOf(["true", "false"]);
        expect(beforeDarkMode).to.be.oneOf(["true", "false"]);
        // assert they are not the same
        expect(afterDarkMode).to.not.eq(beforeDarkMode);
      });
    });
  });
  it("verify language", () => {
    cy.getAllLocalStorage().then((localStorages) => {
      // get the language local storage before clicking
      const beforeLanguage = localStorages[Cypress.env("baseUrl")].language;
      cy.getByDataTestId("language").click();
      // get first language dropdown
      cy.getByDataTestId("language-0").click();
      cy.getAllLocalStorage().then((afterLocalStorages) => {
        // get the language local storage after clicking
        const afterLanguage =
          afterLocalStorages[Cypress.env("baseUrl")].language;
        // assert both are string of FRA or ENG
        expect(beforeLanguage).to.be.oneOf(["FRA", "ENG"]);
        expect(afterLanguage).to.be.oneOf(["FRA", "ENG"]);
        // assert they are not the same
        expect(beforeLanguage).to.not.eq(afterLanguage);
      });
    });
  });
  it("verify user dropdown", () => {
    cy.getByDataTestId("userDropdownButton").click();
    cy.getByDataTestId("dropdownUserName").contains(Cypress.env("username"));
    cy.getByDataTestId("logoutButton").click();
    cy.url().should("contain", "login");
  });
});
