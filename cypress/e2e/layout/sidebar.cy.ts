describe("The Sidebar", () => {
  beforeEach(() => {
    cy.loginToAAD(Cypress.env("loginEmail"), Cypress.env("loginPassword"));
    cy.visit("/");
  });
  // @TODO: fix both tests :
  // first got error on click() terminated
  // second is flacky because X button disappear
  xit("verify each page", () => {
    const pages = [
      "/",
      "/create",
      "/virtual-machines",
      "/devices",
      "/deploys",
      "/projects",
      "/dashboard",
      "/users",
      "/groups",
      "/context",
      "/fqdns",
      "/labels",
      "/logs",
    ];
    cy.viewport(1280, 720);
    cy.getByDataTestId("sidebar").should("be.visible");
    cy.get("ul>a").should("have.length", pages.length);
    // for each page, verify href link thenk click on it and verify url
    cy.get("ul>a").each(($a, index) => {
      const href = $a.attr("href");
      expect(pages[index]).to.eq(href);
      cy.wrap($a).click();
      cy.url().should("contain", href);
    });
  });
  xit("verifiy sidebar collapse on little screen", () => {
    cy.viewport(600, 720);
    // the sidebar should be visible on opening
    cy.getByDataTestId("sidebar").should("be.visible");
    // close the navbar then reopen it with the navbar button
    cy.getByDataTestId("sidebarCloseButton").click();
    cy.getByDataTestId("sidebar").should("not.be.visible");
    cy.getByDataTestId("sidebarOpenButton").click();
    cy.getByDataTestId("sidebar").should("be.visible");
  });
});
