describe("Test the VM buttons", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.loginToAAD(Cypress.env("loginEmail"), Cypress.env("loginPassword"));
    cy.intercept(`${Cypress.env("apiUrl")}/virtual-machines/dkfdof`, {
      fixture: "vm/vm",
    });
    cy.visit("/virtual-machines/dkfdof");
    cy.intercept(
      `${Cypress.env("apiUrl")}/virtual-machines/dkfdof/duplicate`,
    ).as("duplicate");
  });
  it("should test the duplicate button", () => {
    cy.getByDataTestId("duplicateVmButton").click();
    cy.getByDataTestId("inputField-title")
      .should("be.visible")
      .type("Test duplication de la VM");
    cy.getByDataTestId("inputField-description")
      .should("be.visible")
      .type("Description test duplication de la VM");
    cy.getByDataTestId("inputField-referenceUrl")
      .should("be.visible")
      .type("http://ulr-test.fr");
    cy.getByDataTestId("submitButton").click();
    cy.wait("@duplicate").then((interception) => {
      expect(interception.request.body).to.eql({
        id: "dkfdof",
        title: "Test duplication de la VM",
        description: "Description test duplication de la VM",
        referenceUrl: "http://ulr-test.fr",
      });
    });
  });
});
