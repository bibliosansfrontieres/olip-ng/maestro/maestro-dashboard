describe("The Vm History Tab", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.loginToAAD(Cypress.env("loginEmail"), Cypress.env("loginPassword"));
    cy.intercept(`${Cypress.env("apiUrl")}/virtual-machines/dkfdof/history`, {
      fixture: "vm/tabs/vmHistoryList",
    });
    cy.intercept(`${Cypress.env("apiUrl")}/virtual-machines/dkfdof`, {
      fixture: "vm/vm",
    });
    cy.visit("/virtual-machines/dkfdof/history");
  });
  it("verify vm history tab", () => {
    cy.getByDataTestId("vmHistoryTable").should("be.visible");
    cy.getByDataTestId("vmHistoryTableBody").should("be.visible");
    cy.fixture("vm/tabs/vmHistoryList").then(({ data }) => {
      cy.getByDataTestId("vmHistoryTableRow").each(($row, index) => {
        cy.wrap($row).within(() => {
          cy.getByDataTestId("vmHistoryTableDate").then(($el) => {
            expect($el.attr("data-test-value")).to.eq(data[index].createdAt);
          });
          cy.getByDataTestId("informationClickableTable")
            .eq(0)
            .should("contain", data[index].user.name);
          cy.getByDataTestId("informationClickableTable")
            .eq(1)
            .then(($el) => {
              expect($el.attr("data-test-value")).to.eq(data[index].action);
            });
          cy.getByDataTestId("informationClickableTable")
            .eq(2)
            .then(($el) => {
              expect($el.attr("data-test-value")).to.eq(data[index].entity);
            });
          if (index > 0) {
            cy.getByDataTestId("valuePopoverButton").should("be.visible");
            Object.entries(JSON.parse(data[index].values)).forEach(
              (element) => {
                cy.getByDataTestId("valuePopoverBody")
                  .should("contain", element[0])
                  .and("contain", element[1]);
              },
            );
          }
          cy.getByDataTestId("vmHistoryTableDescription").should(
            "contain",
            data[index].description ?? "",
          );
        });
      });
    });
  });
});
