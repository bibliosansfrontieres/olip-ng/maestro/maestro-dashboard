describe("The VM context tab", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.loginToAAD(Cypress.env("loginEmail"), Cypress.env("loginPassword"));
    cy.intercept(`${Cypress.env("apiUrl")}/virtual-machines/dkfdof`, {
      fixture: "vm/vm",
    });
    cy.intercept(`${Cypress.env("apiUrl")}/applications*`, {
      fixture: "vm/tabs/applicationsContext",
    });
    cy.intercept(`${Cypress.env("apiUrl")}/default*`, {
      fixture: "vm/tabs/defaultContext",
    });
    // context 2 because in fixture virtual machine
    cy.intercept(`${Cypress.env("apiUrl")}/contexts/2`).as("PatchContext");

    cy.fixture("vm/tabs/applicationsContext").then(({ data }) => {
      data.forEach((_, index: string | number) => {
        cy.intercept(
          `${Cypress.env("apiUrl")}/contexts/2/${data[index].id}`,
        ).as(`PatchApplication${index}`);
      });
    });

    cy.visit("/virtual-machines/dkfdof/context");
  });

  it("verify the vm context tab", () => {
    cy.getByDataTestId("searchBar").should("be.visible");

    // playlist configuration
    cy.getByDataTestId("playlistConfiguration").should("be.visible");
    cy.getByDataTestId("outsideProjectDownloadsAuthorizedSwitch")
      .should("be.visible")
      .should("be.disabled")
      .should("be.checked");

    // application configuration
    cy.getByDataTestId("applicationConfiguration").should("be.visible");
    cy.fixture("vm/tabs/applicationsContext").then(({ data }) => {
      cy.getByDataTestId("applicationCard").should("have.length", data.length);
      cy.getByDataTestId("applicationCard").each(($appCard, index) => {
        cy.wrap($appCard).within(() => {
          cy.getByDataTestId("applicationCardHeader").should(
            "contain",
            data[index].displayName,
          );
          cy.getByDataTestId("applicationCardDescription").should("be.visible");
          cy.getByDataTestId("applicationCardSeeMore").should("be.visible");
        });
      });
    });
  });

  it("modify the vm context", () => {
    // click on edit button
    cy.getByDataTestId("contextEditButton")
      .should("be.visible")
      .should("not.be.disabled")
      .click();
    // verify modal
    cy.getByDataTestId("modalContent").should("be.visible");
    cy.getByDataTestId("contextModalContinueButton").should("be.visible");
    cy.getByDataTestId("contextModalCancelButton").should("be.visible");
    // confirm edition mode
    cy.getByDataTestId("contextModalContinueButton").click();
    // assert warning edition
    cy.getByDataTestId("warningModifyContext").should("be.visible");

    // assert send right request
    cy.getByDataTestId("outsideProjectDownloadsAuthorizedSwitch").click();

    // change context download playlists
    cy.wait("@PatchContext").then((interception) => {
      expect(interception.request.body).to.eql({
        id: "2",
        isOutsideProjectDownloadsAuthorized: false,
      });
    });

    // change context applications
    cy.getByDataTestId("AppDefaultContext").each(($switch, index) => {
      cy.wrap($switch).click();
      cy.wait(`@PatchApplication${index}`);
    });
  });
});
