// ***********************************************************
// This example support/e2e.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import "./commands";

// Alternatively you can use CommonJS syntax:
// require('./commands')
function loginViaAAD(username: string, password: string) {
  cy.session(
    username,
    () => {
      cy.visit("http://localhost:5173/");
      cy.get('[data-test-id="login-button"]').click();

      // Login to your AAD tenant.
      cy.origin(
        "login.microsoftonline.com",
        {
          args: {
            username_arg: username,
            password_arg: password,
          },
        },
        ({ username_arg, password_arg }) => {
          cy.get('input[type="email"]').type(username_arg, {
            log: false,
          });
          cy.get('input[type="submit"]').click();
          cy.get('input[type="password"]').type(password_arg, {
            log: false,
          });
          cy.get('input[type="submit"]').click();
          cy.get("#idBtn_Back").click();
        },
      );

      // Ensure Microsoft has redirected us back to the sample app with our logged in user.
      cy.url().should("equal", "http://localhost:5173/");
      cy.get('[data-test-id="navbar"]').should("contain", `Accueil`);
    },
    {
      cacheAcrossSpecs: true,
    },
  );
}

Cypress.Commands.add("loginToAAD", (username: string, password: string) => {
  const log = Cypress.log({
    displayName: "Azure Active Directory Login",
    message: [`🔐 Authenticating | ${username}`],
    autoEnd: false,
  });
  log.snapshot("before");

  loginViaAAD(username, password);

  log.snapshot("after");
  log.end();
});

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface Chainable {
      loginToAAD(username: string, password: string): Chainable<void>;
      getByDataTestId(dataTestId: string): Chainable<JQuery<HTMLElement>>;
      // loginCached(): Chainable<void>;
    }
  }
}
