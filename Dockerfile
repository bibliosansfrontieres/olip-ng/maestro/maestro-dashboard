###############
# BUILD STAGE #
###############

FROM node:21-alpine3.19 AS build

# Install install only express and dotenv for server.js in /production
WORKDIR /production
RUN npm install express dotenv

# Install dev node modules and build project in /build
WORKDIR /build
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

####################
# PRODUCTION STAGE #
####################

FROM node:21-alpine3.19 AS production

WORKDIR /app

# Copy node modules for server.js
COPY --from=build /production/node_modules ./node_modules

# Copy builded node project
COPY --from=build /build/dist ./dist
# TODO : Public folder might be useless
COPY --from=build /build/public ./public
COPY --from=build /build/server.js ./server.js
COPY package.json .

ENV NODE_ENV production

# Run entrypoint to update env variables at runtime
COPY entrypoint.sh .
RUN chmod +x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]

CMD [ "node", "server.js" ]
