import { defineConfig } from "vite";

import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  envPrefix: "PUBLIC_",
  resolve: {
    alias: {
      src: "/src",
      assets: "/src/assets/",
      common: "/src/common/",
      components: "/src/components/",
      constants: "/src/constants/",
      contexts: "/src/contexts/",
      enums: "/src/enums/",
      hooks: "/src/hooks/",
      interfaces: "/src/interfaces/",
      loaders: "/src/loaders/",
      pages: "/src/pages/",
      services: "/src/services/",
      translations: "/src/translations/",
      types: "/src/types/",
      utils: "/src/utils/",
      "@axios": "/node_modules/axios",
    },
  },
});
