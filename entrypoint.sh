#!/bin/sh

# Add a variable: "VARIABLE_NAME":"'"${VARIABLE_NAME}"'", \
JSON_STRING='window.configs = { \
  "PUBLIC_API_URL":"'"${PUBLIC_API_URL}"'", \
  "PUBLIC_BASE_URL":"'"${PUBLIC_BASE_URL}"'", \
  "PUBLIC_DASHBOARD_URL":"'"${PUBLIC_DASHBOARD_URL}"'", \
  "PUBLIC_DEBUG":"'"${PUBLIC_DEBUG}"'", \
  "PUBLIC_BASE_URL":"'"${PUBLIC_BASE_URL}"'", \
}'

sed -i "s@// CONFIGURATIONS_PLACEHOLDER@${JSON_STRING}@" /app/dist/index.html

exec "$@"
