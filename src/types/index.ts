import { PermissionNameEnum } from "enums/permissionName.enum";

export type Page = {
  name: string;
  link: string;
  icon?: JSX.Element;
  permission?: PermissionNameEnum;
};
export type Route = { name: string; link: string };
