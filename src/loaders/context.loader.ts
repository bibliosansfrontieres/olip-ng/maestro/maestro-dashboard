import { ApplicationTypeNameEnum } from "enums/applicationTypeName.enum";
import { PageOrderEnum } from "enums/pageOrder.enum";
import { defer } from "react-router-dom";
import { getDefaultContext } from "services/contexts/contexts.service";

export default async function contextLoader({ request }: { request: Request }) {
  const url = new URL(request.url);
  const page = url.searchParams.get("page");
  const order = url.searchParams.get("order") as PageOrderEnum;
  const take = url.searchParams.get("take");
  const type = url.searchParams.get("type") as ApplicationTypeNameEnum;
  const query = url.searchParams.get("query") || undefined;

  const result = getDefaultContext({
    page: Number(page),
    order,
    take: Number(take),
    type,
    query,
  });

  return defer({ result });
}
