import { defer, Params } from "react-router-dom";
import { getFqdn } from "services/fqdnMasks/fqdns.service";

export default async function fqdnLoader({
  params,
}: {
  params: Params;
  request: Request;
}) {
  const id = params.id;

  if (id) {
    const result = getFqdn({ id });
    return defer({ result });
  }
}
