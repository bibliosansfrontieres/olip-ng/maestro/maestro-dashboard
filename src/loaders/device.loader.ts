import { defer, Params } from "react-router-dom";
import { getDevice } from "src/services/devices/devices.service";

export default async function deviceLoader({
  params,
}: {
  params: Params;
  request: Request;
}) {
  const id = params.id;

  if (id) {
    const result = getDevice({ id });
    return defer({ result });
  }
}
