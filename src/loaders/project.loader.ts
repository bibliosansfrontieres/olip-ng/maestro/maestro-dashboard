import { defer, Params } from "react-router-dom";
import { getProject } from "services/projects/projects.service";

export default async function projectLoader({
  params,
}: {
  params: Params;
  request: Request;
}) {
  const id = params.id;

  if (id) {
    const result = getProject({ id });
    return defer({ result });
  }
}
