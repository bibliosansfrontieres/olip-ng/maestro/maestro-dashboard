import { defer, Params } from "react-router-dom";
import { getDeploy } from "services/deploys/deploys.service";

export default async function deployLoader({
  params,
}: {
  params: Params;
  request: Request;
}) {
  const id = params.id;

  if (id) {
    const result = getDeploy({ id: parseInt(id) });
    return defer({ result });
  }
}
