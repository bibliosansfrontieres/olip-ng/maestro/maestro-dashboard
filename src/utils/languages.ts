import { MaestroLanguagesEnum } from "enums/maestroLanguages.enum";

const languages = {
  [MaestroLanguagesEnum.ENG]: "en",
  [MaestroLanguagesEnum.FRA]: "fr",
};
export const convertLanguagesIso6393ToIso6391 = (
  iso6393: MaestroLanguagesEnum,
) => {
  return languages[iso6393];
};
