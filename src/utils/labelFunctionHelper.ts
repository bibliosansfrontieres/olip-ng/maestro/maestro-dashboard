import { DeployStatusEnum } from "enums/deployStatus.enum";
import { ILabelDeploys } from "interfaces/label.interface";

export const nbTotalDeploy = (label: ILabelDeploys) => label.deploys.length;

export const nbArchivedDeploy = (label: ILabelDeploys) => {
  return label.deploys.filter(
    (deploy) => deploy.status === DeployStatusEnum.ARCHIVED,
  ).length;
};

export const nbActiveDeploy = (label: ILabelDeploys) => {
  return label.deploys.filter(
    (deploy) => deploy.status === DeployStatusEnum.DEPLOYED,
  ).length;
};
