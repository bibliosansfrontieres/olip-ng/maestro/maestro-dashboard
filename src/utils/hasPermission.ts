import { PermissionNameEnum } from "enums/permissionName.enum";
import { RoleNameEnum } from "enums/roleName.enum";
import { IUser } from "interfaces/user.interface";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";

export const hasPermission = (user: IUser, permission: PermissionNameEnum) => {
  return user.role.permissions.map((p) => p.name).includes(permission);
};

export const hasPermissions = (
  user: IUser,
  permissions: PermissionNameEnum | PermissionNameEnum[],
) => {
  if (Array.isArray(permissions)) {
    return permissions.every((p) => hasPermission(user, p));
  }
  return hasPermission(user, permissions);
};

export const userCanPatchVm = (
  user: IUser | null,
  vm: IVirtualMachine,
  onlyCreator = false,
) => {
  if (!user) {
    return false;
  }
  if (vm.user.id === user.id) {
    return true;
  }
  if (user.role.name === RoleNameEnum.ADMIN) {
    return true;
  }
  if (onlyCreator === true) {
    return false;
  }
  const userGroupIds = user.groups.map((group) => group.id);
  const vmGroupIds = vm.groups.filter((group) =>
    userGroupIds.includes(group.id),
  );
  return vmGroupIds.length > 0;
};
