function findTranslation<T extends { languageIdentifier: string }>(
  languageIdentifier: string,
  translations: T[],
): T | undefined {
  let translation = undefined;
  for (const translationValue of translations) {
    if (translationValue.languageIdentifier === languageIdentifier) {
      translation = translationValue;
      break;
    }
  }
  return translation;
}

export function getTranslation<T extends { languageIdentifier: string }>(
  languageIdentifier: string,
  translations: T[],
): T {
  let translation = findTranslation(languageIdentifier, translations);

  if (!translation) {
    translation = findTranslation("eng", translations);
  }

  if (!translation) {
    translation = translations[0];
  }

  return translation;
}
