export function capitalize(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
export function camelToSnakeCase(string: string) {
  return string.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);
}

export function snakeToCamelCase(string: string) {
  return string.replace(/(?!^)_(.)/g, (_, char) => char.toUpperCase());
}

export function removeId(string: string | undefined) {
  if (string) {
    return string.replace(/\.id/, "");
  }
  return "";
}
