import { DeployOrderByEnum } from "enums/orderBy/deployOrderBy.enum";
import { DeviceOrderByEnum } from "enums/orderBy/deviceOrderBy.enum";
import { FqdnMaskOrderByEnum } from "enums/orderBy/fqdnMaskOrderBy.enum";
import { GroupOrderByEnum } from "enums/orderBy/groupOrderBy.enum";
import { LabelOrderByEnum } from "enums/orderBy/labelOrderBy.enum";
import { LogOrderByEnum } from "enums/orderBy/logOrderBy.enum";
import { ProjectOrderByEnum } from "enums/orderBy/projectOrderBy.enum";
import { UserOrderByEnum } from "enums/orderBy/userOrderBy.enum";
import { VirtualMachineOrderByEnum } from "enums/orderBy/virtualMachineOrderBy.enum";
import { VirtualMachineSaveOrderByEnum } from "enums/orderBy/virtualMachineSaveOrderBy.enum";
import { PageOrderEnum } from "enums/pageOrder.enum";
import { TableNamesEnum } from "enums/table.enum";
import { ApplicationOrderByEnum } from "src/enums/orderBy/applicationOrderBy.enum";
import { NotificationOrderByEnum } from "src/enums/orderBy/notificationOrderBy.enum";

type TableDefinition = {
  defaultOrderBy: string;
  defaultOrder: PageOrderEnum;
};

export const tableDefinitions: { [key in TableNamesEnum]: TableDefinition } = {
  [TableNamesEnum.VIRTUAL_MACHINE]: {
    defaultOrderBy: VirtualMachineOrderByEnum.CREATED_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.GROUPS]: {
    defaultOrderBy: GroupOrderByEnum.NAME,
    defaultOrder: PageOrderEnum.ASC,
  },
  [TableNamesEnum.HISTORY]: {
    defaultOrderBy: LogOrderByEnum.CREATED_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.DEPLOY]: {
    defaultOrderBy: DeployOrderByEnum.CREATED_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.VIRTUAL_MACHINE_SAVE]: {
    defaultOrderBy: VirtualMachineSaveOrderByEnum.CREATED_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.DEVICE]: {
    defaultOrderBy: DeviceOrderByEnum.LAST_SEEN_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.PROJECT]: {
    defaultOrderBy: ProjectOrderByEnum.UPDATED_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.USER]: {
    defaultOrderBy: UserOrderByEnum.NAME,
    defaultOrder: PageOrderEnum.ASC,
  },
  [TableNamesEnum.FQDN]: {
    defaultOrderBy: FqdnMaskOrderByEnum.LABEL,
    defaultOrder: PageOrderEnum.ASC,
  },
  [TableNamesEnum.LABEL]: {
    defaultOrderBy: LabelOrderByEnum.UPDATED_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.NOTIFICATION]: {
    defaultOrderBy: NotificationOrderByEnum.CREATED_AT,
    defaultOrder: PageOrderEnum.DESC,
  },
  [TableNamesEnum.APPLICATION]: {
    defaultOrderBy: ApplicationOrderByEnum.NAME,
    defaultOrder: PageOrderEnum.DESC,
  },
};
