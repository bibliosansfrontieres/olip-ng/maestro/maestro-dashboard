export const areArrayEqual = (
  array1: { [key: string]: unknown }[],
  array2: { [key: string]: unknown }[],
  filterKey: string = "id",
) => {
  if (array1.length !== array2.length) {
    return false;
  }
  if (array1.length === 0 && array2.length === 0) {
    return true;
  }
  const array1Filtered = array1.map((elt) => elt[filterKey]);
  const array2Filtered = array2.map((elt) => elt[filterKey]);
  array1Filtered.forEach((id) => {
    if (!array2Filtered.includes(id)) {
      return false;
    }
  });
  return true;
};
