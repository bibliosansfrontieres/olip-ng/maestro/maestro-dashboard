import { t } from "i18next";
import { toast } from "react-toastify";
import clearLocalStorage from "utils/clearLocalStorage";
import getEnv from "utils/getEnv";

import { AxiosError } from "@axios";

export function toastError(error: unknown, translation: string) {
  if ((error as AxiosError)?.code !== "ERR_CANCELED") {
    toast.error(translation);
  }
}

export default function consoleError(error: unknown, context?: string) {
  if (getEnv("PUBLIC_DEBUG") === "true") {
    console.error(error, "-----", context);
  }
}

export function handleApiError({
  error,
  translation,
  context,
}: {
  error: unknown;
  translation?: string;
  context?: string;
}) {
  consoleError(error, context);
  if (
    (error as AxiosError)?.response?.status === 401 &&
    // @ts-expect-error because of response type depend from T but we do not know it
    (error as AxiosError)?.response?.data?.message?.toLowerCase() ===
      "Invalid token".toLowerCase()
  ) {
    clearLocalStorage();
    window.location.href = `/login?redirect=${window.location.pathname}`;
  }
  if ((error as AxiosError)?.response?.status === 401) {
    toast.error(t("error.unauthorized"));
    return;
  }
  if (translation) {
    toast.error(translation);
    return;
  }
  toast.error(t("toast.error"));
  return;
}
