import { DeployStatusEnum } from "enums/deployStatus.enum";
import { DeviceStatusEnum } from "enums/deviceStatus.enum";
import { LogLevelEnum } from "enums/logLevel.enum";
import { VirtualMachineApprovalEnum } from "enums/virtualMachineApproval.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import { ProjectStatusEnum } from "src/enums/projectStatus.enum";

export const colorVmStatus = (status: VirtualMachineStatusEnum) => {
  switch (status) {
    case VirtualMachineStatusEnum.CHECKING_PROJECT:
    case VirtualMachineStatusEnum.CREATING_VIRTUAL_MACHINE:
    case VirtualMachineStatusEnum.INSTALLING_PROJECT:
    case VirtualMachineStatusEnum.STARTING:
    case VirtualMachineStatusEnum.STOPPING:
      return "orange";
    case VirtualMachineStatusEnum.PROJECT_ERROR:
    case VirtualMachineStatusEnum.CREATING_VIRTUAL_MACHINE_ERROR:
    case VirtualMachineStatusEnum.OFFLINE:
    case VirtualMachineStatusEnum.INSTALLING_PROJECT_ERROR:
    case VirtualMachineStatusEnum.STARTING_ERROR:
    case VirtualMachineStatusEnum.STOPPING_ERROR:
    case VirtualMachineStatusEnum.IMPORT_DATABASE_ERROR:
      return "red";
    case VirtualMachineStatusEnum.ONLINE:
      return "green";
    default:
      return "gray";
  }
};

export const colorVmApproval = (approval: VirtualMachineApprovalEnum) => {
  switch (approval) {
    case VirtualMachineApprovalEnum.IN_PROGRESS:
      return "orange";
    case VirtualMachineApprovalEnum.APPROVED:
      return "green";
    default:
      return "gray";
  }
};

export const colorLogLevel = (level: string) => {
  switch (level) {
    case LogLevelEnum.DEBUG:
      return "blue";
    case LogLevelEnum.INFO:
      return "green";
    case LogLevelEnum.WARN:
      return "yellow";
    case LogLevelEnum.ERROR:
      return "red";
    default:
      return "gray";
  }
};
export const colorDeviceStatus = (status: DeviceStatusEnum) => {
  switch (status) {
    case DeviceStatusEnum.DEPLOYING:
    case DeviceStatusEnum.INSTALLING:
      return "orange";
    case DeviceStatusEnum.ERROR:
      return "red";
    case DeviceStatusEnum.READY:
    case DeviceStatusEnum.DEPLOYED:
      return "green";
    default:
      return "gray";
  }
};

export const colorDeployStatus = (status: DeployStatusEnum) => {
  switch (status) {
    case DeployStatusEnum.PENDING:
    case DeployStatusEnum.ORDER_SENT:
    case DeployStatusEnum.DEPLOYING:
    case DeployStatusEnum.INSTALLING:
      return "orange";
    case DeployStatusEnum.DEPLOYED:
      return "green";
    case DeployStatusEnum.ERROR:
      return "red";
    default:
      return "gray";
  }
};

export const colorProjectStatus = (status: ProjectStatusEnum) => {
  switch (status) {
    case ProjectStatusEnum.CHECKING:
    case ProjectStatusEnum.DOWNLOADING:
      return "orange";
    case ProjectStatusEnum.READY:
      return "green";
    case ProjectStatusEnum.ERROR:
      return "red";
    default:
      return "gray";
  }
};

export const colorDeviceIsAlive = (isAlive: boolean) => {
  if (isAlive) {
    return "green";
  }
  return "gray";
};

export const colorNotificationIsSeen = (isSeen: boolean) => {
  if (isSeen) {
    return "green";
  }
  return "red";
};
