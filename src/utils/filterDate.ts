import { getDefaultLocale } from "react-datepicker";

export default function filterDate(date: string) {
  const d = new Date(date);
  //@TODO: change toLocaleString for format (see documentation : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString)
  return d.toLocaleString(getDefaultLocale() === "ENG" ? "en-US" : "fr-FR");
}
