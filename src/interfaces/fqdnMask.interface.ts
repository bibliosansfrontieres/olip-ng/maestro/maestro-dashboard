export interface IFqdnMask {
  id: number;
  label: string;
  mask: string;
}
