import { VirtualMachineSaveTypeEnum } from "enums/virtualMachineSaveType.enum";
import { IDeployFull } from "interfaces/deploy.interface";
import { IUser } from "interfaces/user.interface";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";

export interface IVirtualMachineSave {
  id: string;
  name: string;
  description: string | null;
  type: VirtualMachineSaveTypeEnum;
  createdAt: number;
}

export interface IVirtualMachineSaveWithUser extends IVirtualMachineSave {
  user: IUser;
}

export interface IVirtualMachineSaveWithVm extends IVirtualMachineSave {
  virtualMachine: IVirtualMachine;
}

export interface IVirtualMachineSaveWithDeploy extends IVirtualMachineSave {
  deploys: IDeployFull[];
}
