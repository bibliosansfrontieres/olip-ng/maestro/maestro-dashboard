import {
  ICurrentLoadData,
  IDiskLayoutData,
  IDisksIoData,
  IFsSizeData,
  IMemData,
  INetworkStatsData,
} from "interfaces/monitoring.interface";

export interface IMonitoringValues {
  load: ICurrentLoadData;
  memory: IMemData;
  networkStats: INetworkStatsData[];
  diskLayout: IDiskLayoutData[];
  fsSize: IFsSizeData[];
  disksIo: IDisksIoData;
}
