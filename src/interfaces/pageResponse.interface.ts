import { IPagination } from "interfaces/pagination.interface";

export interface IPageResponse<T> {
  data: T[];
  meta: IPagination;
}
