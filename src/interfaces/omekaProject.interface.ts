import { ItemSourceEnum } from "enums/itemSource.enum";
import { ProjectStatusEnum } from "enums/projectStatus.enum";
import { IProjectError } from "interfaces/projectError.interface";
import { IVirtualMachineWithVmSaveAndDeploy } from "interfaces/virtualMachine.interface";

export interface IOmekaProject {
  id: number;
  source: ItemSourceEnum;
  status: ProjectStatusEnum;
  url: string;
  title: string;
  description: string;
  date: string;
  languages: string;
  startDate: string;
  endDate: string;
  partners: string;
  location: string;
  device: string;
  projectManager: string;
  virtualMachines?: IVirtualMachineWithVmSaveAndDeploy[];
  projectErrors?: IProjectError[];
  playlistsCount?: number;
  itemsCount?: number;
  createdAt: string;
  updatedAt: string;
}
