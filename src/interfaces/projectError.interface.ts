import { VirtualMachineErrorEntityEnum } from "src/enums/virtualMachineEntity.enum";

export interface IProjectError {
  id: number;
  entity: VirtualMachineErrorEntityEnum;
  entityId: number;
  message: string;
}
