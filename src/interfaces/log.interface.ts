import { LogActionEnum } from "enums/logAction.enum";
import { LogEntityEnum } from "enums/logEntity.enum";
import { LogLevelEnum } from "enums/logLevel.enum";
import { IUser } from "interfaces/user.interface";

export interface ILog {
  id: number;
  action: LogActionEnum;
  entity: LogEntityEnum | null;
  entityId: string | null;
  values: string | null;
  description: string | null;
  level: LogLevelEnum;
  createdAt: string;
  user: IUser;
}
