import { PageOrderEnum } from "enums/pageOrder.enum";

export interface IPageRequest {
  order?: PageOrderEnum;
  page?: number;
  take?: number;
}

export interface IPageRequestOrderBy extends IPageRequest {
  orderBy?: string;
}
