export interface IFullConvo {
  text: string;
  type: "request" | "response";
  error: boolean;
}

export interface IDeviceExecResponse {
  response: {
    stdout?: string;
    stderr?: string;
    error?: { code: number; killed: boolean; signal: unknown; cmd: string };
  };
}
