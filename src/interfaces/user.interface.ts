import { IGroup } from "interfaces/group.interface";
import { IRole } from "interfaces/role.interface";

export interface IUser {
  id: string;
  mail: string;
  name: string;
  avatar: string;
  role: IRole;
  groups: IGroup[];
}
