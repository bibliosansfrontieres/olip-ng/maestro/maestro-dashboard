import { DeployStatusEnum } from "enums/deployStatus.enum";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { ILabel } from "interfaces/label.interface";
import { IUser } from "interfaces/user.interface";
import { IVirtualMachineSaveWithVm } from "interfaces/virtualMachineSave.interface";

export interface IDeploy {
  id: number;
  status: DeployStatusEnum;
  createdAt: string;
  updatedAt: string;
}

export interface IDeployWithVmAndLabelsAndUser extends IDeploy {
  virtualMachineSave: IVirtualMachineSaveWithVm;
  labels: ILabel[];
  user: IUser;
}

export interface IDeployFull extends IDeployWithVmAndLabelsAndUser {
  device: IDevice & { fqdnMask: IFqdnMask };
}

export interface IDeployWithDevice extends IDeploy {
  device: IDevice;
}
