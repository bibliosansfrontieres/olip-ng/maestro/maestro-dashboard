export interface INotification {
  id: string;
  message: string;
  values: string;
  isSeen: boolean;
  createdAt: string;
  updatedAt: string;
}
