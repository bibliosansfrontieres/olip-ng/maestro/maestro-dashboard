import { IApplication } from "./application.interface";

export interface IContext {
  id: number;
  isOutsideProjectDownloadsAuthorized: boolean;
  isDefaultContext: boolean;
  originalContext: string;
  applications: IApplication[];
}
