import { RoleNameEnum } from "enums/roleName.enum";
import { IPermission } from "interfaces/permission.interface";

export interface IRole {
  id: number;
  name: RoleNameEnum;
  permissions: IPermission[];
}
