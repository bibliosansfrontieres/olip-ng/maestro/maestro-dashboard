import { ItemSourceEnum } from "enums/itemSource.enum";
import { IItem } from "interfaces/item.interface";

export interface IPlaylist {
  id: string;
  image: string;
  source: ItemSourceEnum;
  version: string | null;
  size: number;
}

export interface IPlaylistWithItems extends IPlaylist {
  items: IItem[];
}
