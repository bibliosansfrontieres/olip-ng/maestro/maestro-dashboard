import { ApplicationTypeNameEnum } from "enums/applicationTypeName.enum";

export interface IApplicationTypeTranslation {
  id: number;
  label: string;
  description: string;
  languageIdentifier: string;
}

export interface IApplicationType {
  id: string;
  name: ApplicationTypeNameEnum;
  md5: string;
  applicationTypeTranslations: IApplicationTypeTranslation[];
}

export interface IApplicationTranslation {
  id: number;
  shortDescription: string;
  longDescription: string;
  languageIdentifier: string;
}

export interface IApplication {
  id: string;
  name: string;
  displayName: string;
  size: number;
  version: string;
  logo: string;
  image: string;
  compose: string;
  hooks: string;
  md5: string;
  applicationType: IApplicationType;
  applicationTranslations: IApplicationTranslation[];
}
