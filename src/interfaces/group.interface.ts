import { IUser } from "interfaces/user.interface";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";

export interface IGroup {
  id: string;
  name: string;
  description: string;
  isArchived: boolean;
}

export interface IGroupUsers extends IGroup {
  users?: IUser[];
  virtualMachines?: IVirtualMachine[];
}

export interface IGroupUsersVirtualMachines extends IGroupUsers {
  virtualMachines?: IVirtualMachine[];
}
