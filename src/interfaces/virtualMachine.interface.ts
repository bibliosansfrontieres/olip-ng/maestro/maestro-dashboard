import { VirtualMachineApprovalEnum } from "enums/virtualMachineApproval.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import { IContext } from "interfaces/context.interface";
import { IGroup } from "interfaces/group.interface";
import { IOmekaProject } from "interfaces/omekaProject.interface";
import { IUser } from "interfaces/user.interface";
import { IVirtualMachineSaveWithDeploy } from "interfaces/virtualMachineSave.interface";

export interface IVirtualMachine {
  id: string;
  title: string;
  defaultLanguageIdentifier: string;
  description?: string;
  referenceUrl?: string;
  status: VirtualMachineStatusEnum;
  approval: VirtualMachineApprovalEnum;
  projectVersion: string;
  isUpdateAvailable: boolean;
  isArchived: boolean;
  user: IUser;
  context: IContext;
  groups: IGroup[];
  createdAt: string;
  updatedAt: string;
  project: IOmekaProject;
}

export interface IVirtualMachineWithVmSaveAndDeploy extends IVirtualMachine {
  virtualMachineSaves: IVirtualMachineSaveWithDeploy[];
}
