import { PermissionNameEnum } from "enums/permissionName.enum";

export interface IPermission {
  id: number;
  name: PermissionNameEnum;
}
