export interface IMemData {
  total: number;
  free: number;
  used: number;
  active: number;
  available: number;
  buffcache: number;
  buffers: number;
  cached: number;
  slab: number;
  swaptotal: number;
  swapused: number;
  swapfree: number;
  writeback: number | null;
  dirty: number | null;
}

interface ICurrentLoadCpuData {
  load: number;
  loadUser: number;
  loadSystem: number;
  loadNice: number;
  loadIdle: number;
  loadIrq: number;
  loadSteal: number;
  loadGuest: number;
  rawLoad: number;
  rawLoadUser: number;
  rawLoadSystem: number;
  rawLoadNice: number;
  rawLoadIdle: number;
  rawLoadIrq: number;
  rawLoadSteal: number;
  rawLoadGuest: number;
}
export interface ICurrentLoadData {
  avgLoad: number;
  currentLoad: number;
  currentLoadUser: number;
  currentLoadSystem: number;
  currentLoadNice: number;
  currentLoadIdle: number;
  currentLoadIrq: number;
  currentLoadSteal: number;
  currentLoadGuest: number;
  rawCurrentLoad: number;
  rawCurrentLoadUser: number;
  rawCurrentLoadSystem: number;
  rawCurrentLoadNice: number;
  rawCurrentLoadIdle: number;
  rawCurrentLoadIrq: number;
  rawCurrentLoadSteal: number;
  rawCurrentLoadGuest: number;
  cpus: ICurrentLoadCpuData[];
}

export interface INetworkStatsData {
  iface: string;
  operstate: string;
  rx_bytes: number;
  rx_dropped: number;
  rx_errors: number;
  tx_bytes: number;
  tx_dropped: number;
  tx_errors: number;
  rx_sec: number;
  tx_sec: number;
  ms: number;
}

interface ISmartData {
  json_format_version: number[];
  smartctl: {
    version: number[];
    platform_info: string;
    build_info: string;
    argv: string[];
    exit_status: number;
  };
  device: {
    name: string;
    info_name: string;
    type: string;
    protocol: string;
  };
  model_family?: string;
  model_name?: string;
  serial_number?: string;
  firmware_version?: string;
  smart_status: {
    passed: boolean;
  };
  trim?: {
    supported: boolean;
  };
  ata_smart_attributes?: {
    revision: number;
    table: {
      id: number;
      name: string;
      value: number;
      worst: number;
      thresh: number;
      when_failed: string;
      flags: {
        value: number;
        string: string;
        prefailure: boolean;
        updated_online: boolean;
        performance: boolean;
        error_rate: boolean;
        event_count: boolean;
        auto_keep: boolean;
      };
      raw: {
        value: number;
        string: string;
      };
    }[];
  };
  ata_smart_error_log?: {
    summary: {
      revision: number;
      count: number;
    };
  };
  ata_smart_self_test_log?: {
    standard: {
      revision: number;
      table: {
        type: {
          value: number;
          string: string;
        };
        status: {
          value: number;
          string: string;
          passed: boolean;
        };
        lifetime_hours: number;
      }[];
      count: number;
      error_count_total: number;
      error_count_outdated: number;
    };
  };
  nvme_pci_vendor?: {
    id: number;
    subsystem_id: number;
  };
  nvme_smart_health_information_log?: {
    critical_warning?: number;
    temperature?: number;
    available_spare?: number;
    available_spare_threshold?: number;
    percentage_used?: number;
    data_units_read?: number;
    data_units_written?: number;
    host_reads?: number;
    host_writes?: number;
    controller_busy_time?: number;
    power_cycles?: number;
    power_on_hours?: number;
    unsafe_shutdowns?: number;
    media_errors?: number;
    num_err_log_entries?: number;
    warning_temp_time?: number;
    critical_comp_time?: number;
    temperature_sensors?: number[];
  };
  user_capacity?: {
    blocks: number;
    bytes: number;
  };
  logical_block_size?: number;
  temperature: {
    current: number;
  };
  power_cycle_count: number;
  power_on_time: {
    hours: number;
  };
}

export interface IDiskLayoutData {
  device: string;
  type: string;
  name: string;
  vendor: string;
  size: number;
  bytesPerSector: number;
  totalCylinders: number;
  totalHeads: number;
  totalSectors: number;
  totalTracks: number;
  tracksPerCylinder: number;
  sectorsPerTrack: number;
  firmwareRevision: string;
  serialNum: string;
  interfaceType: string;
  smartStatus: string;
  temperature: null | number;
  smartData?: ISmartData;
}

export interface IFsSizeData {
  fs: string;
  type: string;
  size: number;
  used: number;
  available: number;
  use: number;
  mount: string;
  rw: boolean | null;
}

export interface IDisksIoData {
  rIO: number;
  wIO: number;
  tIO: number;
  rIO_sec: number | null;
  wIO_sec: number | null;
  tIO_sec: number | null;
  rWaitTime: number;
  wWaitTime: number;
  tWaitTime: number;
  rWaitPercent: number | null;
  wWaitPercent: number | null;
  tWaitPercent: number | null;
  ms: number;
}
