import { IDeploy } from "./deploy.interface";

export interface ILabel {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
}

export interface ILabelDeploys extends ILabel {
  deploys: IDeploy[];
}
