import { DeviceStatusEnum } from "enums/deviceStatus.enum";

export interface IDevice {
  id: string;
  status: DeviceStatusEnum;
  vendor: string;
  os: string;
  macAddress: string;
  publicIpv4: string | null;
  ipv4FqdnName: string | null;
  ipv4FqdnArpa: string | null;
  privateIpv4: string | null;
  publicIpv6: string | null;
  ipv6FqdnName: string | null;
  ipv6FqdnArpa: string | null;
  serialId: string;
  isAlive: boolean;
  lastSeenAt: string;
  createdAt: string;
  updatedAt: string;
  blinking: boolean;
}
