import { ItemSourceEnum } from "src/enums/itemSource.enum";

export interface IItem {
  id: string;
  thumbnail: string | null;
  createdCountry: string;
  version: string | null;
  source: ItemSourceEnum;
}
