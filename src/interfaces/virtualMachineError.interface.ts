import { VirtualMachineErrorEntityEnum } from "enums/virtualMachineEntity.enum";

export interface IVirtualMachineError {
  id: number;
  entity: VirtualMachineErrorEntityEnum;
  entityId: number;
  message: string;
}
