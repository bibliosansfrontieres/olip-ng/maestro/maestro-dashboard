import { BooleanEnum } from "enums/boolean.enum";
import { t } from "i18next";
import { toast } from "react-toastify";
import { getApplications } from "services/applications/applications.service";
import { IGetApplicationsParams } from "services/applications/interfaces/getApplications.interface";
import {
  IDeleteContextApplicationParams,
  IDeleteContextApplicationResponse,
} from "services/contexts/interfaces/deleteContextApplication.interface";
import {
  IDeleteContextDefaultApplicationParams,
  IDeleteContextDefaultApplicationResponse,
} from "services/contexts/interfaces/deleteContextsDefaultApplication.interface";
import { IGetContextsDefaultResponse } from "services/contexts/interfaces/getContextsDefault.interface";
import {
  IPatchContextParams,
  IPatchContextResponse,
} from "services/contexts/interfaces/patchContext.interface";
import {
  IPatchContextDefaultParams,
  IPatchContextDefaultResponse,
} from "services/contexts/interfaces/patchContextDefault.interface";
import {
  IPostContextApplicationParams,
  IPostContextApplicationResponse,
} from "services/contexts/interfaces/postContextApplication.interface";
import {
  IPostContextDefaultApplicationParams,
  IPostContextDefaultApplicationResponse,
} from "services/contexts/interfaces/postContextDefaultApplication.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getContextsDefault = async (): Promise<
  ApiRequestResponse<IGetContextsDefaultResponse>
> => {
  const path = "/contexts/default";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("contexts.get_context_default.error"),
    });
    return undefined;
  }
};

export const getDefaultContext = async (params?: IGetApplicationsParams) => {
  const defaultContext = await getContextsDefault();
  const applications = await getApplications({
    order: params?.order,
    type: params?.type,
    page: Number(params?.page) || undefined,
    take: Number(params?.take) || undefined,
    query: params?.query,
    isOlipExcluded: BooleanEnum.TRUE,
  });
  return { defaultContext, applications };
};

export const patchDefaultContext = async (
  params: IPatchContextDefaultParams,
): Promise<ApiRequestResponse<IPatchContextDefaultResponse>> => {
  const path = "/contexts/default";
  try {
    const result = await Axios.patch(path, params);
    toast.success(t("contexts.patch_default_context.success"));
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("contexts.patch_default_context.error"),
    });
    return undefined;
  }
};

export const postDefaultContextApplication = async (
  params: IPostContextDefaultApplicationParams,
): Promise<ApiRequestResponse<IPostContextDefaultApplicationResponse>> => {
  const path = `/contexts/default/${params.applicationId}`;
  try {
    const result = await Axios.post(path);
    toast.success(t("contexts.post_default_context_application.success"));
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("contexts.post_default_context_application.error"),
    });
    return undefined;
  }
};

export const deleteDefaultContextApplication = async (
  params: IDeleteContextDefaultApplicationParams,
): Promise<ApiRequestResponse<IDeleteContextDefaultApplicationResponse>> => {
  const path = `/contexts/default/${params.applicationId}`;
  try {
    const result = await Axios.delete(path);
    toast.success(t("contexts.delete_default_context_application.success"));
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("contexts.delete_default_context_application.error"),
    });
    return undefined;
  }
};

export const patchContext = async (
  params: IPatchContextParams,
): Promise<ApiRequestResponse<IPatchContextResponse>> => {
  const path = `/contexts/${params.id}`;
  try {
    const result = await Axios.patch(path, params);
    toast.success(t("contexts.patch_context.success"));
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("contexts.patch_context.error") });
    return undefined;
  }
};

export const postContextApplication = async (
  params: IPostContextApplicationParams,
): Promise<ApiRequestResponse<IPostContextApplicationResponse>> => {
  const path = `/contexts/${params.id}/${params.applicationId}`;
  try {
    const result = await Axios.post(path);
    toast.success(t("contexts.post_context_application.success"));
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("contexts.post_context_application.error"),
    });
    return undefined;
  }
};

export const deleteContextApplication = async (
  params: IDeleteContextApplicationParams,
): Promise<ApiRequestResponse<IDeleteContextApplicationResponse>> => {
  const path = `/contexts/${params.id}/${params.applicationId}`;
  try {
    const result = await Axios.delete(path);
    toast.success(t("contexts.delete_context_application.success"));
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("contexts.delete_context_application.error"),
    });
    return undefined;
  }
};
