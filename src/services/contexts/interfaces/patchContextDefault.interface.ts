import { IContext } from "interfaces/context.interface";

export interface IPatchContextDefaultResponse extends IContext {}

export interface IPatchContextDefaultParams {
  isOutsideProjectDownloadsAuthorized: boolean;
}
export interface IPostContextDefaultRequest
  extends IPatchContextDefaultParams {}
