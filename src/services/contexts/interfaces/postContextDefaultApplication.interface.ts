import { IContext } from "interfaces/context.interface";

export interface IPostContextDefaultApplicationResponse extends IContext {}

export interface IPostContextDefaultApplicationParams {
  applicationId: string;
}
export interface IPostContextDefaultApplicationRequest
  extends IPostContextDefaultApplicationParams {}
