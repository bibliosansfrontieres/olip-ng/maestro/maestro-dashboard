import { IContext } from "interfaces/context.interface";

export interface IPostContextApplicationResponse extends IContext {}

export interface IPostContextApplicationParams {
  applicationId: string;
  id: string;
}
export interface IPostContextApplicationRequest
  extends IPostContextApplicationParams {}
