export interface IDeleteContextDefaultApplicationResponse {}

export interface IDeleteContextDefaultApplicationParams {
  applicationId: string;
}
export interface IDeleteContextDefaultApplicationRequest
  extends IDeleteContextDefaultApplicationParams {}
