import { IContext } from "interfaces/context.interface";

export interface IDeleteContextApplicationResponse extends IContext {}

export interface IDeleteContextApplicationParams {
  applicationId: string;
  id: string;
}
export interface IDeleteContextApplicationRequest
  extends IDeleteContextApplicationParams {}
