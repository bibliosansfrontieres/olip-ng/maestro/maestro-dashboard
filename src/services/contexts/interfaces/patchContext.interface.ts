import { IContext } from "interfaces/context.interface";

export interface IPatchContextResponse extends IContext {}

export interface IPatchContextParams {
  id: string;
  isOutsideProjectDownloadsAuthorized: boolean;
}
export interface IPostContextRequest extends IPatchContextParams {}
