import { IContext } from "interfaces/context.interface";

export interface IGetContextsDefaultResponse extends IContext {}
