import {
  IPostTokenRequest,
  IPostTokenResponse,
} from "services/auth/interfaces/postToken.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import handleApiError from "utils/handleApiError";

export const postToken = async (
  params: IPostTokenRequest,
): Promise<ApiRequestResponse<IPostTokenResponse>> => {
  const path = "/auth/token";
  try {
    const result = await Axios.post(path, params);
    return result.data;
  } catch (error) {
    handleApiError({ error });
    return undefined;
  }
};
