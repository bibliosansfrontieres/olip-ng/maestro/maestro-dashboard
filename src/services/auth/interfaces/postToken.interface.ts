export interface IPostTokenRequest {
  refreshToken: string;
}

export interface IPostTokenResponse {
  accessToken: string;
  expiresAt: number;
}
