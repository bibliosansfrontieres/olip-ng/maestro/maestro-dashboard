import { IDeployFull } from "interfaces/deploy.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetDeploysResponse extends IPageResponse<IDeployFull> {}

export interface IGetDeploysParams extends IPageRequest {
  statuses?: string;
  userIds?: string;
  labelIds?: string;
  query?: string;
  orderBy?: string;
  createdDateRange?: string;
}
