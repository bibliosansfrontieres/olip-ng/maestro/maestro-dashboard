export interface IPatchDeployParams {
  id: string;
  labelIds: number[];
}

export interface IPatchDeployResponse {}
