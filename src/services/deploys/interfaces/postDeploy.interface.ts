export interface IPostDeployParams {
  virtualMachineSaveId: string;
  fqdnMaskId: number;
  labelIds: number[];
  devicesCount: number;
}

export interface IPostDeployResponse {}
