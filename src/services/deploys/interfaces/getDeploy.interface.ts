import { IDeployFull } from "interfaces/deploy.interface";

export interface IGetDeployParams {
  id: number;
}

export interface IGetDeployResponse extends IDeployFull {}
