import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetDeployParams,
  IGetDeployResponse,
} from "services/deploys/interfaces/getDeploy.interface";
import {
  IGetDeploysParams,
  IGetDeploysResponse,
} from "services/deploys/interfaces/getDeploys.interface";
import {
  IPatchDeployParams,
  IPatchDeployResponse,
} from "services/deploys/interfaces/patchDeploy.interface";
import {
  IPostDeployParams,
  IPostDeployResponse,
} from "services/deploys/interfaces/postDeploy.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const postDeploy = async (
  params: IPostDeployParams,
): Promise<ApiRequestResponse<IPostDeployResponse>> => {
  const path = "/deploys";

  try {
    await Axios.post(path, params);
    // because a notification is already sent via socket
    // toast.success(t("deploy.post_deploy.success"));
    return true;
  } catch (error) {
    if (
      //@ts-expect-error because the error contains a message but there isn't in AxiosError type
      (error as AxiosError).response?.data?.message!.includes(
        "not enough devices available",
      )
    ) {
      toast.error(t("deploy.post_deploy.not_enough_devices_error"));
      return undefined;
    }
    handleApiError({ error, translation: t("deploy.post_deploy.error") });
    return undefined;
  }
};

export const getDeploys = async (
  params: IGetDeploysParams,
): Promise<ApiRequestResponse<IGetDeploysResponse>> => {
  const path = "/deploys";
  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("deploy.get_deploys.error") });
    return undefined;
  }
};

export const getDeploy = async (
  params: IGetDeployParams,
): Promise<ApiRequestResponse<IGetDeployResponse>> => {
  const path = `/deploys/${params.id}`;

  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("deploy.get_deploy.error") });
    return undefined;
  }
};

export const patchDeploy = async (
  params: IPatchDeployParams,
): Promise<ApiRequestResponse<IPatchDeployResponse>> => {
  const path = `/deploys/${params.id}`;

  try {
    await Axios.patch(path, params);
    toast.success(t("deploy.patch_deploy.success"));
    return true;
  } catch (error) {
    handleApiError({ error, translation: t("deploy.patch_deploy.error") });
    return undefined;
  }
};
