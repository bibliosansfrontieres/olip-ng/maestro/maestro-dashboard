import { ILabel } from "interfaces/label.interface";

export interface IPostLabelParams {
  name: string;
}
export interface IPostLabelResponse extends ILabel {}
