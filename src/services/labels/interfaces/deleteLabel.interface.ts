export interface IDeleteLabelParams {
  id: string;
}

export interface IDeleteLabelResponse {}
