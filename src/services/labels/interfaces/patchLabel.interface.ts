import { ILabel } from "interfaces/label.interface";

export interface IPatchLabelParams {
  id: number;
  name: string;
}
export interface IPatchLabelResponse extends ILabel {}
