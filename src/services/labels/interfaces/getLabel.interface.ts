import { ILabel } from "interfaces/label.interface";

export interface IGetLabelParams {
  id: string;
}

export interface IGetLabelResponse extends ILabel {}
