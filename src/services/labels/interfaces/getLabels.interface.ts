import { ILabelDeploys } from "interfaces/label.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetLabelsParams extends IPageRequest {
  query?: string;
  createdDateRange?: string;
  updatedDateRange?: string;
  orderBy?: string;
}

export interface IGetLabelsResponse extends IPageResponse<ILabelDeploys> {}
