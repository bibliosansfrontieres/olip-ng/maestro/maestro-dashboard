import { AxiosError } from "axios";
import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IDeleteLabelParams,
  IDeleteLabelResponse,
} from "services/labels/interfaces/deleteLabel.interface";
import {
  IGetLabelParams,
  IGetLabelResponse,
} from "services/labels/interfaces/getLabel.interface";
import {
  IGetLabelsParams,
  IGetLabelsResponse,
} from "services/labels/interfaces/getLabels.interface";
import {
  IPatchLabelParams,
  IPatchLabelResponse,
} from "services/labels/interfaces/patchLabel.interface";
import {
  IPostLabelParams,
  IPostLabelResponse,
} from "services/labels/interfaces/postLabel.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getLabels = async (
  params: IGetLabelsParams,
): Promise<ApiRequestResponse<IGetLabelsResponse>> => {
  const path = "/labels";
  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("label.get_labels.error") });
    return undefined;
  }
};

export const postLabel = async (
  params: IPostLabelParams,
): Promise<ApiRequestResponse<IPostLabelResponse>> => {
  const path = "/labels";
  try {
    const result = await Axios.post(path, params);
    toast.success(t("label.post_label.success"));
    return result.data;
  } catch (error) {
    if (
      //@ts-expect-error because the error contains a message but there isn't in AxiosError type
      (error as AxiosError).response?.data?.message!.includes(
        "name must be longer than or equal to 3 characters",
      )
    ) {
      toast.error(t("label.post_label.name_error"));
      return undefined;
    }
    handleApiError({ error, translation: t("label.post_label.error") });
    return undefined;
  }
};

export const getLabel = async (
  params: IGetLabelParams,
): Promise<ApiRequestResponse<IGetLabelResponse>> => {
  const path = `/labels/${params.id}`;
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("label.get_label.error") });
    return undefined;
  }
};

export const patchLabel = async (
  params: IPatchLabelParams,
): Promise<ApiRequestResponse<IPatchLabelResponse>> => {
  const path = `/labels/${params.id}`;
  try {
    const result = await Axios.patch(path, params);
    return result.data;
  } catch (error) {
    if (
      //@ts-expect-error because the error contains a message but there isn't in AxiosError type
      (error as AxiosError).response?.data?.message!.includes(
        "name must be longer than or equal to 3 characters",
      )
    ) {
      toast.error(t("label.post_label.name_error"));
      return undefined;
    }
    if (
      //@ts-expect-error because the error contains a message but there isn't in AxiosError type
      (error as AxiosError).response?.data?.message!.includes(
        "label name already exists",
      )
    ) {
      toast.error(t("label.patch_label.already_exist_name_error"));
      return undefined;
    }
    handleApiError({ error, translation: t("label.patch_label.error") });
    return undefined;
  }
};

export const deleteLabel = async (
  params: IDeleteLabelParams,
): Promise<ApiRequestResponse<IDeleteLabelResponse>> => {
  const path = `/labels/${params.id}`;
  try {
    await Axios.delete(path);
    toast.success(t("label.delete_label.success"));
    return true;
  } catch (error) {
    handleApiError({ error, translation: t("label.patch_label.error") });
    return undefined;
  }
};
