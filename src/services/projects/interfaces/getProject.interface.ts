import { IOmekaProject } from "interfaces/omekaProject.interface";

export interface IGetProjectParams {
  id: string;
}

export interface IGetProjectResponse extends IOmekaProject {}
