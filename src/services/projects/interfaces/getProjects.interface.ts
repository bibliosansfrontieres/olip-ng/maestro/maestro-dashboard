import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { IOmekaProject } from "src/interfaces/omekaProject.interface";

export interface IGetProjectsResponse extends IPageResponse<IOmekaProject> {}

export interface IGetProjectsParams extends IPageRequest {
  query?: string;
  orderBy?: string;
  updatedDateRange?: string;
  statuses?: string;
}
