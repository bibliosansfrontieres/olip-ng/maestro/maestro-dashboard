import { AxiosError } from "axios";
import { t } from "i18next";
import { toast } from "react-toastify";
import {
  ICreateProjectParams,
  ICreateProjectResponse,
} from "services/projects/interfaces/createProject.interface";
import {
  IGetProjectParams,
  IGetProjectResponse,
} from "services/projects/interfaces/getProject.interface";
import {
  IGetProjectsParams,
  IGetProjectsResponse,
} from "services/projects/interfaces/getProjects.interface";
import {
  IPatchProjectParams,
  IPatchProjectResponse,
} from "services/projects/interfaces/patchProject.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getProject = async (
  params: IGetProjectParams,
): Promise<ApiRequestResponse<IGetProjectResponse>> => {
  const path = `/projects/${params.id}`;

  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("project.get_project.error") });
    return undefined;
  }
};

export const getFullProject = async (
  params: IGetProjectParams,
): Promise<ApiRequestResponse<IGetProjectResponse>> => {
  const path = `/projects/full/${params.id}`;

  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("project.get_project.error") });
    return undefined;
  }
};

export const getProjects = async (
  params: IGetProjectsParams,
): Promise<ApiRequestResponse<IGetProjectsResponse>> => {
  const path = "/projects";
  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("project.get_projects.error") });
    return undefined;
  }
};

export const patchProject = async (
  params: IPatchProjectParams,
): Promise<ApiRequestResponse<IPatchProjectResponse>> => {
  const path = `/projects/${params.id}`;
  try {
    //@TODO: remove toast info (and translation) when the back is refactored and answer instantly
    toast.info(t("project.patch_project.sent"));
    return Axios.patch(path, params);
  } catch (error) {
    handleApiError({ error, translation: t("project.patch_project.error") });
    return undefined;
  }
};

export const createProject = async (
  params: ICreateProjectParams,
): Promise<ApiRequestResponse<ICreateProjectResponse>> => {
  const path = `/projects/${params.id}`;
  try {
    //@TODO: remove toast info (and translation) when the back is refactored and answer instantly
    toast.info(t("project.create_project.sent"));
    await Axios.post(path);
    toast.success(t("project.create_project.success"));
    return true;
  } catch (error) {
    if ((error as AxiosError).response?.status === 409) {
      toast.error(t("project.create_project_conflict_error"));
    } else if ((error as AxiosError).code === "ECONNABORTED") {
      return undefined;
    } else {
      handleApiError({ error, translation: t("project.create_project.error") });
    }
    return undefined;
  }
};
