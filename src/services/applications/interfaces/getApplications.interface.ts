import { ApplicationTypeNameEnum } from "enums/applicationTypeName.enum";
import { BooleanEnum } from "enums/boolean.enum";
import { IApplication } from "interfaces/application.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetApplicationsResponse extends IPageResponse<IApplication> {}

export interface IGetApplicationsRequest extends IPageRequest {
  query?: string;
  type?: ApplicationTypeNameEnum;
  isOlipExcluded?: BooleanEnum;
}

export interface IGetApplicationsParams extends IGetApplicationsRequest {}
