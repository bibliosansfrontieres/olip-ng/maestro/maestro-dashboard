import { t } from "i18next";
import {
  IGetApplicationsParams,
  IGetApplicationsRequest,
  IGetApplicationsResponse,
} from "services/applications/interfaces/getApplications.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getApplications = async (
  params?: IGetApplicationsParams,
): Promise<ApiRequestResponse<IGetApplicationsResponse>> => {
  const path = "/applications";
  const requestParams: IGetApplicationsRequest = {
    ...params,
  };
  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("applications.get_applications.error"),
    });
    return undefined;
  }
};
