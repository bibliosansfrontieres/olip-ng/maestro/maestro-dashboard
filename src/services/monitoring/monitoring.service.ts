import { t } from "i18next";
import {
  ICurrentLoadData,
  IDiskLayoutData,
  IDisksIoData,
  IFsSizeData,
  IMemData,
  INetworkStatsData,
} from "interfaces/monitoring.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getLoad = async (): Promise<
  ApiRequestResponse<ICurrentLoadData>
> => {
  const path = "/monitoring/load";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("monitoring.get_load.error") });
    return undefined;
  }
};

export const getMemory = async (): Promise<ApiRequestResponse<IMemData>> => {
  const path = "/monitoring/memory";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("monitoring.get_memory.error") });
    return undefined;
  }
};

export const getNetworkStats = async (): Promise<
  ApiRequestResponse<INetworkStatsData[]>
> => {
  const path = "/monitoring/network-stats";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("monitoring.get_network_stats.error"),
    });
    return undefined;
  }
};

export const getDiskLayout = async (): Promise<
  ApiRequestResponse<IDiskLayoutData[]>
> => {
  const path = "/monitoring/disk-layout";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("monitoring.get_disk_layout.error"),
    });
    return undefined;
  }
};

export const getFsSize = async (): Promise<
  ApiRequestResponse<IFsSizeData[]>
> => {
  const path = "/monitoring/fs-size";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("monitoring.get_fs_size.error"),
    });
    return undefined;
  }
};

export const getDisksIo = async (): Promise<
  ApiRequestResponse<IDisksIoData>
> => {
  const path = "/monitoring/disks-io";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("monitoring.get_disk_io.error"),
    });
    return undefined;
  }
};
