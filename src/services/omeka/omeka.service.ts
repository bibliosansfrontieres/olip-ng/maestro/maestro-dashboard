import { AxiosError } from "axios";
import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetOmekaCheckProjectParams,
  IGetOmekaCheckProjectResponse,
} from "services/omeka/interfaces/getOmekaCheckProject.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getOmekaCheckProject = async (
  params: IGetOmekaCheckProjectParams,
): Promise<ApiRequestResponse<IGetOmekaCheckProjectResponse>> => {
  const path = `/omeka/check-project/${params.projectId}`;

  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    if ((error as AxiosError).response?.status === 404) {
      toast.error(t("omeka.check_project_not_found"));
    } else {
      handleApiError({ error, translation: t("omeka.check_project_error") });
    }
    return undefined;
  }
};
