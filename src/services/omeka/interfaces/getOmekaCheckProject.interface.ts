import { IOmekaProject } from "interfaces/omekaProject.interface";

export interface IGetOmekaCheckProjectResponse extends IOmekaProject {}

export interface IGetOmekaCheckProjectParams {
  projectId: string;
}
