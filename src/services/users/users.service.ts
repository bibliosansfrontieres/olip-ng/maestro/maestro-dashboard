import { t } from "i18next";
import { IGroup } from "interfaces/group.interface";
import { toast } from "react-toastify";
import {
  IDeleteUserGroupsDeleteParams,
  IDeleteUserGroupsDeleteResponse,
} from "services/users/interfaces/deleteUserGroupsDelete.interface";
import { IGetProfileResponse } from "services/users/interfaces/getProfile.interface";
import {
  IGetUserParams,
  IGetUserResponse,
} from "services/users/interfaces/getUser.interface";
import {
  IGetUsersParams,
  IGetUsersRequest,
  IGetUsersResponse,
} from "services/users/interfaces/getUsers.interface";
import {
  IGetUsersIdGroupParams,
  IGetUsersIdGroupResponse,
} from "services/users/interfaces/getUsersIdGroup.interface";
import {
  IPatchUserParams,
  IPatchUserRequest,
  IPatchUserResponse,
} from "services/users/interfaces/patchUser.interface";
import {
  IPostUserGroupsAddParams,
  IPostUserGroupsAddResponse,
} from "services/users/interfaces/postUserGroupsAdd.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

import { AxiosRequestConfig } from "@axios";

export const getProfile = async (): Promise<IGetProfileResponse> => {
  const path = "/users/profile";
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error });
    throw error;
  }
};

export const getUsersIdGroups = async (
  params: IGetUsersIdGroupParams,
  axiosConfig?: AxiosRequestConfig,
): Promise<ApiRequestResponse<IGetUsersIdGroupResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/users/${id}/groups`;
  try {
    const result = await Axios.get(path, {
      params: requestParams,
      ...axiosConfig,
    });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("users.get_users.error") });
    return undefined;
  }
};

export const getUsers = async (
  params?: IGetUsersParams,
): Promise<ApiRequestResponse<IGetUsersResponse>> => {
  const path = "/users";
  const requestParams: IGetUsersRequest = {
    ...params,
  };
  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("users.get_users.error") });
    return undefined;
  }
};

export const patchUser = async (
  params: IPatchUserParams,
): Promise<ApiRequestResponse<IPatchUserResponse>> => {
  const path = `/users/${params.id}`;
  const requestParams: IPatchUserRequest = {
    roleName: params.roleName,
  };

  try {
    const result = await Axios.patch(path, requestParams);
    toast.success(t("users.patch_users.success", { name: result.data.name }));
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("users.patch_users.error") });
    return undefined;
  }
};
export const postUserGroupsAdd = async (
  params: IPostUserGroupsAddParams,
): Promise<ApiRequestResponse<IPostUserGroupsAddResponse>> => {
  const { userId, groupId } = params;
  const path = `/users/${userId}/groups/add/${groupId}`;
  try {
    const result = await Axios.post(path);
    toast.success(
      t("users.post_user_group_add.success", {
        userName: result.data.name,
        groupName: result.data.groups.find(
          (group: IGroup) => group.id === groupId,
        ).name,
      }),
    );
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("users.post_user_group_add.error"),
    });
    return undefined;
  }
};

export const deleteUserGroupsDelete = async (
  params: IDeleteUserGroupsDeleteParams,
): Promise<ApiRequestResponse<IDeleteUserGroupsDeleteResponse>> => {
  const { userId, groupId } = params;
  const path = `/users/${userId}/groups/delete/${groupId}`;
  try {
    const result = await Axios.delete(path);
    toast.success(
      t("users.delete_user_group_delete.success", {
        userName: result.data.name,
      }),
    );
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("users.delete_user_group_delete.error"),
    });
    return undefined;
  }
};

export const getUser = async (
  params: IGetUserParams,
): Promise<ApiRequestResponse<IGetUserResponse>> => {
  const { id } = params;
  const path = `/users/${id}`;
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("users.get_user.error") });
    return undefined;
  }
};
