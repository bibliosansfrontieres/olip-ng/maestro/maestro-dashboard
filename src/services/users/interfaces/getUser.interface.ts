import { IUser } from "interfaces/user.interface";

export interface IGetUserResponse extends IUser {}

export interface IGetUserParams {
  id: string;
}
