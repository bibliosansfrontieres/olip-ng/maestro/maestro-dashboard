import { RoleNameEnum } from "enums/roleName.enum";
import { IUser } from "interfaces/user.interface";

export interface IPatchUserResponse extends IUser {}

export interface IPatchUserRequest {
  roleName?: RoleNameEnum;
}

export interface IPatchUserParams extends IPatchUserRequest {
  id: string;
}
