import { IUser } from "interfaces/user.interface";

export interface IDeleteUserGroupsDeleteResponse extends IUser {}

export interface IDeleteUserGroupsDeleteParams {
  userId: string;
  groupId: string;
}
