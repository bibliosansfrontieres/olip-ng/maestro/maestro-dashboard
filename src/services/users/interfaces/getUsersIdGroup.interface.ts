import { BooleanEnum } from "enums/boolean.enum";
import { IGroupUsers } from "interfaces/group.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetUsersIdGroupResponse extends IPageResponse<IGroupUsers> {}

export interface IGetUsersIdGroupRequest extends IPageRequest {
  query?: string;
  isUserInGroup?: BooleanEnum;
}

export interface IGetUsersIdGroupParams extends IGetUsersIdGroupRequest {
  id: string;
}
