import { RoleNameEnum } from "enums/roleName.enum";
import { UsersOrderFieldsEnum } from "enums/usersOrderFields.enum";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { IUser } from "interfaces/user.interface";

export interface IGetUsersResponse extends IPageResponse<IUser> {}

export interface IGetUsersRequest extends IPageRequest {
  query?: string;
  roleName?: RoleNameEnum;
  orderField?: UsersOrderFieldsEnum;
}

export interface IGetUsersParams extends IGetUsersRequest {}
