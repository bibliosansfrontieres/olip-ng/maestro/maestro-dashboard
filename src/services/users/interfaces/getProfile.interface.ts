import { IUser } from "interfaces/user.interface";

export interface IGetProfileResponse extends IUser {}
