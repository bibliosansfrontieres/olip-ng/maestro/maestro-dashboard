import { IUser } from "interfaces/user.interface";

export interface IPostUserGroupsAddResponse extends IUser {}

export interface IPostUserGroupsAddParams {
  userId: string;
  groupId: string;
}
