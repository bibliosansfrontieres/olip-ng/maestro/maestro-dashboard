import { IFqdnMask } from "interfaces/fqdnMask.interface";

export interface IGetFqdnParams {
  id: string;
}

export interface IGetFqdnResponse extends IFqdnMask {}
