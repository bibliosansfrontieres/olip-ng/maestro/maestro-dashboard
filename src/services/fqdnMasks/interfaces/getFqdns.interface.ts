import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { IDevice } from "src/interfaces/device.interface";
import { IFqdnMask } from "src/interfaces/fqdnMask.interface";

export interface IGetFqdnsResponse
  extends IPageResponse<IFqdnMask & { devices: IDevice[] }> {}

export interface IGetFqdnsParams extends IPageRequest {
  query?: string;
  orderBy?: string;
}
