export interface IPatchFqdnParams {
  id: number;
  mask: string;
  label?: string;
}

export interface IPatchFqdnResponse extends IPatchFqdnParams {}
