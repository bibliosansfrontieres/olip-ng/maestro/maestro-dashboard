import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetFqdnParams,
  IGetFqdnResponse,
} from "services/fqdnMasks/interfaces/getFqdn.interface";
import {
  IGetFqdnsParams,
  IGetFqdnsResponse,
} from "services/fqdnMasks/interfaces/getFqdns.interface";
import {
  IPatchFqdnParams,
  IPatchFqdnResponse,
} from "services/fqdnMasks/interfaces/patchFqdn.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getFqdns = async (
  params: IGetFqdnsParams,
): Promise<ApiRequestResponse<IGetFqdnsResponse>> => {
  const path = `/fqdn-masks`;

  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("fqdn.get_fqdns.error") });
    return undefined;
  }
};

export const getFqdn = async (
  params: IGetFqdnParams,
): Promise<ApiRequestResponse<IGetFqdnResponse>> => {
  const path = `/fqdn-masks/${params.id}`;

  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("fqdn.get_fqdn.error") });
    return undefined;
  }
};

export const patchFqdn = async (
  params: IPatchFqdnParams,
): Promise<ApiRequestResponse<IPatchFqdnResponse>> => {
  const path = `/fqdn-masks/${params.id}`;

  try {
    const result = await Axios.patch(path, params);
    toast.success(t("fqdn.patch_fqdn.success"));
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("fqdn.patch_fqdn.error") });
    return undefined;
  }
};
