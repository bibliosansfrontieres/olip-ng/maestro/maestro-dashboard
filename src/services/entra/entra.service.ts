import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetEntraUsersParams,
  IGetEntraUsersRequest,
  IGetEntraUsersResponse,
} from "services/entra/interfaces/getEntraUsers.interface";
import {
  IGetLoginUrlParams,
  IGetLoginUrlRequest,
  IGetLoginUrlResponse,
} from "services/entra/interfaces/getLoginUrl.interface";
import {
  IPostImportUserParams,
  IPostImportUserResponse,
} from "services/entra/interfaces/postImportUser.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import getEnv from "utils/getEnv";
import { handleApiError } from "utils/handleApiError";

import { AxiosRequestConfig } from "@axios";

export const getLoginUrl = async (
  params?: IGetLoginUrlParams,
): Promise<IGetLoginUrlResponse> => {
  const path = "/entra/login-url";
  const requestParams: IGetLoginUrlRequest = {
    dashboardUrl: getEnv("PUBLIC_DASHBOARD_URL"),
    ...params,
  };
  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error });
    throw error;
  }
};

export const getEntraUsers = async (
  params: IGetEntraUsersParams,
  axiosConfig?: AxiosRequestConfig,
): Promise<ApiRequestResponse<IGetEntraUsersResponse>> => {
  const path = "/entra/users";
  const requestParams: IGetEntraUsersRequest = {
    ...params,
  };
  try {
    const result = await Axios.get(path, {
      params: requestParams,
      ...axiosConfig,
    });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("users.get_entra_users.error") });
    return undefined;
  }
};

export const postImportUser = async (
  params: IPostImportUserParams,
): Promise<ApiRequestResponse<IPostImportUserResponse>> => {
  const path = "/entra/import-user";
  try {
    const result = await Axios.post(path, params);
    toast.success(
      t("users.post_import_user.success", { name: result.data.name }),
    );
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("users.post_import_user.error") });
    return undefined;
  }
};
