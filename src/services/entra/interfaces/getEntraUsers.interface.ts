import { EntraUserDto } from "interfaces/entraUser.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetEntraUsersResponse extends IPageResponse<EntraUserDto> {}

export interface IGetEntraUsersRequest {
  query: string;
}

export interface IGetEntraUsersParams extends IGetEntraUsersRequest {}
