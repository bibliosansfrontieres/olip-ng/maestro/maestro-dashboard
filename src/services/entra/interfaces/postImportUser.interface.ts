export interface IPostImportUserParams {
  id: string;
}

export interface IPostImportUserResponse {
  id: string;
  name: string;
  mail: string;
  avatar: string;
}
