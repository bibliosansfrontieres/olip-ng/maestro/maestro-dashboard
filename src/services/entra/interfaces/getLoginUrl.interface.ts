export interface IGetLoginUrlResponse {
  url: string;
}
export interface IGetLoginUrlParams {
  redirect?: string;
}
export interface IGetLoginUrlRequest {
  dashboardUrl: string;
  redirect?: string;
}
