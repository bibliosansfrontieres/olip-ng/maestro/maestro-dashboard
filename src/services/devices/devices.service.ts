import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetDeviceParams,
  IGetDeviceResponse,
} from "services/devices/interfaces/getDevice.interface";
import {
  IGetDevicesParams,
  IGetDevicesResponse,
} from "services/devices/interfaces/getDevices.interface";
import {
  IGetDevicesFromMaskParams,
  IGetDevicesFromMaskResponse,
} from "services/devices/interfaces/getDevicesFromMask.interface";
import {
  IPatchDevicesParams,
  IPatchDevicesResponse,
} from "services/devices/interfaces/patchDevices.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getDevicesFromMask = async (
  params: IGetDevicesFromMaskParams,
): Promise<ApiRequestResponse<IGetDevicesFromMaskResponse[]>> => {
  const path = `/devices/mask`;
  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("device.get_devices_mask.error") });
    return undefined;
  }
};
export const getDevices = async (
  params: IGetDevicesParams,
): Promise<ApiRequestResponse<IGetDevicesResponse>> => {
  const path = `/devices`;

  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("device.get_devices.error") });
    return undefined;
  }
};

export const getDevice = async (
  params: IGetDeviceParams,
): Promise<ApiRequestResponse<IGetDeviceResponse>> => {
  const path = `/devices/${params.id}`;
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("device.get_device.error") });
    return undefined;
  }
};
export const patchDevicesBlink = async (
  params: IPatchDevicesParams,
): Promise<ApiRequestResponse<IPatchDevicesResponse>> => {
  const path = `/devices/blink`;
  try {
    await Axios.patch(path, params);
    toast.success(t("device.patch_devices_blink.success"));
    return true;
  } catch (error) {
    handleApiError({
      error,
      translation: t("device.patch_devices_blink.error"),
    });
    return undefined;
  }
};

export const patchDevicesReset = async (
  params: IPatchDevicesParams,
): Promise<ApiRequestResponse<IPatchDevicesResponse>> => {
  const path = `/devices/reset`;
  try {
    await Axios.patch(path, params);
    toast.success(t("device.patch_devices_reset.success"));
    return true;
  } catch (error) {
    handleApiError({
      error,
      translation: t("device.patch_devices_reset.error"),
    });
    return undefined;
  }
};
