import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";

export interface IGetDeviceParams {
  id: string;
}

export interface IGetDeviceResponse extends IDevice {
  fqdnMask: IFqdnMask;
  deploys: IDeployWithVmAndLabelsAndUser[] | [];
}
