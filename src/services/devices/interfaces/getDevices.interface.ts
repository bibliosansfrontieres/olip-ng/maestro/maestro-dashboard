import { BooleanEnum } from "enums/boolean.enum";
import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetDevicesResponse
  extends IPageResponse<
    IDevice & {
      fqdnMask: IFqdnMask;
      deploys: IDeployWithVmAndLabelsAndUser[] | [];
    }
  > {}

export interface IGetDevicesParams extends IPageRequest {
  query?: string;
  orderBy?: string;
  isAliveOnly?: BooleanEnum;
  lastSeenDateRange?: string;
  fqdnMaskIds?: string;
  labelIds?: string;
  statuses?: string;
}
