import { IDevice } from "interfaces/device.interface";

export interface IGetDevicesFromMaskParams {
  mask: string;
}

export interface IGetDevicesFromMaskResponse extends IDevice {}
