import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetNotificationsParams,
  IGetNotificationsResponse,
} from "services/notifications/interfaces/getNotifications.interface";
import {
  IPatchNotificationsParams,
  IPatchNotificationsResponse,
} from "services/notifications/interfaces/patchNotifications.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getNotifications = async (
  params: IGetNotificationsParams,
): Promise<ApiRequestResponse<IGetNotificationsResponse>> => {
  const path = "/notifications";

  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({
      error,
      translation: t("notification.get_notifications.error"),
    });
    return undefined;
  }
};

export const patchNotifications = async (
  params: IPatchNotificationsParams,
): Promise<ApiRequestResponse<IPatchNotificationsResponse>> => {
  const path = "/notifications";
  const markAsRead = params.isSeen;
  try {
    await Axios.patch(path, params);
    toast.success(
      t(
        `notification.patch_notifications.${markAsRead ? "seen" : "unseen"}.success`,
      ),
    );
    return true;
  } catch (error) {
    handleApiError({
      error,
      translation: t(
        `notification.patch_notifications.${markAsRead ? "seen" : "unseen"}.error`,
      ),
    });
    return undefined;
  }
};

export const patchAllNotifications = async (): Promise<
  ApiRequestResponse<IPatchNotificationsResponse>
> => {
  const path = "/notifications/all";
  try {
    await Axios.patch(path);
    toast.success(t("notification.patch_notifications_all.success"));
    return true;
  } catch (error) {
    handleApiError({
      error,
      translation: t("notification.patch_notifications_all.error"),
    });
    return undefined;
  }
};
