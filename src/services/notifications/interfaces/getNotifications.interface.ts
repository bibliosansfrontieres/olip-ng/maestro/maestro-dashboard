import { INotification } from "interfaces/notification.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetNotificationsResponse
  extends IPageResponse<INotification> {}

export interface IGetNotificationsParams extends IPageRequest {
  isNotSeenOnly?: string;
  orderBy?: string;
}
