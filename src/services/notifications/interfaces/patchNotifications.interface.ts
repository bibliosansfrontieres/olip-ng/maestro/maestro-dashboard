export interface IPatchNotificationsParams {
  notificationIds: string[];
  isSeen: boolean;
}

export interface IPatchNotificationsResponse {}
