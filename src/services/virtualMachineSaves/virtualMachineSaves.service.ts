import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetVirtualMachineSavesParams,
  IGetVirtualMachineSavesResponse,
} from "services/virtualMachineSaves/interfaces/getVirtualMachineSaves.interface";
import {
  IPatchVirtualMachineSaveParams,
  IPatchVirtualMachineSaveResponse,
} from "services/virtualMachineSaves/interfaces/patchVirtualMachineSave.interface";
import {
  IPostVirtualMachineSavesParams,
  IPostVirtualMachineSavesResponse,
} from "services/virtualMachineSaves/interfaces/postVirtualMachineSave.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const postVirtualMachineSaves = async (
  params: IPostVirtualMachineSavesParams,
): Promise<ApiRequestResponse<IPostVirtualMachineSavesResponse>> => {
  const { vmId, ...requestParams } = params;
  const path = `/virtual-machine-saves/${vmId}`;

  try {
    const result = await Axios.post(path, { ...requestParams });
    toast.success(t("vm.post_vm_saves.success"));
    return result;
  } catch (error) {
    handleApiError({ error, translation: t("vm.post_vm_saves.error") });
    return undefined;
  }
};

export const patchVirtualMachineSaves = async (
  params: IPatchVirtualMachineSaveParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineSaveResponse>> => {
  const { saveId, ...requestParams } = params;
  const path = `/virtual-machine-saves/${saveId}`;

  try {
    const result = await Axios.patch(path, { ...requestParams });
    toast.success(t("vm.patch_vm_saves.success"));
    return result;
  } catch (error) {
    handleApiError({ error, translation: t("vm.patch_vm_saves.error") });
    return undefined;
  }
};

export const getVirtualMachineSaves = async (
  params: IGetVirtualMachineSavesParams,
): Promise<ApiRequestResponse<IGetVirtualMachineSavesResponse>> => {
  const { vmId, ...requestParams } = params;
  const path = `/virtual-machine-saves/${vmId}`;
  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm_saves.error") });
    return undefined;
  }
};
