export interface IPostVirtualMachineSavesRequest {
  name: string;
  description: string;
}
export interface IPostVirtualMachineSavesParams
  extends IPostVirtualMachineSavesRequest {
  vmId: string;
}

export interface IPostVirtualMachineSavesResponse {}
