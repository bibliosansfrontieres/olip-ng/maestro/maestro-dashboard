import { VirtualMachineSaveOrderByEnum } from "enums/orderBy/virtualMachineSaveOrderBy.enum";
import { VirtualMachineSaveTypeEnum } from "enums/virtualMachineSaveType.enum";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { IUser } from "interfaces/user.interface";
import { IVirtualMachineSave } from "interfaces/virtualMachineSave.interface";
import { IVirtualMachine } from "src/interfaces/virtualMachine.interface";

export interface IGetVirtualMachineSavesRequest extends IPageRequest {
  orderBy?: VirtualMachineSaveOrderByEnum;
  dateRange?: string;
  types?: VirtualMachineSaveTypeEnum;
  query?: string;
  userIds?: string;
}

export interface IGetVirtualMachineSavesParams
  extends IGetVirtualMachineSavesRequest {
  vmId: string;
}

export interface IGetVirtualMachineSavesResponse
  extends IPageResponse<
    IVirtualMachineSave & {
      user: IUser;
      createdVirtualMachines: IVirtualMachine[];
    }
  > {}
