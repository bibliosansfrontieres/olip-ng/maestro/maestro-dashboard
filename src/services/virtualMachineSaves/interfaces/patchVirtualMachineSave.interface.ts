export interface IPatchVirtualMachineSaveRequest {
  name: string;
  description: string;
}
export interface IPatchVirtualMachineSaveParams
  extends IPatchVirtualMachineSaveRequest {
  saveId: string;
}

export interface IPatchVirtualMachineSaveResponse {}
