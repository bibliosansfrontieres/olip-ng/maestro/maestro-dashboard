import { ILog } from "interfaces/log.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetLogsParams extends IPageRequest {
  orderBy?: string;
  actions?: string;
  entities?: string;
  levels?: string;
  userIds?: string;
  dateRange?: string;
}

export interface IGetLogsResponse extends IPageResponse<ILog> {}
