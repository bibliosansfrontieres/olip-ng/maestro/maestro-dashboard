import { t } from "i18next";
import {
  IGetLogsParams,
  IGetLogsResponse,
} from "services/log/interfaces/getLogs.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const getLogs = async (
  params: IGetLogsParams,
): Promise<ApiRequestResponse<IGetLogsResponse>> => {
  const path = "/logs";
  try {
    const result = await Axios.get(path, { params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("log.get_logs.error") });
    return undefined;
  }
};
