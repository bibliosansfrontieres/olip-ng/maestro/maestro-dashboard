export interface IPatchGroupResponse {
  id: string;
  name: string;
  description: string;
  isArchived: boolean;
}
export interface IPatchGroupRequest {
  name?: string;
  description?: string;
  isArchived?: boolean;
}

export interface IPatchGroupParams extends IPatchGroupRequest {
  id: string;
}
