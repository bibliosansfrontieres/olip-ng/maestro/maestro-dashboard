export interface IPostGroupResponse {
  id: string;
  name: string;
  description: string;
  isArchived: boolean;
}

export interface IPostGroupRequest {
  name: string;
  description?: string;
}

export interface IPostGroupParams extends IPostGroupRequest {}
