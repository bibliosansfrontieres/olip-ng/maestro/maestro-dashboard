import { IGroup } from "interfaces/group.interface";

export interface IGetGroupResponse extends IGroup {}

export interface IGetGroupRequest {
  id: string;
}

export interface IGetGroupParams extends IGetGroupRequest {}
