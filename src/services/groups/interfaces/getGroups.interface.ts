import { BooleanEnum } from "enums/boolean.enum";
import { IGroupUsers } from "interfaces/group.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetGroupsResponse extends IPageResponse<IGroupUsers> {}

export interface IGetGroupsRequest extends IPageRequest {
  query?: string;
  isExcludingArchived?: BooleanEnum;
  isOnlyTakingUserGroups?: BooleanEnum;
}

export interface IGetGroupsParams extends IGetGroupsRequest {}
