import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IGetGroupParams,
  IGetGroupResponse,
} from "services/groups/interfaces/getGroup.interface";
import {
  IGetGroupsParams,
  IGetGroupsRequest,
  IGetGroupsResponse,
} from "services/groups/interfaces/getGroups.interface";
import {
  IPatchGroupParams,
  IPatchGroupRequest,
  IPatchGroupResponse,
} from "services/groups/interfaces/patchGroup.interface";
import {
  IPostGroupParams,
  IPostGroupResponse,
} from "services/groups/interfaces/postGroup.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

import { AxiosError, AxiosRequestConfig } from "@axios";

export const getGroups = async (
  params?: IGetGroupsParams,
  axiosConfig?: AxiosRequestConfig,
): Promise<ApiRequestResponse<IGetGroupsResponse>> => {
  const path = "/groups";
  const requestParams: IGetGroupsRequest = {
    ...params,
  };
  try {
    const result = await Axios.get(path, {
      params: requestParams,
      ...axiosConfig,
    });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("groups.get_groups.error") });
    return undefined;
  }
};

export const postGroup = async (
  params: IPostGroupParams,
): Promise<ApiRequestResponse<IPostGroupResponse>> => {
  const path = "/groups";
  try {
    const result = await Axios.post(path, params);
    toast.success(t("groups.post_group.success", { name: result.data.name }));
    return result.data;
  } catch (error) {
    handleApiError({ error });
    if ((error as AxiosError).response?.status === 409) {
      toast.error(t("groups.post_group_conflict_error"));
    } else {
      toast.error(t("groups.post_group.error"));
    }
    return undefined;
  }
};

export const getGroup = async (
  params: IGetGroupParams,
): Promise<ApiRequestResponse<IGetGroupResponse>> => {
  const path = `/groups/${params.id}`;
  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error });
    return undefined;
  }
};

export const patchGroup = async (
  params: IPatchGroupParams,
): Promise<ApiRequestResponse<IPatchGroupResponse>> => {
  const path = `/groups/${params.id}`;
  const requestParams: IPatchGroupRequest = {
    name: params.name,
    description: params.description,
    isArchived: params.isArchived,
  };
  try {
    const result = await Axios.patch(path, requestParams);
    if (params.isArchived && !params?.name) {
      toast.success(
        t("groups.patch_group_delete.success", { name: result.data.name }),
      );
    } else {
      toast.success(
        t("groups.patch_group.success", { name: result.data.name }),
      );
    }
    return result.data;
  } catch (error) {
    handleApiError({ error });
    if (params.isArchived && !params?.name) {
      toast.error(t("groups.patch_group_delete.error"));
    } else if ((error as AxiosError).response?.status === 409) {
      toast.error(t("groups.post_group_conflict_error"));
    } else {
      toast.error(t("groups.patch_group.error"));
    }
    return undefined;
  }
};
