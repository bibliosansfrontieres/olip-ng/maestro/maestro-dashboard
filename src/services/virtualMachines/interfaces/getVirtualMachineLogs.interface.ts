import { VirtualMachineContainerEnum } from "enums/virtualMachineContainer.enum";

export interface IGetVirtualMachineLogsParams {
  id: string;
  container: VirtualMachineContainerEnum;
  maxLines?: string;
}

export interface IGetVirtualMachineLogsResponse {
  html: string;
}
