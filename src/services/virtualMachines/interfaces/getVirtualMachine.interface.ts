import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { IVirtualMachineSaveWithVm } from "interfaces/virtualMachineSave.interface";

export interface IGetVirtualMachineParams {
  id: string;
}

export interface IGetVirtualMachineResponse extends IVirtualMachine {
  originVirtualMachineSave: IVirtualMachineSaveWithVm | null;
}
