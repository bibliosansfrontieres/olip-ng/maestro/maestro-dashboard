import { IVirtualMachine } from "interfaces/virtualMachine.interface";

export interface IPostVirtualMachinesResponse extends IVirtualMachine {}

export interface IPostVirtualMachinesRequest {
  defaultLanguageIdentifier: string;
  title: string;
  description?: string;
  referenceUrl?: string;
}
export interface IPostVirtualMachinesParams
  extends IPostVirtualMachinesRequest {
  projectId: number;
}
