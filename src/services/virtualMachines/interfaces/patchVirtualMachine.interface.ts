import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";

export interface IPatchVirtualMachinesRequest {
  title?: string;
  description?: string;
  referenceUrl?: string;
  defaultLanguageIdentifier?: string;
}

export interface IPatchVirtualMachinesParams
  extends IPatchVirtualMachinesRequest {
  id: string;
}

export interface IPatchVirtualMachinesResponse
  extends IGetVirtualMachineResponse {}
export interface IPatchVirtualMachineParams {
  id: string;
}
export interface IPatchVirtualMachineResponse {}

export interface IPatchVirtualMachineArchiveParams
  extends IPatchVirtualMachineParams {
  isArchived: true;
}

export interface IPatchVirtualMachineImportParams
  extends IPatchVirtualMachineParams {
  saveId: string;
}
export interface IPatchVirtualMachineApprovalApproveParams
  extends IPatchVirtualMachineParams {
  name: string;
}

export interface IPatchVirtualMachineApprovalApproveRequest {
  name: string;
}
