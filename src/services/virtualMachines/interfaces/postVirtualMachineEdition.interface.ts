import { IVirtualMachine } from "interfaces/virtualMachine.interface";

export interface IPostVirtualMachineEditionRequest {
  title: string;
  description?: string;
  referenceUrl?: string;
}

export interface IPostVirtualMachineDuplicateParams
  extends IPostVirtualMachineEditionRequest {
  id: string;
}

export interface IPostVirtualMachineFromSaveParams
  extends IPostVirtualMachineEditionRequest {
  saveId: string;
}
export interface IPostVirtualMachineEditionResponse extends IVirtualMachine {}
