import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "src/interfaces/pageResponse.interface";
import { IVirtualMachineError } from "src/interfaces/virtualMachineError.interface";

export interface IGetVirtualMachineErrorsParams extends IPageRequest {
  id: string;
}

export interface IGetVirtualMachineErrorsResponse
  extends IPageResponse<IVirtualMachineError> {}
