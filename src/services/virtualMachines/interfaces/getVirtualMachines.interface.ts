import { BooleanEnum } from "enums/boolean.enum";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";

export interface IGetVirtualMachinesResponse
  extends IPageResponse<IVirtualMachine> {}

export interface IGetVirtualMachinesParams extends IPageRequest {
  query?: string;
  orderBy?: string;
  isUserVmsFiltered?: BooleanEnum;
  statuses?: string;
  approvals?: string;
  groups?: string;
  users?: string;
  createdDateRange?: string;
  updatedDateRange?: string;
}
