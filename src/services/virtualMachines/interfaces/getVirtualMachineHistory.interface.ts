import { LogOrderByEnum } from "enums/orderBy/logOrderBy.enum";
import { ILog } from "interfaces/log.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetVirtualMachineHistoryRequest extends IPageRequest {
  dateRange?: string;
  userIds?: string;
  actions?: string;
  entities?: string;
  levels?: string;
  orderBy?: LogOrderByEnum;
  query?: string;
}

export interface IGetVirtualMachineHistoryParams
  extends IGetVirtualMachineHistoryRequest {
  id: string;
}

export interface IGetVirtualMachineHistoryResponse
  extends IPageResponse<ILog> {}
