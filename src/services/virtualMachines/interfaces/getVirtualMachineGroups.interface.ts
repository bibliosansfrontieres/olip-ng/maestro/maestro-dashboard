import { IGroup } from "interfaces/group.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";

export interface IGetVirtualMachineGroupsRequest extends IPageRequest {
  query?: string;
}

export interface IGetVirtualMachineGroupsParams
  extends IGetVirtualMachineGroupsRequest {
  id: string;
}

export interface IGetVirtualMachineGroupsResponse
  extends IPageResponse<IGroup> {}
