import { IDeployFull } from "interfaces/deploy.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { VirtualMachineDeployOrderByEnum } from "src/enums/orderBy/virtualMachineDeployOrderBy.enum";

export interface IGetVirtualMachineDeploysRequest extends IPageRequest {
  statuses?: string;
  userIds?: string;
  labelIds?: string;
  createdDateRange?: string;
  orderBy?: VirtualMachineDeployOrderByEnum;
  query?: string;
}

export interface IGetVirtualMachineDeploysParams
  extends IGetVirtualMachineDeploysRequest {
  id: string;
}

export interface IGetVirtualMachineDeploysResponse
  extends IPageResponse<IDeployFull> {}
