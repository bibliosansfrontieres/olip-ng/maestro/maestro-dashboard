export interface IDeleteVirtualMachineGroupResponse {}

export interface IDeleteVirtualMachineGroupParams {
  id: string;
  groupId: string;
}
