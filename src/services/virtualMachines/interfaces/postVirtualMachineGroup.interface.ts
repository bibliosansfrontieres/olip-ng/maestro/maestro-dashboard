export interface IPostVirtualMachineGroupResponse {}

export interface IPostVirtualMachineGroupParams {
  id: string;
  groupId: string;
}
