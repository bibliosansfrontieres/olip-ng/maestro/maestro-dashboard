import { VirtualMachineContainerEnum } from "enums/virtualMachineContainer.enum";

export interface IGetVirtualMachineLogsDownloadParams {
  id: string;
  container: VirtualMachineContainerEnum;
}
