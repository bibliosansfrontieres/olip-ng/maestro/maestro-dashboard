import { t } from "i18next";
import { toast } from "react-toastify";
import {
  IDeleteVirtualMachineGroupParams,
  IDeleteVirtualMachineGroupResponse,
} from "services/virtualMachines/interfaces/deleteVirtualMachineGroup.interface";
import {
  IGetVirtualMachineParams,
  IGetVirtualMachineResponse,
} from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import {
  IGetVirtualMachineDeploysParams,
  IGetVirtualMachineDeploysResponse,
} from "services/virtualMachines/interfaces/getVirtualMachineDeploys.interface";
import {
  IGetVirtualMachineErrorsParams,
  IGetVirtualMachineErrorsResponse,
} from "services/virtualMachines/interfaces/getVirtualMachineErrors.interface";
import {
  IGetVirtualMachineGroupsParams,
  IGetVirtualMachineGroupsResponse,
} from "services/virtualMachines/interfaces/getVirtualMachineGroups.interface";
import {
  IGetVirtualMachineHistoryParams,
  IGetVirtualMachineHistoryResponse,
} from "services/virtualMachines/interfaces/getVirtualMachineHistory.interface";
import {
  IGetVirtualMachineLogsParams,
  IGetVirtualMachineLogsResponse,
} from "services/virtualMachines/interfaces/getVirtualMachineLogs.interface";
import { IGetVirtualMachineLogsDownloadParams } from "services/virtualMachines/interfaces/getVirtualMachineLogsDownload.interface";
import {
  IGetVirtualMachinesParams,
  IGetVirtualMachinesResponse,
} from "services/virtualMachines/interfaces/getVirtualMachines.interface";
import {
  IPatchVirtualMachineApprovalApproveParams,
  IPatchVirtualMachineArchiveParams,
  IPatchVirtualMachineImportParams,
  IPatchVirtualMachineParams,
  IPatchVirtualMachineResponse,
  IPatchVirtualMachinesParams,
  IPatchVirtualMachinesResponse,
} from "services/virtualMachines/interfaces/patchVirtualMachine.interface";
import {
  IPostVirtualMachineDuplicateParams,
  IPostVirtualMachineEditionResponse,
  IPostVirtualMachineFromSaveParams,
} from "services/virtualMachines/interfaces/postVirtualMachineEdition.interface";
import {
  IPostVirtualMachineGroupParams,
  IPostVirtualMachineGroupResponse,
} from "services/virtualMachines/interfaces/postVirtualMachineGroup.interface";
import {
  IPostVirtualMachinesParams,
  IPostVirtualMachinesResponse,
} from "services/virtualMachines/interfaces/postVirtualMachines.interface";
import Axios from "src/axios";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { handleApiError } from "utils/handleApiError";

export const patchVirtualMachine = async (
  params: IPatchVirtualMachinesParams,
): Promise<ApiRequestResponse<IPatchVirtualMachinesResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}`;

  try {
    const result = await Axios.patch(path, requestParams);
    toast.success(t("vm.patch_vm.success"));
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.patch_vm.error") });
    return undefined;
  }
};

export const patchVirtualMachineStart = async (
  params: IPatchVirtualMachineParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const path = `/virtual-machines/${params.id}/start`;
  try {
    const result = await Axios.patch(path);
    toast.success(t("vm.patch_vm_start.success"));
    return result;
  } catch (error) {
    handleApiError({ error, translation: t("vm.patch_vm_start.error") });
    return undefined;
  }
};

export const patchVirtualMachineStop = async (
  params: IPatchVirtualMachineParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const path = `/virtual-machines/${params.id}/stop`;
  try {
    const result = await Axios.patch(path);
    toast.success(t("vm.patch_vm_stop.success"));
    return result;
  } catch (error) {
    handleApiError({ error, translation: t("vm.patch_vm_stop.error") });
    return undefined;
  }
};

export const patchVirtualMachineArchive = async (
  params: IPatchVirtualMachineArchiveParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/archive`;
  try {
    const result = await Axios.patch(path, requestParams);
    toast.success(
      requestParams.isArchived
        ? t("vm.patch_vm_archive.success")
        : t("vm.patch_vm_unarchive.success"),
    );
    return result;
  } catch (error) {
    handleApiError({ error, translation: t("vm.patch_vm_archive.error") });
    return undefined;
  }
};

export const patchVirtualMachineImport = async (
  params: IPatchVirtualMachineImportParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const path = `/virtual-machines/${params.id}/import/${params.saveId}`;
  try {
    const result = await Axios.patch(path, undefined, { timeout: 60000 });
    toast.success(t("vm.patch_vm_import.success"));
    return result;
  } catch (error) {
    handleApiError({ error, translation: t("vm.patch_vm_import.error") });
    return undefined;
  }
};

export const patchVirtualMachineApproval = async (
  params: IPatchVirtualMachineParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const path = `/virtual-machines/${params.id}/approval`;
  try {
    const result = await Axios.patch(path);
    toast.success(t("vm.patch_vm_approval.success"));
    return result;
  } catch (error) {
    handleApiError({ error, translation: t("vm.patch_vm_approval.error") });
    return undefined;
  }
};

export const patchVirtualMachineApprovalApprove = async (
  params: IPatchVirtualMachineApprovalApproveParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/approval/approve`;
  try {
    const result = await Axios.patch(path, requestParams);
    toast.success(t("vm.patch_vm_approval_approve.success"));
    return result;
  } catch (error) {
    handleApiError({
      error,
      translation: t("vm.patch_vm_approval_approve.error"),
    });
    return undefined;
  }
};

export const patchVirtualMachineApprovalReject = async (
  params: IPatchVirtualMachineParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const path = `/virtual-machines/${params.id}/approval/reject`;
  try {
    const result = await Axios.patch(path);
    toast.success(t("vm.patch_vm_approval_reject.success"));
    return result;
  } catch (error) {
    handleApiError({
      error,
      translation: t("vm.patch_vm_approval_reject.error"),
    });
    return undefined;
  }
};

export const patchVirtualMachineRefreshProject = async (
  params: IPatchVirtualMachineParams,
): Promise<ApiRequestResponse<IPatchVirtualMachineResponse>> => {
  const path = `/virtual-machines/${params.id}/refresh-project`;
  try {
    const result = await Axios.patch(path);
    toast.success(t("vm.patch_vm_refresh_project.success"));
    return result;
  } catch (error) {
    handleApiError({
      error,
      translation: t("vm.patch_vm_refresh_project.error"),
    });
    return undefined;
  }
};

export const postVirtualMachines = async (
  params: IPostVirtualMachinesParams,
): Promise<ApiRequestResponse<IPostVirtualMachinesResponse>> => {
  const path = "/virtual-machines";

  try {
    const result = await Axios.post(path, params);
    // because a notification is already sent via socket
    // toast.success(t("vm.post_vm.success"));
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.post_vm.error") });
    return undefined;
  }
};

export const postVirtualMachineDuplicate = async (
  params: IPostVirtualMachineDuplicateParams,
): Promise<ApiRequestResponse<IPostVirtualMachineEditionResponse>> => {
  const path = `/virtual-machines/${params.id}/duplicate`;
  try {
    const result = await Axios.post(path, params);
    toast.success(t("vm.post_vm_duplicate.success"));
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.post_vm_duplicate.error") });
    return undefined;
  }
};

export const getVirtualMachines = async (
  params?: IGetVirtualMachinesParams,
): Promise<ApiRequestResponse<IGetVirtualMachinesResponse>> => {
  const path = "/virtual-machines";
  try {
    const result = await Axios.get(path, { params: params });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vms.error") });
    return undefined;
  }
};

export const getVirtualMachineErrors = async (
  params: IGetVirtualMachineErrorsParams,
): Promise<ApiRequestResponse<IGetVirtualMachineErrorsResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/errors`;
  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm_errors.error") });
    return undefined;
  }
};

export const getVirtualMachineHistory = async (
  params: IGetVirtualMachineHistoryParams,
): Promise<ApiRequestResponse<IGetVirtualMachineHistoryResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/history`;

  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm_history.error") });
    return undefined;
  }
};
export const postVirtualMachineFromSave = async (
  params: IPostVirtualMachineFromSaveParams,
): Promise<ApiRequestResponse<IPostVirtualMachineEditionResponse>> => {
  const path = `/virtual-machines/${params.saveId}`;
  try {
    const result = await Axios.post(path, params);
    toast.success(t("vm.post_vm_from_save.success"));
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.post_vm_from_save.error") });
    return undefined;
  }
};

export const getVirtualMachineGroups = async (
  params: IGetVirtualMachineGroupsParams,
): Promise<ApiRequestResponse<IGetVirtualMachineGroupsResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/groups`;

  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm_groups.error") });
    return undefined;
  }
};

export const getVirtualMachineLogs = async (
  params: IGetVirtualMachineLogsParams,
): Promise<ApiRequestResponse<IGetVirtualMachineLogsResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/logs`;
  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm_logs.error") });
    return undefined;
  }
};

export const getVirtualMachineLogsDownload = async (
  params: IGetVirtualMachineLogsDownloadParams,
): Promise<ApiRequestResponse<Blob>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/logs/download`;
  try {
    const result = await Axios.get(path, {
      params: requestParams,
      responseType: "blob",
    });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm_logs.error") });
    return undefined;
  }
};

export const postVirtualMachineGroup = async (
  params: IPostVirtualMachineGroupParams,
): Promise<ApiRequestResponse<IPostVirtualMachineGroupResponse>> => {
  const { id, groupId } = params;
  const path = `/virtual-machines/${id}/groups/${groupId}`;
  try {
    await Axios.post(path);
    toast.success(t("vm.post_vm_group.success"));
    return true;
  } catch (error) {
    handleApiError({
      error,
      translation: t("vm.post_vm_group.error"),
    });
    return undefined;
  }
};

export const deleteVirtualMachineGroup = async (
  params: IDeleteVirtualMachineGroupParams,
): Promise<ApiRequestResponse<IDeleteVirtualMachineGroupResponse>> => {
  const { id, groupId } = params;
  const path = `/virtual-machines/${id}/groups/${groupId}`;
  try {
    await Axios.delete(path);
    toast.success(t("vm.delete_vm_group.success"));
    return true;
  } catch (error) {
    handleApiError({
      error,
      translation: t("vm.delete_vm_group.error"),
    });
    return undefined;
  }
};

export const getVirtualMachine = async (
  params: IGetVirtualMachineParams,
): Promise<ApiRequestResponse<IGetVirtualMachineResponse>> => {
  const path = `/virtual-machines/${params.id}`;

  try {
    const result = await Axios.get(path);
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm.error") });
    return undefined;
  }
};

export const getVirtualMachineDeploys = async (
  params: IGetVirtualMachineDeploysParams,
): Promise<ApiRequestResponse<IGetVirtualMachineDeploysResponse>> => {
  const { id, ...requestParams } = params;
  const path = `/virtual-machines/${id}/deploys`;
  try {
    const result = await Axios.get(path, { params: requestParams });
    return result.data;
  } catch (error) {
    handleApiError({ error, translation: t("vm.get_vm_deploys.error") });
    return undefined;
  }
};
