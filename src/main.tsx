import React from "react";
import ProtectedRoute from "common/UserProtectedRoute";
import InformationTab from "components/vm/Tabs/InformationTab/InformationTab";
import LogsTab from "components/vm/Tabs/LogsTab/LogsTab";
import { HomePageContextProvider } from "contexts/homePage.context";
import { LanguageContextProvider } from "contexts/language.context";
import { LogsContextProvider } from "contexts/logs.context";
import { ThemeContextProvider } from "contexts/theme.context";
import { UserContextProvider } from "contexts/user.context";
import { VmContextProvider } from "contexts/vm.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import contextLoader from "loaders/context.loader";
import deployLoader from "loaders/deploy.loader";
import deviceLoader from "loaders/device.loader";
import fqdnLoader from "loaders/fqdn.loader";
import projectLoader from "loaders/project.loader";
import AuthPage from "pages/Auth.page.tsx";
import ContextPage from "pages/Context.page";
import CreateVMPage from "pages/CreateVM.page";
import DashboardPage from "pages/Dashboard.page";
import DeployPage from "pages/Deploy.page";
import DeploysPage from "pages/Deploys.page";
import DevicePage from "pages/Device.page";
import DevicesPage from "pages/Devices.page";
import ErrorPage from "pages/Error.page";
import ExecDevicePage from "pages/ExecDevice.page";
import FqdnPage from "pages/Fqdn.page";
import FqdnsPage from "pages/Fqdns.page";
import GroupsPage from "pages/Groups.page";
import HomePage from "pages/Home.page";
import LabelsPage from "pages/Labels.page";
import Login from "pages/Login.page";
import LogsPage from "pages/Logs.page";
import MainLayout from "pages/MainLayout.page";
import NotificationsPage from "pages/Notifications.page";
import ProjectPage from "pages/Project.page";
import ProjectsPage from "pages/Projects.page";
import UsersPage from "pages/Users.page";
import VMs from "pages/VMs.page";
import VmTabPage from "pages/VmTabs/VmTab.page";
import VmTabContextPage from "pages/VmTabs/VmTabContext.page";
import VmTabDeployPage from "pages/VmTabs/VmTabDeploy.page";
import VmTabGroupsPage from "pages/VmTabs/VmTabGroups.page";
import VmTabHistoryPage from "pages/VmTabs/VmTabHistory.page";
import VmTabSavePage from "pages/VmTabs/VmTabSave.page";
import ReactDOM from "react-dom/client";
import { I18nextProvider } from "react-i18next";
import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import i18n from "src/i18n.ts";

import { ChakraProvider } from "@chakra-ui/react";

import "react-toastify/dist/ReactToastify.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <ProtectedRoute />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <MainLayout />,
        children: [
          {
            index: true,
            element: (
              <HomePageContextProvider>
                <HomePage />
              </HomePageContextProvider>
            ),
          },
          {
            path: "/notifications",
            element: <NotificationsPage />,
          },
          {
            path: "/virtual-machines",
            element: (
              <ProtectedRoute
                permissions={PermissionNameEnum.READ_VIRTUAL_MACHINE}
              >
                <VMs />
              </ProtectedRoute>
            ),
          },
          {
            path: "/create",
            element: (
              <ProtectedRoute
                permissions={PermissionNameEnum.CREATE_VIRTUAL_MACHINE}
              >
                <CreateVMPage />
              </ProtectedRoute>
            ),
          },
          {
            path: "virtual-machines/:id",
            element: (
              <ProtectedRoute
                permissions={PermissionNameEnum.READ_VIRTUAL_MACHINE}
              >
                <VmContextProvider>
                  <VmTabPage />
                </VmContextProvider>
              </ProtectedRoute>
            ),
            children: [
              {
                index: true,
                id: "information",
                element: <InformationTab />,
              },
              {
                id: "context",
                path: "context",
                element: <VmTabContextPage />,
                loader: contextLoader,
              },
              {
                id: "logs",
                path: "logs",
                element: <LogsTab />,
              },
              {
                id: "history",
                path: "history",
                element: <VmTabHistoryPage />,
              },
              {
                id: "groups",
                path: "groups",
                element: <VmTabGroupsPage />,
              },
              {
                id: "save",
                path: "save",
                element: <VmTabSavePage />,
              },
              {
                id: "deploy",
                path: "deploy",
                element: <VmTabDeployPage />,
              },
            ],
          },
          {
            path: "/devices",
            element: (
              <ProtectedRoute permissions={PermissionNameEnum.READ_DEVICE}>
                <DevicesPage />
              </ProtectedRoute>
            ),
          },
          {
            path: "devices/:id",
            element: (
              <ProtectedRoute permissions={PermissionNameEnum.READ_DEVICE}>
                <DevicePage />
              </ProtectedRoute>
            ),
            loader: deviceLoader,
          },
          {
            path: "devices/:id/exec",
            element: (
              <ProtectedRoute permissions={PermissionNameEnum.EXEC_DEVICE}>
                <ExecDevicePage />
              </ProtectedRoute>
            ),
          },
          {
            path: "/deploys",
            element: (
              <ProtectedRoute permissions={PermissionNameEnum.READ_DEPLOY}>
                <DeploysPage />
              </ProtectedRoute>
            ),
          },
          {
            path: "/deploys/:id",
            element: (
              <ProtectedRoute permissions={PermissionNameEnum.READ_DEPLOY}>
                <DeployPage />
              </ProtectedRoute>
            ),
            loader: deployLoader,
          },
          {
            path: "/projects",
            element: (
              <ProtectedRoute permissions={PermissionNameEnum.READ_PROJECT}>
                <ProjectsPage />
              </ProtectedRoute>
            ),
          },
          {
            path: "projects/:id",
            element: (
              <ProtectedRoute permissions={PermissionNameEnum.READ_PROJECT}>
                <ProjectPage />
              </ProtectedRoute>
            ),
            loader: projectLoader,
          },
          {
            path: "/",
            id: "admin",
            element: <Outlet />,
            children: [
              {
                path: "/dashboard",
                element: (
                  <ProtectedRoute permissions={PermissionNameEnum.READ_USER}>
                    <DashboardPage />
                  </ProtectedRoute>
                ),
              },
              {
                path: "/users",
                element: (
                  <ProtectedRoute permissions={PermissionNameEnum.READ_USER}>
                    <UsersPage />
                  </ProtectedRoute>
                ),
              },
              {
                path: "/groups",
                element: (
                  <ProtectedRoute permissions={PermissionNameEnum.READ_GROUP}>
                    <GroupsPage />
                  </ProtectedRoute>
                ),
              },
              {
                path: "/context",
                element: (
                  <ProtectedRoute
                    permissions={PermissionNameEnum.UPDATE_DEFAULT_CONTEXT}
                  >
                    <ContextPage />
                  </ProtectedRoute>
                ),
                loader: contextLoader,
              },
              {
                path: "/fqdns",
                element: (
                  <ProtectedRoute permissions={PermissionNameEnum.READ_FQDN}>
                    <FqdnsPage />
                  </ProtectedRoute>
                ),
              },
              {
                path: "fqdns/:id",
                element: (
                  <ProtectedRoute
                    permissions={[
                      PermissionNameEnum.READ_FQDN,
                      PermissionNameEnum.READ_DEVICE,
                    ]}
                  >
                    <FqdnPage />
                  </ProtectedRoute>
                ),
                loader: fqdnLoader,
              },
              {
                path: "/labels",
                element: (
                  <ProtectedRoute permissions={PermissionNameEnum.READ_LABEL}>
                    <LabelsPage />
                  </ProtectedRoute>
                ),
              },
              {
                path: "/logs",
                element: (
                  <ProtectedRoute permissions={PermissionNameEnum.READ_LOGS}>
                    <LogsContextProvider>
                      <LogsPage />
                    </LogsContextProvider>
                  </ProtectedRoute>
                ),
              },
            ],
          },
        ],
      },
    ],
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/auth",
    element: <AuthPage />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ChakraProvider>
      <I18nextProvider i18n={i18n}>
        <UserContextProvider>
          <ThemeContextProvider>
            <LanguageContextProvider>
              <ToastContainer />
              <RouterProvider router={router} />
            </LanguageContextProvider>
          </ThemeContextProvider>
        </UserContextProvider>
      </I18nextProvider>
    </ChakraProvider>
  </React.StrictMode>,
);
