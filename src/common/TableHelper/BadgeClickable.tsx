import useHandleQueryParams from "hooks/useHandleQueryParams";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import { Badge } from "@chakra-ui/react";

export function BadgeClickableTable<T extends string>({
  colorFunction,
  enumValue,
  queryParamName,
  translateKeysEnumName,
}: {
  colorFunction: (enumValue: T) => string;
  enumValue: T;
  queryParamName: string;
  translateKeysEnumName: string;
}) {
  const { updateQueryParam } = useHandleQueryParams();
  const { t } = useTranslation();
  return (
    <Badge
      data-test-id="badgeClickableTable"
      className="hover:underline hover:cursor-pointer"
      colorScheme={colorFunction(enumValue)}
      onClick={() =>
        updateQueryParam([
          {
            name: queryParamName,
            value: enumValue,
            defaultValue: "",
          },
          {
            name: "page",
            value: "1",
            defaultValue: "1",
          },
        ])
      }
    >
      {t(`enums.${translateKeysEnumName}.${enumValue.toLowerCase()}`)}
    </Badge>
  );
}

export function BadgeClickablePage<T extends string>({
  colorFunction,
  enumValue,
  queryParamName,
  translateKeysEnumName,
  redirectRoute,
}: {
  colorFunction: (enumValue: T) => string;
  enumValue: T;
  queryParamName: string;
  translateKeysEnumName: string;
  redirectRoute: string;
}) {
  const { t } = useTranslation();
  return (
    <Badge
      className="hover:underline hover:cursor-pointer"
      colorScheme={colorFunction(enumValue)}
    >
      <Link to={`/${redirectRoute}?${queryParamName}=${enumValue}`}>
        {t(`enums.${translateKeysEnumName}.${enumValue.toLowerCase()}`)}
      </Link>
    </Badge>
  );
}
