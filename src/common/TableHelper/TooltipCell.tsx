import { ReactNode } from "react";

import { Td, Tooltip } from "@chakra-ui/react";

export const TooltipCell = ({
  element,
  children,
  dataTestInfo,
}: {
  element?: string | null;
  children?: ReactNode;
  dataTestInfo?: string;
}) => (
  <Td className="min-w-24 max-w-52">
    <Tooltip
      className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
      label={element}
      placement="top"
      openDelay={500}
      aria-label="title"
    >
      <p className="truncate" data-test-id={`tooltipCell-${dataTestInfo}`}>
        {element || children}
      </p>
    </Tooltip>
  </Td>
);

export const TooltipP = ({
  element,
  children,
  pStyle,
}: {
  element?: string | null;
  children?: ReactNode;
  pStyle?: string;
}) => (
  <Tooltip
    className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
    label={element}
    placement="top"
    openDelay={500}
    aria-label="title"
  >
    <p className={`truncate ${pStyle}`} data-test-id="tooltipP">
      {element || children}
    </p>
  </Tooltip>
);
