import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import { Tooltip } from "@chakra-ui/react";
import { IconDefinition } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const RedirectButton = ({
  href,
  icon,
  translationKey,
  target,
  dataTestInfo,
}: {
  href: string;
  icon: IconDefinition | string;
  translationKey: string;
  target?: string;
  dataTestInfo?: string;
}) => {
  const { t } = useTranslation();
  return (
    <Tooltip
      className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
      label={t(translationKey)}
      placement="top"
      openDelay={500}
      aria-label={t(translationKey)}
    >
      <Link
        data-test-id={`redirectButtonLink-${dataTestInfo}`}
        className="h-8 w-8 z-10 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex text-md items-center justify-center rounded-full p-2 text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        target={target}
        to={href}
      >
        {typeof icon === "string" ? (
          icon
        ) : (
          <FontAwesomeIcon className="my-[1px]" icon={icon} />
        )}
      </Link>
    </Tooltip>
  );
};
