import { PageOrderEnum } from "enums/pageOrder.enum";
import { TableNamesEnum } from "enums/table.enum";
import { IPageRequestOrderBy } from "interfaces/pageRequest.interface";
import { tableDefinitions } from "src/utils/tableHeader";

import {
  faSort,
  faSortDown,
  faSortUp,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function SortIcons<
  T extends { [key: string]: string },
  U extends IPageRequestOrderBy,
>({
  tableName,
  ColumnEnum,
  header,
  queryParams,
}: {
  tableName: TableNamesEnum;
  ColumnEnum: T;
  header: string;
  queryParams: U | undefined;
}) {
  let icon = faSort;
  let sort = null;
  const orderBy = queryParams?.["orderBy"] as keyof T;
  const order = queryParams?.["order"] as keyof U;

  if (ColumnEnum[header] === orderBy) {
    sort = order || PageOrderEnum.DESC;
  }

  const tableDefinition = tableDefinitions[tableName];

  if (!orderBy) {
    if (ColumnEnum[header] === tableDefinition?.defaultOrderBy) {
      sort = order || tableDefinition?.defaultOrder;
    }
  }

  if (sort) {
    icon = sort === PageOrderEnum.ASC ? faSortUp : faSortDown;
  }

  return (
    <FontAwesomeIcon
      className={`dark:text-white ml-3 ${icon === faSort ? "" : "text-gray-600"}`}
      icon={icon}
    />
  );
}
