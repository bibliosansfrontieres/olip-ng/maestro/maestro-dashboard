import { useContext, useEffect, useState } from "react";
import {
  customStyles,
  customStylesDark,
} from "common/TableHelper/reactSelectStyle";
import { ThemeContext } from "contexts/theme.context";
import useHandleQueryParams from "hooks/useHandleQueryParams";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { useTranslation } from "react-i18next";
import { MultiValue } from "react-select";
import AsyncSelect from "react-select/async";
import { ApiRequestResponse } from "types/apiRequestResponse.type";
import { areArrayEqual } from "utils/arrayManipulation";

import { FormLabel } from "@chakra-ui/react";

export function AsyncMultiSelect<
  T extends { id: string | number; name?: string; label?: string },
>({
  queryParamsName,
  singleGet,
  multiGet,
}: {
  queryParamsName: string;
  singleGet: ({ id }: { id: string }) => Promise<ApiRequestResponse<T>>;
  multiGet: ({
    query,
  }: {
    query: string;
  }) => Promise<ApiRequestResponse<IPageResponse<T>>>;
}) {
  const { darkMode } = useContext(ThemeContext);

  const { t } = useTranslation();
  const { queryParams, updateQueryParam } = useHandleQueryParams();
  const [value, setValue] = useState<{ value: string; label: string }[] | []>(
    [],
  );

  useEffect(() => {
    const addNewValues = async (
      valueFiltered: { value: string; label: string }[],
      idsList: string[],
    ) => {
      for (const id of idsList) {
        const element = await singleGet({ id: id });
        if (element) {
          valueFiltered.push({
            value: id,
            label: element?.name || element?.label || "",
          });
        }
      }
    };
    const getValueFiltered = async () => {
      if (queryParams[queryParamsName]) {
        const idsList = queryParams[queryParamsName].split(",");
        const valueFiltered = value.filter((elt) =>
          idsList.includes(elt.value),
        );
        const idsListFiltered = idsList.filter(
          (elt) => !valueFiltered.map((v) => v.value).includes(elt),
        );
        await addNewValues(valueFiltered, idsListFiltered);
        return valueFiltered;
      }
      return [];
    };
    const setSelectValue = async () => {
      const valueFiltered = await getValueFiltered();
      if (!areArrayEqual(valueFiltered, value, "value")) {
        setValue(valueFiltered);
      }
    };
    setSelectValue();
  }, [queryParams, queryParamsName, singleGet, value]);

  const handleSearch = async (inputValue: string) => {
    const result = await multiGet({ query: inputValue });
    const options = result
      ? result.data.map((elt) => ({
          value: elt.id.toString(),
          label: elt.name || elt.label || "",
        }))
      : [];
    return options.filter(
      (option) => !value.map((v) => v.value).includes(option.value),
    );
  };

  const handleChange = (
    event: MultiValue<{
      value: string;
      label: string;
    }>,
  ) => {
    const valueQueryParams = event.reduce(
      (acc, curr) => acc + "," + curr.value,
      "",
    );
    updateQueryParam([
      {
        name: queryParamsName,
        value: valueQueryParams.length > 0 ? valueQueryParams.slice(1) : "",
        defaultValue: "",
      },
      {
        name: "page",
        value: "1",
        defaultValue: "1",
      },
    ]);
  };

  return (
    <>
      <div>
        <FormLabel
          htmlFor={`asyncMultiSelect-${queryParamsName}`}
          className="text-sm text-navy-700 dark:text-white ml-3 font-bold "
        >
          {t(`common.async_multi_select.label.${queryParamsName}`)}
        </FormLabel>
        <AsyncSelect
          styles={darkMode ? customStylesDark : customStyles}
          id={`asyncMultiSelect-${queryParamsName}`}
          isMulti
          onChange={handleChange}
          value={value}
          placeholder={t(
            `common.async_multi_select.placeholder.${queryParamsName}`,
          )}
          loadOptions={handleSearch}
          noOptionsMessage={() => t("common.async_multi_select.no_user")}
          loadingMessage={() => t("common.async_multi_select.load")}
        />
      </div>
    </>
  );
}

export default AsyncMultiSelect;
