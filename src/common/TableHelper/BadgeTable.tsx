import { useTranslation } from "react-i18next";

import { Badge } from "@chakra-ui/react";

export function BadgeTable<T extends string>({
  colorFunction,
  enumValue,
  translateKeysEnumName,
}: {
  colorFunction: (enumValue: T) => string;
  enumValue: T;
  translateKeysEnumName: string;
}) {
  const { t } = useTranslation();
  return (
    <Badge colorScheme={colorFunction(enumValue)}>
      {t(`enums.${translateKeysEnumName}.${enumValue.toLowerCase()}`)}
    </Badge>
  );
}
