import { OptionProps } from "react-select";

import { CSSObject } from "@emotion/react";
export const customStylesDark = {
  control: (base: CSSObject) => ({
    ...base,
    background: "#111c44",
    borderRadius: "0.375rem",
    borderColor: "#293357",
    "&:hover": {
      borderColor: "white",
    },
    height: "2.5rem",
  }),
  menu: (base: CSSObject) => ({
    ...base,
    borderRadius: 0,
    marginTop: 0,
  }),
  menuList: (base: CSSObject) => ({
    ...base,
    padding: 0,
    background: "#1B254B",
  }),
  placeholder: (base: CSSObject) => ({
    ...base,
    color: "white",
    opacity: 0.5,
  }),
  option: (base: CSSObject, state: OptionProps) => ({
    ...base,
    backgroundColor: state.isSelected ? "#0b1437" : "#1B254B",
    "&:hover": {
      backgroundColor: "#0b1437",
    },
  }),
  dropdownIndicator: (base: CSSObject) => ({
    ...base,
    color: "white",
    "&:hover": {
      color: "white",
    },
  }),
  clearIndicator: (base: CSSObject) => ({
    ...base,
    color: "white",
    "&:hover": {
      color: "white",
      cursor: "pointer",
    },
  }),
  input: (base: CSSObject) => ({
    ...base,
    color: "white",
  }),
  singleValue: (base: CSSObject) => ({
    ...base,
    color: "white",
  }),
  multiValue: (base: CSSObject) => ({
    ...base,
    color: "white",
    backgroundColor: "#0b1437",
  }),
  multiValueRemove: (base: CSSObject) => ({
    ...base,
    color: "white",
    backgroundColor: "#0b1437",
    "&:hover": {
      color: "white",
      cursor: "pointer",
      backgroundColor: "#0b1437",
    },
  }),
  // label on the select
  multiValueLabel: (base: CSSObject) => ({
    ...base,
    color: "white",
  }),
  // label on the dropdown
  container: (base: CSSObject) => ({
    ...base,
    color: "white",
  }),
};

export const customStyles = {
  control: (base: CSSObject) => ({
    ...base,
    borderColor: "#e2e8f0",
    "&:hover": {
      borderColor: "#cbd5e0",
    },
    borderRadius: "0.375rem",
    height: "2.5rem",
  }),
  input: (base: CSSObject) => ({
    ...base,
    color: "#1b254b",
  }),
  singleValue: (base: CSSObject) => ({
    ...base,
    color: "#1b254b",
  }),
  multiValue: (base: CSSObject) => ({
    ...base,
    color: "#1b254b",
    backgroundColor: "#f4f7fe",
  }),
  multiValueRemove: (base: CSSObject) => ({
    ...base,
    color: "#1b254b",
    backgroundColor: "#f4f7fe",
    "&:hover": {
      color: "#1b254b",
      cursor: "pointer",
      backgroundColor: "#f4f7fe",
    },
  }),
  placeholder: (base: CSSObject) => ({
    ...base,
    color: "#1b254b",
    opacity: 0.5,
  }),
  clearIndicator: (base: CSSObject) => ({
    ...base,
    color: "#1b254b",
    "&:hover": {
      color: "#1b254b",
    },
  }),
  dropdownIndicator: (base: CSSObject) => ({
    ...base,
    color: "#1b254b",
    "&:hover": {
      color: "#1b254b",
    },
  }),
};

export const customStylesInputError = {
  ...customStyles,
  control: (base: CSSObject) => ({
    ...base,
    borderColor: "red",
    borderRadius: "0.375rem",
    height: "2.5rem",
  }),
};

export const customStylesDarkInputError = {
  ...customStylesDark,
  control: (base: CSSObject) => ({
    ...base,
    background: "#111c44",
    borderRadius: "0.375rem",
    borderColor: "red",
    height: "2.5rem",
  }),
};
