import { useEffect, useState } from "react";
import Switch from "components/switch/Switch";
import { BooleanEnum } from "enums/boolean.enum";
import useHandleQueryParams from "hooks/useHandleQueryParams";
import { useTranslation } from "react-i18next";

export const SwitchOnly = ({
  queryParamsName,
  defaultSwitchValue,
  defaultQueryParamValue,
}: {
  queryParamsName: string;
  defaultSwitchValue: boolean;
  defaultQueryParamValue: boolean;
}) => {
  const [isOnly, setIsOnly] = useState(defaultSwitchValue);
  const { t } = useTranslation();
  const { queryParams, updateQueryParam } = useHandleQueryParams();

  useEffect(() => {
    const isOnlyValue = queryParams[queryParamsName]
      ? queryParams[queryParamsName] === BooleanEnum.TRUE
      : defaultQueryParamValue;
    setIsOnly(isOnlyValue);
  }, [queryParams, queryParamsName, defaultQueryParamValue]);
  return (
    <div className="flex gap-1">
      <Switch
        id={queryParamsName}
        checked={isOnly}
        onChange={() => {
          setIsOnly((prev) => !prev);
          updateQueryParam([
            {
              name: queryParamsName,
              value: !isOnly ? BooleanEnum.TRUE : BooleanEnum.FALSE,
              defaultValue: defaultQueryParamValue
                ? BooleanEnum.TRUE
                : BooleanEnum.FALSE,
            },
            {
              name: "page",
              value: "1",
              defaultValue: "1",
            },
          ]);
        }}
      />
      <label
        htmlFor={queryParamsName}
        className="text-sm text-navy-700 dark:text-white"
      >
        {t(`common.switch_only.label.${queryParamsName}`)}
      </label>
    </div>
  );
};
