import useHandleQueryParams from "hooks/useHandleQueryParams";
import { Link } from "react-router-dom";

export function InformationClickableTable<T extends string>({
  value,
  dataTestValue,
  queryParamName,
  filterValue,
  extraStyle,
}: {
  value: T | React.ReactNode;
  dataTestValue?: string;
  queryParamName: string;
  filterValue: string;
  extraStyle?: string;
}) {
  const { updateQueryParam } = useHandleQueryParams();
  return (
    <div
      data-test-id="informationClickableTable"
      data-test-value={dataTestValue}
      className={`hover:underline hover:cursor-pointer ${extraStyle}`}
      onClick={() =>
        updateQueryParam([
          {
            name: queryParamName,
            value: filterValue,
            defaultValue: "",
          },
          {
            name: "page",
            value: "1",
            defaultValue: "1",
          },
        ])
      }
    >
      {value}
    </div>
  );
}

export function InformationClickablePage<T extends string>({
  value,
  queryParamName,
  filterValue,
  redirectRoute,
}: {
  value: T;
  queryParamName: string;
  filterValue: string;
  redirectRoute: string;
}) {
  return (
    <Link
      className="hover:underline hover:cursor-pointer"
      to={`/${redirectRoute}?${queryParamName}=${filterValue}`}
    >
      {value}
    </Link>
  );
}
