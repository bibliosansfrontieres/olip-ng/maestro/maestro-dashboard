import SortIcons from "common/TableHelper/SortIcons";
import { PageOrderEnum } from "enums/pageOrder.enum";
import { TableNamesEnum } from "enums/table.enum";
import useHandleQueryParams from "hooks/useHandleQueryParams";
import { useTranslation } from "react-i18next";
import { tableDefinitions } from "utils/tableHeader";

import { Th } from "@chakra-ui/react";

export function ColumnHeaderHelper<
  T extends { [key: string]: string },
  U extends { [key: string]: string },
>({
  tableName,
  headers,
  orderByEnum,
}: {
  tableName: TableNamesEnum;
  headers: T;
  orderByEnum: U;
}) {
  const { t } = useTranslation();
  const { queryParams, updateQueryParam } = useHandleQueryParams();

  const handleSort = (field: string) => {
    const getParams = (params: string) => queryParams?.[params];
    let newOrder = PageOrderEnum.ASC;
    const newOrderBy = orderByEnum[field.toUpperCase()];
    const tableDefinition = tableDefinitions[tableName];
    const prevOrder =
      (getParams("order") as PageOrderEnum) || tableDefinition?.defaultOrder;
    const prevOrderBy = getParams("orderBy") || tableDefinition?.defaultOrderBy;

    if (
      newOrderBy === prevOrderBy &&
      (prevOrder === undefined ||
        (prevOrder && prevOrder === PageOrderEnum.ASC))
    ) {
      newOrder = PageOrderEnum.DESC;
    }

    updateQueryParam([
      { name: "order", value: newOrder },
      {
        name: "orderBy",
        value: newOrderBy,
      },
      {
        name: "page",
        value: "1",
        defaultValue: "1",
      },
    ]);
  };
  const sortingHeaders = Object.keys(orderByEnum);
  return Object.keys(headers).map((header) => (
    <Th
      key={`Th-${header}`}
      className={`${sortingHeaders.includes(header) ? "cursor-pointer" : "cursor-default"} border-b-[1px] border-gray-200 pt-4 pb-2`}
      onClick={() =>
        sortingHeaders.includes(header) ? handleSort(header) : null
      }
    >
      {header !== "ACTIONS" && (
        <div className="flex items-center justify-between text-xs text-gray-200">
          <p className="text-sm font-bold text-gray-600 dark:text-white">
            {t(`${tableName}.table.${header.toLowerCase()}`)}
          </p>
          {sortingHeaders.includes(header) && (
            <SortIcons
              tableName={tableName}
              ColumnEnum={headers}
              header={header}
              queryParams={queryParams}
            />
          )}
        </div>
      )}
    </Th>
  ));
}

export function ColumnHeaderHelperWithoutSort<
  T extends { [key: string]: string },
>({ tableName, headers }: { tableName: TableNamesEnum; headers: T }) {
  const { t } = useTranslation();

  return Object.keys(headers).map((header) => (
    <Th
      key={`Th-${header}`}
      className="border-b-[1px] border-gray-200 pt-4 pb-2"
    >
      {header !== "ACTIONS" && (
        <div className="flex items-center justify-between text-xs text-gray-200">
          <p className="text-sm font-bold text-gray-600 dark:text-white">
            {t(`${tableName}.table.${header.toLowerCase()}`)}
          </p>
        </div>
      )}
    </Th>
  ));
}
