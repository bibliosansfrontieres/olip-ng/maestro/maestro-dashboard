import { Link } from "react-router-dom";
import getEnv from "utils/getEnv";

import {
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverTrigger,
} from "@chakra-ui/react";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const ValueCell = ({ value }: { value: string | null }): JSX.Element => {
  const handleJsonValue = (valueJSON: string): JSX.Element => {
    const newTab = Object.entries(JSON.parse(valueJSON)) as [string, string][];
    return (
      <>
        {newTab.map((item, index) => (
          <div className="flex gap-2 w-full" key={index}>
            <p className="font-bold">{`${item[0]} : `}</p>
            {typeof item[1] === "string" && item[1].startsWith("http") ? (
              <Link
                to={item[1]}
                target={
                  item[1].startsWith(getEnv("PUBLIC_DASHBOARD_URL"))
                    ? ""
                    : "_blank"
                }
                className="underline"
              >
                {item[1]}
              </Link>
            ) : (
              <p>{item[1]}</p>
            )}
          </div>
        ))}
      </>
    );
  };

  return (
    <>
      {value && value !== "{}" && (
        <Popover variant="custom">
          <PopoverTrigger>
            <IconButton
              size="sm"
              data-test-id="valuePopoverButton"
              aria-label="Value"
              icon={<FontAwesomeIcon icon={faEye} />}
            />
          </PopoverTrigger>
          <PopoverContent w="100%" p={4} className="dark:!bg-navy-800">
            <PopoverArrow className="dark:!bg-navy-800" />
            <PopoverCloseButton />
            <PopoverBody data-test-id="valuePopoverBody">
              {handleJsonValue(value)}
            </PopoverBody>
          </PopoverContent>
        </Popover>
      )}
    </>
  );
};
