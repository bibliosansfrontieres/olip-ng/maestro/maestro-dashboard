import { useContext, useEffect, useState } from "react";
import {
  customStyles,
  customStylesDark,
} from "common/TableHelper/reactSelectStyle";
import { ThemeContext } from "contexts/theme.context";
import useHandleQueryParams from "hooks/useHandleQueryParams";
import { useTranslation } from "react-i18next";
import Select, { MultiValue } from "react-select";

import { FormLabel } from "@chakra-ui/react";
export function ReactMultiSelect<T extends { [key: string]: string }>({
  enumList,
  queryParamsEnumName,
  translateKeysEnumName,
}: {
  enumList: T;
  queryParamsEnumName: string;
  translateKeysEnumName: string;
}) {
  const { t } = useTranslation();
  const { darkMode } = useContext(ThemeContext);
  const [value, setValue] = useState<{ value: string; label: string }[]>([]);
  const { queryParams, updateQueryParam } = useHandleQueryParams();
  const options = Object.entries(enumList).map(([key, enumValue]) => ({
    value: enumValue,
    label: t(`enums.${translateKeysEnumName}.${key.toLowerCase()}`),
  }));

  useEffect(() => {
    const valueObject = [] as { value: string; label: string }[];
    if (queryParams?.[queryParamsEnumName]) {
      queryParams[queryParamsEnumName].split(",").forEach((elt) =>
        valueObject.push({
          value: elt,
          label: t(`enums.${translateKeysEnumName}.${elt.toLowerCase()}`),
        }),
      );
    }
    setValue(valueObject);
  }, [queryParams, queryParamsEnumName, translateKeysEnumName, t]);

  const handleChange = (
    event: MultiValue<{
      value: string;
      label: string;
    }>,
  ) => {
    const valueQueryParams = event.reduce(
      (acc, curr) => acc + "," + curr.value,
      "",
    );
    updateQueryParam([
      {
        name: queryParamsEnumName,
        value: valueQueryParams.length > 0 ? valueQueryParams.slice(1) : "",
        defaultValue: "",
      },
      {
        name: "page",
        value: "1",
        defaultValue: "1",
      },
    ]);
  };

  return (
    <div className="flex flex-col">
      <FormLabel
        htmlFor={`select-${translateKeysEnumName}`}
        className="text-sm text-navy-700 dark:text-white ml-3 font-bold "
      >
        {t(`common.react_multi_select.label.${translateKeysEnumName}`)}
      </FormLabel>
      <Select
        styles={darkMode ? customStylesDark : customStyles}
        id={`select-${translateKeysEnumName}`}
        onChange={handleChange}
        isMulti
        placeholder={t(
          `common.react_multi_select.placeholder.${translateKeysEnumName}`,
        )}
        value={value}
        options={options}
      />
    </div>
  );
}
