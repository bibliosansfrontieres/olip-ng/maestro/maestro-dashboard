import { useContext } from "react";
import {
  customStyles,
  customStylesDark,
  customStylesDarkInputError,
  customStylesInputError,
} from "common/TableHelper/reactSelectStyle";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { ThemeContext } from "contexts/theme.context";
import { useField } from "formik";
import { useTranslation } from "react-i18next";
import { MultiValue } from "react-select";
import AsyncCreatableSelect from "react-select/async-creatable";
import { IGetLabelsResponse } from "services/labels/interfaces/getLabels.interface";
import { IPostLabelResponse } from "services/labels/interfaces/postLabel.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

import { FormLabel } from "@chakra-ui/react";

const AsyncCreateMultiSelectForm = ({
  multiGet,
  postNewValue,
  asyncName,
  required,
  state,
}: {
  multiGet: ({
    query,
  }: {
    query: string;
  }) => Promise<ApiRequestResponse<IGetLabelsResponse>>;
  asyncName: string;
  postNewValue: ({
    name,
  }: {
    name: string;
  }) => Promise<ApiRequestResponse<IPostLabelResponse>>;
  required?: boolean;
  state?: string;
}) => {
  const { darkMode } = useContext(ThemeContext);
  let styles;
  if (darkMode) {
    styles = state === "error" ? customStylesDarkInputError : customStylesDark;
  } else {
    styles = state === "error" ? customStylesInputError : customStyles;
  }
  const [field, meta, helpers] = useField(asyncName);
  const { setValue } = helpers;
  const { t } = useTranslation();
  const handleSearch = async (inputValue: string) => {
    const result = await multiGet({ query: inputValue });
    const options = result
      ? result.data.map((elt) => ({
          value: elt.id,
          label: elt.name,
        }))
      : [];
    return options;
  };

  const handleChange = (
    event: MultiValue<{
      value: number;
      label: string;
    }>,
  ) => {
    if (event) {
      setValue(event);
    }
  };

  const handleCreate = async (inputValue: string) => {
    if (inputValue) {
      const value = await postNewValue({ name: inputValue });
      if (value) {
        handleChange(
          field.value
            ? [
                ...field.value,
                {
                  value: value.id,
                  label: value.name,
                },
              ]
            : [
                {
                  value: value.id,
                  label: value.name,
                },
              ],
        );
      }
    }
  };
  return (
    <>
      <div>
        <FormLabel
          htmlFor={`asyncMultiSelect-${asyncName}`}
          className={`text-sm mt-3 text-navy-700 dark:text-white ml-3 font-bold ${required ? 'after:content-["*"] after:ml-0.5 after:text-red-500' : ""}`}
        >
          {t(`common.async_select.label.${asyncName}`)}
        </FormLabel>
        <AsyncCreatableSelect
          maxMenuHeight={200}
          required={required}
          styles={styles}
          id={`asyncMultiSelect-${asyncName}`}
          onChange={handleChange}
          isMulti
          onCreateOption={handleCreate}
          value={field.value}
          placeholder={t(`common.async_select.placeholder.${asyncName}`)}
          loadOptions={handleSearch}
          noOptionsMessage={() => t("common.async_multi_select.no_user")}
          loadingMessage={() => t("common.async_multi_select.load")}
          formatCreateLabel={(inputValue: string) =>
            t("common.async_create_multi_select.create", { name: inputValue })
          }
          menuPosition="fixed"
        />
        {meta.touched && meta.error ? (
          <FormErrorMessageText text={meta.error} />
        ) : null}
      </div>
    </>
  );
};

export default AsyncCreateMultiSelectForm;
