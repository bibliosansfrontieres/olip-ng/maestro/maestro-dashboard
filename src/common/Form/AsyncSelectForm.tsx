import { useContext } from "react";
import {
  customStyles,
  customStylesDark,
  customStylesDarkInputError,
  customStylesInputError,
} from "common/TableHelper/reactSelectStyle";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { ThemeContext } from "contexts/theme.context";
import { useField } from "formik";
import { useTranslation } from "react-i18next";
import { SingleValue } from "react-select";
import AsyncSelect from "react-select/async";
import { IGetFqdnsResponse } from "services/fqdnMasks/interfaces/getFqdns.interface";
import { IGetVirtualMachineSavesResponse } from "services/virtualMachineSaves/interfaces/getVirtualMachineSaves.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

import { FormLabel } from "@chakra-ui/react";

const AsyncSelectForm = ({
  multiGet,
  asyncName,
  required,
  state,
}: {
  multiGet: ({
    query,
    vmId,
    types,
  }: {
    query: string;
    vmId?: number;
    types?: string[];
  }) => Promise<
    ApiRequestResponse<IGetFqdnsResponse | IGetVirtualMachineSavesResponse>
  >;
  asyncName: string;
  required?: boolean;
  state?: string;
}) => {
  const { darkMode } = useContext(ThemeContext);
  let styles;
  if (darkMode) {
    styles = state === "error" ? customStylesDarkInputError : customStylesDark;
  } else {
    styles = state === "error" ? customStylesInputError : customStyles;
  }
  const [field, meta, helpers] = useField(asyncName);
  const { setValue } = helpers;
  const { t } = useTranslation();

  const handleSearch = async (inputValue: string) => {
    const result = await multiGet({ query: inputValue });
    const options = result
      ? result.data.map((elt) => ({
          value: elt.id,
          //@ts-expect-error because it depends on the return type which depends of the nameField
          label: asyncName === "fqdn" ? elt.label : elt.name,
        }))
      : [];
    return options;
  };

  const handleChange = (
    event: SingleValue<{
      value: number | string;
      label: string;
    }>,
  ) => {
    if (event) {
      setValue(event);
    }
  };

  return (
    <>
      <div>
        <FormLabel
          htmlFor={`asyncMultiSelect-${asyncName}`}
          className={`text-sm mt-3 text-navy-700 dark:text-white ml-3 font-bold ${required ? 'after:content-["*"] after:ml-0.5 after:text-red-500' : ""}`}
        >
          {t(`common.async_select.label.${asyncName}`)}
        </FormLabel>
        <AsyncSelect
          required={required}
          styles={styles}
          id={`asyncMultiSelect-${asyncName}`}
          onChange={handleChange}
          value={field.value}
          placeholder={t(`common.async_select.placeholder.${asyncName}`)}
          loadOptions={handleSearch}
          defaultOptions
          noOptionsMessage={() => t("common.async_multi_select.no_user")}
          loadingMessage={() => t("common.async_multi_select.load")}
        />
        {meta.touched && meta.error ? (
          <FormErrorMessageText text={meta.error} />
        ) : null}
      </div>
    </>
  );
};

export default AsyncSelectForm;
