import { useTranslation } from "react-i18next";
import { Link, useNavigate, useParams } from "react-router-dom";

import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function GoBackButton() {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const hasPreviousPage = window.history.length > 2;
  const params = useParams();
  if (!params.id) {
    return <></>;
  }
  if (hasPreviousPage) {
    return (
      <button
        className="flex items-center text-sm text-gray-600 hover:cursor-pointer hover:underline gap-2"
        onClick={() => navigate(-1)}
      >
        <FontAwesomeIcon icon={faChevronLeft} />
        <p>{t("button.go_back")}</p>
      </button>
    );
  }
  return (
    <Link to={".."}>
      <div className="flex items-center hover:cursor-pointer hover:underline text-sm text-gray-600 gap-2">
        <FontAwesomeIcon className="h-3 w-3" icon={faChevronLeft} />
        <p>{t("button.go_back")}</p>
      </div>
    </Link>
  );
}
