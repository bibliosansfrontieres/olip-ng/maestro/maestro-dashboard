import { ReactNode, useContext, useEffect } from "react";
import { UserContext } from "contexts/user.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { Navigate, Outlet, useLocation } from "react-router-dom";
import { hasPermissions } from "utils/hasPermission";

export default function UserProtectedRoute({
  permissions,
  children,
}: {
  permissions?: PermissionNameEnum | PermissionNameEnum[];
  children?: ReactNode;
}): JSX.Element {
  const { pathname } = useLocation();

  const { checkToken, user } = useContext(UserContext);

  useEffect(() => {
    checkToken();
  }, [pathname, checkToken]);

  const checkConnected = () => {
    if (!user) {
      return <Navigate to={`/login?redirect=${pathname}`} replace />;
    }
    if (!permissions || hasPermissions(user, permissions)) {
      return children ? children : <Outlet />;
    }
    return <Navigate to="/" />;
  };
  return <>{checkConnected()}</>;
}
