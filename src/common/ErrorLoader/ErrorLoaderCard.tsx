import Card from "components/card/Card";
import { useTranslation } from "react-i18next";

export default function ErrorLoaderCard({
  translation,
}: {
  translation: string;
}): JSX.Element {
  const { t } = useTranslation();
  return (
    <Card extraStyle="h-max">
      <p className="text-xl text-navy-700 dark:text-white">
        {t(`${translation}`)}
      </p>
    </Card>
  );
}
