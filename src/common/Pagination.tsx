import useHandleQueryParams from "hooks/useHandleQueryParams";
import { IPagination } from "interfaces/pagination.interface";

import {
  faAngleLeft,
  faAngleRight,
  faAnglesLeft,
  faAnglesRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Pagination({ meta }: { meta: IPagination }) {
  const { queryParams, updateQueryParam } = useHandleQueryParams();

  const handlePagination = (page: number) => {
    updateQueryParam({
      name: "page",
      value: page.toString(),
      defaultValue: "1",
    });
  };

  const handleChangeTake = (take: string) => {
    const newPage =
      Math.floor((meta.take * (meta.page - 1)) / parseInt(take)) + 1;
    updateQueryParam([
      {
        name: "take",
        value: take,
        defaultValue: "10",
      },
      {
        name: "page",
        value: newPage.toString(),
        defaultValue: "1",
      },
    ]);
  };

  const displayedPages = () => {
    const pages = [];
    if (meta.pageCount <= 7) {
      for (let i = 1; i <= meta.pageCount; i++) {
        pages.push(i);
      }
    } else if (meta.page <= 4) {
      for (let i = 1; i <= 5; i++) {
        pages.push(i);
      }
      pages.push("...");
      pages.push(meta.pageCount);
    } else if (meta.page >= meta.pageCount - 3) {
      pages.push(1);
      pages.push("...");
      for (let i = meta.pageCount - 4; i <= meta.pageCount; i++) {
        pages.push(i);
      }
    } else {
      pages.push(1);
      pages.push("...");
      for (let i = meta.page - 1; i <= meta.page + 1; i++) {
        pages.push(i);
      }
      pages.push("...");
      pages.push(meta.pageCount);
    }
    return pages;
  };

  const borderClass = "border-brand-500 dark:border-brand-400 ";
  const textClass = "text-brand-500 dark:text-brand-400";
  const buttonClass =
    "flex items-center justify-center rounded-full border p-2 text-md transition duration-200 hover:cursor-pointer w-8 h-8";
  const activeClass =
    "bg-brand-500 dark:bg-brand-400 text-white dark:text-white";
  const disabledClass =
    "disabled:cursor-default disabled:text-gray-400 dark:disabled:text-gray-500 disabled:border-gray-400 dark:disabled:border-gray-500";

  const widthPage = (page: number) => {
    if (page >= 1000) {
      return "!w-14";
    } else if (page >= 100) {
      return "!w-12";
    } else if (page >= 10) {
      return "!w-10";
    }
    return "w-8";
  };

  return (
    <div className="flex items-center mt-3 justify-between">
      <select
        className="relative cursor-default rounded-md py-1.5 pl-3 pr-1 text-left text-brand-500 dark:text-brand-400 shadow-sm ring-1 ring-inset focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6 bg-white dark:bg-navy-800 ring-brand-500 dark:ring-brand-400"
        value={queryParams?.take || "10"}
        onChange={(e) => handleChangeTake(e.target.value)}
      >
        {[2, 10, 20, 30, 40, 50].map((pageSize) => (
          <option key={`pageSize-${pageSize}`} value={pageSize}>
            {pageSize}
          </option>
        ))}
      </select>
      <div className="flex items-center gap-2">
        <button
          className={`${borderClass} ${textClass} ${buttonClass} ${disabledClass}`}
          onClick={() => handlePagination(1)}
          disabled={!meta.hasPreviousPage}
        >
          <FontAwesomeIcon icon={faAnglesLeft} />
        </button>
        <button
          className={`${borderClass} ${textClass} ${buttonClass} ${disabledClass}`}
          onClick={() => handlePagination(meta.page - 1)}
          disabled={!meta.hasPreviousPage}
        >
          <FontAwesomeIcon icon={faAngleLeft} />
        </button>

        {displayedPages().map((page, index) => {
          if (typeof page === "number") {
            return (
              <button
                className={`${borderClass} ${textClass} ${buttonClass} ${page === meta.page ? activeClass : ""} ${widthPage(page)}`}
                onClick={() => handlePagination(page)}
                key={`page-${index}`}
              >
                {`${page}`}
              </button>
            );
          }
          if (typeof page === "string") {
            return (
              <button
                className={`${borderClass} ${textClass} ${buttonClass} !cursor-default`}
                key={`...-${index}`}
              >
                {page}
              </button>
            );
          }
        })}

        <button
          className={`${borderClass} ${textClass} ${buttonClass} ${disabledClass}`}
          onClick={() => handlePagination(meta.page + 1)}
          disabled={!meta.hasNextPage}
        >
          <FontAwesomeIcon icon={faAngleRight} />
        </button>
        <button
          className={`${borderClass} ${textClass} ${buttonClass} ${disabledClass}`}
          onClick={() => handlePagination(meta.pageCount)}
          disabled={!meta.hasNextPage}
        >
          <FontAwesomeIcon icon={faAnglesRight} />
        </button>
      </div>
    </div>
  );
}
