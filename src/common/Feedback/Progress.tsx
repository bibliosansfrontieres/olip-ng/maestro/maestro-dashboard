const Progress = (props: { value: number }) => {
  const { value } = props;
  return (
    <div className="h-2 rounded-full bg-gray-200 dark:bg-navy-700">
      <div
        className="flex h-full items-center justify-center rounded-full bg-brand-500 dark:bg-brand-400"
        style={{ width: `${value}%` }}
      />
    </div>
  );
};

export default Progress;
