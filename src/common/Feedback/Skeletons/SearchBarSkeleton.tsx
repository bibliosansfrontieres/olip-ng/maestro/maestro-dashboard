import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function SearchBarSkeleton({
  extraStyles,
}: {
  extraStyles?: string;
}) {
  return (
    <div
      className={`rounded-full h-10 bg-gray-200 dark:bg-navy-700 ${extraStyles}`}
    >
      <FontAwesomeIcon
        className="pl-3 pt-3 text-xl h-4 w-4 text-gray-400 dark:text-white"
        icon={faMagnifyingGlass}
      />
    </div>
  );
}
