import Card from "components/card/Card";

export default function CardSkeleton(): JSX.Element {
  return (
    <Card extraStyle="h-screen animate-pulse">
      <div className="flex h-full w-full bg-gray-200 dark:bg-navy-700 rounded-md"></div>
    </Card>
  );
}
