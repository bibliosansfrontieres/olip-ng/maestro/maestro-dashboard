import Switch from "components/switch/Switch";

import { Skeleton } from "@chakra-ui/react";

export default function SwitchSkeleton(): JSX.Element {
  return (
    <div className="animate-pulse">
      <div className="flex gap-1 mt-2">
        <Switch disabled={true} checked={true} />
        <Skeleton height="20px" width="100px" mb={2} />
      </div>
    </div>
  );
}
