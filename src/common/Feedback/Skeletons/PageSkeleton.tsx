export default function PageSkeleton(): JSX.Element {
  return (
    <div className="animate-pulse">
      <div className="flex h-screen w-screen">
        <div className="w-[240px] bg-gray-200 dark:!bg-navy-700"></div>
        <div className="w-full bg-gray-300 dark:!bg-navy-800"></div>
      </div>
    </div>
  );
}
