import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ButtonSkeleton() {
  return (
    <button className="btn-primary flex w-40" disabled={true}>
      <FontAwesomeIcon className="mr-2 self-center" icon={faPlus} />
    </button>
  );
}
