export default function TableSkeleton({
  nbLines,
}: {
  nbLines: number;
}): JSX.Element {
  return (
    <div className="pt-5 animate-pulse">
      <div className="h-11 bg-gray-200 dark:bg-navy-700 rounded-md" />
      <div className="animate-none h-px my-[1px] min-h-px bg-gray-200 dark:border-white/10" />
      {[...Array(nbLines)].map((i, index) => (
        <div
          key={`${i}-${index}`}
          className="h-16 mb-[1px] bg-gray-200 dark:bg-navy-700 rounded-md"
        />
      ))}
    </div>
  );
}
