export default function OutletSkeleton(): JSX.Element {
  return (
    <div className="animate-pulse">
      <div className="h-full bg-gray-200 mb-4 dark:!bg-navy-800 "></div>
    </div>
  );
}
