import { useContext } from "react";
import { ThemeContext } from "contexts/theme.context";
import AsyncSelect from "react-select/async";

import { Skeleton } from "@chakra-ui/react";
import { CSSObject } from "@emotion/react";

export default function MultiSelectSkeleton(): JSX.Element {
  const { darkMode } = useContext(ThemeContext);

  return (
    <div className="animate-pulse ">
      <div>
        <Skeleton height="20px" mb={2} />
        <AsyncSelect
          id="asyncMultiSelect"
          isDisabled
          styles={{
            control: (base: CSSObject) => ({
              ...base,
              backgroundColor: darkMode ? "#1b254b" : "#e9ecef",
              borderColor: darkMode ? "#293357" : "#e2e8f0",
            }),
          }}
        />
      </div>
    </div>
  );
}
