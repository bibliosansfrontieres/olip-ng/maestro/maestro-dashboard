import { Skeleton } from "@chakra-ui/react";

export default function DatePickerSkeleton(): JSX.Element {
  return (
    <div className="animate-pulse ">
      <div>
        <Skeleton height="20px" mb={2} />
        <div className="h-10 w-40 rounded-md border bg-white/0 p-3 text-sm outline-none border-gray-200 hover:border-gray-300 dark:!border-white/10 dark:hover:!border-white text-navy-700 dark:text-white" />
      </div>
    </div>
  );
}
