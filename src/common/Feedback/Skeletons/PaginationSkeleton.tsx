import {
  faAngleLeft,
  faAngleRight,
  faAnglesLeft,
  faAnglesRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function PaginationSkeleton(): JSX.Element {
  const textClass = "text-brand-500 dark:text-brand-400";
  const buttonClass =
    "flex items-center justify-center rounded-full border p-2 text-md transition duration-200 hover:cursor-pointer w-8 h-8";
  const disabledClass =
    "disabled:cursor-default disabled:text-gray-400 dark:disabled:text-gray-500 disabled:border-gray-400 dark:disabled:border-gray-500";
  return (
    <div className="flex items-center mt-3 justify-between animate-pulse">
      <select
        className={`relative cursor-default rounded-md py-1.5 pl-3 pr-1 text-left ring-gray-400 dark:ring-gray-500 shadow-sm ring-1 ring-inset focus:outline-none focus:ring-2 sm:text-sm sm:leading-6 dark:bg-navy-800 ${disabledClass}`}
        disabled={true}
      ></select>
      <div className="flex items-center gap-2">
        <button
          className={`${textClass} ${buttonClass} ${disabledClass}`}
          disabled={true}
        >
          <FontAwesomeIcon icon={faAnglesLeft} />
        </button>
        <button
          className={`${textClass} ${buttonClass} ${disabledClass}`}
          disabled={true}
        >
          <FontAwesomeIcon icon={faAngleLeft} />
        </button>

        {[""].map((page, index) => {
          return (
            <button
              className={`${textClass} ${buttonClass} ${disabledClass}`}
              key={`...-${index}`}
              disabled={true}
            >
              {page}
            </button>
          );
        })}

        <button
          className={`${textClass} ${buttonClass} ${disabledClass}`}
          disabled={true}
        >
          <FontAwesomeIcon icon={faAngleRight} />
        </button>
        <button
          className={`${textClass} ${buttonClass} ${disabledClass}`}
          disabled={true}
        >
          <FontAwesomeIcon icon={faAnglesRight} />
        </button>
      </div>
    </div>
  );
}
