import ButtonSkeleton from "common/Feedback/Skeletons/ButtonSkeleton";
import PaginationSkeleton from "common/Feedback/Skeletons/PaginationSkeleton";
import SearchBarSkeleton from "common/Feedback/Skeletons/SearchBarSkeleton";
import TableHelperSkeleton from "common/Feedback/Skeletons/TableHelperSkeleton";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Card from "components/card/Card";

export function SimpleTableCardSkeleton({
  filters = [],
  haveButton = false,
  noSearchBar = false,
  nbLines = 10,
}: {
  filters?: ("switch" | "select" | "datePicker")[];
  nbLines?: number;
  haveButton?: boolean;
  noSearchBar?: boolean;
}) {
  return (
    <Card>
      <>
        {!noSearchBar && (
          <header className="animate-pulse relative flex flex-wrap items-center justify-between">
            <SearchBarSkeleton extraStyles={haveButton ? "w-64" : "w-full"} />
            {haveButton && <ButtonSkeleton />}
          </header>
        )}
        <TableHelperSkeleton filters={filters} />
        <TableSkeleton nbLines={nbLines} />
        <PaginationSkeleton />
      </>
    </Card>
  );
}
