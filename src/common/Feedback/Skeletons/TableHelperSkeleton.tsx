import DatePickerSkeleton from "common/Feedback/Skeletons/DatePickerSkeleton";
import MultiSelectSkeleton from "common/Feedback/Skeletons/MultiSelectSkeleton";

import SwitchSkeleton from "./SwitchSkeleton";

export default function TableHelperSkeleton({
  filters,
}: {
  filters: ("select" | "datePicker" | "switch")[];
}): JSX.Element {
  return (
    <div className="mt-2 flex flex-wrap gap-4">
      {filters.map((filter, index) => {
        switch (filter) {
          case "select":
            return <MultiSelectSkeleton key={index} />;
          case "datePicker":
            return <DatePickerSkeleton key={index} />;
          case "switch":
            return <SwitchSkeleton key={index} />;
          default:
            return null;
        }
      })}
    </div>
  );
}
