import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

export default function ErrorList({ translation }: { translation: string }) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  return (
    <div className="flex flex-col items-center pt-10 gap-2">
      <p className="text-xl text-navy-700 dark:text-white">
        {t(`${translation}`)}
      </p>
      <button className="btn-primary mt-5" onClick={() => navigate(0)}>
        {t("button.refresh_page")}
      </button>
    </div>
  );
}
