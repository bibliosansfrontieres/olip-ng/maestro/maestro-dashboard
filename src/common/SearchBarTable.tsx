import { useEffect, useState } from "react";
import useHandleQueryParams from "hooks/useHandleQueryParams";
import { useTranslation } from "react-i18next";

import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function SearchBarTable() {
  const { t } = useTranslation();
  const { queryParams, updateQueryParam } = useHandleQueryParams();
  const [value, setValue] = useState<string>(queryParams?.query || "");

  useEffect(() => {
    const timeOutId = setTimeout(() => {
      if (
        // only if queryParams !== value (work also if value is empty and queryParams undefined)
        !(queryParams?.query === undefined && value === "") &&
        queryParams?.query !== value
      ) {
        updateQueryParam([
          {
            name: "query",
            value: value,
            defaultValue: "",
          },
          {
            name: "page",
            value: "1",
            defaultValue: "1",
          },
        ]);
      }
      //Your search query and it will run the function after 1sec from user stops typing
    }, 1000);
    return () => clearTimeout(timeOutId);
  }, [value, updateQueryParam, queryParams?.query]);

  return (
    <div
      data-test-id="searchBar"
      className="flex items-center rounded-full h-10 bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white"
    >
      <p className="pl-3 pr-2 text-xl">
        <FontAwesomeIcon
          className="h-4 w-4 text-gray-400 dark:text-white"
          icon={faMagnifyingGlass}
        />
      </p>
      <input
        autoFocus
        type="text"
        value={value}
        placeholder={t("navbar.search")}
        onChange={(e) => setValue(e.target.value)}
        className="block h-full !w-full rounded-full bg-lightPrimary text-sm font-medium text-navy-700 outline-none placeholder:!text-gray-400 dark:bg-navy-900 dark:text-white dark:placeholder:!text-white sm:w-fit"
      />
    </div>
  );
}
