import { Dispatch, SetStateAction, useEffect, useState } from "react";
import { format } from "date-fns";
import { MaestroLanguagesEnum } from "enums/maestroLanguages.enum";
import { IPageRequest } from "interfaces/pageRequest.interface";
import ReactDatePicker, { getDefaultLocale } from "react-datepicker";
import { useTranslation } from "react-i18next";
import { camelToSnakeCase } from "utils/stringModification";

import { FormLabel } from "@chakra-ui/react";

import "react-datepicker/dist/react-datepicker.css";

export const DatePickerQuery = <T extends IPageRequest>({
  queryParamsName,
  setParams,
}: {
  queryParamsName: string;
  setParams: Dispatch<SetStateAction<T>>;
}) => {
  const { t } = useTranslation();
  const [datePickerRange, setDatePickerRange] = useState<(Date | null)[]>([
    null,
    null,
  ]);

  useEffect(() => {
    // reset date value when clearing
    if (datePickerRange[0] === null || datePickerRange[1] === null) {
      setParams((previousParams: T) => ({
        ...previousParams,
        [queryParamsName]: undefined,
      }));
      return;
    }
  }, [queryParamsName, datePickerRange, setParams]);

  return (
    <div>
      <FormLabel
        htmlFor={`datePicker-${queryParamsName}`}
        className="text-sm text-navy-700 dark:text-white ml-3 font-bold "
      >
        {t(`common.date_picker.label.${camelToSnakeCase(queryParamsName)}`)}
      </FormLabel>
      <ReactDatePicker
        id={`datePicker-${queryParamsName}`}
        placeholderText={t(`common.date_picker.placeholder`)}
        dateFormat={
          getDefaultLocale() === MaestroLanguagesEnum.ENG
            ? "MM/dd/yyyy"
            : "dd/MM/yyyy"
        }
        selectsRange={true}
        startDate={datePickerRange[0]}
        endDate={datePickerRange[1]}
        onChange={(update) => {
          const formatUpdate = update.map((date) => {
            return date ? format(date, "MM/dd/yyyy") : "";
          });
          let dateRange = "";
          if (!formatUpdate.includes("")) {
            dateRange = formatUpdate[0] + "," + formatUpdate[1];
          }
          if (dateRange !== "") {
            setParams((previousParams) => ({
              ...previousParams,
              [queryParamsName]: dateRange,
            }));
          }
          setDatePickerRange(update);
        }}
        isClearable
        className="h-10 rounded-md border bg-white/0 p-3 text-sm outline-none border-gray-200 hover:border-gray-300 dark:!border-white/10 dark:hover:!border-white text-navy-700 dark:text-white"
      />
    </div>
  );
};

export default DatePickerQuery;
