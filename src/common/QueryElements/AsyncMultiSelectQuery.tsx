import { Dispatch, SetStateAction, useContext, useState } from "react";
import {
  customStyles,
  customStylesDark,
} from "common/TableHelper/reactSelectStyle";
import { ThemeContext } from "contexts/theme.context";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { useTranslation } from "react-i18next";
import { MultiValue } from "react-select";
import AsyncSelect from "react-select/async";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

import { FormLabel } from "@chakra-ui/react";

export function AsyncMultiSelectQuery<
  T extends { id: string | number; name?: string; label?: string },
  U extends IPageRequest,
>({
  queryParamsName,
  multiGet,
  setParams,
}: {
  queryParamsName: string;
  multiGet: ({
    query,
  }: {
    query: string;
  }) => Promise<ApiRequestResponse<IPageResponse<T>>>;
  setParams: Dispatch<SetStateAction<U>>;
}) {
  const { darkMode } = useContext(ThemeContext);

  const { t } = useTranslation();
  const [value, setValue] = useState<
    MultiValue<{
      value: string;
      label: string;
    }>
  >([]);

  const handleSearch = async (inputValue: string) => {
    const result = await multiGet({ query: inputValue });
    const options = result
      ? result.data.map((elt) => ({
          value: elt.id.toString(),
          label: elt.name || elt.label || "",
        }))
      : [];
    return options.filter(
      (option) => !value.map((v) => v.value).includes(option.value),
    );
  };

  const handleChange = (
    event: MultiValue<{
      value: string;
      label: string;
    }>,
  ) => {
    const valueQueryParams = event.reduce(
      (acc, curr) => acc + "," + curr.value,
      "",
    );
    setValue(event);
    setParams((prev) => ({
      ...prev,
      [queryParamsName]:
        valueQueryParams.length > 0 ? valueQueryParams.slice(1) : "",
    }));
  };

  return (
    <>
      <div>
        <FormLabel
          htmlFor={`asyncMultiSelect-${queryParamsName}`}
          className="text-sm text-navy-700 dark:text-white ml-3 font-bold "
        >
          {t(`common.async_multi_select.label.${queryParamsName}`)}
        </FormLabel>
        <AsyncSelect
          styles={darkMode ? customStylesDark : customStyles}
          id={`asyncMultiSelect-${queryParamsName}`}
          isMulti
          onChange={handleChange}
          value={value}
          placeholder={t(
            `common.async_multi_select.placeholder.${queryParamsName}`,
          )}
          loadOptions={handleSearch}
          noOptionsMessage={() => t("common.async_multi_select.no_user")}
          loadingMessage={() => t("common.async_multi_select.load")}
        />
      </div>
    </>
  );
}

export default AsyncMultiSelectQuery;
