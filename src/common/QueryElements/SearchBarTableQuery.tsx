import { Dispatch, SetStateAction, useState } from "react";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { useTranslation } from "react-i18next";

import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function SearchBarTableQuery<T extends IPageRequest>({
  setParams,
}: {
  setParams: Dispatch<SetStateAction<T>>;
}) {
  const { t } = useTranslation();
  const [value, setValue] = useState<string>("");

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setParams((previousParams: T) => ({
      ...previousParams,
      query: event.target.value,
      page: 1,
    }));
  };
  return (
    <div className="flex items-center rounded-full h-10 bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white">
      <p className="pl-3 pr-2 text-xl">
        <FontAwesomeIcon
          className="h-4 w-4 text-gray-400 dark:text-white"
          icon={faMagnifyingGlass}
        />
      </p>
      <input
        type="text"
        value={value}
        placeholder={t("navbar.search")}
        onChange={(e) => {
          setValue(e.target.value);
          handleSearch(e);
        }}
        className="block h-full !w-full rounded-full bg-lightPrimary text-sm font-medium text-navy-700 outline-none placeholder:!text-gray-400 dark:bg-navy-900 dark:text-white dark:placeholder:!text-white sm:w-fit"
      />
    </div>
  );
}
