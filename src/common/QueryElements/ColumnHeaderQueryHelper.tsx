import { Dispatch, SetStateAction } from "react";
import SortIcons from "common/TableHelper/SortIcons";
import { PageOrderEnum } from "enums/pageOrder.enum";
import { TableNamesEnum } from "enums/table.enum";
import { IPageRequestOrderBy } from "interfaces/pageRequest.interface";
import { useTranslation } from "react-i18next";

import { Th } from "@chakra-ui/react";

export function ColumnHeaderQueryHelper<
  T extends { [key: string]: string },
  U extends { [key: string]: string },
  V extends IPageRequestOrderBy,
>({
  tableName,
  headers,
  orderByEnum,
  queryParams,
  setQueryParams,
}: {
  tableName: TableNamesEnum;
  headers: T;
  orderByEnum: U;
  queryParams: V;
  setQueryParams: Dispatch<SetStateAction<V>>;
}) {
  const { t } = useTranslation();

  const handleSort = (field: string) => {
    const getParams = (parameters: keyof V) => queryParams?.[parameters];
    let newOrder = PageOrderEnum.ASC;
    const newOrderBy = orderByEnum[field.toUpperCase()];
    const prevOrder = getParams("order") as PageOrderEnum;
    const prevOrderBy = getParams("orderBy");

    if (
      newOrderBy === prevOrderBy &&
      (prevOrder === undefined ||
        (prevOrder && prevOrder === PageOrderEnum.ASC))
    ) {
      newOrder = PageOrderEnum.DESC;
    }

    setQueryParams((prev) => ({
      ...prev,
      ["order"]: newOrder,
      ["orderBy"]: newOrderBy,
      page: "1",
    }));
  };
  const sortingHeaders = Object.keys(orderByEnum);
  return Object.keys(headers).map((header) => (
    <Th
      key={`Th-${header}`}
      className={`${sortingHeaders.includes(header) ? "cursor-pointer" : "cursor-default"} border-b-[1px] border-gray-200 pt-4 pb-2`}
      onClick={() =>
        sortingHeaders.includes(header) ? handleSort(header) : null
      }
    >
      {header !== "ACTIONS" && (
        <div className="flex items-center justify-between text-xs text-gray-200">
          <p className="text-sm font-bold text-gray-600 dark:text-white">
            {t(`${tableName}.table.${header.toLowerCase()}`)}
          </p>
          {sortingHeaders.includes(header) && (
            <SortIcons
              tableName={tableName}
              ColumnEnum={headers}
              header={header}
              queryParams={queryParams}
            />
          )}
        </div>
      )}
    </Th>
  ));
}
