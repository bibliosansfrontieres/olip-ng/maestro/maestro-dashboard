import { Dispatch, SetStateAction, useContext, useState } from "react";
import {
  customStyles,
  customStylesDark,
} from "common/TableHelper/reactSelectStyle";
import { ThemeContext } from "contexts/theme.context";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { useTranslation } from "react-i18next";
import Select, { MultiValue } from "react-select";

import { FormLabel } from "@chakra-ui/react";

export function ReactMultiSelectQuery<
  T extends { [key: string]: string },
  U extends IPageRequest,
>({
  enumList,
  queryParamsEnumName,
  translateKeysEnumName,
  setParams,
}: {
  enumList: T;
  queryParamsEnumName: string;
  translateKeysEnumName: string;
  setParams: Dispatch<SetStateAction<U>>;
}) {
  const { t } = useTranslation();
  const { darkMode } = useContext(ThemeContext);
  const [value, setValue] = useState<
    MultiValue<{
      value: string;
      label: string;
    }>
  >([]);
  const options = Object.keys(enumList).map((option) => ({
    value: option,
    label: t(`enums.${translateKeysEnumName}.${option.toLowerCase()}`),
  }));

  const handleChange = (
    event: MultiValue<{
      value: string;
      label: string;
    }>,
  ) => {
    const valueQueryParams = event.reduce(
      (acc, curr) => acc + "," + curr.value,
      "",
    );
    setValue(event);
    setParams((prev) => ({
      ...prev,
      [queryParamsEnumName]:
        valueQueryParams.length > 0 ? valueQueryParams.slice(1) : "",
    }));
  };

  return (
    <div className="flex flex-col">
      <FormLabel
        htmlFor={`select-${translateKeysEnumName}`}
        className="text-sm text-navy-700 dark:text-white ml-3 font-bold "
      >
        {t(`common.react_multi_select.label.${translateKeysEnumName}`)}
      </FormLabel>
      <Select
        styles={darkMode ? customStylesDark : customStyles}
        id={`select-${translateKeysEnumName}`}
        onChange={handleChange}
        isMulti
        placeholder={t(
          `common.react_multi_select.placeholder.${translateKeysEnumName}`,
        )}
        value={value}
        options={options}
      />
    </div>
  );
}
