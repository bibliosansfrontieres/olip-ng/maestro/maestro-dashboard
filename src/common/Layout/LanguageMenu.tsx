import { useContext } from "react";
import { LanguageContext } from "contexts/language.context";
import { MaestroLanguagesEnum } from "enums/maestroLanguages.enum";
import { useTranslation } from "react-i18next";

import { Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function LanguageMenu() {
  const { language, setLanguage } = useContext(LanguageContext);
  const { t } = useTranslation();

  return (
    <div
      className="cursor-pointer text-gray-600 dark:text-white font-bold mr-5"
      data-test-id="language"
    >
      <Menu>
        <MenuButton>
          <div className="flex items-center gap-2 text-sm">
            {t(`language.${language.toLowerCase()}`)}
            <FontAwesomeIcon icon={faChevronDown} />
          </div>
        </MenuButton>
        <MenuList
          minW="0"
          w={"90px"}
          className="dark:bg-navy-700 dark:border-0"
        >
          {Object.keys(MaestroLanguagesEnum)
            .filter((lang) => lang !== language)
            .map((lang, index) => (
              <MenuItem
                data-test-id={`language-${index}`}
                onClick={() => setLanguage(lang as MaestroLanguagesEnum)}
                key={lang}
                className="text-sm dark:bg-navy-700 hover:dark:bg-navy-800"
              >
                {t(`navbar.languages.${lang.toLowerCase()}`)}
              </MenuItem>
            ))}
        </MenuList>
      </Menu>
    </div>
  );
}
