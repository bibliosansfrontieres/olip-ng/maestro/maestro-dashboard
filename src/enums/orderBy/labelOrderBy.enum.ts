export enum LabelOrderByEnum {
  ID = "label.id",
  NAME = "label.name",
  CREATED_AT = "label.createdAt",
  UPDATED_AT = "label.updatedAt",
}
