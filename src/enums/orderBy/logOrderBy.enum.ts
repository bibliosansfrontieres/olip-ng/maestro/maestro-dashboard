export enum LogOrderByEnum {
  ID = "log.id",
  USER_ID = "user.id",
  ACTION = "log.action",
  ENTITY = "log.entity",
  ENTITY_ID = "entity.id",
  LEVEL = "log.level",
  CREATED_AT = "log.createdAt",
}
