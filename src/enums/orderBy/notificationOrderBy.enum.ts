export enum NotificationOrderByEnum {
  ID = "notification.id",
  CREATED_AT = "notification.createdAt",
}
