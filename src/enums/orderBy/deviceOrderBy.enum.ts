export enum DeviceOrderByEnum {
  ID = "device.id",
  IS_ALIVE = "device.isAlive",
  LAST_SEEN_AT = "device.lastSeenAt",
  CREATED_AT = "device.createdAt",
  PRIVATE_IPV4 = "device.privateIpv4",
  STATUS = "device.status",
}
