export enum ProjectOrderByEnum {
  ID = "project.id",
  TITLE = "project.title",
  UPDATED_AT = "project.updatedAt",
}
