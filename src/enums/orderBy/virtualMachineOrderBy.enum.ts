export enum VirtualMachineOrderByEnum {
  ID = "virtualMachine.id",
  TITLE = "virtualMachine.title",
  PROJECT_ID = "virtualMachine.project.id",
  STATUS = "virtualMachine.status",
  APPROVAL = "virtualMachine.approval",
  USER = "user.id",
  CREATED_AT = "virtualMachine.createdAt",
  UPDATED_AT = "virtualMachine.updatedAt",
}
