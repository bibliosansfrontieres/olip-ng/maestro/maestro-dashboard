export enum UsersOrderFieldsEnum {
  ID = "ID",
  NAME = "NAME",
  MAIL = "MAIL",
  ROLE = "ROLE",
}
