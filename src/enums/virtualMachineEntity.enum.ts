export enum VirtualMachineErrorEntityEnum {
  PROJECT = "PROJECT",
  ITEM = "ITEM",
  PACKAGE = "PACKAGE",
}
