export enum TableNamesEnum {
  APPLICATION = "application",
  DEPLOY = "deploy",
  DEVICE = "device",
  FQDN = "fqdn",
  LABEL = "label",
  HISTORY = "vm.history_tab",
  NOTIFICATION = "notification",
  PROJECT = "project",
  VIRTUAL_MACHINE = "vm",
  GROUPS = "groups",
  VIRTUAL_MACHINE_SAVE = "vm.save_tab",
  USER = "users",
}
