export enum DeviceTableInDevicePageColumEnum {
  MAC_ADDRESS = "mac_address",
  STATUS = "device.status",
  FQDN_MASK = "fqdnMask.id",
  PRIVATE_IPV4 = "device.privateIpv4",
  IS_ALIVE = "device.isAlive",
  LABELS = "labels",
  DEPLOY = "deploy",
  LAST_SEEN_AT = "device.lastSeenAt",
  CREATED_AT = "device.createdAt",
  ACTIONS = "actions",
}

export enum DeviceTableColumEnum {
  MAC_ADDRESS = "mac_address",
  STATUS = "device.status",
  PRIVATE_IPV4 = "device.privateIpv4",
  IS_ALIVE = "device.isAlive",
  LAST_SEEN_AT = "device.lastSeenAt",
  CREATED_AT = "device.createdAt",
  ACTIONS = "actions",
}
