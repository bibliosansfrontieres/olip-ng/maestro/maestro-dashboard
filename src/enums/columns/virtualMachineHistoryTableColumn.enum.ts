export enum VirtualMachineHistoryTableColumEnum {
  CREATED_AT = "log.createdAt",
  USER_ID = "user.id",
  ACTION = "log.action",
  ENTITY = "log.entity",
  VALUES = "values",
  LEVEL = "log.level",
  DESCRIPTION = "description",
}
