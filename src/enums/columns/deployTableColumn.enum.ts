export enum DeployTableColumVmTabEnum {
  ID = "deploy.id",
  DEVICE = "device.id",
  FQDN_MASK = "fqdnMask.id",
  STATUS = "deploy.status",
  USER = "user.id",
  LABELS = "labels",
  CREATED_AT = "deploy.createdAt",
  ACTIONS = "actions",
}

export enum DeployTableColumEnum {
  ID = "deploy.id",
  VM = "vm",
  DEVICE = "device.id",
  FQDN_MASK = "fqdnMask.id",
  STATUS = "deploy.status",
  USER = "user.id",
  LABELS = "labels",
  CREATED_AT = "deploy.createdAt",
  ACTIONS = "actions",
}

export enum DeployTableColumDevicePageEnum {
  ID = "deploy.id",
  VM = "vm",
  STATUS = "deploy.status",
  USER = "user.id",
  LABELS = "labels",
  CREATED_AT = "deploy.createdAt",
  ACTIONS = "actions",
}

export enum DeployTableInLabelColumEnum {
  ID = "deploy.id",
  STATUS = "deploy.status",
  CREATED_AT = "deploy.createdAt",
}
