export enum UserTableColumnEnum {
  NAME = "name",
  MAIL = "mail",
  AVATAR = "avatar",
  ROLE = "role",
  ACTIONS = "actions",
}

export enum UserTableVmTabsColumnEnum {
  NAME = "name",
  MAIL = "mail",
  AVATAR = "avatar",
}

export enum EntraUserTableColumnEnum {
  NAME = "name",
  MAIL = "mail",
  ACTIONS = "actions",
}
