export enum ProjectTableColumnEnum {
  ID = "project.id",
  TITLE = "project.title",
  DESCRIPTION = "project.description",
  UPDATED_AT = "project.updatedAt",
  STATUS = "project.status",
  PLAYLISTS = "project.playlists",
  ITEMS = "project.items",
  VIRTUAL_MACHINES = "project.virtualMachines",
  DEVICES = "project.devices",
  ACTIONS = "actions",
}
