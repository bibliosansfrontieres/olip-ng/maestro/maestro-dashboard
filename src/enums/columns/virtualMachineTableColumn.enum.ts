export enum VirtualMachineTableColumEnum {
  ID = "virtualMachine.id",
  TITLE = "virtualMachine.title",
  PROJECT_ID = "virtualMachine.project.id",
  STATUS = "virtualMachine.status",
  APPROVAL = "virtualMachine.approval",
  USER = "user.id",
  GROUPS = "groups",
  CREATED_AT = "virtualMachine.createdAt",
  UPDATED_AT = "virtualMachine.updatedAt",
  ACTIONS = "actions",
}

export enum VirtualMachineTableProjectModalColumnEnum {
  ID = "virtualMachine.id",
  TITLE = "virtualMachine.title",
  STATUS = "virtualMachine.status",
  APPROVAL = "virtualMachine.approval",
  USER = "user.id",
  UPDATED_AT = "virtualMachine.updatedAt",
  ACTIONS = "actions",
}
