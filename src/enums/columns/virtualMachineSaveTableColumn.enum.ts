export enum VirtualMachineSaveTableColumEnum {
  NAME = "virtualMachineSave.name",
  DESCRIPTION = "virtualMachineSave.description",
  CREATED_AT = "virtualMachineSave.createdAt",
  USER_ID = "user.id",
  TYPE = "type",
  ACTIONS = "actions",
}
