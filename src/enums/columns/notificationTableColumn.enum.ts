export enum NotificationTableColumnEnum {
  MESSAGE = "message",
  VALUES = "values",
  IS_SEEN = "is_seen",
  CREATED_AT = "notification.createdAt",
  UPDATED_AT = "updatedAt",
}
