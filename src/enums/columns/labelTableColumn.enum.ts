export enum LabelTableColumEnumWithoutSort {
  NAME = "label.name",
  CREATED_AT = "label.createdAt",
  UPDATED_AT = "label.updatedAt",
}

export enum LabelTableColumEnum {
  NAME = "label.name",
  DEPLOY_TOTAL = "deployTotal",
  CREATED_AT = "label.createdAt",
  UPDATED_AT = "label.updatedAt",
  ACTIONS = "actions",
}
