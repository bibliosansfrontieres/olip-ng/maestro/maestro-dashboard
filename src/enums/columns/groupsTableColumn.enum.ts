export enum GroupsTableColumnEnum {
  NAME = "group.name",
  DESCRIPTION = "description",
  ACTIONS = "actions",
}
