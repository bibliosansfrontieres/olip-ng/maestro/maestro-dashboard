export enum FqdnMaskTableColumEnum {
  LABEL = "fqdnMask.label",
  MASK = "mask",
  DEVICES = "devices",
  ACTIONS = "actions",
}
