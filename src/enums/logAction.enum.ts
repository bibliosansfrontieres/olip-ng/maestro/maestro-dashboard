export enum LogActionEnum {
  CREATE = "CREATE",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
  IMPORT = "IMPORT",
  LOGIN = "LOGIN",
}
