import { io } from "socket.io-client";
import { v4 as uuidv4 } from "uuid";

import getEnv from "./utils/getEnv";

if (localStorage.getItem("websocket-clientId") === null) {
  localStorage.setItem("websocket-clientId", uuidv4());
}

export const socket = io(getEnv("PUBLIC_API_URL"), {
  auth: {
    clientId: localStorage.getItem("websocket-clientId"),
    token: localStorage.getItem("accessToken"),
  },
});
