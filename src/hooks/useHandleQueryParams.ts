import { useMemo } from "react";
import { useSearchParams } from "react-router-dom";

export type SetParam = { name: string; value: string; defaultValue?: string };

const useHandleQueryParams = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const queryParams = useMemo(
    () => Object.fromEntries(searchParams.entries()),
    [searchParams],
  );

  const updateQueryParam = (
    changeParams: SetParam | SetParam[],
    replace = false,
  ) => {
    const newParams = Array.isArray(changeParams)
      ? changeParams
      : [changeParams];

    if (replace) {
      const actualParams = new URLSearchParams();
      for (const { name, value, defaultValue } of newParams) {
        if (value !== defaultValue) {
          actualParams.set(name, value);
        }
      }
      setSearchParams(actualParams);
      return actualParams;
    }

    setSearchParams((actualParams) => {
      newParams.map(({ name, value, defaultValue }) => {
        if (value === defaultValue) {
          actualParams.delete(name);
        } else {
          actualParams.set(name, value);
        }
      });
      return actualParams;
    });
  };

  return { queryParams, updateQueryParam };
};

export default useHandleQueryParams;
