import { useContext } from "react";
import { UserContext } from "contexts/user.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import {
  hasPermission,
  hasPermissions,
  userCanPatchVm,
} from "utils/hasPermission";

export const usePermissions = (
  permissions: PermissionNameEnum | PermissionNameEnum[],
) => {
  const { user } = useContext(UserContext);
  if (!user) return false;
  if (Array.isArray(permissions)) {
    return hasPermissions(user, permissions);
  }
  return hasPermission(user, permissions);
};

export const useCanPatchVm = (vm: IVirtualMachine, onlyCreator = false) => {
  const { user } = useContext(UserContext);
  if (!user) return false;
  return userCanPatchVm(user, vm, onlyCreator);
};
