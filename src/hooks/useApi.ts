import {
  Dispatch,
  SetStateAction,
  useContext,
  useEffect,
  useState,
} from "react";
import { UserContext } from "contexts/user.context";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

/**
 * Hook to handle API requests and provide a simple way to manage loading state and errors.
 * @template T The type of data returned by the API.
 * @param {function(): Promise<ApiRequestResponse<T>>} callApi A function that returns a Promise of type ApiRequestResponse<T>, where T is the type of data returned by the API.
 * @returns {{isLoading: boolean, error: boolean, result: T | undefined, setReload: Dispatch<SetStateAction<boolean>>}} An object with four properties: isLoading, error, result, and setReload.
 *   - isLoading: true if the API request is currently in progress.
 *   - error: true if an error occurred during the API request.
 *   - result: The data returned by the API, of type T.
 *   - setReload: A function that triggers a reload of the data.
 */
const useApi = <T>(
  callApi: () => Promise<ApiRequestResponse<T>>,
): {
  isLoading: boolean;
  error: boolean;
  result: T | undefined;
  setReload: Dispatch<SetStateAction<boolean>>;
} => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [result, setResult] = useState<T>();
  const { checkToken } = useContext(UserContext);
  const [reload, setReload] = useState(false);

  useEffect(() => {
    /**
     * Initiates an API request using the provided callApi function and handles the response.
     *
     * @async
     * @returns {Promise<void>}
     */
    const launchCallApi = async (): Promise<void> => {
      setIsLoading(true);
      await checkToken();
      const resultCall = await callApi();
      if (resultCall) {
        setResult(resultCall);
        setError(false);
        setIsLoading(false);
      } else {
        setIsLoading(false);
        setError(true);
      }
    };
    launchCallApi();
    if (reload) setReload(false);
  }, [callApi, checkToken, reload]);

  return { isLoading, result, error, setReload };
};

export default useApi;
