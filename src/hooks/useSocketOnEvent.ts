import { useEffect } from "react";
import { socket } from "src/socket.io";

export type UseSocketOnEventReturns<T> = {
  result: T | null;
};
const useSocketOnEvent = <T>(
  eventName: string,
  eventFunction: (result: T) => void,
): void => {
  useEffect(() => {
    socket.on(eventName, eventFunction);

    return () => {
      socket.off(eventName, eventFunction);
    };
  }, [eventFunction, eventName]);

  return;
};

export default useSocketOnEvent;
