import { routesList } from "constants/routesList";
import { t } from "i18next";
import { useLocation } from "react-router-dom";

export default function useRouteTranslation() {
  const location = useLocation();
  if (!location.pathname.split("/")[2]) {
    const routeName = routesList.find(
      (route) => route.link === location.pathname,
    )?.name;
    return t(`${routeName}`);
  }

  const id = location.pathname.split("/")[2];
  if (location.pathname.split("/")[1] === "virtual-machines") {
    return t("sidebar.pages.vm_details", { id: id });
  } else if (location.pathname.split("/")[1] === "devices") {
    if (location.pathname.split("/")[3] === "exec") {
      return t("sidebar.pages.devices_exec", { id: id });
    }
    return t("sidebar.pages.devices_details");
  } else if (location.pathname.split("/")[1] === "fqdns") {
    return t("sidebar.pages.fqdns_details", { id: id });
  } else if (location.pathname.split("/")[1] === "deploys") {
    return t("sidebar.pages.deploys_details", { id: id });
  } else if (location.pathname.split("/")[1] === "projects") {
    return t("sidebar.pages.projects_details", { id: id });
  }
}
