import { useContext } from "react";
import Modal from "components/modal/Modal";
import { LabelsContext } from "contexts/labels.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { ILabelDeploys } from "interfaces/label.interface";
import { useTranslation } from "react-i18next";
import { deleteLabel } from "services/labels/labels.service";

import { Tooltip, useDisclosure } from "@chakra-ui/react";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function DeleteLabelModal({ label }: { label: ILabelDeploys }) {
  const havePermission = usePermissions(PermissionNameEnum.DELETE_LABEL);

  const { setReload } = useContext(LabelsContext);
  const { t } = useTranslation();
  const {
    isOpen: isOpenDelete,
    onOpen: onOpenDelete,
    onClose: onCloseDelete,
  } = useDisclosure();

  const onDeleteLabel = async (id: string) => {
    await deleteLabel({ id });
    setReload(true);
    onCloseDelete();
  };

  return (
    <>
      {havePermission && (
        <>
          <Tooltip
            className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
            label={t("labels.table.actions.delete")}
            placement="top"
            openDelay={500}
            aria-label={t("labels.table.actions.delete")}
          >
            <button
              onClick={onOpenDelete}
              disabled={!havePermission}
              className="h-8 w-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
            >
              <FontAwesomeIcon className="h-3 w-3" icon={faTrash} />
            </button>
          </Tooltip>
          <Modal disclosure={{ isOpen: isOpenDelete, onClose: onCloseDelete }}>
            <>
              <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
                {t("labels.table.actions.delete")}
              </h1>
              <p className=" text-md dark:text-white text-navy-700">
                {t("labels.table.actions.delete_text", { name: label.name })}
              </p>
              <div className="flex flex-row-reverse gap-2 mt-2">
                <button
                  onClick={() => onDeleteLabel(label.id.toString())}
                  className="rounded-xl border-2 border-red-500 hover:border-red-600 hover:text-red-600 text-red-500 dark:border-red-400 dark:hover:border-red-300 dark:text-red-400 dark:hover:text-red-300 px-5 py-3 font-medium transition duration-200"
                >
                  {t("button.delete")}
                </button>
              </div>
            </>
          </Modal>
        </>
      )}
    </>
  );
}
