import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { FormikHelpers, useFormik } from "formik";
import { ILabelDeploys } from "interfaces/label.interface";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { patchLabel } from "services/labels/labels.service";
import { LabelsContext } from "src/contexts/labels.context";
import { nbTotalDeploy } from "utils/labelFunctionHelper";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

interface IProps {
  name: string;
}
export default function LabelEditionForm({
  label,
  onClose,
}: {
  label: ILabelDeploys;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const { setReload: refreshLabels } = useContext(LabelsContext);
  const validationSchema = Yup.object({
    name: Yup.string()
      .min(3, t("labels.edition.validation.name.min"))
      .required(t("labels.edition.validation.name.required")),
  });
  const formik = useFormik({
    initialValues: {
      name: label.name,
    } as IProps,
    validationSchema: validationSchema,
    onSubmit: async (
      values: IProps,
      { setSubmitting }: FormikHelpers<IProps>,
    ) => {
      const initialValues = {
        name: label.name,
      };

      if (values.name === initialValues.name) {
        setSubmitting(false);
        toast.error(t("groups.patch_group.no_change"));

        return;
      }
      const result = await patchLabel({ ...values, id: label.id });
      setSubmitting(false);
      if (result) {
        refreshLabels(true);
        onClose();
      }
    },
  });

  return (
    <>
      <h1 className="mb-2 text-2xl font-bold dark:text-white text-navy-700">
        {t("labels.table.actions.edit")}
      </h1>
      {nbTotalDeploy(label) > 0 && (
        <h2 className="text-xl mb-2 font-bold dark:text-red-400 text-red-500">
          {t("labels.table.actions.warning")}
        </h2>
      )}
      <form onSubmit={formik.handleSubmit}>
        <FormControl isInvalid={!!formik.errors.name}>
          <InputField
            id="name"
            label={t("groups.creation.label.name")}
            extra="mt-3"
            type="text"
            placeholder={t("groups.creation.placeholder.name")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="name"
            value={formik.values.name}
            state={formik.errors.name ? "error" : undefined}
            required={true}
            autofocus={true}
          />
          {formik.errors.name && (
            <FormErrorMessageText text={formik.errors.name} />
          )}
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5 "
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
