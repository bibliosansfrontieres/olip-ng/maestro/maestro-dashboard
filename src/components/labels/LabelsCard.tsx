import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import LabelTable from "components/labels/LabelTable";
import { LabelsContext } from "contexts/labels.context";

export default function LabelsCard() {
  const { labels, isLoading, error } = useContext(LabelsContext);
  return (
    <>
      {labels && !isLoading && !error && (
        <>
          <LabelTable labels={labels.data} inLabelPage={true} />
          <Pagination meta={labels.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !labels && <ErrorList translation="label.get_labels.error" />}
    </>
  );
}
