import DeployTable from "components/deploys/deployTable/DeployTable";
import Modal from "components/modal/Modal";
import { ILabelDeploys } from "interfaces/label.interface";
import { useTranslation } from "react-i18next";
import {
  nbActiveDeploy,
  nbArchivedDeploy,
} from "src/utils/labelFunctionHelper";

import { Tooltip, useDisclosure } from "@chakra-ui/react";
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function DeployTableModal({ label }: { label: ILabelDeploys }) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <div className="flex gap-2 items-center">
        <Tooltip
          label={
            <>
              <p>
                {t("label.table.deploy_total_tootlip.active", {
                  active: nbActiveDeploy(label as ILabelDeploys),
                })}
              </p>
              <p>
                {t("label.table.deploy_total_tootlip.archived", {
                  archive: nbArchivedDeploy(label as ILabelDeploys),
                })}
              </p>
            </>
          }
          placement="top"
          openDelay={500}
          aria-label="title"
        >
          <FontAwesomeIcon
            onClick={onOpen}
            className="hover:cursor-pointer text-brand-500 hover:enabled:text-brand-600 active:text-brand-700 dark:text-brand-400 dark:hover:enabled:text-brand-300 dark:active:text-brand-200 h-5 w-5 "
            icon={faCircleInfo}
          />
        </Tooltip>
      </div>
      <Modal
        disclosure={{
          isOpen: isOpen,
          onClose: onClose,
        }}
      >
        <>
          <div className="flex place-content-between items-center p-3">
            <h1 className="text-2xl font-bold dark:text-white text-navy-700">
              {t("label.deploys_modal.title")}
            </h1>
          </div>
          <DeployTable
            data={(label as ILabelDeploys).deploys}
            inLabelTabs={true}
          />
        </>
      </Modal>
    </>
  );
}
