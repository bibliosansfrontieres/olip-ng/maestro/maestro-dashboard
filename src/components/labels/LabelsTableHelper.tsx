import DatePicker from "common/TableHelper/DatePicker";

export default function LabelsTableHelper() {
  return (
    <div className="mt-2 flex flex-wrap gap-4">
      <DatePicker queryParamsName="createdDateRange" />
      <DatePicker queryParamsName="updatedDateRange" />
    </div>
  );
}
