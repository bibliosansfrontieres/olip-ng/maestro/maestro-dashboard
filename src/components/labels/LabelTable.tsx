import {
  ColumnHeaderHelper,
  ColumnHeaderHelperWithoutSort,
} from "common/TableHelper/ColumnHeaderHelper";
import DeleteLabelModal from "components/labels/DeleteLabelModal";
import DeployTableModal from "components/labels/DeployTableModal";
import EditLabelModal from "components/labels/EditLabelModal";
import {
  LabelTableColumEnum,
  LabelTableColumEnumWithoutSort,
} from "enums/columns/labelTableColumn.enum";
import { LabelOrderByEnum } from "enums/orderBy/labelOrderBy.enum";
import { TableNamesEnum } from "enums/table.enum";
import { ILabel, ILabelDeploys } from "interfaces/label.interface";
import filterDate from "utils/filterDate";
import { nbTotalDeploy } from "utils/labelFunctionHelper";

import { Table, TableContainer, Tbody, Td, Thead, Tr } from "@chakra-ui/react";

export default function LabelTable({
  labels,
  inLabelPage = false,
}: {
  labels: ILabelDeploys[] | ILabel[];
  inLabelPage?: boolean;
}) {
  return (
    <TableContainer pt={5}>
      <Table variant="unstyled">
        <Thead>
          <Tr>
            {inLabelPage ? (
              <ColumnHeaderHelper<
                typeof LabelTableColumEnum,
                typeof LabelOrderByEnum
              >
                tableName={TableNamesEnum.LABEL}
                headers={LabelTableColumEnum}
                orderByEnum={LabelOrderByEnum}
              />
            ) : (
              <ColumnHeaderHelperWithoutSort<
                typeof LabelTableColumEnumWithoutSort
              >
                tableName={TableNamesEnum.LABEL}
                headers={LabelTableColumEnumWithoutSort}
              />
            )}
          </Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {labels.map((label) => {
            return (
              <Tr key={`Tr-${label.id}`} className="h-16">
                <Td>
                  <p>{label.name}</p>
                </Td>
                {inLabelPage && (
                  <>
                    <Td>
                      <div className="flex items-center gap-2">
                        <p>{nbTotalDeploy(label as ILabelDeploys)}</p>
                        <DeployTableModal label={label as ILabelDeploys} />
                      </div>
                    </Td>
                  </>
                )}
                <Td>
                  <p>{filterDate(label.createdAt)}</p>
                </Td>
                <Td>
                  <p>{filterDate(label.updatedAt)}</p>
                </Td>
                {inLabelPage && (
                  <>
                    <Td>
                      <div className="flex gap-2">
                        <EditLabelModal label={label as ILabelDeploys} />
                        {nbTotalDeploy(label as ILabelDeploys) === 0 && (
                          <DeleteLabelModal label={label as ILabelDeploys} />
                        )}
                      </div>
                    </Td>
                  </>
                )}
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
