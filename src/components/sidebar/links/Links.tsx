/* eslint-disable */
import { Link as ReactRouterLink, useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse } from "@fortawesome/free-solid-svg-icons";

import { menuAdminListPages, menuListPages } from "constants/menuListPages";
import { Page } from "src/types";
import { useContext } from "react";
import { UserContext } from "contexts/user.context";
import { PermissionNameEnum } from "enums/permissionName.enum";

const Link = ({ route, index }: { route: Page; index: number }) => {
  const location = useLocation();
  const { t } = useTranslation();
  const activeRoute = (routeName: string) => {
    return routeName === "/"
      ? location.pathname === "/"
      : location.pathname === routeName;
  };
  return (
    <ReactRouterLink key={index} to={route.link}>
      <div className="relative mb-3 flex">
        <li className="my-[3px] flex items-center px-8" key={index}>
          <span
            className={`h-6 w-6 flex justify-center items-center ${
              activeRoute(route.link) === true
                ? "font-bold text-brand-500 dark:text-white"
                : "font-medium text-gray-600"
            }`}
          >
            {route.icon ? route.icon : <FontAwesomeIcon icon={faHouse} />}{" "}
          </span>
          <p
            className={`ml-4 ${
              activeRoute(route.link) === true
                ? "font-bold text-navy-700 dark:text-white"
                : "font-medium text-gray-600"
            }`}
          >
            {t(`${route.name}`)}
          </p>
        </li>
        {activeRoute(route.link) ? (
          <div className="absolute right-0 top-px h-9 w-1 rounded-lg bg-brand-500 dark:bg-brand-400" />
        ) : null}
      </div>
    </ReactRouterLink>
  );
};
export default function Links(): JSX.Element {
  const { user } = useContext(UserContext);
  const { t } = useTranslation();
  const filteredRoute = (permission?: PermissionNameEnum) => {
    if (!permission) return true;
    if (
      user?.role.permissions
        .map((permission) => permission.name)
        .includes(permission)
    )
      return true;
    return false;
  };
  return (
    <>
      {menuListPages
        .filter((route) => filteredRoute(route.permission))
        .map((route, index) => (
          <Link key={index} route={route} index={index} />
        ))}
      {user?.role.permissions
        .map((permission) => permission.name)
        .includes(PermissionNameEnum.ADMIN_MENU) && (
        <>
          <div className="m-5 mt-7 h-px bg-gray-300 dark:bg-white/30" />
          <div className="ml-4 mb-3 font-poppins text-sm font-bold uppercase text-gray-600 dark:text-white">
            {t("sidebar.admin")}
          </div>
          {menuAdminListPages
            .filter((route) => filteredRoute(route.permission))
            .map((route, index) => (
              <Link key={index} route={route} index={index} />
            ))}
        </>
      )}
    </>
  );
}
