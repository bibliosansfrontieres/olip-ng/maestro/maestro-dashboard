import Links from "components/sidebar/links/Links";

import { faX } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Sidebar({
  open,
  onClose,
}: {
  open: boolean;
  onClose: React.MouseEventHandler<HTMLSpanElement>;
}): JSX.Element {
  return (
    <div
      data-test-id="sidebar"
      className={`sm:none duration-175 linear fixed !z-50 max-h-screen overflow-y-scroll flex w-[240px] min-h-full flex-col bg-white pb-10 shadow-2xl shadow-white/5 transition-all dark:!bg-navy-800 dark:text-white md:!z-50 lg:!z-50 xl:!z-0 ${
        open ? "translate-x-0" : "-translate-x-96"
      }`}
    >
      <span
        className="absolute top-4 right-4 block cursor-pointer xl:hidden"
        onClick={onClose}
        data-test-id="sidebarCloseButton"
      >
        <FontAwesomeIcon icon={faX} />
      </span>

      <div className={`mt-[50px] mb-16 flex justify-center`}>
        <div className="mt-1 h-2.5 font-poppins text-[26px] font-bold uppercase text-navy-700 dark:text-white">
          MAESTRO
        </div>
      </div>
      <div className="my-5 h-px min-h-px bg-gray-300 dark:bg-white/30" />
      {/* Nav item */}

      <ul className="mb-auto pt-1">
        <Links />
      </ul>

      {/* Nav item end */}
    </div>
  );
}
