import { useContext } from "react";
import LanguageMenu from "common/Layout/LanguageMenu";
import { ThemeContext } from "contexts/theme.context";

import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function FixedPlugin() {
  const { darkMode, setDarkMode } = useContext(ThemeContext);

  return (
    <div className="fixed bottom-[60px] left-[35px] !z-[99] flex items-center gap-4">
      <button
        className="border-px flex h-[60px] w-[60px] items-center justify-center rounded-full border-[#6a53ff] bg-gradient-to-br from-brandLinear to-blueSecondary p-0"
        onClick={() => {
          if (darkMode) {
            setDarkMode(false);
          } else {
            setDarkMode(true);
          }
        }}
      >
        <div className="cursor-pointer text-gray-600">
          {darkMode ? (
            <FontAwesomeIcon
              className="h-4 w-4 text-gray-600 dark:text-white"
              icon={faSun}
            />
          ) : (
            <FontAwesomeIcon
              className="h-4 w-4 text-gray-600 dark:text-white"
              icon={faMoon}
            />
          )}
        </div>
      </button>
      <LanguageMenu />
    </div>
  );
}
