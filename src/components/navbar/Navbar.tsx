import { useCallback, useContext, useRef } from "react";
import GoBackButton from "common/GoBackButton";
import LanguageMenu from "common/Layout/LanguageMenu";
import Dropdown from "components/dropdown/Dropdown";
import { ThemeContext } from "contexts/theme.context";
import { UserContext } from "contexts/user.context";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import useRouteTranslation from "hooks/useRouteTranslation";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import {
  getNotifications,
  patchAllNotifications,
} from "services/notifications/notifications.service";
import clearLocalStorage from "utils/clearLocalStorage";

import {
  Box,
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Tooltip,
  useDisclosure,
  useOutsideClick,
} from "@chakra-ui/react";
import {
  faBars,
  faBell,
  faMoon,
  faSun,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Navbar({
  onOpenSidenav,
  numberNotificationsUnread,
}: {
  onOpenSidenav: () => void;
  numberNotificationsUnread: number;
}) {
  const routeName = useRouteTranslation();
  const navigate = useNavigate();
  const logOut = () => {
    clearLocalStorage();
    navigate("/login");
  };
  const { user } = useContext(UserContext);
  const { onOpen, isOpen, onClose } = useDisclosure();
  // to close the popover even when its not focused
  const popoverRef = useRef(null);
  useOutsideClick({
    ref: popoverRef,
    handler: onClose,
  });

  const { t } = useTranslation();
  const { darkMode, setDarkMode } = useContext(ThemeContext);

  const { result, setReload: refresh } = useApi(
    //@TODO: verify the numberNotificationsUnread is useful or not as a dependency (was here in useApi)
    useCallback(async () => {
      if (isOpen) {
        return getNotifications({ order: PageOrderEnum.DESC });
      }
    }, [isOpen]),
  );

  const handleJsonValue = (valueJSON: string): JSX.Element => {
    const newTab = Object.entries(JSON.parse(valueJSON)) as [string, string][];
    return (
      <>
        {newTab.map(
          (item, index) =>
            !item[1].startsWith("http") && (
              <div className="flex gap-2 w-full" key={index}>
                <p className="font-bold">{`${item[0]} : `}</p>
                <p>{item[1]}</p>
              </div>
            ),
        )}
      </>
    );
  };

  return (
    <>
      <nav
        data-test-id="navbar"
        className="z-20 p-3 sticky top-4 flex flex-row flex-wrap items-center justify-between rounded-xl bg-white/10 backdrop-blur-xl dark:bg-[#0b14374d]"
      >
        <div className="ml-[6px]">
          <GoBackButton />
          <p className="shrink text-[33px] text-navy-700 dark:text-white">
            <a
              href="#top"
              className="font-bold hover:text-navy-700 dark:hover:text-white"
              data-test-id="routeName"
            >
              {t(`${routeName}`)}
            </a>
          </p>
        </div>

        <div
          data-test-id="badgeMenu"
          className="relative mt-[3px] flex h-[61px] max-w-[450px] min-w-40 items-center justify-around rounded-full bg-white pl-5 pr-2 py-2 shadow-xl shadow-shadow-500 dark:!bg-navy-800 dark:shadow-none"
        >
          <div className="flex gap-2 items-center text-xl">
            <span
              data-test-id="sidebarOpenButton"
              className="cursor-pointer text-gray-600 dark:text-white xl:hidden"
              onClick={onOpenSidenav}
            >
              <FontAwesomeIcon icon={faBars} className="h-5 w-5" />
            </span>
            <Box ref={popoverRef}>
              <Popover
                onOpen={() => {
                  refresh(true);
                  onOpen();
                }}
                isOpen={isOpen}
                onClose={onClose}
              >
                <PopoverTrigger>
                  <button data-test-id="notificationsButton">
                    <span className="fa-layers">
                      <FontAwesomeIcon
                        className="h-4 w-4 text-gray-600 dark:text-white"
                        icon={faBell}
                      />
                      {numberNotificationsUnread > 0 && (
                        <span className="fa-layers-counter text-4xl">
                          {numberNotificationsUnread}
                        </span>
                      )}
                    </span>
                  </button>
                </PopoverTrigger>
                <PopoverContent className="dark:!bg-navy-800">
                  <PopoverHeader>
                    <div className="flex items-center justify-between">
                      <p className="text-base font-bold text-navy-700 dark:text-white">
                        {t("notification.title")}
                      </p>
                      <button
                        data-test-id="readAllNotificationsButton"
                        className="text-sm font-bold text-navy-700 dark:text-white underline"
                        onClick={() => {
                          patchAllNotifications();
                          onClose();
                        }}
                      >
                        {t("notification.table.buttons.all_read")}
                      </button>
                    </div>
                  </PopoverHeader>
                  <PopoverBody>
                    {result &&
                      result.data.length > 0 &&
                      result.data.map((notification) => (
                        <div
                          className="flex w-full items-center"
                          key={notification.id}
                        >
                          <div className="ml-2 flex h-full w-full flex-col justify-center rounded-lg px-1 text-sm">
                            <Tooltip
                              className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
                              label={handleJsonValue(notification.values)}
                              placement="top"
                              openDelay={500}
                              aria-label="title"
                            >
                              <p
                                className={`mb-1 text-left text-base ${notification.isSeen ? "" : "font-bold"} text-gray-900 dark:text-white`}
                              >
                                {notification.message}
                              </p>
                            </Tooltip>
                          </div>
                        </div>
                      ))}
                    <button
                      data-test-id="seeAllNotificationsButton"
                      className="text-sm font-bold text-navy-700 dark:text-white underline"
                      onClick={() => {
                        navigate("/notifications");
                        onClose();
                      }}
                    >
                      {t("notification.see_more")}
                    </button>
                  </PopoverBody>
                </PopoverContent>
              </Popover>
            </Box>
            <div
              data-test-id="darkMode"
              className="cursor-pointer mb-1 text-gray-600"
              onClick={() => {
                if (darkMode) {
                  setDarkMode(false);
                } else {
                  setDarkMode(true);
                }
              }}
            >
              {darkMode ? (
                <FontAwesomeIcon
                  className="h-4 w-4 text-gray-600 dark:text-white"
                  icon={faSun}
                />
              ) : (
                <FontAwesomeIcon
                  className="h-4 w-4 text-gray-600 dark:text-white"
                  icon={faMoon}
                />
              )}
            </div>
            <LanguageMenu />
          </div>
          <Dropdown
            button={
              <img
                data-test-id="userDropdownButton"
                className="h-10 w-10 min-h-10 min-w-10 rounded-full"
                src={user?.avatar ?? "https://ui-avatars.com/api"}
                alt={`${user?.name ?? "avatar"}`}
              />
            }
            classNames={"py-2 top-8 -left-[180px] w-max"}
            children={
              <div className="flex w-56 flex-col justify-start rounded-[20px] bg-white bg-cover bg-no-repeat shadow-xl shadow-shadow-500 dark:!bg-navy-700 dark:text-white dark:shadow-none">
                <div className="mt-3 ml-4">
                  <div className="flex items-center gap-2">
                    <p
                      data-test-id="dropdownUserName"
                      className="text-sm font-bold text-navy-700 dark:text-white"
                    >
                      {t("navbar.hello", { name: user?.name ?? "" })}
                    </p>
                  </div>
                </div>
                <div className="mt-3 h-px w-full bg-gray-200 dark:bg-white/20 " />
                <button
                  onClick={logOut}
                  data-test-id="logoutButton"
                  className="my-4 ml-2 text-left text-sm font-medium text-red-500 hover:text-red-500"
                >
                  {t("navbar.log_out")}
                </button>
              </div>
            }
          />
        </div>
      </nav>
    </>
  );
}
