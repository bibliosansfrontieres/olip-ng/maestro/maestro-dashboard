import {
  Modal as ChakraModal,
  ModalContent,
  ModalOverlay,
} from "@chakra-ui/modal";
import { faX } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Modal({
  disclosure: { isOpen, onClose },
  children,
}: {
  disclosure: { isOpen: boolean; onClose: () => void };
  children: JSX.Element;
}) {
  return (
    <>
      <ChakraModal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent
          data-test-id="modalContent"
          className="!w-max !max-w-[85%] !max-h-[80vh] min-w-[40vw] rounded-[10px] dark:!bg-navy-800 px-[30px] pt-[35px] pb-[40px] flex flex-col bg-white overflow-x-scrolltext-navy-700 dark:text-white"
        >
          <>
            <button
              onClick={onClose}
              className="absolute right-2 top-2 w-8 h-8 rounded-full hover:border-2 hover:text-brand-600 text-brand-500 dark:text-brand-400 dark:hover:text-brand-300"
            >
              <FontAwesomeIcon icon={faX} />
            </button>
            <div className="overflow-y-scroll">{children}</div>
          </>
        </ModalContent>
      </ChakraModal>
    </>
  );
}
