import { Trans, useTranslation } from "react-i18next";

export const ConfirmationModal = ({
  onClose,
  validateFunction,
  translationKey,
}: {
  onClose: () => void;
  validateFunction: () => void;
  translationKey: string;
}) => {
  const { t } = useTranslation();
  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t(`${translationKey}.title`)}
      </h1>
      <p className="mt-3 text-red-500 dark:text-red-400 text-lg font-bold w-[40vw]">
        {
          <Trans
            i18nKey={`${translationKey}.warning_text`}
            components={{ br: <br /> }}
          />
        }
      </p>
      <div className="flex justify-end pt-8 gap-7">
        <button
          className="btn-primary-base bg-red-600 hover:bg-red-700 dark:bg-red-500 dark:hover:bg-red-400 disabled:hover:bg-red-600 disabled:hover:dark:bg-red-500"
          onClick={() => {
            onClose();
            validateFunction();
          }}
        >
          {t(`${translationKey}.validate`)}
        </button>
        <button
          className="border-[1px] border-red-500 hover:border-red-600 dark:border-red-400 dark:hover:text-red-300 text-red-500 hover:text-red-600 dark:text-red-400 px-5 py-3 text-base font-medium rounded-xl transition duration-200"
          onClick={onClose}
        >
          {t("button.cancel")}
        </button>
      </div>
    </>
  );
};
