import DeviceTable from "components/devices/deviceTable/DeviceTable";
import Modal from "components/modal/Modal";
import { IDevice } from "interfaces/device.interface";
import { IVirtualMachineWithVmSaveAndDeploy } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";

import { Tooltip, useDisclosure } from "@chakra-ui/react";

export default function DeviceModal({
  virtualMachines,
  inProjectPage = false,
}: {
  virtualMachines: IVirtualMachineWithVmSaveAndDeploy[];
  inProjectPage?: boolean;
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();

  const devices = [] as unknown as IDevice[];

  for (const vm of virtualMachines) {
    if (vm.virtualMachineSaves) {
      for (const vmSave of vm.virtualMachineSaves) {
        for (const deploy of vmSave.deploys) {
          devices.push(deploy.device);
        }
      }
    }
  }
  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
        label={t("project.device_modal.open")}
        placement="top"
        openDelay={500}
        aria-label={t("project.device_modal.open")}
      >
        {inProjectPage ? (
          <button
            data-test-id="projectPageDeviceModalButton"
            className="btn-primary-base truncate
                 bg-brand-500 hover:bg-brand-600 dark:bg-brand-400 dark:hover:bg-brand-300
         disabled:opacity-25 disabled:dark:opacity-40 disabled:cursor-not-allowed "
            onClick={onOpen}
            disabled={!devices.length}
          >
            {t("project.device_modal.button", { count: devices.length })}
          </button>
        ) : (
          <button
            data-test-id="projectListDeviceModalButton"
            onClick={onOpen}
            className="text-md w-8 h-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
          >
            {devices.length}
          </button>
        )}
      </Tooltip>
      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="text-2xl font-bold">
            {t("project.device_modal.title")}
          </h1>
          <DeviceTable devices={devices} inDevicePage={false} />
        </>
      </Modal>
    </>
  );
}
