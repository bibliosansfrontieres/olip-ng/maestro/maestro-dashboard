import { useContext } from "react";
import { BadgeClickableTable } from "common/TableHelper/BadgeClickable";
import { RedirectButton } from "common/TableHelper/Buttons";
import { ColumnHeaderHelper } from "common/TableHelper/ColumnHeaderHelper";
import { TooltipCell } from "common/TableHelper/TooltipCell";
import DeviceModal from "components/projects/projectTable/DeviceModal/DeviceModal";
import VmModal from "components/projects/projectTable/VmModal/VmModal";
import { ProjectsContext } from "contexts/projects.context";
import { ProjectTableColumnEnum } from "enums/columns/projectTableColumn.enum";
import { ProjectOrderByEnum } from "enums/orderBy/projectOrderBy.enum";
import { TableNamesEnum } from "enums/table.enum";
import { IOmekaProject } from "interfaces/omekaProject.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { ProjectStatusEnum } from "src/enums/projectStatus.enum";
import { patchProject } from "src/services/projects/projects.service";
import { colorProjectStatus } from "utils/colorEnum";
import filterDate from "utils/filterDate";

import {
  Table,
  TableContainer,
  Tbody,
  Td,
  Thead,
  Tooltip,
  Tr,
} from "@chakra-ui/react";
import {
  faArrowsRotate,
  faEye,
  faUpRightFromSquare,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ProjectTable({
  projects,
}: {
  projects: IOmekaProject[];
}) {
  const { t } = useTranslation();
  const { setReload: refreshProjects } = useContext(ProjectsContext);

  return (
    <>
      <TableContainer pt={5}>
        <Table variant="unstyled">
          <Thead>
            <Tr>
              <ColumnHeaderHelper<
                typeof ProjectTableColumnEnum,
                typeof ProjectOrderByEnum
              >
                tableName={TableNamesEnum.PROJECT}
                headers={ProjectTableColumnEnum}
                orderByEnum={ProjectOrderByEnum}
              />
            </Tr>
          </Thead>
          <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
            {projects.map((project) => {
              return (
                <Tr key={`Tr-${project.id}`} className="h-16">
                  <TooltipCell>
                    <Link
                      to={`/projects/${project.id}`}
                      className="underline"
                      data-test-id="projectId"
                    >
                      {project.id}
                    </Link>
                  </TooltipCell>
                  <TooltipCell element={project.title} dataTestInfo="title" />
                  <TooltipCell
                    element={project.description}
                    dataTestInfo="description"
                  />
                  <Td>
                    <p data-test-id="updatedDate">
                      {filterDate(project.updatedAt.toString())}
                    </p>
                  </Td>
                  <Td>
                    <BadgeClickableTable
                      colorFunction={colorProjectStatus}
                      enumValue={project.status}
                      queryParamName="statuses"
                      translateKeysEnumName="project_status"
                    />
                  </Td>
                  <Td>
                    {project.playlistsCount && (
                      <p data-test-id="projectPlaylistsCount">
                        {project.playlistsCount}
                      </p>
                    )}
                  </Td>
                  <Td>
                    {project.itemsCount && (
                      <p data-test-id="projectItemsCount">
                        {project.itemsCount}
                      </p>
                    )}
                  </Td>
                  <Td>
                    {project.virtualMachines && (
                      <VmModal virtualMachines={project.virtualMachines} />
                    )}
                  </Td>
                  <Td>
                    {project.virtualMachines && (
                      <DeviceModal virtualMachines={project.virtualMachines} />
                    )}
                  </Td>
                  <Td>
                    <div className="flex gap-2">
                      <RedirectButton
                        dataTestInfo="projectId"
                        href={`/projects/${project.id}`}
                        icon={faEye}
                        translationKey="project.table.open_project"
                      />
                      <RedirectButton
                        dataTestInfo="omekaProject"
                        href={`http://omeka.tm.bsf-intranet.org/admin/items/show/${project.id}`}
                        icon={faUpRightFromSquare}
                        translationKey="project.table.open_project_omeka"
                        target="_blank"
                      />
                      <Tooltip
                        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
                        label={t("project.update_project")}
                        placement="top"
                        openDelay={500}
                        aria-label={t("project.update_project")}
                      >
                        <button
                          data-test-id="refreshProjectButton"
                          className="text-md w-8 h-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
                          onClick={() => {
                            patchProject({ id: project.id });
                            refreshProjects(true);
                          }}
                          disabled={
                            project.status === ProjectStatusEnum.CHECKING
                          }
                        >
                          <FontAwesomeIcon icon={faArrowsRotate} />
                        </button>
                      </Tooltip>
                    </div>
                  </Td>
                </Tr>
              );
            })}
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}
