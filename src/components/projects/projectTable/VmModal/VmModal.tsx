import Modal from "components/modal/Modal";
import VmTable from "components/vm/VMList/VmTable/VmTable";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";

import { Tooltip, useDisclosure } from "@chakra-ui/react";

export default function VmModal({
  virtualMachines,
  inProjectPage = false,
}: {
  virtualMachines: IVirtualMachine[];
  inProjectPage?: boolean;
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();

  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
        label={t("project.vm_modal.open")}
        placement="top"
        openDelay={500}
        aria-label={t("project.vm_modal.open")}
      >
        {inProjectPage ? (
          <button
            data-test-id="projectPageVmModalButton"
            className="btn-primary-base truncate
                 bg-brand-500 hover:bg-brand-600 dark:bg-brand-400 dark:hover:bg-brand-300
         disabled:opacity-25 disabled:dark:opacity-40 disabled:cursor-not-allowed "
            onClick={onOpen}
            disabled={!virtualMachines.length}
          >
            {t("project.vm_modal.button", { count: virtualMachines.length })}
          </button>
        ) : (
          <button
            data-test-id="projectListVmModalButton"
            onClick={onOpen}
            className="text-md w-8 h-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
          >
            {virtualMachines.length}
          </button>
        )}
      </Tooltip>
      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="text-2xl font-bold">{t("project.vm_modal.title")}</h1>
          <VmTable tableData={virtualMachines} inProjectModal={true} />
        </>
      </Modal>
    </>
  );
}
