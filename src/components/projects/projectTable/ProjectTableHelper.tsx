import DatePicker from "src/common/TableHelper/DatePicker";
import { ReactMultiSelect } from "src/common/TableHelper/ReactMultiSelect";
import { ProjectStatusEnum } from "src/enums/projectStatus.enum";

export default function ProjectTableHelper() {
  return (
    <div className="mt-4 flex flex-wrap gap-4 items-center">
      <ReactMultiSelect<typeof ProjectStatusEnum>
        enumList={ProjectStatusEnum}
        queryParamsEnumName="statuses"
        translateKeysEnumName="project_status"
      />
      <DatePicker queryParamsName="updatedDateRange" />
    </div>
  );
}
