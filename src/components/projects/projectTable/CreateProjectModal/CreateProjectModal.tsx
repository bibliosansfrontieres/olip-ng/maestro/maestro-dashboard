import { useContext } from "react";
import Modal from "components/modal/Modal";
import VerifyProject from "components/projects/projectCreation/VerifyProject";
import { ProjectsContext } from "contexts/projects.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { useTranslation } from "react-i18next";

import { useDisclosure } from "@chakra-ui/react";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function CreateProjectModal() {
  const { t } = useTranslation();
  const { setReload: refresh } = useContext(ProjectsContext);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const handleClose = () => {
    onClose();
    //@TODO: remove setTimeout once there is websockets and back is refactored
    setTimeout(() => {
      refresh(true);
    }, 2000);
  };
  return (
    <>
      <button
        className="btn-primary"
        onClick={onOpen}
        disabled={!usePermissions(PermissionNameEnum.CREATE_PROJECT)}
      >
        <FontAwesomeIcon className="mr-2" icon={faPlus} />
        {t("project.import.button")}
      </button>

      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
            {t("project.import.button")}
          </h1>
          <VerifyProject inVmPage={false} onCloseCreateModal={handleClose} />
        </>
      </Modal>
    </>
  );
}
