import Card from "components/card/Card";
import DeviceModal from "components/projects/projectTable/DeviceModal/DeviceModal";
import VmModal from "components/projects/projectTable/VmModal/VmModal";
import { GenericButton } from "components/vm/Tabs/VmButtons/VmButtons";
import { ProjectStatusEnum } from "enums/projectStatus.enum";
import { IOmekaProject } from "interfaces/omekaProject.interface";
import { useTranslation } from "react-i18next";
import { Link, useRevalidator } from "react-router-dom";
import { patchProject } from "services/projects/projects.service";

import { Tooltip } from "@chakra-ui/react";
import { faArrowsRotate } from "@fortawesome/free-solid-svg-icons";

export default function ProjectCard({
  project,
  inProjectPage = false,
}: {
  project: IOmekaProject;
  inProjectPage?: boolean;
}) {
  const { t } = useTranslation();
  const revalidator = useRevalidator();
  const projectInformationsTable = [
    {
      translation: "project.information.start_date",
      value: project?.startDate,
      dataTestId: "projectStartDate",
    },
    {
      translation: "project.information.end_date",
      value: project?.endDate,
      dataTestId: "projectEndDate",
    },
    {
      translation: "project.information.languages",
      value: project?.languages.replace(",", " "),
      dataTestId: "projectLanguages",
    },
    {
      translation: "project.information.location",
      value: project?.location,
      dataTestId: "projectLocation",
    },
    {
      translation: "project.information.device",
      value: project?.device,
      dataTestId: "projectNumberDevice",
    },
    {
      translation: "project.information.project_manager",
      value: project?.projectManager.replace("\r\n", " "),
      dataTestId: "projectProjectManager",
    },
    {
      translation: "project.information.partners",
      value: project?.partners.replace("\r\n", " "),
      dataTestId: "projectPartners",
    },
  ];

  const refreshProject = async () => {
    await patchProject({ id: project.id });
    revalidator.revalidate();
  };
  return (
    <Card>
      <>
        <div className="flex items-center justify-between">
          <p
            className="text-navy-700 dark:text-white text-lg font-bold mb-4"
            data-test-id="projectInformation"
          >
            {t("vm.information_tab.omeka_informations.title")}
          </p>
          {project.virtualMachines && inProjectPage && (
            <div className="flex gap-2 mr-10">
              <VmModal
                virtualMachines={project.virtualMachines}
                inProjectPage
              />
              <DeviceModal
                virtualMachines={project.virtualMachines}
                inProjectPage
              />
              <GenericButton
                color="orange"
                handleClick={refreshProject}
                icon={faArrowsRotate}
                displayConditions={inProjectPage}
                disabledConditions={
                  project.status === ProjectStatusEnum.CHECKING
                }
                translationKey="vm.tabs.actions.button.refresh_project"
                dataTestId="refreshProjectButton"
              />
            </div>
          )}
        </div>
        <div className="flex flex-col gap-1 ml-3 text-base text-navy-700 dark:text-white">
          <p
            className="font-bold"
            data-test-id="projectTitle"
          >{`${project.title} - ${project.id}`}</p>

          <p data-test-id="projectDescription">{project.description}</p>
          <Link
            data-test-id="redirectButtonLink-omekaProject"
            className="underline cursor-pointer"
            target="_blank"
            to={`http://omeka.tm.bsf-intranet.org/admin/items/show/${project.id}`}
          >
            {`http://omeka.tm.bsf-intranet.org/admin/items/show/${project.id}`}
          </Link>
          <div className="mt-4">
            {projectInformationsTable.map((informationLine, index) => {
              return (
                <Tooltip
                  className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 sm:hidden"
                  label={`${t(`${informationLine.translation}`)} ${informationLine.value}`}
                  placement="top"
                  openDelay={500}
                  aria-label={t(`${informationLine.translation}`)}
                  key={`tooltip-${index}`}
                >
                  <div className="flex gap-2" key={index}>
                    <p className="font-bold truncate">
                      {t(`${informationLine.translation}`)}
                    </p>
                    <p
                      className="truncate"
                      data-test-id={informationLine.dataTestId}
                    >
                      {informationLine.value}
                    </p>
                  </div>
                </Tooltip>
              );
            })}
          </div>
        </div>
      </>
    </Card>
  );
}
