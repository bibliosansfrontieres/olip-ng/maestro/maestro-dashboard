import { IOmekaProject } from "interfaces/omekaProject.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

export default function ProjectInformation({
  project,
}: {
  project: IOmekaProject | undefined;
}) {
  const { t } = useTranslation();

  if (!project) {
    return;
  }

  const informationsTab = [
    {
      translation: "project.information.start_date",
      value: project.startDate,
    },
    {
      translation: "project.information.end_date",
      value: project.endDate,
    },
    {
      translation: "project.information.languages",
      value: project.languages.replace(",", " "),
    },
    {
      translation: "project.information.location",
      value: project.location,
    },
    {
      translation: "project.information.device",
      value: project.device,
    },
    {
      translation: "project.information.project_manager",
      value: project.projectManager.replace("\r\n", " "),
    },
    {
      translation: "project.information.partners",
      value: project?.partners.replace("\r\n", " "),
    },
  ];

  return (
    <div className="flex flex-col gap-1 ml-3 text-base text-navy-700 dark:text-white">
      <p className="font-bold">{project.title}</p>
      <p>{project.description}</p>
      <Link
        className="underline cursor-pointer"
        to={`http://omeka.tm.bsf-intranet.org/admin/items/show/${project.id}`}
        target="_blank"
      >
        {`http://omeka.tm.bsf-intranet.org/admin/items/show/${project.id}`}
      </Link>
      <div className="mt-4">
        {informationsTab.map((informationLine, index) => {
          return (
            <div className="flex gap-2" key={index}>
              <p className="font-bold">{t(`${informationLine.translation}`)}</p>
              <p>{informationLine.value}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
