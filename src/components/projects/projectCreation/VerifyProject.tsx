import { useState } from "react";
import Modal from "components/modal/Modal";
import ProjectIdForm from "components/projects/projectCreation/ProjectIdForm/ProjectIdForm";
import ProjectInformation from "components/projects/projectCreation/ProjectInformation/ProjectInformation";
import VmForm from "components/vm/VMCreation/VmForm/VmForm";
import { IOmekaProject } from "interfaces/omekaProject.interface";
import { useTranslation } from "react-i18next";
import { useRevalidator } from "react-router-dom";
import { createProject } from "services/projects/projects.service";

import { Button, useDisclosure } from "@chakra-ui/react";

export default function VerifyProject({
  inVmPage,
  onCloseCreateModal,
}: {
  inVmPage: boolean;
  onCloseCreateModal?: () => void;
}) {
  const [project, setProject] = useState<IOmekaProject | undefined>(undefined);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();
  const revalidator = useRevalidator();

  const onCreateProject = () => {
    if (project) {
      createProject({ id: project.id });
      onCloseCreateModal && onCloseCreateModal();
      revalidator.revalidate();
    }
  };
  return (
    <>
      <ProjectIdForm
        onOpen={onOpen}
        setProject={setProject}
        inVmPage={inVmPage}
      />
      {project && inVmPage && (
        <Modal disclosure={{ isOpen, onClose }}>
          <>
            <ProjectInformation project={project} />
            <Button className="mt-4" onClick={onClose}>
              {t("button.close")}
            </Button>
          </>
        </Modal>
      )}
      {project && inVmPage && (
        <>
          <p className="text-center text-lg text-navy-700 dark:text-white ml-3 font-bold">
            {project.title}
          </p>
          <VmForm project={project} />
        </>
      )}
      {project && !inVmPage && (
        <>
          <ProjectInformation project={project} />
          <button
            className="btn-primary mt-5"
            disabled={!project}
            onClick={onCreateProject}
          >
            {t("project.create.button")}
          </button>
        </>
      )}
    </>
  );
}
