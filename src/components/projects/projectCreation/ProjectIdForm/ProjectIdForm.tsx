import { useState } from "react";
import InputField from "components/shared/fields/InputField";
import { useFormik } from "formik";
import { IOmekaProject } from "interfaces/omekaProject.interface";
import { useTranslation } from "react-i18next";
import { getOmekaCheckProject } from "services/omeka/omeka.service";

import { FormControl, Tooltip } from "@chakra-ui/react";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ProjectIdForm({
  onOpen,
  setProject,
  inVmPage,
}: {
  onOpen: () => void;
  setProject: (project: IOmekaProject | undefined) => void;
  inVmPage: boolean;
}) {
  const [isValid, setIsValid] = useState<boolean>(false);
  const { t } = useTranslation();
  const projectIdFormik = useFormik({
    initialValues: { projectId: "" },
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(false);
      const result = await getOmekaCheckProject(values);
      if (result) {
        setProject(result);
        setIsValid(true);
      }
      onOpen();
    },
  });
  const handleCancel = () => {
    setProject(undefined);
    setIsValid(false);
  };

  return (
    <div className="flex">
      <form onSubmit={projectIdFormik.handleSubmit} className="flex gap-3 grow">
        <FormControl className="grow">
          <InputField
            id="projectId"
            label={t("project.form.project_id.label")}
            extra="mt-3"
            type="text"
            placeholder={t("project.form.project_id.placeholder")}
            onChange={projectIdFormik.handleChange}
            onBlur={projectIdFormik.handleBlur}
            name="projectId"
            disabled={projectIdFormik.isSubmitting || isValid}
            value={projectIdFormik.values.projectId}
            state={projectIdFormik.errors.projectId ? "error" : undefined}
            required={true}
            autofocus={true}
          />
        </FormControl>
        {!isValid && (
          <button
            type="submit"
            className="btn-primary h-10 self-end"
            disabled={projectIdFormik.isSubmitting || isValid}
          >
            {t("project.form.submit")}
          </button>
        )}
      </form>
      {isValid && inVmPage && (
        <Tooltip
          className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 pl-3 max-w-32"
          label={t("project.open_modal")}
          placement="top"
          openDelay={500}
          aria-label={t("project.open_modal")}
        >
          <button
            onClick={onOpen}
            disabled={!isValid}
            className="h-10 w-12 self-end flex items-center justify-center p-2 transition duration-200 hover:cursor-pointer disabled:opacity-25 disabled:cursor-not-allowed"
          >
            <FontAwesomeIcon
              className="h-5 w-5 text-brand-500 dark:text-brand-400"
              icon={faInfoCircle}
            />
          </button>
        </Tooltip>
      )}
      {isValid && (
        <button
          className="rounded-xl px-5 ml-3 text-base font-medium transition duration-200 border border-red-500 text-red-500 dark:border-red-400 dark:text-red-400 h-10 self-end"
          onClick={handleCancel}
          disabled={!isValid}
        >
          {t("project.form.correct")}
        </button>
      )}
    </div>
  );
}
