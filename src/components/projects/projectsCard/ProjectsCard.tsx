import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import ProjectTable from "components/projects/projectTable/ProjectTable";
import { ProjectsContext } from "src/contexts/projects.context";

export default function ProjectsCard() {
  const { projects, isLoading, error } = useContext(ProjectsContext);
  return (
    <div data-test-id="projectsCard">
      {projects && !isLoading && !error && (
        <>
          <ProjectTable projects={projects.data} />
          <Pagination meta={projects.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !projects && (
        <ErrorList translation="project.get_projects.error" />
      )}
    </div>
  );
}
