import { FormErrorMessage } from "@chakra-ui/react";

export default function FormErrorMessageText(props: {
  text: string;
  extraStyle?: string;
}) {
  const { text, extraStyle } = props;
  return (
    <FormErrorMessage
      className={`text-red-500 dark:text-red-400 ml-1 text-sm ${extraStyle}`}
    >
      {text}
    </FormErrorMessage>
  );
}
