// Custom components

import { ChangeEvent, ForwardedRef, forwardRef } from "react";

import { FormLabel, Input } from "@chakra-ui/react";

const InputField = forwardRef<
  HTMLInputElement,
  {
    id: string;
    label?: string;
    extra?: string;
    placeholder?: string;
    variant?: string;
    state?: string;
    disabled?: boolean;
    type?: string;
    name?: string;
    value?: string | number;
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
    onBlur?: (event: ChangeEvent<HTMLInputElement>) => void;
    required?: boolean;
    autofocus?: boolean;
  }
>(function (
  props: {
    id: string;
    label?: string;
    extra?: string;
    placeholder?: string;
    variant?: string;
    state?: string;
    disabled?: boolean;
    type?: string;
    name?: string;
    value?: string | number;
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
    onBlur?: (event: ChangeEvent<HTMLInputElement>) => void;
    required?: boolean;
    autofocus?: boolean;
  },
  ref: ForwardedRef<HTMLInputElement>,
) {
  const {
    label,
    id,
    extra,
    type,
    placeholder,
    state,
    disabled,
    onChange,
    name,
    value,
    required,
    autofocus,
  } = props;

  return (
    <div className={`${extra}`}>
      {label && (
        <FormLabel
          htmlFor={id}
          className={`text-sm text-navy-700 dark:text-white ml-3 font-bold ${required ? 'after:content-["*"] after:ml-0.5 after:text-red-500' : ""}`}
        >
          {label}
        </FormLabel>
      )}
      <Input
        disabled={disabled}
        type={type}
        id={id}
        ref={ref}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        value={value}
        required={required}
        autoFocus={autofocus}
        data-test-id={`inputField-${id}`}
        className={`mt-2 flex h-12 w-full items-center justify-center rounded-xl border bg-white/0 p-3 text-sm outline-none ${
          state === "error"
            ? "border-red-500 text-red-500 placeholder:text-red-500 dark:!border-red-400 dark:!text-red-400 dark:placeholder:!text-red-400"
            : "border-gray-200 dark:!border-white/10 dark:hover:!border-white text-navy-700 dark:text-white"
        }`}
      />
    </div>
  );
});

export default InputField;
