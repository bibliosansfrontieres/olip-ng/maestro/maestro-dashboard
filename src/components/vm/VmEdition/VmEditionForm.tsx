import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import { translationsList } from "constants/translations";
import { VmContext } from "contexts/vm.context";
import { useFormik } from "formik";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";
import { IPatchVirtualMachinesRequest } from "services/virtualMachines/interfaces/patchVirtualMachine.interface";
import { IPostVirtualMachinesRequest } from "services/virtualMachines/interfaces/postVirtualMachines.interface";
import { patchVirtualMachine } from "services/virtualMachines/virtualMachines.service";

import { FormControl, FormLabel, Select, Textarea } from "@chakra-ui/react";

export default function VmEditionForm({
  vm,
  onClose,
}: {
  vm: IVirtualMachine;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const { setReload } = useContext(VmContext);

  const formik = useFormik({
    initialValues: {
      defaultLanguageIdentifier: vm.defaultLanguageIdentifier,
      title: vm.title,
      description: vm.description ?? "",
      referenceUrl: vm.referenceUrl ?? "",
    } as IPostVirtualMachinesRequest,
    onSubmit: async (values, { setSubmitting }) => {
      const initialValues = {
        defaultLanguageIdentifier: vm.defaultLanguageIdentifier,
        title: vm.title,
        description: vm.description ?? "",
        referenceUrl: vm.referenceUrl ?? "",
      };
      const modifiedValues = {} as IPatchVirtualMachinesRequest;
      // add only different values
      for (const key of Object.keys(values)) {
        if (
          values[key as keyof IPatchVirtualMachinesRequest] !==
          initialValues[key as keyof IPatchVirtualMachinesRequest]
        ) {
          modifiedValues[key as keyof IPatchVirtualMachinesRequest] =
            values[key as keyof IPatchVirtualMachinesRequest];
        }
      }
      const result = await patchVirtualMachine({
        ...modifiedValues,
        id: vm.id,
      });
      setSubmitting(false);
      if (result) {
        setReload(true);
        onClose();
      }
    },
  });
  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("project.form.edition")}
      </h1>
      <form
        onSubmit={formik.handleSubmit}
        className="text-navy-700 dark:text-white"
      >
        <FormControl isInvalid={!!formik.errors.title}>
          <InputField
            id="title"
            label={t("project.form.label.title")}
            extra="mt-3"
            type="text"
            placeholder={t("project.form.placeholder.title")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="title"
            value={formik.values.title}
            state={formik.errors.title ? "error" : undefined}
            required={true}
            autofocus={true}
          />
        </FormControl>
        <FormControl>
          <FormLabel
            htmlFor="select"
            className={`text-sm mb-2 mt-3 ml-3 font-bold after:content-["*"] after:ml-0.5 after:text-red-500`}
          >
            {t("project.form.language")}
          </FormLabel>
          <Select
            id="defaultLanguageIdentifier"
            value={formik.values.defaultLanguageIdentifier}
            onChange={formik.handleChange}
            className="text-sm font-medium border-gray-200 dark:!border-white/10"
          >
            {translationsList.map((language) => {
              return (
                <option key={language} value={language}>
                  {t(`language.${language}`)}
                </option>
              );
            })}
          </Select>
        </FormControl>
        <FormControl>
          <FormLabel
            htmlFor="description"
            className="text-sm mt-3 ml-3 font-bold "
          >
            {t("project.form.label.description")}
          </FormLabel>
          <Textarea
            id="description"
            className="border-gray-200 dark:!border-white/10"
            placeholder={t("project.form.placeholder.description")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="description"
            value={formik.values.description}
          />
        </FormControl>
        <FormControl>
          <InputField
            label={t("project.form.label.reference_url")}
            id="referenceUrl"
            extra="mt-3"
            type="text"
            placeholder={t("project.form.placeholder.reference_url")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="referenceUrl"
            value={formik.values.referenceUrl}
          />
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5"
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
