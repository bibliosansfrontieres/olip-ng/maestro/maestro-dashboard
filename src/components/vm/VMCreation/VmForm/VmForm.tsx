import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { translationsList } from "constants/translations";
import { useFormik } from "formik";
import { IOmekaProject } from "interfaces/omekaProject.interface";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { IPostVirtualMachinesRequest } from "services/virtualMachines/interfaces/postVirtualMachines.interface";
import { postVirtualMachines } from "services/virtualMachines/virtualMachines.service";
import * as Yup from "yup";

import { FormControl, FormLabel, Select, Textarea } from "@chakra-ui/react";

export default function VmForm({ project }: { project: IOmekaProject }) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const validationSchema = Yup.object({
    title: Yup.string().required(t("project.form.validation.title.required")),
  });

  const projectFormik = useFormik({
    initialValues: {
      defaultLanguageIdentifier: "eng",
      title: "",
      description: "",
      referenceUrl: "",
    } as IPostVirtualMachinesRequest,
    validationSchema: validationSchema,
    onSubmit: async (values, { setSubmitting }) => {
      const modifiedValues = { ...values } as IPostVirtualMachinesRequest;

      for (const key of Object.keys(values)) {
        if (
          key !== "title" &&
          key !== "defaultLanguageIdentifier" &&
          values[key as keyof IPostVirtualMachinesRequest] === ""
        ) {
          modifiedValues[key as "description" | "referenceUrl"] = undefined;
        }
      }
      const submitValues = {
        ...modifiedValues,
        projectId: project!.id,
      };
      setSubmitting(false);
      const result = await postVirtualMachines(submitValues);
      if (result) {
        navigate(`/virtual-machines/${result.id}`);
      }
    },
  });
  return (
    <form onSubmit={projectFormik.handleSubmit} className="mt-4">
      <FormControl isInvalid={!!projectFormik.errors.title}>
        <InputField
          id="title"
          label={t("project.form.label.title")}
          extra="mt-3"
          type="text"
          placeholder={t("project.form.placeholder.title")}
          onChange={projectFormik.handleChange}
          onBlur={projectFormik.handleBlur}
          name="title"
          value={projectFormik.values.title}
          state={projectFormik.errors.title ? "error" : undefined}
          required={true}
          autofocus={true}
        />
        {projectFormik.errors.title && (
          <FormErrorMessageText text={projectFormik.errors.title} />
        )}
      </FormControl>
      <FormControl>
        <FormLabel
          htmlFor="select"
          className={`text-sm mb-2 mt-3 ml-3 font-bold after:content-["*"] after:ml-0.5 after:text-red-500`}
        >
          {t("project.form.language")}
        </FormLabel>
        <Select
          id="defaultLanguageIdentifier"
          value={projectFormik.values.defaultLanguageIdentifier}
          onChange={projectFormik.handleChange}
          className="text-sm font-medium border-gray-200 dark:!border-white/10"
        >
          {translationsList.map((language) => {
            return (
              <option key={language} value={language}>
                {t(`language.${language}`)}
              </option>
            );
          })}
        </Select>
      </FormControl>
      <FormControl>
        <FormLabel
          htmlFor="description"
          className="text-sm mt-3 ml-3 font-bold "
        >
          {t("project.form.label.description")}
        </FormLabel>
        <Textarea
          id="description"
          className="border-gray-200 dark:!border-white/10"
          placeholder={t("project.form.placeholder.description")}
          onChange={projectFormik.handleChange}
          onBlur={projectFormik.handleBlur}
          name="description"
          value={projectFormik.values.description}
        />
      </FormControl>
      <FormControl>
        <InputField
          label={t("project.form.label.reference_url")}
          id="referenceUrl"
          extra="mt-3"
          type="text"
          placeholder={t("project.form.placeholder.reference_url")}
          onChange={projectFormik.handleChange}
          onBlur={projectFormik.handleBlur}
          name="referenceUrl"
          value={projectFormik.values.referenceUrl}
        />
      </FormControl>
      <button
        type="submit"
        className="btn-primary mt-5"
        disabled={projectFormik.isSubmitting}
      >
        {t("button.submit")}
      </button>
    </form>
  );
}
