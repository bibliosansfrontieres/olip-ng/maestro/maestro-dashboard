import { Dispatch, SetStateAction } from "react";
import { ColumnHeaderQueryHelper } from "common/QueryElements/ColumnHeaderQueryHelper";
import { BadgeClickableTable } from "common/TableHelper/BadgeClickable";
import { BadgeTable } from "common/TableHelper/BadgeTable";
import { RedirectButton } from "common/TableHelper/Buttons";
import {
  ColumnHeaderHelper,
  ColumnHeaderHelperWithoutSort,
} from "common/TableHelper/ColumnHeaderHelper";
import { InformationClickableTable } from "common/TableHelper/InformationClickable";
import {
  VirtualMachineTableColumEnum,
  VirtualMachineTableProjectModalColumnEnum,
} from "enums/columns/virtualMachineTableColumn.enum";
import { VirtualMachineOrderByEnum } from "enums/orderBy/virtualMachineOrderBy.enum";
import { TableNamesEnum } from "enums/table.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import { IGroup } from "interfaces/group.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { colorVmApproval, colorVmStatus } from "utils/colorEnum";
import filterDate from "utils/filterDate";
import getEnv from "utils/getEnv";

import {
  Badge,
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverTrigger,
  Table,
  TableContainer,
  Tbody,
  Td,
  Thead,
  Tooltip,
  Tr,
} from "@chakra-ui/react";
import { faEye, faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const GroupsCell = ({ groups }: { groups: IGroup[] | undefined }) => {
  return (
    <div className="flex justify-center">
      {groups && groups.length > 0 && (
        <Popover variant="custom">
          <PopoverTrigger>
            <IconButton
              size="sm"
              aria-label="Groups"
              icon={<FontAwesomeIcon icon={faEye} />}
            />
          </PopoverTrigger>
          <PopoverContent w="100%" p={4} className="dark:!bg-navy-800">
            <PopoverArrow className="dark:!bg-navy-800" />
            <PopoverCloseButton />
            <PopoverBody>
              {groups.map((group) => {
                return (
                  <InformationClickableTable
                    key={group.id}
                    value={group.name}
                    queryParamName="groups"
                    filterValue={group.id}
                  />
                );
              })}
            </PopoverBody>
          </PopoverContent>
        </Popover>
      )}
    </div>
  );
};

export default function VmTable<V extends IPageRequest>({
  tableData,
  inProjectModal = false,
  inHomePage = false,
  setParams,
  params,
}: {
  tableData: IVirtualMachine[];
  inProjectModal?: boolean;
  inHomePage?: boolean;
  setParams?: Dispatch<SetStateAction<V>>;
  params?: V;
}) {
  const { t } = useTranslation();

  return (
    <TableContainer pt={5}>
      <Table variant="unstyled">
        <Thead>
          <Tr>
            {inProjectModal && (
              <ColumnHeaderHelperWithoutSort<
                typeof VirtualMachineTableProjectModalColumnEnum
              >
                tableName={TableNamesEnum.VIRTUAL_MACHINE}
                headers={VirtualMachineTableProjectModalColumnEnum}
              />
            )}
            {inHomePage && setParams && params && (
              <ColumnHeaderQueryHelper<
                typeof VirtualMachineTableColumEnum,
                typeof VirtualMachineOrderByEnum,
                V
              >
                tableName={TableNamesEnum.VIRTUAL_MACHINE}
                headers={VirtualMachineTableColumEnum}
                orderByEnum={VirtualMachineOrderByEnum}
                setQueryParams={setParams}
                queryParams={params}
              />
            )}
            {!inProjectModal && !inHomePage && (
              <ColumnHeaderHelper<
                typeof VirtualMachineTableColumEnum,
                typeof VirtualMachineOrderByEnum
              >
                tableName={TableNamesEnum.VIRTUAL_MACHINE}
                headers={VirtualMachineTableColumEnum}
                orderByEnum={VirtualMachineOrderByEnum}
              />
            )}
          </Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {tableData.map((vm) => {
            return (
              <Tr key={`Tr-${vm.id}`} className="h-16">
                <Td className="min-w-[50px]">
                  <Link to={`/virtual-machines/${vm.id}`} className="underline">
                    {vm.id}
                  </Link>
                </Td>
                <Td className="min-w-24 max-w-52">
                  <Tooltip
                    className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
                    label={vm.title}
                    placement="top"
                    openDelay={500}
                    aria-label="title"
                  >
                    <p className="truncate">{vm.title}</p>
                  </Tooltip>
                </Td>
                {!inProjectModal && (
                  <Td className="min-w-[50px]">
                    <Link
                      className="underline cursor-pointer text-sm"
                      to={`http://omeka.tm.bsf-intranet.org/admin/items/show/${vm.project.id}`}
                      target="_blank"
                    >
                      {vm.project.id}
                    </Link>
                  </Td>
                )}
                <Td>
                  {vm.isArchived && (
                    <Badge colorScheme="red">{t("vm.table.archived")}</Badge>
                  )}
                  {!vm.isArchived && !inHomePage && (
                    <BadgeClickableTable
                      colorFunction={colorVmStatus}
                      enumValue={vm.status}
                      queryParamName="statuses"
                      translateKeysEnumName="vm_status"
                    />
                  )}

                  {!vm.isArchived && inHomePage && (
                    <BadgeTable
                      colorFunction={colorVmStatus}
                      enumValue={vm.status}
                      translateKeysEnumName="vm_status"
                    />
                  )}
                </Td>
                <Td>
                  {inHomePage ? (
                    <BadgeTable
                      colorFunction={colorVmApproval}
                      enumValue={vm.approval}
                      translateKeysEnumName="vm_approval"
                    />
                  ) : (
                    <BadgeClickableTable
                      colorFunction={colorVmApproval}
                      enumValue={vm.approval}
                      queryParamName="approvals"
                      translateKeysEnumName="vm_approval"
                    />
                  )}
                </Td>
                <Td>
                  {inHomePage ? (
                    <div>{vm.user.name}</div>
                  ) : (
                    <InformationClickableTable
                      value={vm.user.name}
                      queryParamName="users"
                      filterValue={vm.user.id}
                    />
                  )}
                </Td>
                {!inProjectModal && (
                  <Td>
                    <GroupsCell groups={vm.groups} />
                  </Td>
                )}
                {!inProjectModal && (
                  <Td>
                    <p>{filterDate(vm.createdAt.toString())}</p>
                  </Td>
                )}
                <Td>
                  <p>{filterDate(vm.updatedAt.toString())}</p>
                </Td>
                <Td>
                  <div className="flex gap-2">
                    <RedirectButton
                      href={`/virtual-machines/${vm.id}`}
                      icon={faEye}
                      translationKey="vm.table.buttons.open_maestro"
                    />
                    {vm.status === VirtualMachineStatusEnum.ONLINE && (
                      <RedirectButton
                        href={`http://${vm.id}.${getEnv("PUBLIC_BASE_URL")}`}
                        icon={faUpRightFromSquare}
                        translationKey="vm.table.buttons.open_olip"
                        target="_blank"
                      />
                    )}
                  </div>
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
