import { useCallback } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import VmTable from "components/vm/VMList/VmTable/VmTable";
import { BooleanEnum } from "enums/boolean.enum";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useSearchParams } from "react-router-dom";
import { IGetVirtualMachinesResponse } from "services/virtualMachines/interfaces/getVirtualMachines.interface";
import { getVirtualMachines } from "services/virtualMachines/virtualMachines.service";

export default function VMsCard() {
  const [params] = useSearchParams();

  const {
    result: vms,
    isLoading,
    error,
  } = useApi<IGetVirtualMachinesResponse>(
    useCallback(async () => {
      return await getVirtualMachines({
        order: params.get("order") as PageOrderEnum,
        orderBy: params.get("orderBy") || undefined,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        query: params.get("query") || undefined,
        isUserVmsFiltered:
          params.get("isUserVmsFiltered") === "TRUE"
            ? BooleanEnum.TRUE
            : BooleanEnum.FALSE,
        statuses: params.get("statuses") || undefined,
        approvals: params.get("approvals") || undefined,
        groups: params.get("groups") || undefined,
        users: params.get("users") || undefined,
        createdDateRange: params.get("createdDateRange") || undefined,
        updatedDateRange: params.get("updatedDateRange") || undefined,
      });
    }, [params]),
  );
  return (
    <>
      {vms && !isLoading && !error && (
        <>
          <VmTable tableData={vms.data} />
          <Pagination meta={vms.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !vms && <ErrorList translation="vm.get_vms.error" />}
    </>
  );
}
