import { Dispatch, SetStateAction } from "react";
import { AsyncMultiSelectQuery } from "common/QueryElements/AsyncMultiSelectQuery";
import DatePickerQuery from "common/QueryElements/DatePickerQuery";
import { ReactMultiSelectQuery } from "common/QueryElements/ReactMultiSelectQuery";
import { VirtualMachineApprovalEnum } from "enums/virtualMachineApproval.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { getGroups } from "services/groups/groups.service";

export default function VmTableQueryHelper<T extends IPageRequest>({
  setParams,
}: {
  setParams: Dispatch<SetStateAction<T>>;
}) {
  return (
    <div className="mt-4 flex flex-wrap gap-4 items-center">
      <ReactMultiSelectQuery<typeof VirtualMachineStatusEnum, T>
        enumList={VirtualMachineStatusEnum}
        queryParamsEnumName="statuses"
        translateKeysEnumName="vm_status"
        setParams={setParams}
      />
      <AsyncMultiSelectQuery
        queryParamsName="groups"
        multiGet={getGroups}
        setParams={setParams}
      />
      <ReactMultiSelectQuery<typeof VirtualMachineApprovalEnum, T>
        enumList={VirtualMachineApprovalEnum}
        queryParamsEnumName="approvals"
        translateKeysEnumName="vm_approval"
        setParams={setParams}
      />
      <DatePickerQuery
        queryParamsName="createdDateRange"
        setParams={setParams}
      />
      <DatePickerQuery
        queryParamsName="updatedDateRange"
        setParams={setParams}
      />
    </div>
  );
}
