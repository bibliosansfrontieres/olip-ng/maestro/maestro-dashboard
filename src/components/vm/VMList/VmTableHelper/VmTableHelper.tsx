import AsyncMultiSelect from "common/TableHelper/AsyncMultiSelect";
import DatePicker from "common/TableHelper/DatePicker";
import { ReactMultiSelect } from "common/TableHelper/ReactMultiSelect";
import { SwitchOnly } from "common/TableHelper/SwitchOnly";
import { VirtualMachineApprovalEnum } from "enums/virtualMachineApproval.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import { getGroup, getGroups } from "services/groups/groups.service";
import { getUser, getUsers } from "services/users/users.service";

export default function VmTableHelper() {
  return (
    <div className="mt-4 flex flex-wrap gap-4 items-center">
      <SwitchOnly
        queryParamsName="isUserVmsFiltered"
        defaultSwitchValue={false}
        defaultQueryParamValue={false}
      />
      <ReactMultiSelect<typeof VirtualMachineStatusEnum>
        enumList={VirtualMachineStatusEnum}
        queryParamsEnumName="statuses"
        translateKeysEnumName="vm_status"
      />
      <AsyncMultiSelect
        queryParamsName="users"
        singleGet={getUser}
        multiGet={getUsers}
      />
      <ReactMultiSelect<typeof VirtualMachineApprovalEnum>
        enumList={VirtualMachineApprovalEnum}
        queryParamsEnumName="approvals"
        translateKeysEnumName="vm_approval"
      />
      <AsyncMultiSelect
        queryParamsName="groups"
        singleGet={getGroup}
        multiGet={getGroups}
      />
      <DatePicker queryParamsName="createdDateRange" />
      <DatePicker queryParamsName="updatedDateRange" />
    </div>
  );
}
