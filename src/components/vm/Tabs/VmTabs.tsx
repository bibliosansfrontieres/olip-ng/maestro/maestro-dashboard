import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router-dom";

import { Tab, TabList, Tabs } from "@chakra-ui/react";
export default function VmTabs() {
  const navigate = useNavigate();
  const location = useLocation();
  const { t } = useTranslation();

  const allyClassName = (name: string) => {
    const tab =
      location.pathname.split("/").length > 3
        ? location.pathname.split("/")[3]
        : "information";
    let className = "";
    if (tab === name) {
      className +=
        "!text-brand-500 dark:!text-brand-400 !border-brand-500 dark:!border-brand-400";
    } else {
      className += "!text-navy-700 dark:!text-white !border-0";
    }
    return className;
  };
  return (
    <Tabs width="100%">
      <TabList>
        <Tab
          className={allyClassName("information")}
          onClick={() => navigate("")}
        >
          {t("vm.tabs.information")}
        </Tab>
        <Tab
          className={allyClassName("context")}
          onClick={() => {
            navigate("context");
          }}
        >
          {t("vm.tabs.context")}
        </Tab>
        <Tab className={allyClassName("logs")} onClick={() => navigate("logs")}>
          {t("vm.tabs.logs")}
        </Tab>
        <Tab
          className={allyClassName("history")}
          onClick={() => navigate("history")}
        >
          {t("vm.tabs.history")}
        </Tab>
        <Tab
          className={allyClassName("groups")}
          onClick={() => navigate("groups")}
        >
          {t("vm.tabs.groups")}
        </Tab>
        <Tab className={allyClassName("save")} onClick={() => navigate("save")}>
          {t("vm.tabs.save")}
        </Tab>
        <Tab
          className={allyClassName("deploy")}
          onClick={() => navigate("deploy")}
        >
          {t("vm.tabs.deploy")}
        </Tab>
      </TabList>
    </Tabs>
  );
}
