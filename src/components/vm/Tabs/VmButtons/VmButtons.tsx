import { useContext } from "react";
import Modal from "components/modal/Modal";
import ApproveForm from "components/vm/Tabs/VmButtons/ApproveForm/ApproveForm";
import DeployForm from "components/vm/Tabs/VmButtons/DeployForm/DeployForm";
import SaveForm from "components/vm/Tabs/VmButtons/SaveForm/SaveForm";
import VmEditForm from "components/vm/Tabs/VmEditForm/VmEditForm";
import { LogsContext } from "contexts/logs.context";
import { VmSavesContext } from "contexts/vmSaves.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { VirtualMachineApprovalEnum } from "enums/virtualMachineApproval.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import { useCanPatchVm, usePermissions } from "hooks/usePermission";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import {
  patchVirtualMachineApproval,
  patchVirtualMachineApprovalReject,
  patchVirtualMachineArchive,
  patchVirtualMachineRefreshProject,
  patchVirtualMachineStart,
  patchVirtualMachineStop,
} from "services/virtualMachines/virtualMachines.service";
import getEnv from "utils/getEnv";

import { Tooltip, useDisclosure } from "@chakra-ui/react";
import {
  faArrowsRotate,
  faCheck,
  faClone,
  faFloppyDisk,
  faPlay,
  faQuestion,
  faRocket,
  faStop,
  faTrash,
  faUpRightFromSquare,
  faXmark,
  IconDefinition,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const GenericButton = ({
  color = "brand",
  handleClick,
  icon,
  displayConditions,
  disabledConditions,
  translationKey,
  dataTestId,
}: {
  color?: string;
  handleClick: () => void;
  icon: IconDefinition;
  displayConditions: boolean;
  disabledConditions: boolean;
  translationKey: string;
  dataTestId?: string;
}) => {
  const permission = usePermissions(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE);
  const { t } = useTranslation();
  let colorClassName;
  switch (color) {
    case "red":
      colorClassName =
        "bg-red-600 hover:bg-red-700 dark:bg-red-500 dark:hover:bg-red-400 disabled:hover:bg-red-600 disabled:hover:dark:bg-red-500";
      break;
    case "orange":
      colorClassName =
        "bg-orange-500 hover:bg-orange-600 dark:hover:bg-orange-400 disabled:hover:bg-orange-500";
      break;
    case "green":
      colorClassName =
        "bg-green-500 hover:bg-green-600 dark:hover:bg-green-400 disabled:hover:bg-green-500";
      break;
    default:
      colorClassName =
        "bg-brand-500 hover:bg-brand-600 dark:bg-brand-400 dark:hover:bg-brand-300";
      break;
  }
  return (
    <>
      {displayConditions && (
        <Tooltip
          className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 lg:hidden"
          label={t(translationKey)}
          placement="top"
          openDelay={500}
          aria-label={t(translationKey)}
        >
          <button
            className={`btn-primary-base truncate
             ${colorClassName}
             disabled:opacity-25 disabled:dark:opacity-40 disabled:cursor-not-allowed `}
            onClick={handleClick}
            disabled={!permission || disabledConditions}
            data-test-id={dataTestId}
          >
            <FontAwesomeIcon className="mr-2" icon={icon} />
            {t(translationKey)}
          </button>
        </Tooltip>
      )}
    </>
  );
};

const DeleteButton = ({ vm }: { vm: IVirtualMachine }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();
  const userCanPatch = useCanPatchVm(vm);
  const deleteVm = async () => {
    await patchVirtualMachineArchive({
      id: vm.id,
      isArchived: true,
    });
  };
  const ConfirmationModal = () => (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("vm.tabs.actions.delete_modal.title")}
      </h1>
      <p className="mt-3 text-red-500 dark:text-red-400 text-lg font-bold w-[40vw]">
        {t("vm.tabs.actions.delete_modal.warning_text")}
      </p>
      <div className="flex justify-end pt-8 gap-7">
        <button
          className="btn-primary-base bg-red-600 hover:bg-red-700 dark:bg-red-500 dark:hover:bg-red-400 disabled:hover:bg-red-600 disabled:hover:dark:bg-red-500"
          onClick={() => {
            onClose();
            deleteVm();
          }}
        >
          {t("vm.tabs.actions.button.delete")}
        </button>
        <button
          className="border-[1px] border-red-500 hover:border-red-600 dark:border-red-400 dark:hover:text-red-300 text-red-500 hover:text-red-600 dark:text-red-400 px-5 py-3 text-base font-medium rounded-xl transition duration-200"
          onClick={onClose}
        >
          {t("button.cancel")}
        </button>
      </div>
    </>
  );
  return (
    <>
      <GenericButton
        color="red"
        handleClick={onOpen}
        icon={faTrash}
        displayConditions={
          vm.approval === VirtualMachineApprovalEnum.NONE &&
          !vm.isArchived &&
          [
            VirtualMachineStatusEnum.ONLINE,
            VirtualMachineStatusEnum.OFFLINE,
          ].includes(vm.status)
        }
        disabledConditions={
          [
            VirtualMachineStatusEnum.STARTING,
            VirtualMachineStatusEnum.STOPPING,
          ].includes(vm.status) || !userCanPatch
        }
        translationKey="vm.tabs.actions.button.delete"
      />
      <Modal
        disclosure={{
          isOpen,
          onClose,
        }}
      >
        <ConfirmationModal />
      </Modal>
    </>
  );
};

const RedirectButton = ({
  href,
  icon,
  displayConditions,
  translationKey,
}: {
  href: string;
  icon: IconDefinition;
  displayConditions: boolean;
  translationKey: string;
}) => {
  const permission = usePermissions(PermissionNameEnum.UPDATE_VIRTUAL_MACHINE);
  const { t } = useTranslation();

  return (
    <>
      {displayConditions && permission && (
        <Tooltip
          className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 lg:hidden"
          label={t(translationKey)}
          placement="top"
          openDelay={500}
          aria-label={t(translationKey)}
        >
          <Link
            className="btn-primary truncate flex items-center"
            to={href}
            target="_blank"
          >
            <FontAwesomeIcon className="mr-2" icon={icon} />
            {t(translationKey)}
          </Link>
        </Tooltip>
      )}
    </>
  );
};

const ApprovalApproveButton = ({ vm }: { vm: IVirtualMachine }) => {
  const userCanPatch = useCanPatchVm(vm);

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <GenericButton
        color="green"
        handleClick={onOpen}
        icon={faCheck}
        displayConditions={
          vm.approval === VirtualMachineApprovalEnum.IN_PROGRESS &&
          !vm.isArchived
        }
        disabledConditions={!userCanPatch}
        translationKey="vm.tabs.actions.button.approval_approve"
      />
      <Modal
        disclosure={{
          isOpen: isOpen,
          onClose: onClose,
        }}
      >
        <ApproveForm vm={vm} onClose={onClose} />
      </Modal>
    </>
  );
};

const SaveButton = ({ vm }: { vm: IVirtualMachine }) => {
  const {
    isOpen: isOpenConfirmationModal,
    onOpen: onOpenConfirmationModal,
    onClose: onCloseConfirmationModal,
  } = useDisclosure();
  const {
    isOpen: isOpenSaveFormModal,
    onOpen: onOpenSaveFormModal,
    onClose: onCloseSaveFormModal,
  } = useDisclosure();
  const { t } = useTranslation();

  const ConfirmationModal = () => (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("vm.tabs.actions.save.modal.title")}
      </h1>
      <p className="mt-3 text-red-500 dark:text-red-400 text-lg font-bold w-[40vw]">
        {t("vm.tabs.actions.save.modal.warning_text")}
      </p>
      <p className="mt-1 text-red-500 dark:text-red-400 text-lg font-bold w-[40vw]">
        {t("vm.tabs.actions.save.modal.warning_text_2")}
      </p>
      <div className="flex pt-8 gap-7">
        <button
          className="btn-primary"
          onClick={() => {
            onCloseConfirmationModal();
            onOpenSaveFormModal();
          }}
        >
          {t("vm.tabs.actions.save.modal.continue")}
        </button>
        <button
          className="border-[1px] border-red-500 hover:border-red-600 dark:border-red-400 dark:hover:text-red-300 text-red-500 hover:text-red-600 dark:text-red-400 px-5 py-3 text-base font-medium rounded-xl transition duration-200"
          onClick={onCloseConfirmationModal}
        >
          {t("vm.tabs.actions.save.modal.button.cancel")}
        </button>
      </div>
    </>
  );
  return (
    <>
      <GenericButton
        handleClick={
          vm.approval === VirtualMachineApprovalEnum.IN_PROGRESS
            ? onOpenConfirmationModal
            : onOpenSaveFormModal
        }
        icon={faFloppyDisk}
        displayConditions={
          [
            VirtualMachineStatusEnum.ONLINE,
            VirtualMachineStatusEnum.OFFLINE,
          ].includes(vm.status) && !vm.isArchived
        }
        disabledConditions={false}
        translationKey="vm.tabs.actions.button.save"
      />
      <Modal
        disclosure={{
          isOpen: isOpenConfirmationModal,
          onClose: onCloseConfirmationModal,
        }}
      >
        <ConfirmationModal />
      </Modal>
      <Modal
        disclosure={{
          isOpen: isOpenSaveFormModal,
          onClose: onCloseSaveFormModal,
        }}
      >
        <SaveForm vm={vm} onClose={onCloseSaveFormModal} />
      </Modal>
    </>
  );
};

const DuplicateButton = ({ vm }: { vm: IVirtualMachine }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const userCanDuplicate = usePermissions(
    PermissionNameEnum.CREATE_VIRTUAL_MACHINE,
  );

  return (
    <>
      <GenericButton
        handleClick={onOpen}
        icon={faClone}
        displayConditions={[
          VirtualMachineStatusEnum.ONLINE,
          VirtualMachineStatusEnum.OFFLINE,
        ].includes(vm.status)}
        disabledConditions={!userCanDuplicate}
        translationKey="vm.tabs.actions.button.duplicate"
        dataTestId="duplicateVmButton"
      />
      <Modal
        disclosure={{
          isOpen,
          onClose,
        }}
      >
        <VmEditForm vmOrSaveId={vm.id} onClose={onClose} from="duplicate" />
      </Modal>
    </>
  );
};

const DeployButton = ({ vm }: { vm: IVirtualMachine }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const userCanPatch = useCanPatchVm(vm);
  const canCreateDeploy = usePermissions(PermissionNameEnum.CREATE_DEPLOY);

  return (
    <>
      <GenericButton
        handleClick={onOpen}
        icon={faRocket}
        displayConditions={
          [
            VirtualMachineStatusEnum.ONLINE,
            VirtualMachineStatusEnum.OFFLINE,
          ].includes(vm.status) &&
          vm.approval === VirtualMachineApprovalEnum.APPROVED
        }
        disabledConditions={!userCanPatch || !canCreateDeploy}
        translationKey="vm.tabs.actions.button.deploy"
      />
      <Modal
        disclosure={{
          isOpen,
          onClose,
        }}
      >
        <DeployForm vm={vm} onClose={onClose} />
      </Modal>
    </>
  );
};
export default function VmButtons({ vm }: { vm: IVirtualMachine }) {
  const userCanPatch = useCanPatchVm(vm);
  const { setReload: setReloadHistory } = useContext(LogsContext);
  const { setReload: setReloadVmSaves } = useContext(VmSavesContext);

  const startVmEnums = [
    VirtualMachineStatusEnum.OFFLINE,
    VirtualMachineStatusEnum.INSTALLING_PROJECT,
    VirtualMachineStatusEnum.STARTING,
  ];
  const stopVmEnums = [
    VirtualMachineStatusEnum.ONLINE,
    VirtualMachineStatusEnum.STOPPING,
  ];

  const startVm = async () => {
    await patchVirtualMachineStart({
      id: vm.id,
    });
  };
  const stopVm = async () => {
    await patchVirtualMachineStop({
      id: vm.id,
    });
  };
  const startStopVm = async () => {
    if (startVmEnums.includes(vm.status)) {
      await startVm();
    } else {
      await stopVm();
    }
    setReloadHistory(true);
  };

  const askApproval = async () => {
    await patchVirtualMachineApproval({
      id: vm.id,
    });
    setReloadHistory(true);
    setReloadVmSaves(true);
  };

  const rejectVm = async () => {
    await patchVirtualMachineApprovalReject({
      id: vm.id,
    });
    setReloadHistory(true);
  };

  const refreshProject = async () => {
    await patchVirtualMachineRefreshProject({
      id: vm.id,
    });
  };

  return (
    <div className="flex gap-2 flex-wrap sm:flex-nowrap mb-4">
      <DeleteButton vm={vm} />
      <GenericButton
        color={startVmEnums.includes(vm.status) ? "green" : "red"}
        handleClick={startStopVm}
        icon={startVmEnums.includes(vm.status) ? faPlay : faStop}
        displayConditions={
          !vm.isArchived && startVmEnums.concat(stopVmEnums).includes(vm.status)
        }
        disabledConditions={
          [
            VirtualMachineStatusEnum.INSTALLING_PROJECT,
            VirtualMachineStatusEnum.STARTING,
            VirtualMachineStatusEnum.STOPPING,
          ].includes(vm.status) ||
          vm.approval === VirtualMachineApprovalEnum.IN_PROGRESS ||
          !userCanPatch
        }
        translationKey={
          startVmEnums.includes(vm.status)
            ? "vm.tabs.actions.button.start"
            : "vm.tabs.actions.button.stop"
        }
      />
      <GenericButton
        handleClick={askApproval}
        icon={faQuestion}
        displayConditions={
          vm.approval === VirtualMachineApprovalEnum.NONE &&
          !vm.isArchived &&
          [
            VirtualMachineStatusEnum.ONLINE,
            VirtualMachineStatusEnum.OFFLINE,
          ].includes(vm.status)
        }
        disabledConditions={
          [
            VirtualMachineApprovalEnum.APPROVED,
            VirtualMachineApprovalEnum.IN_PROGRESS,
          ].includes(vm.approval) || !userCanPatch
        }
        translationKey="vm.tabs.actions.button.approval"
      />
      <ApprovalApproveButton vm={vm} />
      <GenericButton
        color="red"
        handleClick={rejectVm}
        icon={faXmark}
        displayConditions={
          vm.approval === VirtualMachineApprovalEnum.IN_PROGRESS &&
          !vm.isArchived
        }
        disabledConditions={!userCanPatch}
        translationKey="vm.tabs.actions.button.approval_reject"
      />
      <SaveButton vm={vm} />
      <DuplicateButton vm={vm} />
      <GenericButton
        color="orange"
        handleClick={refreshProject}
        icon={faArrowsRotate}
        displayConditions={
          [
            VirtualMachineStatusEnum.PROJECT_ERROR,
            VirtualMachineStatusEnum.CHECKING_PROJECT,
          ].includes(vm.status) && !vm.isArchived
        }
        disabledConditions={
          vm.status === VirtualMachineStatusEnum.CHECKING_PROJECT ||
          !userCanPatch
        }
        translationKey="vm.tabs.actions.button.refresh_project"
      />
      <RedirectButton
        href={`http://${vm.id}.${getEnv("PUBLIC_BASE_URL")}`}
        icon={faUpRightFromSquare}
        displayConditions={vm.status === VirtualMachineStatusEnum.ONLINE}
        translationKey="vm.tabs.actions.button.access_vm"
      />
      <DeployButton vm={vm} />
    </div>
  );
}
