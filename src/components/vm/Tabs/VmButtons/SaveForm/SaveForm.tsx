import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { VmSavesContext } from "contexts/vmSaves.context";
import { FormikHelpers, useFormik } from "formik";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";
import { IPostVirtualMachineSavesRequest } from "services/virtualMachineSaves/interfaces/postVirtualMachineSave.interface";
import { postVirtualMachineSaves } from "services/virtualMachineSaves/virtualMachineSaves.service";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

export default function SaveForm({
  vm,
  onClose,
}: {
  vm: IVirtualMachine;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const { setReload } = useContext(VmSavesContext);

  const validationSchema = Yup.object({
    name: Yup.string()
      .min(3, t("vm.saves.creation.validation.name.length"))
      .required(t("vm.saves.creation.validation.name.required")),
  });
  const formik = useFormik({
    initialValues: {
      name: "",
      description: "",
    } as IPostVirtualMachineSavesRequest,
    validationSchema: validationSchema,
    onSubmit: async (
      values: IPostVirtualMachineSavesRequest,
      { setSubmitting }: FormikHelpers<IPostVirtualMachineSavesRequest>,
    ) => {
      setSubmitting(false);
      const result = await postVirtualMachineSaves({ vmId: vm.id, ...values });
      if (result) {
        onClose();
        if (location.pathname.split("/")[3] === "save") {
          setReload(true);
        }
      }
    },
  });

  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("vm.saves.creation.title")}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <FormControl isInvalid={!!formik.errors.name}>
          <InputField
            id="name"
            label={t("vm.saves.creation.label.name")}
            extra="mt-3"
            type="text"
            placeholder={t("vm.saves.creation.placeholder.name")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="name"
            value={formik.values.name}
            state={formik.errors.name ? "error" : undefined}
            required={true}
            autofocus={true}
          />
          {formik.errors.name && (
            <FormErrorMessageText text={formik.errors.name} />
          )}
        </FormControl>
        <FormControl>
          <InputField
            label={t("vm.saves.creation.label.description")}
            id="description"
            extra="mt-3"
            type="text"
            placeholder={t("vm.saves.creation.placeholder.description")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="description"
            value={formik.values.description}
          />
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5 "
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
