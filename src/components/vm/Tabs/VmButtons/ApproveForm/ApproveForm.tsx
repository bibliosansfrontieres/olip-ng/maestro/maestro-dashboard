import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { VmContext } from "contexts/vm.context";
import { FormikHelpers, useFormik } from "formik";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";
import { IPatchVirtualMachineApprovalApproveRequest } from "services/virtualMachines/interfaces/patchVirtualMachine.interface";
import { patchVirtualMachineApprovalApprove } from "services/virtualMachines/virtualMachines.service";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

export default function ApproveForm({
  vm,
  onClose,
}: {
  vm: IVirtualMachine;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const { setReload } = useContext(VmContext);
  const validationSchema = Yup.object({
    name: Yup.string().required(
      t("vm.approval.creation.validation.name.required"),
    ),
  });
  const formik = useFormik({
    initialValues: {
      name: "",
    } as IPatchVirtualMachineApprovalApproveRequest,
    validationSchema: validationSchema,
    onSubmit: async (
      values: IPatchVirtualMachineApprovalApproveRequest,
      {
        setSubmitting,
      }: FormikHelpers<IPatchVirtualMachineApprovalApproveRequest>,
    ) => {
      setSubmitting(false);
      const result = await patchVirtualMachineApprovalApprove({
        id: vm.id,
        ...values,
      });
      if (result) {
        setReload(true);
        onClose();
      }
    },
  });

  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("vm.approval.creation.title")}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <FormControl isInvalid={!!formik.errors.name}>
          <InputField
            id="name"
            label={t("vm.approval.creation.label.name")}
            extra="mt-3"
            type="text"
            placeholder={t("vm.approval.creation.placeholder.name")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="name"
            value={formik.values.name}
            state={formik.errors.name ? "error" : undefined}
            required={true}
            autofocus={true}
          />
          {formik.errors.name && (
            <FormErrorMessageText text={formik.errors.name} />
          )}
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5 "
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
