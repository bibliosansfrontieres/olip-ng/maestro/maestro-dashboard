import { useEffect, useState } from "react";
import AsyncCreateMultiSelectForm from "common/Form/AsyncCreateMultiSelectForm";
import AsyncSelectForm from "common/Form/AsyncSelectForm";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { VirtualMachineSaveTypeEnum } from "enums/virtualMachineSaveType.enum";
import { Form, Formik, FormikHelpers, FormikProps } from "formik";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { IVirtualMachineSave } from "interfaces/virtualMachineSave.interface";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { postDeploy } from "services/deploys/deploys.service";
import { getFqdns } from "services/fqdnMasks/fqdns.service";
import { getLabels, postLabel } from "services/labels/labels.service";
import { getVirtualMachineSaves } from "services/virtualMachineSaves/virtualMachineSaves.service";
import * as Yup from "yup";

import { FormControl, FormLabel, SkeletonText } from "@chakra-ui/react";

interface SelectValue {
  value: number;
  label: string;
}
interface IProps {
  fqdn: SelectValue;
  labels: SelectValue[];
  devicesCount: number;
}

export default function DeployForm({
  vm,
  onClose,
}: {
  vm: IVirtualMachine;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const initialValues: IProps = {
    fqdn: null as unknown as SelectValue,
    labels: null as unknown as SelectValue[],
    devicesCount: "" as unknown as number,
  };

  const [vmSave, setVmSave] = useState(
    undefined as unknown as IVirtualMachineSave,
  );
  useEffect(() => {
    const getVmSave = async () => {
      const result = await getVirtualMachineSaves({
        vmId: vm.id,
        types: VirtualMachineSaveTypeEnum.APPROVE,
      });
      if (result) {
        setVmSave(result.data[0]);
      }
    };
    getVmSave();
  }, [vm]);

  const validationSchema = Yup.object({
    fqdn: Yup.object({
      value: Yup.number()
        .min(1, t("deploy.validation.fqdn.required"))
        .required(t("deploy.validation.fqdn.required")),
    }).required(t("deploy.validation.fqdn.required")),
    devicesCount: Yup.number()
      .min(1, t("deploy.validation.devicesCount.min"))
      .max(3, t("deploy.validation.devicesCount.max"))
      .required(t("deploy.validation.devicesCount.required")),
    labels: Yup.array()
      .min(1, t("deploy.validation.labels.required"))
      .required(t("deploy.validation.labels.required")),
  });
  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("deploy.form.title")}
      </h1>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={async (
          values: IProps,
          { setSubmitting }: FormikHelpers<IProps>,
        ) => {
          const deployParams = {
            virtualMachineSaveId: vmSave?.id,
            fqdnMaskId: values.fqdn.value,
            devicesCount: values.devicesCount,
            labelIds: values.labels.map((label) => label.value),
          };
          const result = await postDeploy(deployParams);
          if (result) {
            onClose();
            navigate(`deploy`);
          }
          setSubmitting(false);
        }}
      >
        {(props: FormikProps<IProps>) => (
          <Form>
            <div className="text-sm mt-3 ml-3 mb-2 flex items-center flex-row">
              <FormLabel>{t("deploy.form.vm_save")}</FormLabel>
              {vmSave ? (
                <p className="italic mb-2">{vmSave.name}</p>
              ) : (
                <SkeletonText
                  width="150px"
                  skeletonHeight="3"
                  className="mb-2 ml-2"
                  noOfLines={1}
                />
              )}
            </div>
            <FormControl isInvalid={!!props.errors.fqdn}>
              <AsyncSelectForm
                multiGet={() => getFqdns({ take: 1000 })}
                asyncName="fqdn"
                required={true}
                state={props.errors.fqdn ? "error" : undefined}
              />
              {props.errors.fqdn && (
                <FormErrorMessageText
                  text={
                    typeof props.errors.fqdn === "string"
                      ? props.errors.fqdn
                      : props.errors.fqdn.value!
                  }
                />
              )}
            </FormControl>

            <FormControl isInvalid={!!props.errors.devicesCount}>
              <InputField
                id="devicesCount"
                label={t("deploy.form.devicesCount.label")}
                extra="mt-3"
                type="number"
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                name="devicesCount"
                placeholder={t("deploy.form.devicesCount.placeholder")}
                value={props.values.devicesCount}
                state={props.errors.devicesCount ? "error" : undefined}
                required={true}
              />
              {props.errors.devicesCount && (
                <FormErrorMessageText text={props.errors.devicesCount} />
              )}
            </FormControl>
            <FormControl isInvalid={!!props.errors.labels}>
              <AsyncCreateMultiSelectForm
                required={true}
                postNewValue={postLabel}
                multiGet={getLabels}
                asyncName="labels"
                state={props.errors.labels ? "error" : undefined}
              />
              {props.errors.labels && (
                <FormErrorMessageText
                  text={
                    typeof props.errors.labels === "string"
                      ? props.errors.labels
                      : ""
                  }
                />
              )}
            </FormControl>
            <button
              type="submit"
              className="btn-primary mt-5 "
              // disabled={
              //   props.isSubmitting ||
              //   !props.isValid ||
              //   JSON.stringify(props.initialValues) ===
              //     JSON.stringify(props.values)
              // }
            >
              {t("button.submit")}
            </button>
          </Form>
        )}
      </Formik>
    </>
  );
}
