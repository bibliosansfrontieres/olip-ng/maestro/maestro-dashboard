import { BadgeClickableTable } from "common/TableHelper/BadgeClickable";
import { ColumnHeaderHelper } from "common/TableHelper/ColumnHeaderHelper";
import { InformationClickableTable } from "common/TableHelper/InformationClickable";
import { ValueCell } from "common/TableHelper/ValueCell";
import { VirtualMachineHistoryTableColumEnum } from "enums/columns/virtualMachineHistoryTableColumn.enum";
import { LogOrderByEnum } from "enums/orderBy/logOrderBy.enum";
import { TableNamesEnum } from "enums/table.enum";
import { ILog } from "interfaces/log.interface";
import { useTranslation } from "react-i18next";
import { colorLogLevel } from "utils/colorEnum";
import filterDate from "utils/filterDate";

import { Table, TableContainer, Tbody, Td, Thead, Tr } from "@chakra-ui/react";

export default function HistoryTable({ tableData }: { tableData: ILog[] }) {
  const { t } = useTranslation();
  return (
    <TableContainer pt={5} data-test-id="vmHistoryTable">
      <Table variant="unstyled">
        <Thead>
          <Tr>
            <ColumnHeaderHelper<
              typeof VirtualMachineHistoryTableColumEnum,
              typeof LogOrderByEnum
            >
              tableName={TableNamesEnum.HISTORY}
              headers={VirtualMachineHistoryTableColumEnum}
              orderByEnum={LogOrderByEnum}
            />
          </Tr>
        </Thead>
        <Tbody
          data-test-id="vmHistoryTableBody"
          className="text-sm font-bold text-navy-700 dark:text-white"
        >
          {tableData.map((log) => {
            return (
              <Tr
                key={`Tr-${log.id}`}
                className="h-16"
                data-test-id="vmHistoryTableRow"
              >
                <Td>
                  <p
                    data-test-id="vmHistoryTableDate"
                    data-test-value={log.createdAt}
                  >
                    {filterDate(log.createdAt)}
                  </p>
                </Td>
                <Td>
                  <InformationClickableTable
                    value={log.user.name}
                    queryParamName="userIds"
                    filterValue={log.user.id}
                  />
                </Td>
                <Td>
                  <InformationClickableTable
                    dataTestValue={log.action}
                    value={t(`enums.log_action.${log.action.toLowerCase()}`)}
                    queryParamName="actions"
                    filterValue={log.action}
                  />
                </Td>
                <Td>
                  {log?.entity && (
                    <InformationClickableTable
                      dataTestValue={log.entity}
                      value={t(`enums.log_entity.${log.entity.toLowerCase()}`)}
                      queryParamName="entities"
                      filterValue={log.entity}
                    />
                  )}
                </Td>
                <Td>
                  <ValueCell value={log.values} />
                </Td>
                <Td>
                  <BadgeClickableTable
                    colorFunction={colorLogLevel}
                    enumValue={log.level}
                    queryParamName="levels"
                    translateKeysEnumName="log_level"
                  />
                </Td>
                <Td>
                  <p data-test-id="vmHistoryTableDescription">
                    {log.description}
                  </p>
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
