import ContextCard from "components/context/ContextCard";
import { IApplication } from "interfaces/application.interface";
import { IContext } from "interfaces/context.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { useOutletContext } from "react-router-dom";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import { useCanPatchVm } from "src/hooks/usePermission";

export default function ContextTab({
  applications,
  defaultContext,
}: {
  applications: IPageResponse<IApplication>;
  defaultContext: IContext;
}) {
  const vm = useOutletContext() as IGetVirtualMachineResponse;
  const canPatchVm = useCanPatchVm(vm);

  return (
    <ContextCard
      applications={applications}
      context={vm.context}
      defaultContext={defaultContext}
      isDisabled={vm.isArchived || !canPatchVm}
    />
  );
}
