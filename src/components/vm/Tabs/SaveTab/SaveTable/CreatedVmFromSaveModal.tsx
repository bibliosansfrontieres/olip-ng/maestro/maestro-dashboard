import Modal from "components/modal/Modal";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import {
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tooltip,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";

export default function CreatedVmFromSaveModal({
  saveName,
  createdVirtualMachines,
}: {
  saveName: string;
  createdVirtualMachines: IVirtualMachine[];
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();
  const headers = ["id", "title", "actions"];
  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 pl-3 max-w-32"
        label={t("vm.save_tab.created_vm_from_save_modal")}
        placement="top"
        openDelay={500}
        aria-label={t("vm.save_tab.created_vm_from_save_modal")}
      >
        <button
          onClick={onOpen}
          className="text-md w-8 h-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-lg text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          {createdVirtualMachines.length}
        </button>
      </Tooltip>
      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="mb-[20px] font-bold text-2xl dark:text-white text-navy-700">
            {t("vm.save_tab.created_vm_from_save_modal_title")}
            {saveName}
          </h1>
          <TableContainer>
            <Table variant="unstyled">
              <Thead>
                <Tr>
                  {headers.map((header) => {
                    return (
                      <Th
                        key={`Th-${header}`}
                        className="border-b-[1px] border-gray-200 pt-4 pb-2"
                      >
                        {header !== "actions" && (
                          <p className="text-sm font-bold text-gray-600 dark:text-white">
                            {t(`vm.save_tab.table.${header}`)}
                          </p>
                        )}
                      </Th>
                    );
                  })}
                </Tr>
              </Thead>
              <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
                {createdVirtualMachines.map((vm) => {
                  return (
                    <Tr key={`Tr-${vm.id}`} className="h-16">
                      <Td className="min-w-[50px]">
                        <Link
                          className="underline"
                          to={`/virtual-machines/${vm.id}`}
                        >
                          {vm.id}
                        </Link>
                      </Td>
                      <Td className="min-w-24 max-w-52">
                        <p className="truncate">{vm.title}</p>
                      </Td>
                      <Td></Td>
                    </Tr>
                  );
                })}
              </Tbody>
            </Table>
          </TableContainer>
        </>
      </Modal>
    </>
  );
}
