import { useEffect, useState } from "react";
import Modal from "components/modal/Modal";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import { useCanPatchVm, usePermissions } from "hooks/usePermission";
import { useTranslation } from "react-i18next";
import { useOutletContext, useRevalidator } from "react-router-dom";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import { patchVirtualMachineImport } from "services/virtualMachines/virtualMachines.service";

import { useDisclosure } from "@chakra-ui/hooks";
import { Tooltip } from "@chakra-ui/react";
import { faFileImport } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ImportFromSaveModal({ saveId }: { saveId: string }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isImporting, setIsImporting] = useState(false);
  const { t } = useTranslation();
  const revalidator = useRevalidator();
  const vm = useOutletContext() as IGetVirtualMachineResponse;
  const userCanPatch = useCanPatchVm(vm);
  useEffect(() => {
    setIsImporting(false);
  }, []);
  const onSave = async () => {
    setIsImporting(true);
    const result = await patchVirtualMachineImport({ id: vm.id, saveId });
    if (result) {
      onClose();
      revalidator.revalidate();
    }
  };
  const disabledImport =
    ![
      VirtualMachineStatusEnum.ONLINE,
      VirtualMachineStatusEnum.OFFLINE,
    ].includes(vm.status) || vm.isArchived;
  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 pl-3 max-w-32"
        label={t("vm.save_tab.import_from_save_modal.title")}
        placement="top"
        openDelay={500}
        aria-label={t("vm.save_tab.import_from_save_modal.title")}
      >
        <button
          onClick={onOpen}
          disabled={
            !usePermissions([PermissionNameEnum.CREATE_VIRTUAL_MACHINE]) ||
            !userCanPatch ||
            disabledImport
          }
          className="bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-4 w-4" icon={faFileImport} />
        </button>
      </Tooltip>

      <Modal disclosure={{ isOpen, onClose }}>
        <>
          {!isImporting ? (
            <>
              <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
                {t("vm.save_tab.import_from_save_modal.header")}
              </h1>
              <p className="mt-1 text-red-500 dark:text-red-400 text-lg font-bold w-[40vw]">
                {t("vm.save_tab.import_from_save_modal.warning_text")}
              </p>
            </>
          ) : (
            <p>{t("vm.save_tab.import_from_save_modal.importing_text")}</p>
          )}
          <div className="flex pt-8 gap-7">
            <button
              className="btn-primary"
              onClick={onSave}
              disabled={!userCanPatch || isImporting}
            >
              {t("vm.save_tab.import_from_save_modal.continue")}
            </button>
            <button
              className="border-[1px] border-red-500 hover:border-red-600 dark:border-red-400 dark:hover:text-red-300 text-red-500 hover:text-red-600 dark:text-red-400 px-5 py-3 text-base font-medium rounded-xl transition duration-200 disabled:opacity-25 disabled:cursor-not-allowed"
              onClick={onClose}
              disabled={isImporting}
            >
              {t("vm.tabs.actions.save.modal.button.cancel")}
            </button>
          </div>
        </>
      </Modal>
    </>
  );
}
