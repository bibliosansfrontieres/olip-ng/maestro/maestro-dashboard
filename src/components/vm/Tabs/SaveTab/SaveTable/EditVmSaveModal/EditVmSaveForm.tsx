import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { VmSavesContext } from "contexts/vmSaves.context";
import { FormikHelpers, useFormik } from "formik";
import { IVirtualMachineSave } from "interfaces/virtualMachineSave.interface";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { IPatchVirtualMachineSaveRequest } from "services/virtualMachineSaves/interfaces/patchVirtualMachineSave.interface";
import { patchVirtualMachineSaves } from "services/virtualMachineSaves/virtualMachineSaves.service";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

export default function EditVmSaveForm({
  save,
  onClose,
}: {
  save: IVirtualMachineSave;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const { setReload } = useContext(VmSavesContext);
  const validationSchema = Yup.object({
    name: Yup.string()
      .min(3, t("vm.saves.creation.validation.name.length"))
      .required(t("vm.saves.creation.validation.name.required")),
  });
  const formik = useFormik({
    initialValues: {
      name: save.name,
      description: save.description ?? "",
    },
    validationSchema: validationSchema,
    onSubmit: async (
      values: IPatchVirtualMachineSaveRequest,
      { setSubmitting }: FormikHelpers<IPatchVirtualMachineSaveRequest>,
    ) => {
      const initialValues = {
        name: save.name,
        description: save.description ?? "",
      };
      const modifiedValues = {} as IPatchVirtualMachineSaveRequest;
      const keyValues = Object.keys(values);
      // add only different values
      for (const key of keyValues) {
        if (
          values[key as keyof IPatchVirtualMachineSaveRequest] !==
          initialValues[key as keyof IPatchVirtualMachineSaveRequest]
        ) {
          modifiedValues[key as keyof IPatchVirtualMachineSaveRequest] =
            values[key as keyof IPatchVirtualMachineSaveRequest];
        }
      }
      if (Object.keys(modifiedValues).length === 0) {
        setSubmitting(false);
        toast.error(t("groups.patch_group.no_change"));

        return;
      }
      const result = await patchVirtualMachineSaves({
        ...modifiedValues,
        saveId: save.id,
      });
      setSubmitting(false);
      if (result) {
        setReload(true);
        onClose();
      }
    },
  });
  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("vm.saves.edition.title")}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <FormControl isInvalid={!!formik.errors.name}>
          <InputField
            id="name"
            label={t("vm.saves.creation.label.name")}
            extra="mt-3"
            type="text"
            placeholder={t("vm.saves.creation.placeholder.name")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="name"
            value={formik.values.name}
            state={formik.errors.name ? "error" : undefined}
            required={true}
            autofocus={true}
          />
          {formik.errors.name && (
            <FormErrorMessageText text={formik.errors.name} />
          )}
        </FormControl>
        <FormControl>
          <InputField
            label={t("vm.saves.creation.label.description")}
            id="description"
            extra="mt-3"
            type="text"
            placeholder={t("vm.saves.creation.placeholder.description")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="description"
            value={formik.values.description}
          />
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5 "
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
