import Modal from "components/modal/Modal";
import EditVmSaveForm from "components/vm/Tabs/SaveTab/SaveTable/EditVmSaveModal/EditVmSaveForm";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IVirtualMachineSave } from "interfaces/virtualMachineSave.interface";
import { useTranslation } from "react-i18next";

import { Tooltip, useDisclosure } from "@chakra-ui/react";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function EditVmSaveModal({
  save,
}: {
  save: IVirtualMachineSave;
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();
  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 pl-3 max-w-32"
        label={t("vm.save_tab.edit_vm_save.title")}
        placement="top"
        openDelay={500}
        aria-label={t("vm.save_tab.edit_vm_save.title")}
      >
        <button
          onClick={onOpen}
          disabled={!usePermissions([PermissionNameEnum.SAVE_VIRTUAL_MACHINE])}
          className="bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-4 w-4" icon={faPen} />
        </button>
      </Tooltip>

      <Modal disclosure={{ isOpen, onClose }}>
        <EditVmSaveForm save={save} onClose={onClose} />
      </Modal>
    </>
  );
}
