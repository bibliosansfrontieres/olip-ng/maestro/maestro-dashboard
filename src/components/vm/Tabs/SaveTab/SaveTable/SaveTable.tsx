import { ColumnHeaderHelper } from "common/TableHelper/ColumnHeaderHelper";
import { InformationClickableTable } from "common/TableHelper/InformationClickable";
import CreatedVmFromSaveModal from "components/vm/Tabs/SaveTab/SaveTable/CreatedVmFromSaveModal";
import CreateVmFromSaveModal from "components/vm/Tabs/SaveTab/SaveTable/CreateVmFromSaveModal";
import EditVmSaveModal from "components/vm/Tabs/SaveTab/SaveTable/EditVmSaveModal/EditVmSaveModal";
import ImportFromSaveModal from "components/vm/Tabs/SaveTab/SaveTable/ImportFromSaveModal";
import { VirtualMachineSaveTableColumEnum } from "enums/columns/virtualMachineSaveTableColumn.enum";
import { VirtualMachineSaveOrderByEnum } from "enums/orderBy/virtualMachineSaveOrderBy.enum";
import { TableNamesEnum } from "enums/table.enum";
import { IUser } from "interfaces/user.interface";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { IVirtualMachineSave } from "interfaces/virtualMachineSave.interface";
import { useTranslation } from "react-i18next";
import filterDate from "utils/filterDate";

import { Table, TableContainer, Tbody, Td, Thead, Tr } from "@chakra-ui/react";

export default function SaveTable({
  tableData,
}: {
  tableData: (IVirtualMachineSave & {
    user: IUser;
    createdVirtualMachines: IVirtualMachine[];
  })[];
}) {
  const { t } = useTranslation();
  return (
    <TableContainer pt={5}>
      <Table variant="unstyled">
        <Thead>
          <Tr>
            <ColumnHeaderHelper<
              typeof VirtualMachineSaveTableColumEnum,
              typeof VirtualMachineSaveOrderByEnum
            >
              tableName={TableNamesEnum.VIRTUAL_MACHINE_SAVE}
              headers={VirtualMachineSaveTableColumEnum}
              orderByEnum={VirtualMachineSaveOrderByEnum}
            />
          </Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {tableData.map((save) => {
            return (
              <Tr key={`Tr-${save.id}`} className="h-16">
                <Td>
                  <p>{save.name}</p>
                </Td>
                <Td>
                  <p>{save.description}</p>
                </Td>
                <Td>
                  <p>{filterDate(save.createdAt.toString())}</p>
                </Td>
                <Td>
                  <InformationClickableTable
                    value={save.user.name}
                    queryParamName="userIds"
                    filterValue={save.user.id}
                  />
                </Td>
                <Td>
                  <InformationClickableTable
                    value={t(`enums.vm_save_type.${save.type.toLowerCase()}`)}
                    queryParamName="types"
                    filterValue={save.type}
                  />
                </Td>
                <Td className="flex gap-2 border-white/0 py-3 pr-4 items-center">
                  <CreateVmFromSaveModal saveId={save.id} />
                  <ImportFromSaveModal saveId={save.id} />
                  {save.createdVirtualMachines.length > 0 && (
                    <CreatedVmFromSaveModal
                      saveName={save.name}
                      createdVirtualMachines={save.createdVirtualMachines}
                    />
                  )}
                  <EditVmSaveModal save={save} />
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
