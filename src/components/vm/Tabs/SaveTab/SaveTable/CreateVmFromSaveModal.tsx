import Modal from "components/modal/Modal";
import VmEditForm from "components/vm/Tabs/VmEditForm/VmEditForm";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { useTranslation } from "react-i18next";

import { useDisclosure } from "@chakra-ui/hooks";
import { Tooltip } from "@chakra-ui/react";
import { faClone } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function CreateVmFromSaveModal({ saveId }: { saveId: string }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();

  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 pl-3 max-w-32"
        label={t("vm.save_tab.create_vm_save_modal")}
        placement="top"
        openDelay={500}
        aria-label={t("vm.save_tab.create_vm_save_modal")}
      >
        <button
          onClick={onOpen}
          disabled={
            !usePermissions([PermissionNameEnum.CREATE_VIRTUAL_MACHINE])
          }
          className="bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-4 w-4" icon={faClone} />
        </button>
      </Tooltip>

      <Modal disclosure={{ isOpen, onClose }}>
        <VmEditForm vmOrSaveId={saveId} onClose={onClose} from="save" />
      </Modal>
    </>
  );
}
