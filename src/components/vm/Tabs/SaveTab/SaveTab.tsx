import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import SaveTable from "components/vm/Tabs/SaveTab/SaveTable/SaveTable";
import { VmSavesContext } from "contexts/vmSaves.context";

export default function SaveTab() {
  const { saves, isLoading, error } = useContext(VmSavesContext);
  return (
    <>
      {saves && !isLoading && !error && (
        <>
          <SaveTable tableData={saves.data} />
          <Pagination meta={saves.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !saves && <ErrorList translation="vm.get_vm_saves.error" />}
    </>
  );
}
