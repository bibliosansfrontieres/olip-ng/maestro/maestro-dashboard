import DatePicker from "common/TableHelper/DatePicker";
import { ReactMultiSelect } from "common/TableHelper/ReactMultiSelect";
import { VirtualMachineSaveTypeEnum } from "enums/virtualMachineSaveType.enum";
import AsyncMultiSelect from "src/common/TableHelper/AsyncMultiSelect";
import { getUser, getUsers } from "src/services/users/users.service";

export default function SaveTableHelper() {
  return (
    <div className="mt-2 flex flex-wrap gap-4">
      <DatePicker queryParamsName="dateRange" />
      <AsyncMultiSelect
        queryParamsName="userIds"
        singleGet={getUser}
        multiGet={getUsers}
      />
      <ReactMultiSelect<typeof VirtualMachineSaveTypeEnum>
        enumList={VirtualMachineSaveTypeEnum}
        queryParamsEnumName="types"
        translateKeysEnumName="vm_save_type"
      />
    </div>
  );
}
