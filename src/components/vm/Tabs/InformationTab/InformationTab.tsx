import { useCallback } from "react";
import Pagination from "common/Pagination";
import ProjectCard from "components/projects/projectCard/ProjectCard";
import ErrorCard from "components/vm/Tabs/InformationTab/ErrorCard";
import InformationCard from "components/vm/Tabs/InformationTab/InformationCard";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import useApi from "hooks/useApi";
import useHandleQueryParams from "hooks/useHandleQueryParams";
import { useOutletContext } from "react-router-dom";
import { IGetProjectResponse } from "services/projects/interfaces/getProject.interface";
import { getProject } from "services/projects/projects.service";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import { IGetVirtualMachineErrorsResponse } from "services/virtualMachines/interfaces/getVirtualMachineErrors.interface";
import { getVirtualMachineErrors } from "services/virtualMachines/virtualMachines.service";
import CardSkeleton from "src/common/Feedback/Skeletons/CardSkeleton";

export default function InformationTab() {
  const vm = useOutletContext() as IGetVirtualMachineResponse;
  const { result: project, isLoading: projectLoading } =
    useApi<IGetProjectResponse>(
      useCallback(async () => {
        const errorsStatus = [
          VirtualMachineStatusEnum.CHECKING_PROJECT,
          VirtualMachineStatusEnum.PROJECT_ERROR,
        ];
        if (!errorsStatus.includes(vm.status))
          return getProject({ id: vm.project.id.toString() });
      }, [vm.status, vm.project.id]),
    );
  const { queryParams } = useHandleQueryParams();

  const { result: errors } = useApi<IGetVirtualMachineErrorsResponse>(
    useCallback(async () => {
      if (vm.status === VirtualMachineStatusEnum.PROJECT_ERROR)
        return getVirtualMachineErrors({ id: vm.id, ...queryParams });
    }, [vm.status, queryParams, vm.id]),
  );

  return (
    <div className="flex flex-col gap-4">
      <InformationCard vm={vm} />
      {project && !projectLoading && <ProjectCard project={project} />}
      {projectLoading && !project && <CardSkeleton />}
      {errors && vm.status === VirtualMachineStatusEnum.PROJECT_ERROR && (
        <>
          <ErrorCard errors={errors.data} count={errors.meta.count} />
          <Pagination meta={errors.meta} />
        </>
      )}
    </div>
  );
}
