import Card from "components/card/Card";
import { VirtualMachineErrorEntityEnum } from "enums/virtualMachineEntity.enum";
import { IVirtualMachineError } from "interfaces/virtualMachineError.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { capitalize } from "utils/stringModification";

import { Table, TableContainer, Tbody, Td, Tr } from "@chakra-ui/react";
import { faArrowUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ErrorCard({
  errors,
  count,
}: {
  errors: IVirtualMachineError[];
  count: number;
}) {
  const { t } = useTranslation();

  return (
    <Card>
      <>
        <p
          className="text-navy-700 dark:text-white text-lg font-bold mb-4"
          data-test-id="errorTitle"
        >
          {t("vm.information_tab.errors.title", {
            count: count,
          })}
        </p>
        <TableContainer>
          <Table className="w-full">
            <Tbody>
              {errors.map((error, index) => (
                <Tr
                  key={`Tr-${error.id}`}
                  className={`${index % 2 === 0 ? "bg-lightPrimary dark:bg-navy-900" : "bg-white dark:bg-navy-800"}`}
                >
                  <Td className="flex gap-2 !border-0" data-test-id="errorCell">
                    <p
                      className="text-sm font-bold text-navy-700 dark:text-white"
                      data-test-id="errorText"
                    >
                      {`${capitalize(error.entity)} #${error.entityId} : ${error.message}`}
                    </p>
                    <Link
                      data-test-id="errorLink"
                      to={
                        error.entity === VirtualMachineErrorEntityEnum.PACKAGE
                          ? `https://omeka.tm.bsf-intranet.org/admin/package-manager/index/show/id/${error.entityId}`
                          : `https://omeka.tm.bsf-intranet.org/admin/items/show/${error.entityId}`
                      }
                      target="_blank"
                    >
                      <FontAwesomeIcon
                        data-test-id="errorIcon"
                        icon={faArrowUpRightFromSquare}
                      />
                    </Link>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </TableContainer>
      </>
    </Card>
  );
}
