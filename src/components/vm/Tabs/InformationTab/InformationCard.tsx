import { BadgeClickablePage } from "common/TableHelper/BadgeClickable";
import { InformationClickablePage } from "common/TableHelper/InformationClickable";
import Card from "components/card/Card";
import Modal from "components/modal/Modal";
import VmEditionForm from "components/vm/VmEdition/VmEditionForm";
import { useCanPatchVm } from "hooks/usePermission";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import { colorVmApproval, colorVmStatus } from "utils/colorEnum";
import { capitalize } from "utils/stringModification";

import { Badge, Tooltip, useDisclosure } from "@chakra-ui/react";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function InformationCard({
  vm,
}: {
  vm: IGetVirtualMachineResponse;
}) {
  const canPatchVm = useCanPatchVm(vm);
  const { t } = useTranslation();
  const vmInformationsTable = [
    {
      translation: "project_id",
      value: <p className="truncate">{vm.project.id}</p>,
      tooltip: vm.project.id,
    },
    {
      translation: "default_language_identifier",
      value: (
        <p className="truncate">
          {t(`language.${vm.defaultLanguageIdentifier}`)}
        </p>
      ),
      tooltip: vm.defaultLanguageIdentifier,
    },
    {
      translation: "status",
      value: (
        <BadgeClickablePage
          colorFunction={colorVmStatus}
          enumValue={vm.status}
          queryParamName="statuses"
          translateKeysEnumName="vm_status"
          redirectRoute="virtual-machines"
        />
      ),
      tooltip: t(`enums.vm_status.${vm.status.toLowerCase()}`),
    },
    {
      translation: "approval",
      value: (
        <BadgeClickablePage
          colorFunction={colorVmApproval}
          enumValue={vm.approval}
          queryParamName="approvals"
          translateKeysEnumName="vm_approval"
          redirectRoute="virtual-machines"
        />
      ),
      tooltip: t(`enums.vm_approval.${vm.approval.toLowerCase()}`),
    },
    {
      translation: "project_version",
      value: <p className="truncate">{vm.projectVersion}</p>,
      tooltip: vm.projectVersion,
    },
    {
      translation: "user.name_mail",
      value: (
        <InformationClickablePage
          value={vm.user.name + " - " + vm.user.mail}
          queryParamName="users"
          filterValue={vm.user.id}
          redirectRoute="virtual-machines"
        />
      ),
    },
  ];
  const { isOpen, onOpen, onClose } = useDisclosure();

  if (vm.referenceUrl && vm.referenceUrl !== "") {
    vmInformationsTable.splice(1, 0, {
      translation: "reference_url",
      value: (
        <Link className="underline" to={vm.referenceUrl} target="_blank">
          {vm.referenceUrl}
        </Link>
      ),
      tooltip: vm.referenceUrl,
    });
  }
  return (
    <Card>
      <>
        <div className="flex place-content-between items-center">
          <p className="text-navy-700 dark:text-white text-lg font-bold">
            {t("vm.information_tab.vm_informations.title")}
          </p>
          {vm.isUpdateAvailable && (
            <Badge colorScheme="red" className="mt-0.5">
              {t("vm.information_tab.vm_informations.is_update_available")}
            </Badge>
          )}
          <button
            className="border-[1px] border-brand-500 hover:border-brand-600 dark:border-brand-400 dark:hover:text-brand-300 text-brand-500 hover:text-brand-600 dark:text-brand-400 disabled:cursor-not-allowed disabled:opacity-40 p-2 rounded-lg transition duration-200 flex"
            onClick={onOpen}
            disabled={!canPatchVm || vm.isArchived}
          >
            <FontAwesomeIcon className="h-4 w-4" icon={faPen} />
          </button>
          <Modal disclosure={{ isOpen, onClose }}>
            <VmEditionForm vm={vm} onClose={onClose} />
          </Modal>
        </div>
        <div className="flex flex-col gap-1 ml-3 text-navy-700 dark:text-white">
          <p className="font-bold text-xl">{capitalize(vm.title)}</p>
          {vm.description && vm.description !== "" && (
            <span style={{ whiteSpace: "pre-line" }}>{vm.description}</span>
          )}
          <div className="flex gap-2 mt-4">
            <p className="font-bold">
              {t(`vm.information_tab.vm_informations.id`)}
            </p>
            <p>{vm.id}</p>
          </div>
          {vmInformationsTable.map((informationLine, index) => {
            return (
              <Tooltip
                className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 sm:hidden"
                label={`${t(
                  `vm.information_tab.vm_informations.${informationLine.translation}`,
                )} ${informationLine.tooltip}`}
                placement="top"
                openDelay={500}
                aria-label={t(
                  `vm.information_tab.vm_informations.${informationLine.translation}`,
                )}
                key={`tooltip-${index}`}
              >
                <div className="flex gap-2 items-center" key={index}>
                  <p className="font-bold truncate">
                    {t(
                      `vm.information_tab.vm_informations.${informationLine.translation}`,
                    )}
                  </p>
                  {informationLine.value}
                </div>
              </Tooltip>
            );
          })}
          {vm.originVirtualMachineSave && (
            <div className="flex gap-1 items-center">
              <Tooltip
                className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 sm:hidden"
                label={t("vm.information_tab.vm_informations.vm_save", {
                  saveName: vm.originVirtualMachineSave?.name,
                })}
                placement="top"
                openDelay={500}
                aria-label={t("vm.information_tab.vm_informations.vm_save", {
                  saveName: vm.originVirtualMachineSave?.name,
                })}
              >
                <p className="truncate">
                  {t("vm.information_tab.vm_informations.vm_save", {
                    saveName: vm.originVirtualMachineSave.name,
                  })}
                </p>
              </Tooltip>
              <Link
                className="underline cursor-pointer"
                to={`/virtual-machines/${vm.originVirtualMachineSave.virtualMachine.id}`}
              >
                {vm.originVirtualMachineSave.virtualMachine.id}
              </Link>
              <Tooltip
                className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 sm:hidden"
                label={vm.originVirtualMachineSave.virtualMachine.title}
                placement="top"
                openDelay={500}
                aria-label={vm.originVirtualMachineSave.virtualMachine.title}
              >
                <p className="italic truncate">
                  ({vm.originVirtualMachineSave.virtualMachine.title})
                </p>
              </Tooltip>
            </div>
          )}
        </div>
      </>
    </Card>
  );
}
