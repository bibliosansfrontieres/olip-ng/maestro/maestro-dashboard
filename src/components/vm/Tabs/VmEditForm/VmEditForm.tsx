import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { VmContext } from "contexts/vm.context";
import { FormikHelpers, useFormik } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { IPostVirtualMachineEditionRequest } from "services/virtualMachines/interfaces/postVirtualMachineEdition.interface";
import {
  postVirtualMachineDuplicate,
  postVirtualMachineFromSave,
} from "services/virtualMachines/virtualMachines.service";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

export default function VmEditForm({
  vmOrSaveId,
  onClose,
  from,
}: {
  vmOrSaveId: string;
  onClose: () => void;
  from: "save" | "duplicate";
}) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { setReload } = useContext(VmContext);

  const validationSchema = Yup.object({
    title: Yup.string().required(
      t(`vm.${from}.creation.validation.title.required`),
    ),
  });
  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      referenceUrl: "",
    } as IPostVirtualMachineEditionRequest,
    validationSchema: validationSchema,
    onSubmit: async (
      values: IPostVirtualMachineEditionRequest,
      { setSubmitting }: FormikHelpers<IPostVirtualMachineEditionRequest>,
    ) => {
      setSubmitting(false);
      let result;
      const filteredEntries = Object.entries(values).filter(
        ([key, value]) => key === "title" || value !== "",
      );
      const filteredValues = Object.fromEntries(
        filteredEntries,
      ) as IPostVirtualMachineEditionRequest;
      if (from === "duplicate") {
        result = await postVirtualMachineDuplicate({
          id: vmOrSaveId,
          ...filteredValues,
        });
      } else if (from === "save") {
        result = await postVirtualMachineFromSave({
          saveId: vmOrSaveId,
          ...filteredValues,
        });
      }
      if (result) {
        onClose();
        setReload(true);
        navigate(`/virtual-machines/${result.id}`);
      }
    },
  });

  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t(`vm.${from}.creation.title`)}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <FormControl isInvalid={!!formik.errors.title}>
          <InputField
            id="title"
            label={t(`vm.${from}.creation.label.title`)}
            extra="mt-3"
            type="text"
            placeholder={t(`vm.${from}.creation.placeholder.title`)}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="title"
            value={formik.values.title}
            state={formik.errors.title ? "error" : undefined}
            required={true}
            autofocus={true}
          />
          {formik.errors.title && (
            <FormErrorMessageText text={formik.errors.title} />
          )}
        </FormControl>
        <FormControl>
          <InputField
            label={t(`vm.${from}.creation.label.description`)}
            id="description"
            extra="mt-3"
            type="text"
            placeholder={t(`vm.${from}.creation.placeholder.description`)}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="description"
            value={formik.values.description}
          />
        </FormControl>
        <FormControl>
          <InputField
            label={t(`vm.${from}.creation.label.reference_url`)}
            id="referenceUrl"
            extra="mt-3"
            type="text"
            placeholder={t(`vm.${from}.creation.placeholder.reference_url`)}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="referenceUrl"
            value={formik.values.referenceUrl}
          />
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5 "
          data-test-id="submitButton"
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
