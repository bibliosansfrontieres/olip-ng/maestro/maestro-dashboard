import { useCallback, useEffect, useRef, useState } from "react";
import Card from "components/card/Card";
import Switch from "components/switch/Switch";
import { VirtualMachineContainerEnum } from "enums/virtualMachineContainer.enum";
import usePrevious from "hooks/usePrevious";
import { useTranslation } from "react-i18next";
import { useOutletContext } from "react-router-dom";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import {
  getVirtualMachineLogs,
  getVirtualMachineLogsDownload,
} from "services/virtualMachines/virtualMachines.service";

import { Card as ChakraCard, Select } from "@chakra-ui/react";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function LogsTab() {
  const vm = useOutletContext() as IGetVirtualMachineResponse;
  const { t } = useTranslation();
  const [logsHtml, setLogsHtml] = useState<string>("");
  const prevLogsHtml = usePrevious(logsHtml);
  const [isRefreshAutomatic, setIsRefreshAutomatic] = useState(false);
  const [container, setContainer] = useState(
    VirtualMachineContainerEnum.OLIP_API,
  );
  const scrollRef = useRef<HTMLDivElement>(null);

  // Scroll to bottom on logs change
  useEffect(() => {
    const scrollToBottom = () => {
      if (scrollRef.current) {
        scrollRef.current.scrollTop = scrollRef.current.scrollHeight;
      }
    };
    if (logsHtml !== prevLogsHtml) {
      scrollToBottom();
    }
  }, [logsHtml, prevLogsHtml]);

  const fetchLogs = useCallback(async () => {
    const response = await getVirtualMachineLogs({
      id: vm.id,
      container: container,
    });
    setLogsHtml(response?.html || "");
  }, [vm.id, container]);

  useEffect(() => {
    fetchLogs();
    let interval: undefined | ReturnType<typeof setInterval> = undefined;
    if (isRefreshAutomatic) {
      interval = setInterval(fetchLogs, 5000);
    }
    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
  }, [fetchLogs, isRefreshAutomatic]);

  const handleDownload = async () => {
    const fileName = `${vm.id}-${container}-${Date.now()}.log`;
    const result = await getVirtualMachineLogsDownload({
      id: vm.id,
      container: container,
    });
    if (result) {
      const fileUrl = window.URL.createObjectURL(result);
      const element = document.createElement("a");
      element.href = fileUrl;
      element.download = fileName;
      document.body.appendChild(element);
      element.click();
      element.remove();
    }
  };

  return (
    <Card>
      <>
        <div className="flex flex-wrap gap-10 items-center pb-10">
          <div className="sm:w-1/6 dark:text-white">
            <Select
              value={container}
              onChange={(e) =>
                setContainer(e.target.value as VirtualMachineContainerEnum)
              }
            >
              <option value={VirtualMachineContainerEnum.OLIP_API}>
                {t("enums.vm_container.olip_api")}
              </option>
              <option value={VirtualMachineContainerEnum.OLIP_DASHBOARD}>
                {t("enums.vm_container.olip_dashboard")}
              </option>
              <option value={VirtualMachineContainerEnum.HAO}>
                {t("enums.vm_container.hao")}
              </option>
            </Select>
          </div>
          <div className="flex gap-2 items-center">
            <Switch
              id="automaticRefresh"
              checked={isRefreshAutomatic}
              onChange={() => setIsRefreshAutomatic((value) => !value)}
            />
            <label
              htmlFor="automaticRefresh"
              className="text-base text-navy-700 dark:text-white"
            >
              {isRefreshAutomatic
                ? t("vm.logs_tab.switch.label.fetch_logs_stop")
                : t("vm.logs_tab.switch.label.fetch_logs_start")}
            </label>
          </div>
          <button className="btn-primary" onClick={handleDownload}>
            <FontAwesomeIcon icon={faDownload} />
            {t("vm.logs_tab.button.download_logs")}
          </button>
        </div>

        <ChakraCard
          backgroundColor="black"
          px="6"
          py="2"
          className="max-h-[40vh] md:max-h-[65vh] overflow-y-scroll"
          ref={scrollRef}
        >
          <div>
            {logsHtml && (
              <div dangerouslySetInnerHTML={{ __html: logsHtml }}></div>
            )}
          </div>
        </ChakraCard>
      </>
    </Card>
  );
}
