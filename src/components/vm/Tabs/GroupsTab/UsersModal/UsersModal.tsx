import Modal from "components/modal/Modal";
import UsersTable from "components/users/UsersTable/UsersTable";
import { IUser } from "interfaces/user.interface";
import { useTranslation } from "react-i18next";

import { useDisclosure } from "@chakra-ui/hooks";
import { Tooltip } from "@chakra-ui/react";
import { faUsers } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function UsersModal({ users }: { users: IUser[] }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();

  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 pl-3 max-w-32"
        label={t("vm.group_tab.users_modal.title")}
        placement="top"
        openDelay={500}
        aria-label={t("vm.group_tab.users_modal.title")}
      >
        <button
          onClick={() => {
            onOpen();
          }}
          className="bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-4 w-4" icon={faUsers} />
        </button>
      </Tooltip>

      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
            {t("vm.group_tab.users_modal.title")}
          </h1>
          <UsersTable tableData={users} isInVmTabs={true} />
        </>
      </Modal>
    </>
  );
}
