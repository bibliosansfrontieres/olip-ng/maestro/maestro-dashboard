import { useCallback, useContext, useEffect, useState } from "react";
import OutletSkeleton from "common/Feedback/Skeletons/OutletSkeleton";
import PaginationQuery from "common/QueryElements/PaginationQuery";
import GroupsTable from "components/groups/GroupsTable";
import Modal from "components/modal/Modal";
import { UserContext } from "contexts/user.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { RoleNameEnum } from "enums/roleName.enum";
import useApi from "hooks/useApi";
import { usePermissions } from "hooks/usePermission";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { useTranslation } from "react-i18next";
import { useOutletContext } from "react-router-dom";
import { getGroups } from "services/groups/groups.service";
import { IGetUsersIdGroupResponse } from "services/users/interfaces/getUsersIdGroup.interface";
import { getUsersIdGroups } from "services/users/users.service";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import { userCanPatchVm } from "utils/hasPermission";

import { useDisclosure } from "@chakra-ui/hooks";
import { Tooltip } from "@chakra-ui/react";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const GroupsTableHelper = ({ value }: { value: string }) => {
  const [params, setParams] = useState({} as IPageRequest);
  const [waitedValue, setWaitedValue] = useState(value);
  const { user } = useContext(UserContext);
  useEffect(() => {
    const timeOutId = setTimeout(() => {
      setWaitedValue(value);
      //Your search query and it will run the function after 1sec from user stops typing
    }, 1000);
    return () => clearTimeout(timeOutId);
  }, [value]);

  const { result, isLoading } = useApi<IGetUsersIdGroupResponse>(
    useCallback(async () => {
      if (user?.role.name === RoleNameEnum.ADMIN)
        return getGroups({ query: waitedValue });
      return getUsersIdGroups({
        query: waitedValue,
        id: user!.id,
        ...params,
      });
    }, [waitedValue, params, user]),
  );

  if (isLoading) {
    return <OutletSkeleton />;
  } else if (!result) {
    return;
  }

  return (
    <>
      <GroupsTable tableData={result.data} isInVmModal={true} />
      <PaginationQuery meta={result.meta} setParams={setParams} />
    </>
  );
};
export default function GroupsAddModal() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();
  const [value, setValue] = useState("");
  const vm = useOutletContext() as IGetVirtualMachineResponse;

  const { user } = useContext(UserContext);
  const hasPermissionAddGroup = usePermissions(
    PermissionNameEnum.UPDATE_VIRTUAL_MACHINE,
  );

  const userCanPatch = userCanPatchVm(user, vm, true);
  return (
    <>
      <Tooltip
        className={`bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 ${hasPermissionAddGroup && userCanPatch ? "hidden" : ""}`}
        label={t("vm.group_tab.button_tooltip")}
        placement="top"
        openDelay={500}
        aria-label={t("vm.group_tab.button_tooltip")}
      >
        <button
          onClick={onOpen}
          className="btn-primary"
          disabled={!hasPermissionAddGroup || !userCanPatch || vm.isArchived}
        >
          {t("vm.group_tab.button")}
        </button>
      </Tooltip>

      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
            {t("vm.group_tab.modal.title")}
          </h1>
          <div className="flex items-center rounded-full h-10 bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white mb-2">
            <p className="pl-3 pr-2 text-xl">
              <FontAwesomeIcon
                className="h-4 w-4 text-gray-400 dark:text-white"
                icon={faMagnifyingGlass}
              />
            </p>
            <input
              type="text"
              value={value}
              placeholder={t("navbar.search")}
              onChange={(e) => setValue(e.target.value)}
              className="block h-full !w-full rounded-full bg-lightPrimary text-sm font-medium text-navy-700 outline-none placeholder:!text-gray-400 dark:bg-navy-900 dark:text-white dark:placeholder:!text-white sm:w-fit"
            />
          </div>
          <GroupsTableHelper value={value} />
        </>
      </Modal>
    </>
  );
}
