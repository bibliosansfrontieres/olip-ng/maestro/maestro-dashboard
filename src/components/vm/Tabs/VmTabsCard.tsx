import { useEffect, useState } from "react";
import VmButtons from "components/vm/Tabs/VmButtons/VmButtons";
import VmTabs from "components/vm/Tabs/VmTabs";
import { LogsContextProvider } from "contexts/logs.context";
import { VmSavesContextProvider } from "contexts/vmSaves.context";
import { VirtualMachineApprovalEnum } from "enums/virtualMachineApproval.enum";
import { VirtualMachineStatusEnum } from "enums/virtualMachineStatus.enum";
import useSocketOnEvent from "hooks/useSocketOnEvent";
import { IVirtualMachine } from "interfaces/virtualMachine.interface";
import { useTranslation } from "react-i18next";
import { Outlet } from "react-router-dom";

import { Badge } from "@chakra-ui/react";

export default function VmTabsCard({ vm }: { vm: IVirtualMachine }) {
  const [socketVm, setSocketVm] = useState(vm);

  useEffect(() => {
    setSocketVm(vm);
  }, [vm]);

  const { t } = useTranslation();

  useSocketOnEvent(
    `virtualMachine.${vm.id}.status`,
    (result: { status: VirtualMachineStatusEnum }) => {
      setSocketVm({ ...socketVm, status: result.status });
    },
  );
  useSocketOnEvent(
    `virtualMachine.${vm.id}.approval`,
    (result: { approval: VirtualMachineApprovalEnum }) => {
      setSocketVm({ ...socketVm, approval: result.approval });
    },
  );

  useSocketOnEvent(
    `virtualMachine.${vm.id}.isArchived`,
    (result: { isArchived: boolean }) => {
      setSocketVm({ ...socketVm, isArchived: result.isArchived });
    },
  );
  useEffect(() => {
    setSocketVm(vm);
  }, [vm]);

  return (
    <>
      {socketVm.isArchived && (
        <Badge colorScheme="red" fontSize="lg" className="mt-0.5 mb-5">
          {t("vm.information_tab.vm_informations.is_archived")}
        </Badge>
      )}
      <LogsContextProvider>
        <VmSavesContextProvider>
          <VmButtons vm={socketVm} />
          <VmTabs />
          <div className="mt-4">
            <Outlet context={socketVm} />
          </div>
        </VmSavesContextProvider>
      </LogsContextProvider>
    </>
  );
}
