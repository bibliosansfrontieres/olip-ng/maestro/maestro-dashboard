import { IApplication } from "interfaces/application.interface";
import { getTranslation } from "utils/getTranslation";

export default function ApplicationModal({
  application,
}: {
  application: IApplication;
}) {
  return (
    <>
      <div className="text-4xl flex gap-2 justify-center items-center font-bold text-navy-700 dark:text-white mb-3">
        <img
          src={application.logo}
          alt={application.displayName}
          className="w-12 h-12"
        />
        {application.displayName}
      </div>
      <div className="w-full mb-5">
        <img
          src={application.image}
          alt={application.displayName}
          className="w-full h-full object-cover rounded-lg"
        />
      </div>
      <p className="text-xl text-center font-bold text-navy-700 dark:text-white">
        {
          getTranslation(
            "fra",
            application.applicationType.applicationTypeTranslations,
          ).label
        }
      </p>
      <p className="text-lg text-navy-700 dark:text-white mt-3">
        {
          getTranslation("fra", application.applicationTranslations)
            .shortDescription
        }
      </p>
      <p className="text-lg text-navy-700 dark:text-white mt-3">
        {
          getTranslation("fra", application.applicationTranslations)
            .longDescription
        }
      </p>
    </>
  );
}
