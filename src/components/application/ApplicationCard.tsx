import { useMemo, useState } from "react";
import ApplicationModal from "components/application/ApplicationModal/ApplicationModal";
import Modal from "components/modal/Modal";
import Switch from "components/switch/Switch";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IApplication } from "interfaces/application.interface";
import { IContext } from "interfaces/context.interface";
import { useTranslation } from "react-i18next";
import {
  deleteContextApplication,
  deleteDefaultContextApplication,
  postContextApplication,
  postDefaultContextApplication,
} from "services/contexts/contexts.service";
import { getTranslation } from "utils/getTranslation";

import { Card, CardBody, CardHeader, useDisclosure } from "@chakra-ui/react";

export default function ApplicationCard({
  application,
  context,
  defaultContext,
  isModifyAuthorized = false,
  isInDefaultContext = false,
}: {
  application: IApplication;
  context: IContext;
  defaultContext?: IContext;
  isModifyAuthorized?: boolean;
  isInDefaultContext?: boolean;
}) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [valueSwitch, setValueSwitch] = useState(
    context.applications.findIndex((app) => app.id === application.id) !== -1,
  );

  const appInDefaultContext =
    defaultContext?.applications.findIndex(
      (app) => app.id === application.id,
    ) !== -1;

  const isSameDefaultContext = useMemo(
    () => (isInDefaultContext ? true : valueSwitch === appInDefaultContext),
    [valueSwitch, appInDefaultContext, isInDefaultContext],
  );

  const userHavePermission = usePermissions(
    isInDefaultContext
      ? PermissionNameEnum.UPDATE_DEFAULT_CONTEXT
      : PermissionNameEnum.UPDATE_CONTEXT,
  );

  const onSwitchChange = async () => {
    let response = undefined;
    if (valueSwitch) {
      if (isInDefaultContext) {
        response = await deleteDefaultContextApplication({
          applicationId: application.id,
        });
      } else {
        response = await deleteContextApplication({
          applicationId: application.id,
          id: context.id.toString(),
        });
      }
    } else if (isInDefaultContext) {
      response = await postDefaultContextApplication({
        applicationId: application.id,
      });
    } else {
      response = await postContextApplication({
        applicationId: application.id,
        id: context.id.toString(),
      });
    }
    if (response !== undefined) {
      setValueSwitch((prev) => !prev);
    }
  };

  const disabled = useMemo(() => {
    if (!userHavePermission) {
      return true;
    }
    return !isModifyAuthorized;
  }, [isModifyAuthorized, userHavePermission]);

  const textValueSwitch = useMemo(() => {
    if (!disabled) {
      return t("applications.application_card.switch.label.add");
    }
    return valueSwitch
      ? t("applications.application_card.switch.label.is_added")
      : t("applications.application_card.switch.label.is_deleted");
  }, [valueSwitch, disabled, t]);

  return (
    <Card
      data-test-id="applicationCard"
      variant="outline"
      className={`p-2 h-60 !rounded-[10px] border-[1px] border-gray-200 dark:border-[#ffffff33] bg-white dark:bg-navy-800 w-72 ${!isSameDefaultContext ? "!border-red-500 dark:!border-red-400" : ""}`}
    >
      <CardHeader
        data-test-id="applicationCardHeader"
        className="text-xl flex gap-1 justify-center items-center font-bold text-navy-700 dark:text-white"
      >
        <img
          src={application.logo}
          alt={application.name}
          className="w-6 h-6"
        />
        {application.displayName}
      </CardHeader>
      <CardBody>
        <p
          className="text-base font-bold text-navy-700 dark:text-white"
          data-test-id="applicationCardDescription"
        >
          {
            getTranslation(
              "fra",
              application.applicationType.applicationTypeTranslations,
            ).label
          }
        </p>
        <p className="text-base text-navy-700 dark:text-white truncate">
          {
            getTranslation("fra", application.applicationTranslations)
              .shortDescription
          }
        </p>
        <button
          data-test-id="applicationCardSeeMore"
          onClick={onOpen}
          className="text-base text-navy-700 dark:text-white underline cursor-pointer"
        >
          {t("applications.application_card.see_more")}
        </button>
        <Modal disclosure={{ isOpen, onClose }}>
          <ApplicationModal application={application} />
        </Modal>
        <div className="flex items-center mt-3 gap-2">
          <Switch
            id="AppDefaultContext"
            checked={valueSwitch}
            onChange={onSwitchChange}
            disabled={disabled}
            dataTestId="AppDefaultContext"
          />
          <label
            htmlFor="AppDefaultContext"
            className={`text-base text-navy-700 dark:text-white ${disabled ? "!text-gray-400" : ""}`}
          >
            {textValueSwitch}
          </label>
        </div>
      </CardBody>
      <p
        className={`text-red-500 dark:!text-red-400 text-sm mt-[2px] mx-auto px-1 bg-white dark:bg-navy-800 ${isSameDefaultContext ? "hidden" : ""}`}
      >
        {t("applications.application_card.different_default_context")}
      </p>
    </Card>
  );
}
