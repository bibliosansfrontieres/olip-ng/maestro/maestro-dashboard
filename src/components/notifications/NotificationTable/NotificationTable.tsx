import { useContext, useEffect, useState } from "react";
import { ColumnHeaderHelper } from "common/TableHelper/ColumnHeaderHelper";
import { TooltipCell } from "common/TableHelper/TooltipCell";
import { ValueCell } from "common/TableHelper/ValueCell";
import { NotificationsContext } from "contexts/notifications.context";
import { NotificationTableColumnEnum } from "enums/columns/notificationTableColumn.enum";
import { NotificationOrderByEnum } from "enums/orderBy/notificationOrderBy.enum";
import { TableNamesEnum } from "enums/table.enum";
import { INotification } from "interfaces/notification.interface";
import { useTranslation } from "react-i18next";
import {
  patchAllNotifications,
  patchNotifications,
} from "services/notifications/notifications.service";
import { colorNotificationIsSeen } from "utils/colorEnum";
import filterDate from "utils/filterDate";

import {
  Badge,
  Button,
  Checkbox,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";

export default function NotificationTable({
  notifications,
}: {
  notifications: INotification[];
}) {
  const { t } = useTranslation();
  const { setReload: refresh } = useContext(NotificationsContext);

  const [checkedNotifications, setCheckedNotifications] = useState(
    Array.from(notifications, () => false),
  );
  const allChecked = checkedNotifications.every(Boolean);
  const isIndeterminate = checkedNotifications.some(Boolean) && !allChecked;

  useEffect(() => {
    setCheckedNotifications(Array.from(notifications, () => false));
  }, [notifications]);

  const readNotifications = async (isSeen: boolean) => {
    const notificationsToRead = checkedNotifications
      .map((checked, index) => (checked ? notifications[index].id : null))
      .filter((notification) => notification !== null) as string[];
    const response = await patchNotifications({
      notificationIds: notificationsToRead,
      isSeen,
    });
    if (response) {
      refresh(true);
    }
  };

  const readAllNotifications = async () => {
    const response = await patchAllNotifications();
    if (response) {
      refresh(true);
    }
  };
  return (
    <>
      <Button colorScheme="blue" onClick={readAllNotifications}>
        {t("notification.table.buttons.all_read")}
      </Button>
      <div className="mt-4 flex flex-wrap gap-4 items-center">
        <Button
          colorScheme="blue"
          isDisabled={
            !(allChecked || isIndeterminate) ||
            checkedNotifications.length === 0
          }
          onClick={() => readNotifications(true)}
        >
          {t("notification.table.buttons.read")}
        </Button>
        <Button
          colorScheme="blue"
          isDisabled={
            !(allChecked || isIndeterminate) ||
            checkedNotifications.length === 0
          }
          onClick={() => readNotifications(false)}
        >
          {t("notification.table.buttons.unread")}
        </Button>
      </div>
      <TableContainer pt={5}>
        <Table variant="unstyled">
          <Thead>
            <Tr>
              <Th className="border-b-[1px] border-gray-200 pt-4 pb-2">
                <Checkbox
                  isChecked={allChecked && checkedNotifications.length > 0}
                  isIndeterminate={isIndeterminate}
                  onChange={(e) =>
                    setCheckedNotifications((prev) =>
                      prev.map(() => e.target.checked),
                    )
                  }
                />
              </Th>
              <ColumnHeaderHelper<
                typeof NotificationTableColumnEnum,
                typeof NotificationOrderByEnum
              >
                tableName={TableNamesEnum.NOTIFICATION}
                headers={NotificationTableColumnEnum}
                orderByEnum={NotificationOrderByEnum}
              />
            </Tr>
          </Thead>
          <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
            {notifications.map((notification, index) => {
              return (
                <Tr key={`Tr-${notification.id}`} className="h-16">
                  <Td>
                    <Checkbox
                      isChecked={checkedNotifications[index]}
                      onChange={(e) =>
                        setCheckedNotifications((prev) => [
                          ...prev.slice(0, index),
                          e.target.checked,
                          ...prev.slice(index + 1),
                        ])
                      }
                    />
                  </Td>
                  <TooltipCell element={notification.message} />
                  <Td>
                    <ValueCell value={notification.values} />
                  </Td>
                  <Td>
                    <Badge
                      colorScheme={colorNotificationIsSeen(notification.isSeen)}
                    >
                      {t(
                        `notification.table.badge.${notification.isSeen ? "seen" : "unseen"}`,
                      )}
                    </Badge>
                  </Td>
                  <Td>
                    <p>{filterDate(notification.createdAt)}</p>
                  </Td>
                  <Td>
                    <p>{filterDate(notification.updatedAt)}</p>
                  </Td>
                </Tr>
              );
            })}
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}
