import { SwitchOnly } from "src/common/TableHelper/SwitchOnly";

export default function NotificationTableHelper() {
  return (
    <div className="my-4 flex flex-wrap gap-4 items-center">
      <SwitchOnly
        queryParamsName="isNotSeenOnly"
        defaultSwitchValue={false}
        defaultQueryParamValue={true}
      />
    </div>
  );
}
