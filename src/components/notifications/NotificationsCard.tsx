import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import NotificationTable from "components/notifications/NotificationTable/NotificationTable";
import { NotificationsContext } from "contexts/notifications.context";

export default function NotificationsCard() {
  const { notifications, isLoading, error } = useContext(NotificationsContext);
  return (
    <>
      {notifications && !isLoading && !error && (
        <>
          <NotificationTable notifications={notifications.data} />
          <Pagination meta={notifications.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !notifications && (
        <ErrorList translation="notification.get_notifications.error" />
      )}
    </>
  );
}
