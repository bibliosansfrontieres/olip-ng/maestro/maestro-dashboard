import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import HistoryTable from "components/vm/Tabs/HistoryTab/HistoryTable/HistoryTable";
import { LogsContext } from "contexts/logs.context";

export default function LogsCard() {
  const { logs, isLoading, error } = useContext(LogsContext);
  return (
    <>
      {logs && !isLoading && !error && (
        <>
          <HistoryTable tableData={logs.data} />
          <Pagination meta={logs.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !logs && <ErrorList translation="log.get_logs.error" />}
    </>
  );
}
