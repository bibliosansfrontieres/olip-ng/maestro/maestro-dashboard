import AsyncMultiSelect from "common/TableHelper/AsyncMultiSelect";
import DatePicker from "common/TableHelper/DatePicker";
import { ReactMultiSelect } from "common/TableHelper/ReactMultiSelect";
import { LogActionEnum } from "enums/logAction.enum";
import { LogEntityEnum } from "enums/logEntity.enum";
import { LogLevelEnum } from "enums/logLevel.enum";
import { VirtualMachineHistoryEntityEnum } from "enums/virtualMachineHistoryEntity.enum";
import { getUser, getUsers } from "services/users/users.service";

export default function LogsTableHelper({
  isInVmTabs,
}: {
  isInVmTabs?: boolean;
}) {
  return (
    <div className="mt-2 flex flex-wrap gap-4">
      <AsyncMultiSelect
        queryParamsName="userIds"
        singleGet={getUser}
        multiGet={getUsers}
      />
      <ReactMultiSelect<typeof LogActionEnum>
        enumList={LogActionEnum}
        queryParamsEnumName="actions"
        translateKeysEnumName="log_action"
      />
      {isInVmTabs ? (
        <ReactMultiSelect<typeof VirtualMachineHistoryEntityEnum>
          enumList={VirtualMachineHistoryEntityEnum}
          queryParamsEnumName="entities"
          translateKeysEnumName="log_entity"
        />
      ) : (
        <ReactMultiSelect<typeof LogEntityEnum>
          enumList={LogEntityEnum}
          queryParamsEnumName="entities"
          translateKeysEnumName="log_entity"
        />
      )}
      <ReactMultiSelect<typeof LogLevelEnum>
        enumList={LogLevelEnum}
        queryParamsEnumName="levels"
        translateKeysEnumName="log_level"
      />

      <DatePicker queryParamsName="dateRange" />
    </div>
  );
}
