import { useContext } from "react";
import microsoftLogo from "assets/img/microsoft/microsoft365.svg";
import { UserContext } from "contexts/user.context";
import { useTranslation } from "react-i18next";
import { Link, Navigate, useSearchParams } from "react-router-dom";

export default function Login({ o365Url }: { o365Url: string }): JSX.Element {
  const { t } = useTranslation();

  const { user, isLogged } = useContext(UserContext);
  const [params] = useSearchParams();
  const redirection = params.get("redirect") || undefined;

  return !isLogged || !user ? (
    <div className="mt-16 mb-16 flex h-full w-full items-center justify-center px-2 lg:mb-10 lg:items-center lg:justify-start">
      {/* Sign in section */}
      <div className="mt-[10vh] w-full max-w-full flex-col items-center lg:pl-0 xl:max-w-[420px]">
        <h4
          className="mb-2.5 text-4xl font-bold text-navy-700 dark:text-white"
          data-test-id="loginTitle"
        >
          {t("login.title")}
        </h4>
        <p className="mb-9 ml-1 text-base text-gray-600">
          {t("login.information")}
        </p>
        <div className="flex items-center">
          <img className="h-12 w-12 mr-3" src={microsoftLogo} alt="" />
          <Link
            to={o365Url}
            data-test-id="login-button"
            className="linear rounded-xl w-full bg-brand-500 py-[12px] text-center text-lg font-medium text-white transition duration-200 hover:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:text-white dark:hover:bg-brand-300 dark:active:bg-brand-200"
          >
            {t("login.button")}
          </Link>
        </div>
      </div>
    </div>
  ) : (
    <Navigate to={redirection || "/"} replace />
  );
}
