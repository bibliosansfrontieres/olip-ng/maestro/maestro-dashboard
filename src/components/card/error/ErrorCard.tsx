import { useTranslation } from "react-i18next";

export default function ErrorCard({
  error,
  extraStyle,
  ...props
}: {
  extraStyle?: string;
  error?: { statusText?: string; message?: string };
  children?: JSX.Element;
}) {
  const { t } = useTranslation();

  return (
    <div
      className={`p-5 relative flex flex-col rounded-[10px] border-[1px] border-gray-200 bg-white bg-clip-border shadow-md shadow-[#F3F3F3] dark:border-[#ffffff33] dark:!bg-navy-800 dark:text-white dark:shadow-none ${extraStyle}`}
      {...props}
    >
      <h1>Oops!</h1>
      <p>{t("error.page.something_went_wrong")}</p>
      <p>
        <i>{error?.statusText || error?.message || "Unknown error"}</i>
      </p>
    </div>
  );
}
