import AsyncMultiSelect from "common/TableHelper/AsyncMultiSelect";
import DatePicker from "common/TableHelper/DatePicker";
import { ReactMultiSelect } from "common/TableHelper/ReactMultiSelect";
import { DeployStatusEnum } from "enums/deployStatus.enum";
import { getLabel, getLabels } from "services/labels/labels.service";
import { getUser, getUsers } from "services/users/users.service";

export default function DeployTableHelper() {
  return (
    <div className="mt-2 flex flex-wrap gap-4">
      <ReactMultiSelect<typeof DeployStatusEnum>
        enumList={DeployStatusEnum}
        queryParamsEnumName="statuses"
        translateKeysEnumName="deploy_status"
      />
      <AsyncMultiSelect
        queryParamsName="userIds"
        singleGet={getUser}
        multiGet={getUsers}
      />
      <AsyncMultiSelect
        queryParamsName="labelIds"
        singleGet={getLabel}
        multiGet={getLabels}
      />
      <DatePicker queryParamsName="createdDateRange" />
    </div>
  );
}
