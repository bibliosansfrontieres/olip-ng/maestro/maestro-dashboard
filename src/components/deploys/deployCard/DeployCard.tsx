import Card from "components/card/Card";
import DeployEditForm from "components/deploys/DeployEditForm/DeployEditForm";
import DeployInformation from "components/devices/DeployInformation";
import Modal from "components/modal/Modal";
import { DeployStatusEnum } from "enums/deployStatus.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IDeployFull } from "interfaces/deploy.interface";
import { useTranslation } from "react-i18next";

import { useDisclosure } from "@chakra-ui/react";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function DeployCard({ deploy }: { deploy: IDeployFull }) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const hasPermissionToEdit = usePermissions(PermissionNameEnum.UPDATE_DEPLOY);

  return (
    <Card>
      <div className="flex flex-col gap-1 ml-3 text-base text-navy-700 dark:text-white">
        <div className="flex justify-between">
          <p className="text-xl font-bold text-navy-700 dark:text-white mx-2">
            {t("device.information.third_section")}
          </p>
          <button
            className="border-[1px] border-brand-500 hover:border-brand-600 dark:border-brand-400 dark:hover:text-brand-300 text-brand-500 hover:text-brand-600 dark:text-brand-400 disabled:cursor-not-allowed disabled:opacity-40 p-2 rounded-lg transition duration-200 flex"
            onClick={onOpen}
            disabled={
              !hasPermissionToEdit ||
              deploy.status === DeployStatusEnum.ARCHIVED
            }
          >
            <FontAwesomeIcon className="h-4 w-4" icon={faPen} />
          </button>
        </div>
        <Modal disclosure={{ isOpen, onClose }}>
          <DeployEditForm
            labels={deploy.labels}
            deployId={deploy.id}
            onClose={onClose}
          />
        </Modal>
        <DeployInformation deploy={deploy} />
      </div>
    </Card>
  );
}
