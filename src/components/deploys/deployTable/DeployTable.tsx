import { BadgeClickableTable } from "common/TableHelper/BadgeClickable";
import { BadgeTable } from "common/TableHelper/BadgeTable";
import {
  ColumnHeaderHelper,
  ColumnHeaderHelperWithoutSort,
} from "common/TableHelper/ColumnHeaderHelper";
import { InformationClickableTable } from "common/TableHelper/InformationClickable";
import LabelsModal from "components/deploys/deployTable/LabelsModal";
import {
  DeployTableColumDevicePageEnum,
  DeployTableColumEnum,
  DeployTableColumVmTabEnum,
  DeployTableInLabelColumEnum,
} from "enums/columns/deployTableColumn.enum";
import { DeployStatusEnum } from "enums/deployStatus.enum";
import { DeployOrderByEnum } from "enums/orderBy/deployOrderBy.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { TableNamesEnum } from "enums/table.enum";
import { usePermissions } from "hooks/usePermission";
import { IDeploy, IDeployFull } from "interfaces/deploy.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { colorDeployStatus } from "utils/colorEnum";
import filterDate from "utils/filterDate";

import {
  Table,
  TableContainer,
  Tbody,
  Td,
  Thead,
  Tooltip,
  Tr,
} from "@chakra-ui/react";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const columnHeaderHelper = ({
  inVmTabs = false,
  inLabelTabs = false,
  inDevicePage = false,
}: {
  inVmTabs?: boolean;
  inLabelTabs?: boolean;
  inDevicePage?: boolean;
}) => {
  if (inVmTabs) {
    return (
      <ColumnHeaderHelper<
        typeof DeployTableColumVmTabEnum,
        typeof DeployOrderByEnum
      >
        tableName={TableNamesEnum.DEPLOY}
        headers={DeployTableColumVmTabEnum}
        orderByEnum={DeployOrderByEnum}
      />
    );
  } else if (inLabelTabs) {
    return (
      <ColumnHeaderHelperWithoutSort<typeof DeployTableInLabelColumEnum>
        tableName={TableNamesEnum.DEPLOY}
        headers={DeployTableInLabelColumEnum}
      />
    );
  } else if (inDevicePage) {
    return (
      <ColumnHeaderHelperWithoutSort<typeof DeployTableColumDevicePageEnum>
        tableName={TableNamesEnum.DEPLOY}
        headers={DeployTableColumDevicePageEnum}
      />
    );
  }

  return (
    <ColumnHeaderHelper<typeof DeployTableColumEnum, typeof DeployOrderByEnum>
      tableName={TableNamesEnum.DEPLOY}
      headers={DeployTableColumEnum}
      orderByEnum={DeployOrderByEnum}
    />
  );
};
export default function DeployTable({
  data,
  inVmTabs = false,
  inLabelTabs = false,
  inDevicePage = false,
}: {
  data: IDeployFull[] | IDeploy[];
  inVmTabs?: boolean;
  inLabelTabs?: boolean;
  inDevicePage?: boolean;
}) {
  const { t } = useTranslation();
  const hasPermissionReadVM = usePermissions(
    PermissionNameEnum.READ_VIRTUAL_MACHINE,
  );
  const hasPermissionReadDevice = usePermissions(
    PermissionNameEnum.READ_DEVICE,
  );

  return (
    <TableContainer pt={5}>
      <Table variant="unstyled">
        <Thead>
          <Tr>{columnHeaderHelper({ inVmTabs, inLabelTabs, inDevicePage })}</Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {data.map((deploy) => {
            return (
              <Tr key={`Tr-${deploy.id}`} className="h-16">
                <Td>
                  <Link to={`/deploys/${deploy.id}`} className="underline">
                    {deploy.id}
                  </Link>
                </Td>
                {!inVmTabs && !inLabelTabs && (
                  <Td>
                    {hasPermissionReadVM ? (
                      <Link
                        to={`/virtual-machines/${(deploy as IDeployFull).virtualMachineSave.virtualMachine.id}`}
                        className="underline"
                      >
                        {`${(deploy as IDeployFull).virtualMachineSave.virtualMachine.title} (${(deploy as IDeployFull).virtualMachineSave.virtualMachine.id})`}
                      </Link>
                    ) : (
                      <p>{`${(deploy as IDeployFull).virtualMachineSave.virtualMachine.title} (${(deploy as IDeployFull).virtualMachineSave.virtualMachine.id})`}</p>
                    )}
                  </Td>
                )}
                {!inLabelTabs && !inDevicePage && (
                  <>
                    <Td>
                      {hasPermissionReadDevice ? (
                        <Link
                          to={`/devices/${(deploy as IDeployFull).device.id}`}
                          className="underline"
                        >
                          {(deploy as IDeployFull).device.macAddress}
                        </Link>
                      ) : (
                        <p>{(deploy as IDeployFull).device.macAddress}</p>
                      )}
                    </Td>
                    <Td className="max-w-[250px]">
                      <Tooltip
                        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
                        label={
                          <>
                            <p>
                              {t("deploy.table.fqdn_mask_tooltip.label", {
                                label: (deploy as IDeployFull).device.fqdnMask
                                  .label,
                              })}
                            </p>
                            <p>
                              {t("deploy.table.fqdn_mask_tooltip.mask", {
                                mask: (deploy as IDeployFull).device.fqdnMask
                                  .mask,
                              })}
                            </p>
                          </>
                        }
                        placement="top"
                        openDelay={500}
                        aria-label="title"
                      >
                        <p>{(deploy as IDeployFull).device.fqdnMask.label}</p>
                      </Tooltip>
                    </Td>
                  </>
                )}
                <Td>
                  {inLabelTabs || inDevicePage ? (
                    <BadgeTable
                      colorFunction={colorDeployStatus}
                      enumValue={deploy.status}
                      translateKeysEnumName="deploy_status"
                    />
                  ) : (
                    <BadgeClickableTable
                      colorFunction={colorDeployStatus}
                      enumValue={deploy.status}
                      queryParamName="statuses"
                      translateKeysEnumName="deploy_status"
                    />
                  )}
                </Td>
                {!inLabelTabs && (
                  <>
                    <Td>
                      {!inDevicePage && (
                        <InformationClickableTable
                          value={(deploy as IDeployFull).user.name}
                          queryParamName="userIds"
                          filterValue={(deploy as IDeployFull).user.id}
                        />
                      )}
                      {inDevicePage && (
                        <p>{(deploy as IDeployFull).user.name}</p>
                      )}
                    </Td>

                    <Td>
                      <LabelsModal
                        labels={(deploy as IDeployFull).labels}
                        deployId={deploy.id}
                        isArchived={deploy.status === DeployStatusEnum.ARCHIVED}
                        clickable={false}
                      />
                    </Td>
                  </>
                )}
                <Td>
                  <p>{filterDate(deploy.createdAt)}</p>
                </Td>
                {!inLabelTabs && (
                  <>
                    <Td>
                      <Link
                        to={`/deploys/${deploy.id}`}
                        className="h-8 w-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
                      >
                        <FontAwesomeIcon className="h-4 w-4" icon={faEye} />
                      </Link>
                    </Td>
                  </>
                )}
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
