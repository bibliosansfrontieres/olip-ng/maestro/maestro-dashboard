import { useEffect, useState } from "react";
import DeployEditForm from "components/deploys/DeployEditForm/DeployEditForm";
import LabelTable from "components/labels/LabelTable";
import Modal from "components/modal/Modal";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { ILabel, ILabelDeploys } from "interfaces/label.interface";
import { useTranslation } from "react-i18next";
import useHandleQueryParams from "src/hooks/useHandleQueryParams";

import { Badge, Tooltip, useDisclosure } from "@chakra-ui/react";
import { faEye, faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function LabelBadge({ label }: { label: ILabelDeploys | ILabel }) {
  const { queryParams, updateQueryParam } = useHandleQueryParams();

  return (
    <Badge
      className="hover:underline hover:cursor-pointer"
      textTransform="none"
      onClick={() => {
        let valueQueryParams = label.id.toString();
        if (queryParams.labelIds) {
          if (queryParams.labelIds.includes(label.id.toString())) {
            return;
          }
          valueQueryParams = queryParams?.labelIds + "," + label.id.toString();
        }
        updateQueryParam([
          {
            name: "labelIds",
            value: valueQueryParams,
            defaultValue: "",
          },
          {
            name: "page",
            value: "1",
            defaultValue: "1",
          },
        ]);
      }}
    >
      {label.name}
    </Badge>
  );
}
export default function LabelsModal({
  labels,
  deployId,
  isArchived,
  clickable = true,
}: {
  labels: ILabelDeploys[] | ILabel[];
  deployId: number;
  isArchived: boolean;
  clickable?: boolean;
}) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isEditting, setIsEditting] = useState(false);
  const hasPermissionToEdit = usePermissions(PermissionNameEnum.UPDATE_DEPLOY);
  useEffect(() => {
    setIsEditting(false);
  }, [isOpen]);

  return (
    <>
      <div className="flex gap-2 items-center">
        {labels.length > 0 && (
          <>
            {clickable ? (
              <LabelBadge label={labels[0]} />
            ) : (
              <Badge textTransform="none" className="text-md">
                {labels[0].name}
              </Badge>
            )}
          </>
        )}
        {labels.length > 1 && (
          <>
            {clickable ? (
              <LabelBadge label={labels[1]} />
            ) : (
              <Badge textTransform="none" className="text-md">
                {labels[1].name}
              </Badge>
            )}
          </>
        )}
        <Tooltip
          className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
          label={t("deploy.labels_modal.open")}
          placement="top"
          openDelay={500}
          aria-label={t("deploy.labels_modal.open")}
        >
          <button
            onClick={onOpen}
            className="text-md w-8 h-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
          >
            {labels.length > 2 ? (
              `+ ${labels.length - 2}`
            ) : (
              <FontAwesomeIcon
                className="h-4 w-4"
                icon={hasPermissionToEdit && !isArchived ? faPen : faEye}
              />
            )}
          </button>
        </Tooltip>
      </div>
      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <div className="flex place-content-between items-center p-3">
            <h1 className="text-2xl font-bold dark:text-white text-navy-700">
              {isEditting
                ? t("deploy.labels_modal.edition")
                : t("deploy.labels_modal.title")}
            </h1>
            {!isEditting && (
              <button
                className="border-[1px] border-brand-500 hover:border-brand-600 dark:border-brand-400 dark:hover:text-brand-300 text-brand-500 hover:text-brand-600 dark:text-brand-400 disabled:cursor-not-allowed disabled:opacity-40 p-2 rounded-lg transition duration-200 flex"
                onClick={() => setIsEditting(true)}
                disabled={!hasPermissionToEdit || isArchived}
              >
                <FontAwesomeIcon className="h-4 w-4" icon={faPen} />
              </button>
            )}
          </div>
          {!isEditting && <LabelTable labels={labels} />}
          {isEditting && (
            <DeployEditForm
              labels={labels}
              deployId={deployId}
              onClose={onClose}
            />
          )}
        </>
      </Modal>
    </>
  );
}
