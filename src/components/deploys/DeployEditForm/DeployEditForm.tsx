import { useContext } from "react";
import AsyncCreateMultiSelectForm from "common/Form/AsyncCreateMultiSelectForm";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { DeploysContext } from "contexts/deploys.context";
import { Formik, FormikHelpers, FormikProps } from "formik";
import { ILabel } from "interfaces/label.interface";
import { useTranslation } from "react-i18next";
import { Form, useLocation, useRevalidator } from "react-router-dom";
import { patchDeploy } from "services/deploys/deploys.service";
import { getLabels, postLabel } from "services/labels/labels.service";
import { DevicesContext } from "src/contexts/devices.context";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

interface SelectValue {
  value: number;
  label: string;
}
interface IProps {
  labels: SelectValue[];
}
export default function DeployEditForm({
  labels,
  deployId,
  onClose,
}: {
  labels: ILabel[];
  deployId: number;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const initialValues: IProps = {
    labels: labels.map((label) => ({
      value: label.id,
      label: label.name,
    })) as SelectValue[],
  };
  const validationSchema = Yup.object({
    labels: Yup.array()
      .min(1, t("deploy.validation.labels.required"))
      .required(t("deploy.validation.labels.required")),
  });
  const revalidator = useRevalidator();
  const { setReload: refreshDeploys } = useContext(DeploysContext);
  const { setReload: refreshDevices } = useContext(DevicesContext);
  const location = useLocation();
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={async (
          values: IProps,
          { setSubmitting }: FormikHelpers<IProps>,
        ) => {
          const deployParams = {
            id: deployId.toString(),
            labelIds: values.labels.map((label) => label.value),
          };
          const result = await patchDeploy(deployParams);
          if (result) {
            if (
              location.pathname.includes("deploys") ||
              location.pathname.includes("virtual-machines")
            ) {
              refreshDeploys(true);
              revalidator.revalidate();
            } else if (location.pathname.includes("devices")) {
              refreshDevices(true);
            }
            onClose();
          }
          setSubmitting(false);
        }}
      >
        {(props: FormikProps<IProps>) => (
          <Form onSubmit={props.handleSubmit}>
            <FormControl isInvalid={!!props.errors.labels}>
              <AsyncCreateMultiSelectForm
                required={true}
                postNewValue={postLabel}
                multiGet={getLabels}
                asyncName="labels"
                state={props.errors.labels ? "error" : undefined}
              />
              {props.errors.labels && (
                <FormErrorMessageText
                  text={
                    typeof props.errors.labels === "string"
                      ? props.errors.labels
                      : ""
                  }
                />
              )}
            </FormControl>
            <button
              className="btn-primary mt-5 "
              type="submit"
              disabled={
                props.isSubmitting ||
                !props.isValid ||
                JSON.stringify(props.initialValues) ===
                  JSON.stringify(props.values)
              }
            >
              {t("button.submit")}
            </button>
          </Form>
        )}
      </Formik>
    </>
  );
}
