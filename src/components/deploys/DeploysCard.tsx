import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import DeployTable from "components/deploys/deployTable/DeployTable";
import { DeploysContext } from "contexts/deploys.context";

export default function DeploysCard() {
  const { deploys, isLoading, error, isInVmTabs } = useContext(DeploysContext);
  return (
    <>
      {deploys && !isLoading && !error && (
        <>
          <DeployTable data={deploys.data} inVmTabs={isInVmTabs} />
          <Pagination meta={deploys.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !deploys && (
        <ErrorList translation="deploy.get_deploys.error" />
      )}
    </>
  );
}
