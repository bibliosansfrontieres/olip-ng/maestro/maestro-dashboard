export default function Footer(): JSX.Element {
  return (
    <footer className="z-[1] flex w-full flex-col items-center justify-between px-1 pb-8 pt-3 lg:px-8 xl:flex-row">
      <h5 className="mb-4 text-center text-sm font-medium text-gray-600 sm:!mb-0 md:text-lg">
        <p className="mb-4 text-center text-sm text-gray-600 sm:!mb-0 md:text-base">
          {new Date().getFullYear()} BSF
        </p>
      </h5>
    </footer>
  );
}
