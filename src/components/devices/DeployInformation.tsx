import { BadgeClickablePage } from "common/TableHelper/BadgeClickable";
import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { colorDeployStatus } from "utils/colorEnum";
import filterDate from "utils/filterDate";

import { Badge } from "@chakra-ui/react";

export default function DeployInformation({
  deploy,
}: {
  deploy: IDeployWithVmAndLabelsAndUser;
}) {
  const { t } = useTranslation();
  const informationList = [
    {
      translation: "status",
      value: (
        <BadgeClickablePage
          colorFunction={colorDeployStatus}
          enumValue={deploy.status}
          queryParamName="statuses"
          translateKeysEnumName="deploy_status"
          redirectRoute="deploys"
        />
      ),
    },
    {
      translation: "created_at",
      value: <p>{filterDate(deploy.createdAt.toString())}</p>,
    },
    {
      translation: "updated_at",
      value: <p>{filterDate(deploy.updatedAt.toString())}</p>,
    },
    {
      translation: "user",
      value: <p>{deploy.user.name + " (" + deploy.user.mail + ")"}</p>,
    },
  ];
  const vmSaveInformationList = [
    {
      translation: "vm_save_id",
      value: <p>{deploy.virtualMachineSave.id}</p>,
    },
    {
      translation: "vm_save_name",
      value: <p>{deploy.virtualMachineSave.name}</p>,
    },
    {
      translation: "vm_save_created_at",
      value: (
        <p>{filterDate(deploy.virtualMachineSave.createdAt.toString())}</p>
      ),
    },
  ];
  const vmInformationList = [
    {
      translation: "vm_id",
      value: (
        <Link
          className="underline"
          to={`/virtual-machines/${deploy.virtualMachineSave.virtualMachine.id}`}
        >
          {deploy.virtualMachineSave.virtualMachine.id}
        </Link>
      ),
    },
    {
      translation: "vm_name",
      value: <p>{deploy.virtualMachineSave.virtualMachine.title}</p>,
    },
  ];
  const labelsInformationList = [
    {
      translation: "labels",
      value: (
        <div className="flex gap-2 flex-wrap">
          {deploy.labels.map((label) => (
            <Badge
              textTransform="none"
              key={label.id}
              className="hover:underline hover:cursor-pointer"
            >
              <Link to={`/deploys?labelIds=${label.id}`}>{label.name}</Link>
            </Badge>
          ))}
        </div>
      ),
    },
  ];
  return (
    <>
      {informationList.map((informationLine, index) => (
        <div className="flex gap-2" key={index}>
          <p className="font-bold">
            {t(`device.information.deploy.${informationLine.translation}`)}
          </p>
          {informationLine.value}
        </div>
      ))}
      <div className="mt-2" />
      {vmSaveInformationList.map((informationLine, index) => (
        <div className="flex gap-2" key={index}>
          <p className="font-bold">
            {t(`device.information.deploy.${informationLine.translation}`)}
          </p>
          {informationLine.value}
        </div>
      ))}
      <div className="mt-2" />

      {vmInformationList.map((informationLine, index) => (
        <div className="flex gap-2" key={index}>
          <p className="font-bold">
            {t(`device.information.deploy.${informationLine.translation}`)}
          </p>
          {informationLine.value}
        </div>
      ))}
      <div className="mt-2" />

      {labelsInformationList.map((informationLine, index) => (
        <div className="flex gap-2" key={index}>
          <p className="font-bold">
            {t(`device.information.deploy.${informationLine.translation}`)}
          </p>
          {informationLine.value}
        </div>
      ))}
    </>
  );
}
