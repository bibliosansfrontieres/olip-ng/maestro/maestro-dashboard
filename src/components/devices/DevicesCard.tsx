import { useContext, useEffect, useState } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import DeviceTable from "components/devices/deviceTable/DeviceTable";
import { DevicesContext } from "contexts/devices.context";
import useSocketOnEvent from "hooks/useSocketOnEvent";

export default function DevicesCard() {
  const { devices, isLoading, error } = useContext(DevicesContext);

  const [socketDevices, setSocketDevices] = useState(devices?.data);

  useEffect(() => {
    setSocketDevices(devices?.data);
  }, [devices?.data]);

  useSocketOnEvent(
    `device.blinking`,
    (result: { id: string; blinking: boolean }) => {
      if (socketDevices?.find((device) => device.id === result.id)) {
        setSocketDevices((prev) =>
          prev?.map((device) =>
            device.id === result.id
              ? { ...device, blinking: result.blinking }
              : device,
          ),
        );
      }
    },
  );
  return (
    <>
      {devices && socketDevices && !isLoading && !error && (
        <>
          <DeviceTable devices={socketDevices} inDevicePage={true} />
          <Pagination meta={devices.meta} />{" "}
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !devices && (
        <ErrorList translation="device.get_devices.error" />
      )}
    </>
  );
}
