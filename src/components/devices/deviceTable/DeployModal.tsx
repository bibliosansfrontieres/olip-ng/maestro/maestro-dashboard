import DeployInformation from "components/devices/DeployInformation";
import Modal from "components/modal/Modal";
import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import { Tooltip, useDisclosure } from "@chakra-ui/react";
import { faRocket } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function DeployModal({
  deploy,
}: {
  deploy: IDeployWithVmAndLabelsAndUser;
}) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
        label={t("device.deploy_modal.open")}
        placement="top"
        openDelay={500}
        aria-label={t("device.deploy_modal.open")}
      >
        <button
          onClick={onOpen}
          disabled={!deploy}
          className="h-8 w-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-3 w-3" icon={faRocket} />
        </button>
      </Tooltip>
      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="mb-[20px] text-2xl font-bold">
            {t("device.information.third_section")}
          </h1>
          <DeployInformation deploy={deploy} />
          <div className="flex justify-end pt-8 gap-7">
            <Link
              className="flex items-center btn-primary "
              to={`/virtual-machines/${deploy.virtualMachineSave.virtualMachine.id}`}
            >
              {t("device.deploy_modal.view_vm")}
            </Link>
            <button
              className="border-[1px] border-red-500 hover:border-red-600 dark:border-red-400 dark:hover:text-red-300 text-red-500 hover:text-red-600 dark:text-red-400 px-5 py-3 text-base font-medium rounded-xl transition duration-200"
              onClick={onClose}
            >
              {t("button.close")}
            </button>
          </div>
        </>
      </Modal>
    </>
  );
}
