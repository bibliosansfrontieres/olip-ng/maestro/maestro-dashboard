import AsyncMultiSelect from "common/TableHelper/AsyncMultiSelect";
import DatePicker from "common/TableHelper/DatePicker";
import { ReactMultiSelect } from "common/TableHelper/ReactMultiSelect";
import { SwitchOnly } from "common/TableHelper/SwitchOnly";
import { DeviceStatusEnum } from "enums/deviceStatus.enum";
import { getFqdn, getFqdns } from "services/fqdnMasks/fqdns.service";
import { getLabel, getLabels } from "services/labels/labels.service";

export default function DeviceTableHelper() {
  return (
    <div className="mt-4 flex flex-wrap gap-4 items-center">
      <SwitchOnly
        queryParamsName="isAliveOnly"
        defaultSwitchValue={true}
        defaultQueryParamValue={true}
      />
      <DatePicker queryParamsName="lastSeenDateRange" />
      <AsyncMultiSelect
        queryParamsName="labelIds"
        singleGet={getLabel}
        multiGet={getLabels}
      />
      <AsyncMultiSelect
        queryParamsName="fqdnMaskIds"
        multiGet={getFqdns}
        singleGet={getFqdn}
      />
      <ReactMultiSelect<typeof DeviceStatusEnum>
        enumList={DeviceStatusEnum}
        queryParamsEnumName="statuses"
        translateKeysEnumName="device_status"
      />
    </div>
  );
}
