import { useState } from "react";
import { BadgeClickableTable } from "common/TableHelper/BadgeClickable";
import { RedirectButton } from "common/TableHelper/Buttons";
import {
  ColumnHeaderHelper,
  ColumnHeaderHelperWithoutSort,
} from "common/TableHelper/ColumnHeaderHelper";
import { InformationClickableTable } from "common/TableHelper/InformationClickable";
import { TooltipCell } from "common/TableHelper/TooltipCell";
import LabelsModal from "components/deploys/deployTable/LabelsModal";
import DeployModal from "components/devices/deviceTable/DeployModal";
import { ConfirmationModal } from "components/modal/ConfirmationModal";
import Modal from "components/modal/Modal";
import {
  DeviceTableColumEnum,
  DeviceTableInDevicePageColumEnum,
} from "enums/columns/deviceTableColumn.enum";
import { DeployStatusEnum } from "enums/deployStatus.enum";
import { DeviceOrderByEnum } from "enums/orderBy/deviceOrderBy.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { TableNamesEnum } from "enums/table.enum";
import { usePermissions } from "hooks/usePermission";
import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import {
  patchDevicesBlink,
  patchDevicesReset,
} from "services/devices/devices.service";
import { colorDeviceIsAlive, colorDeviceStatus } from "utils/colorEnum";
import filterDate from "utils/filterDate";

import {
  Badge,
  Button,
  Checkbox,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tooltip,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import { faEye, faMobileRetro } from "@fortawesome/free-solid-svg-icons";

export default function DeviceTable({
  devices,
  inDevicePage = false,
}: {
  devices: (IDevice & {
    fqdnMask?: IFqdnMask;
    deploys?: IDeployWithVmAndLabelsAndUser[] | [];
  })[];
  inDevicePage?: boolean;
}) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [checkedDevices, setCheckedDevices] = useState(
    Array.from(devices, () => false),
  );
  const hasPermissionToReset = usePermissions(PermissionNameEnum.RESET_DEVICE);
  const hasPermissionToBlink = usePermissions(PermissionNameEnum.BLINK_DEVICE);
  const hasPermissionToExec = usePermissions(PermissionNameEnum.EXEC_DEVICE);
  const allOffline = devices.findIndex((device) => device.isAlive) === -1;
  // verify same size checkeddevices and devices because when reloading there is a delay
  const allChecked =
    checkedDevices.length === devices.length
      ? !allOffline &&
        checkedDevices.every((checkedDevice, index) =>
          devices[index].isAlive ? checkedDevice : true,
        )
      : false;
  const isIndeterminate =
    checkedDevices.length === devices.length
      ? !allOffline &&
        checkedDevices.some((checkedDevice, index) =>
          devices[index].isAlive ? checkedDevice : false,
        ) &&
        !allChecked
      : false;

  const blinkedDevices = async () => {
    const devicesToBlink = checkedDevices
      .map((checked, index) =>
        checked && devices[index].isAlive ? devices[index].id : null,
      )
      .filter((deviceId) => deviceId !== null && deviceId) as string[];
    const response = await patchDevicesBlink({ deviceIds: devicesToBlink });
    if (response) {
      setCheckedDevices(Array.from(devices, () => false));
    }
  };

  const resetDevices = async () => {
    const devicesToReset = checkedDevices
      .map((checked, index) =>
        checked && devices[index].isAlive ? devices[index].id : null,
      )
      .filter((device) => device !== null) as string[];
    const response = await patchDevicesReset({ deviceIds: devicesToReset });
    if (response) {
      setCheckedDevices(Array.from(devices, () => false));
    }
  };

  return (
    <>
      {inDevicePage && (
        <div className="mt-4 flex flex-wrap gap-4 items-center">
          {hasPermissionToBlink && (
            <Button
              colorScheme="blue"
              isDisabled={
                !(allChecked || isIndeterminate) || checkedDevices.length === 0
              }
              onClick={blinkedDevices}
            >
              {t("device.table.buttons.blink")}
            </Button>
          )}
          {hasPermissionToReset && (
            <>
              <Button
                colorScheme="red"
                isDisabled={
                  !(allChecked || isIndeterminate) ||
                  checkedDevices.length === 0
                }
                onClick={onOpen}
              >
                {t("device.table.buttons.reset")}
              </Button>
              <Modal disclosure={{ isOpen, onClose }}>
                <ConfirmationModal
                  onClose={onClose}
                  validateFunction={resetDevices}
                  translationKey="device.table.reset_confirmation_modal"
                />
              </Modal>
            </>
          )}
        </div>
      )}
      <TableContainer pt={5}>
        <Table variant="unstyled">
          <Thead>
            <Tr>
              {inDevicePage && (
                <Th className="border-b-[1px] border-gray-200 pt-4 pb-2">
                  <Checkbox
                    isChecked={allChecked && checkedDevices.length > 0}
                    isIndeterminate={isIndeterminate}
                    onChange={(e) =>
                      setCheckedDevices((prev) =>
                        prev.map(() => e.target.checked),
                      )
                    }
                  />
                </Th>
              )}
              {inDevicePage ? (
                <ColumnHeaderHelper<
                  typeof DeviceTableInDevicePageColumEnum,
                  typeof DeviceOrderByEnum
                >
                  tableName={TableNamesEnum.DEVICE}
                  headers={DeviceTableInDevicePageColumEnum}
                  orderByEnum={DeviceOrderByEnum}
                />
              ) : (
                <ColumnHeaderHelperWithoutSort<typeof DeviceTableColumEnum>
                  tableName={TableNamesEnum.DEVICE}
                  headers={DeviceTableColumEnum}
                />
              )}
            </Tr>
          </Thead>
          <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
            {devices.map((device, index) => {
              return (
                <Tr
                  key={`Tr-${device.id}`}
                  className={`h-16 ${device.blinking ? "animate-pulse" : ""}`}
                >
                  {inDevicePage && (
                    <Td>
                      <Checkbox
                        disabled={!device.isAlive}
                        isChecked={
                          device.isAlive ? checkedDevices[index] : false
                        }
                        onChange={(e) =>
                          setCheckedDevices((prev) => [
                            ...prev.slice(0, index),
                            e.target.checked,
                            ...prev.slice(index + 1),
                          ])
                        }
                      />
                    </Td>
                  )}
                  <TooltipCell>
                    <Link to={`/devices/${device.id}`} className="underline">
                      {device.macAddress}
                    </Link>
                  </TooltipCell>
                  <Td className="min-w-24 max-w-52">
                    <BadgeClickableTable
                      colorFunction={colorDeviceStatus}
                      enumValue={device.status}
                      queryParamName="statuses"
                      translateKeysEnumName="device_status"
                    />
                  </Td>
                  {inDevicePage && (
                    <Td className="max-w-[250px]">
                      {device.fqdnMask && (
                        <InformationClickableTable
                          extraStyle="truncate"
                          value={
                            <Tooltip
                              className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
                              label={
                                <>
                                  <p>
                                    {t("deploy.table.fqdn_mask_tooltip.label", {
                                      label: device.fqdnMask.label,
                                    })}
                                  </p>
                                  <p>
                                    {t("deploy.table.fqdn_mask_tooltip.mask", {
                                      mask: device.fqdnMask.mask,
                                    })}
                                  </p>
                                </>
                              }
                              placement="top"
                              openDelay={500}
                              aria-label="title"
                            >
                              <p>{device.fqdnMask.label}</p>
                            </Tooltip>
                          }
                          queryParamName="fqdnMaskIds"
                          filterValue={device.fqdnMask.id.toString()}
                        />
                      )}
                    </Td>
                  )}
                  <TooltipCell element={device.privateIpv4} />
                  <Td>
                    <Badge colorScheme={colorDeviceIsAlive(device.isAlive)}>
                      {t(
                        `device.table.badge.${device.isAlive ? "online" : "offline"}`,
                      )}
                    </Badge>
                  </Td>
                  {inDevicePage && (
                    <>
                      <Td>
                        {device.deploys?.find(
                          (deploy) =>
                            deploy.status !== DeployStatusEnum.ARCHIVED,
                        ) ? (
                          <LabelsModal
                            isArchived={false}
                            labels={
                              device.deploys.find(
                                (deploy) =>
                                  deploy.status !== DeployStatusEnum.ARCHIVED,
                              )!.labels
                            }
                            deployId={
                              device.deploys.find(
                                (deploy) =>
                                  deploy.status !== DeployStatusEnum.ARCHIVED,
                              )!.id
                            }
                          />
                        ) : (
                          <></>
                        )}
                      </Td>
                      <Td>
                        {device.deploys?.find(
                          (deploy) =>
                            deploy.status !== DeployStatusEnum.ARCHIVED,
                        ) ? (
                          <Link
                            to={`/deploys/${
                              device.deploys?.find(
                                (deploy) =>
                                  deploy.status !== DeployStatusEnum.ARCHIVED,
                              )?.id
                            }`}
                            className="underline"
                          >
                            {
                              device.deploys?.find(
                                (deploy) =>
                                  deploy.status !== DeployStatusEnum.ARCHIVED,
                              )?.id
                            }
                          </Link>
                        ) : (
                          <></>
                        )}
                      </Td>
                    </>
                  )}
                  <Td>
                    <p>{filterDate(device.lastSeenAt.toString())}</p>
                  </Td>
                  <Td>
                    <p>{filterDate(device.createdAt.toString())}</p>
                  </Td>
                  {inDevicePage && (
                    <Td>
                      <div className="flex gap-2">
                        <RedirectButton
                          href={`/devices/${device.id}`}
                          icon={faEye}
                          translationKey="device.table.actions.open"
                        />
                        {device.deploys?.find(
                          (deploy) =>
                            deploy.status !== DeployStatusEnum.ARCHIVED,
                        ) && (
                          <DeployModal
                            deploy={
                              device.deploys.find(
                                (deploy) =>
                                  deploy.status !== DeployStatusEnum.ARCHIVED,
                              )!
                            }
                          />
                        )}
                        {hasPermissionToExec && (
                          <RedirectButton
                            href={`/devices/${device.id}/exec`}
                            icon={faMobileRetro}
                            translationKey="device.exec.button_title"
                          />
                        )}
                      </div>
                    </Td>
                  )}
                </Tr>
              );
            })}
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}
