import { Dispatch, forwardRef, SetStateAction } from "react";
import ExecDeviceConvo from "components/devices/execDevice/ExecDeviceConvo";
import ExecDeviceForm from "components/devices/execDevice/ExecDeviceForm";
import useSocketOnEvent from "hooks/useSocketOnEvent";
import {
  IDeviceExecResponse,
  IFullConvo,
} from "interfaces/fullConvo.interface";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

const ExecDeviceFull = forwardRef<
  HTMLInputElement,
  {
    setFullConvo: Dispatch<SetStateAction<IFullConvo[]>>;
    fullConvo: IFullConvo[];
  }
>(function (
  {
    setFullConvo,
    fullConvo,
  }: {
    setFullConvo: Dispatch<SetStateAction<IFullConvo[]>>;
    fullConvo: IFullConvo[];
  },
  ref,
) {
  const { t } = useTranslation();
  const { id } = useParams();

  useSocketOnEvent(
    `device.${id}.exec.response`,
    (result: IDeviceExecResponse) => {
      setFullConvo((prev) => {
        if (result.response?.error) {
          return [
            ...prev,
            {
              text:
                result.response?.stderr ??
                JSON.stringify(result.response.error),
              type: "response",
              error: true,
            },
          ];
        }
        const text =
          result.response?.stdout ?? "" + result.response?.stderr ?? "";
        return [...prev, { text: text, type: "response", error: false }];
      });
    },
  );
  return (
    <div className="flex flex-col gap-4">
      {fullConvo.length > 0 && <ExecDeviceConvo fullConvo={fullConvo} />}
      <ExecDeviceForm setFullConvo={setFullConvo} ref={ref} />
      {fullConvo.length > 0 && (
        <button className="btn-primary" onClick={() => setFullConvo([])}>
          {t("device.exec.clear")}
        </button>
      )}
    </div>
  );
});

export default ExecDeviceFull;
