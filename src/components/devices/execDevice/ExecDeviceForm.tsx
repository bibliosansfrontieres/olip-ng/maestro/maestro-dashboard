import { Dispatch, forwardRef, SetStateAction } from "react";
import InputField from "components/shared/fields/InputField";
import { useFormik } from "formik";
import { IFullConvo } from "interfaces/fullConvo.interface";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";
import { socket } from "src/socket.io";

interface IProps {
  request: string;
}

const ExecDeviceForm = forwardRef<
  HTMLInputElement,
  {
    setFullConvo: Dispatch<SetStateAction<IFullConvo[]>>;
  }
>(function (
  {
    setFullConvo,
  }: {
    setFullConvo: Dispatch<SetStateAction<IFullConvo[]>>;
  },
  ref,
) {
  const location = useLocation();
  const deviceId = location.pathname.split("/")[2];
  const { t } = useTranslation();
  const formik = useFormik({
    initialValues: {
      request: "",
    } as IProps,
    onSubmit: (values) => {
      socket.emit("device.exec.request", { deviceId, request: values.request });
      setFullConvo((prev) => [
        ...prev,
        { text: values.request, type: "request", error: false },
      ]);
      formik.resetForm();
    },
  });
  return (
    <form onSubmit={formik.handleSubmit} className="flex items-center gap-2">
      <InputField
        autofocus
        ref={ref}
        id="request"
        extra="grow"
        type="text"
        placeholder={t("device.exec.form.placeholder")}
        value={formik.values.request}
        onChange={formik.handleChange}
      />
      <button className="btn-primary " type="submit">
        {t("button.submit")}
      </button>
    </form>
  );
});

export default ExecDeviceForm;
