import { useEffect, useRef, useState } from "react";
import Convert from "ansi-to-html";
import Switch from "components/switch/Switch";
import { IFullConvo } from "interfaces/fullConvo.interface";
import { useTranslation } from "react-i18next";

import { Card as ChakraCard } from "@chakra-ui/react";

const TerminalLine = ({ line }: { line: IFullConvo }) => {
  const convert = new Convert({ newline: true, stream: true, escapeXML: true });
  const convertedText = convert.toHtml(line.text).replace(/ /g, "&nbsp;");

  if (line.error) {
    return (
      <div
        className="text-base text-red-500 font-mono"
        dangerouslySetInnerHTML={{ __html: convertedText }}
      />
    );
  }
  if (line.type === "request") {
    return (
      <div
        className="text-base text-brand-200 font-mono"
        dangerouslySetInnerHTML={{ __html: `> ${convertedText}` }}
      />
    );
  }
  return (
    <div
      className="text-base text-white font-mono"
      dangerouslySetInnerHTML={{ __html: convertedText }}
    />
  );
};

export default function ExecDeviceConvo({
  fullConvo,
}: {
  fullConvo: IFullConvo[];
}) {
  const { t } = useTranslation();

  const scrollRef = useRef<HTMLDivElement>(null);
  const [scrollActivated, setScrollActivated] = useState(true);

  // Scroll to bottom when executing
  useEffect(() => {
    const scrollToBottom = () => {
      if (scrollRef.current) {
        scrollRef.current.scrollTop = scrollRef.current.scrollHeight;
      }
    };
    if (scrollActivated) {
      scrollToBottom();
    }
  }, [fullConvo, scrollActivated]);

  return (
    <div className="flex flex-col">
      <div className="flex gap-1 items-center mb-3">
        <Switch
          id="scrollActivated"
          onChange={() => setScrollActivated(!scrollActivated)}
          checked={scrollActivated}
        />
        <label
          htmlFor="scrollActivated"
          className="text-sm text-navy-700 dark:text-white"
        >
          {t("device.exec.switch")}
        </label>
      </div>
      <ChakraCard
        backgroundColor="black"
        px="6"
        py="2"
        className="max-h-[40vh] md:max-h-[65vh] overflow-y-scroll"
        ref={scrollRef}
      >
        {fullConvo.map((line, index) => (
          <TerminalLine line={line} key={index} />
        ))}
      </ChakraCard>
    </div>
  );
}
