import { BadgeClickablePage } from "common/TableHelper/BadgeClickable";
import { InformationClickablePage } from "common/TableHelper/InformationClickable";
import Card from "components/card/Card";
import DeployTable from "components/deploys/deployTable/DeployTable";
import DeployInformation from "components/devices/DeployInformation";
import { DeployStatusEnum } from "enums/deployStatus.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { useTranslation } from "react-i18next";
import { RedirectButton } from "src/common/TableHelper/Buttons";
import { colorDeviceIsAlive, colorDeviceStatus } from "utils/colorEnum";
import filterDate from "utils/filterDate";
import { snakeToCamelCase } from "utils/stringModification";

import { Badge } from "@chakra-ui/react";
import { faMobileRetro } from "@fortawesome/free-solid-svg-icons";

export default function DeviceCard({
  device,
}: {
  device: IDevice & {
    fqdnMask: IFqdnMask;
    deploys: IDeployWithVmAndLabelsAndUser[] | null;
  };
}) {
  const { t } = useTranslation();
  const hasPermissionToExec = usePermissions(PermissionNameEnum.EXEC_DEVICE);
  const deviceStatusTable = [
    {
      translation: "status",
      value: (
        <BadgeClickablePage
          colorFunction={colorDeviceStatus}
          enumValue={device.status}
          queryParamName="statuses"
          translateKeysEnumName="device_status"
          redirectRoute="devices"
        />
      ),
    },
    {
      translation: "is_alive",
      value: (
        <Badge colorScheme={colorDeviceIsAlive(device.isAlive)}>
          {t(`device.table.badge.${device.isAlive ? "online" : "offline"}`)}
        </Badge>
      ),
    },
  ];
  const deviceStringTable = ["vendor", "os", "serial_id"].map((key) => {
    return {
      translation: key,
      value: <p>{device[snakeToCamelCase(key) as keyof IDevice]}</p>,
    };
  });
  const deviceNetworkTable = [
    "public_ipv4",
    "ipv4_fqdn_name",
    "ipv4_fqdn_arpa",
    "private_ipv4",
    "public_ipv6",
    "ipv6_fqdn_name",
    "ipv6_fqdn_arpa",
  ].map((key) => {
    return {
      translation: key,
      value: <p>{device[snakeToCamelCase(key) as keyof IDevice]}</p>,
    };
  });
  const deviceDateTable = ["last_seen_at", "created_at", "updated_at"].map(
    (key) => {
      return {
        translation: key,
        value: (
          <p>
            {device[snakeToCamelCase(key) as keyof IDevice]
              ? filterDate(
                  device[snakeToCamelCase(key) as keyof IDevice]!.toString(),
                )
              : ""}
          </p>
        ),
      };
    },
  );
  const deviceDeploy = device.deploys?.find(
    (deploy) => deploy.status === DeployStatusEnum.DEPLOYED,
  );

  const archivedDeploys = device.deploys?.filter(
    (deploy) => deploy.status !== DeployStatusEnum.DEPLOYED,
  );

  return (
    <Card>
      <div className="flex flex-col gap-1 ml-3 text-base text-navy-700 dark:text-white">
        <div className="flex items-center justify-between">
          <p className="text-xl font-bold text-navy-700 dark:text-white mx-2 my-3">
            {t("device.information.first_section")}
          </p>
          {hasPermissionToExec && (
            <RedirectButton
              href={`/devices/${device.id}/exec`}
              icon={faMobileRetro}
              translationKey="device.exec.button_title"
            />
          )}
        </div>
        <div className="flex gap-2">
          <div className="font-bold">{t(`device.information.id`)}</div>
          <p>{device.id}</p>
        </div>
        {deviceStatusTable
          .concat(deviceStringTable)
          .concat(deviceDateTable)
          .map((informationLine, index) => {
            return (
              <div className="flex gap-2" key={index}>
                <p className="font-bold">
                  {t(`device.information.${informationLine.translation}`)}
                </p>
                {informationLine.value}
              </div>
            );
          })}
        <p className="text-xl font-bold text-navy-700 dark:text-white mx-2 mt-8 mb-3">
          {t("device.information.second_section")}
        </p>
        <div className="flex gap-2">
          <p className="font-bold">{t(`device.information.mac_address`)}</p>
          {device.macAddress}
        </div>
        <div className="flex gap-2">
          <p className="font-bold">{t(`device.information.fqdn_mask.label`)}</p>
          <InformationClickablePage
            value={device.fqdnMask.label}
            queryParamName="fqdnMaskIds"
            filterValue={device.fqdnMask.id.toString()}
            redirectRoute="devices"
          />
        </div>
        <div className="flex gap-2 mb-4">
          <p className="font-bold">{t(`device.information.fqdn_mask.mask`)}</p>
          {device.fqdnMask.mask}
        </div>
        {deviceNetworkTable.map((informationLine, index) => {
          return (
            <div className="flex gap-2" key={index}>
              <p className="font-bold">
                {t(`device.information.${informationLine.translation}`)}
              </p>
              {informationLine.value}
            </div>
          );
        })}

        {deviceDeploy && (
          <>
            <p className="text-xl font-bold text-navy-700 dark:text-white mx-2 mt-8">
              {t("device.information.third_section")}
            </p>
            <DeployInformation deploy={deviceDeploy} />
          </>
        )}
        {archivedDeploys && archivedDeploys.length > 0 && (
          <>
            <p className="text-xl font-bold text-navy-700 dark:text-white mx-2 mt-8">
              {t("device.information.fourth_section")}
            </p>
            <DeployTable data={archivedDeploys} inDevicePage={true} />
          </>
        )}
      </div>
    </Card>
  );
}
