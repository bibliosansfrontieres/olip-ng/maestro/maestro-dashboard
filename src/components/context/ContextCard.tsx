import { useMemo, useState } from "react";
import Pagination from "common/Pagination";
import { SearchBarTable } from "common/SearchBarTable";
import ApplicationCard from "components/application/ApplicationCard";
import Card from "components/card/Card";
import Modal from "components/modal/Modal";
import Switch from "components/switch/Switch";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IApplication } from "interfaces/application.interface";
import { IContext } from "interfaces/context.interface";
import { IPageResponse } from "interfaces/pageResponse.interface";
import { useTranslation } from "react-i18next";
import {
  patchContext,
  patchDefaultContext,
} from "services/contexts/contexts.service";

import { useDisclosure } from "@chakra-ui/react";
import { faPen, faX } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ContextCard({
  applications,
  context,
  defaultContext,
  isInDefaultContext = false,
  isDisabled = false,
}: {
  applications: IPageResponse<IApplication>;
  context: IContext;
  defaultContext?: IContext;
  isInDefaultContext?: boolean;
  isDisabled?: boolean;
}) {
  const { t } = useTranslation();
  const [valueSwitch, setValueSwitch] = useState(
    context.isOutsideProjectDownloadsAuthorized,
  );
  const havePermission = usePermissions(
    isInDefaultContext
      ? PermissionNameEnum.UPDATE_DEFAULT_CONTEXT
      : PermissionNameEnum.UPDATE_CONTEXT,
  );

  const [isModifyAuthorized, setIsModifyAuthorized] = useState(false);
  const isEditable = isDisabled ? false : havePermission;

  const onSwitchChange = async () => {
    let response = undefined;
    if (isInDefaultContext) {
      response = await patchDefaultContext({
        isOutsideProjectDownloadsAuthorized: !valueSwitch,
      });
    } else {
      response = await patchContext({
        id: context.id.toString(),
        isOutsideProjectDownloadsAuthorized: !valueSwitch,
      });
    }
    if (response) {
      setValueSwitch((prev) => !prev);
    }
  };

  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleClick = () => {
    if (!isModifyAuthorized) {
      onOpen();
      return;
    }
    setIsModifyAuthorized(!isModifyAuthorized);
  };

  const handleCancel = () => {
    setIsModifyAuthorized(false);
    onClose();
  };

  const handleContinue = () => {
    setIsModifyAuthorized(true);
    onClose();
  };

  const disabled = useMemo(() => {
    if (!isEditable) {
      return true;
    }
    return isInDefaultContext ? false : !isModifyAuthorized;
  }, [isModifyAuthorized, isEditable, isInDefaultContext]);

  const textValueSwitch = useMemo(() => {
    if (!disabled) {
      return t("contexts.context_card.switch.label.authorize");
    }
    return valueSwitch
      ? t("contexts.context_card.switch.label.is_authorized")
      : t("contexts.context_card.switch.label.is_prohibited");
  }, [disabled, t, valueSwitch]);

  return (
    <Card>
      <>
        <div>
          <SearchBarTable />
        </div>
        <div
          className={`flex flex-col mt-3 p-2 rounded-[10px] border-[1px] ${isModifyAuthorized ? "border-red-500 dark:border-red-400" : "border-white dark:border-navy-800"}`}
        >
          {!isInDefaultContext && (
            <div className="flex justify-end gap-3 items-center">
              {isModifyAuthorized && (
                <p
                  className="text-red-500 dark:text-red-400 grow text-base font-bold"
                  data-test-id="warningModifyContext"
                >
                  {t("vm.context_tab.warning_modify_context")}
                </p>
              )}
              <button
                className="border-[1px] border-brand-500 hover:border-brand-600 dark:border-brand-400 dark:hover:text-brand-300 text-brand-500 hover:text-brand-600 dark:text-brand-400 disabled:cursor-not-allowed disabled:opacity-40 p-2 rounded-lg transition duration-200 flex"
                onClick={handleClick}
                data-test-id="contextEditButton"
                disabled={!isEditable}
              >
                {isModifyAuthorized ? (
                  <FontAwesomeIcon className="h-4 w-4" icon={faX} />
                ) : (
                  <FontAwesomeIcon className="h-4 w-4" icon={faPen} />
                )}
              </button>
              <Modal disclosure={{ isOpen, onClose }}>
                <>
                  <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
                    {t("vm.context_tab.modal.warning_title")}
                  </h1>
                  <p className="mt-3 text-red-500 dark:text-red-400 text-lg font-bold w-[40vw]">
                    {t("vm.context_tab.modal.warning_text")}
                  </p>
                  <p className="mt-1 text-red-500 dark:text-red-400 text-lg font-bold w-[40vw]">
                    {t("vm.context_tab.modal.warning_text_2")}
                  </p>
                  <div className="flex pt-8 gap-7">
                    <button
                      className="btn-primary"
                      onClick={handleContinue}
                      data-test-id="contextModalContinueButton"
                    >
                      {t("vm.context_tab.modal.button.continue")}
                    </button>
                    <button
                      className="border-[1px] border-red-500 hover:border-red-600 dark:border-red-400 dark:hover:text-red-300 text-red-500 hover:text-red-600 dark:text-red-400 px-5 py-3 text-base font-medium rounded-xl transition duration-200"
                      onClick={handleCancel}
                      data-test-id="contextModalCancelButton"
                    >
                      {t("vm.context_tab.modal.button.cancel")}
                    </button>
                  </div>
                </>
              </Modal>
            </div>
          )}
          <p
            className="text-xl font-bold text-navy-700 dark:text-white mx-2 my-3"
            data-test-id="playlistConfiguration"
          >
            {t("contexts.context_card.first_section")}
          </p>
          <div className="flex mb-5 mt-[-12px] mx-2 justify-between items-center">
            <div className="flex flex-col">
              <div className="flex gap-2 items-center pt-[20px]">
                <Switch
                  id="outsideProjectDownloadsAuthorized"
                  dataTestId="outsideProjectDownloadsAuthorizedSwitch"
                  checked={valueSwitch}
                  onChange={onSwitchChange}
                  disabled={disabled}
                ></Switch>
                <label
                  htmlFor="outsideProjectDownloadsAuthorized"
                  className={`text-base text-navy-700 dark:text-white ${(!isModifyAuthorized && !isInDefaultContext) || !isEditable ? "!text-gray-400" : ""}`}
                >
                  {textValueSwitch}
                </label>
              </div>
              <div className="text-red-500 dark:!text-red-400 text-sm ml-[48px] h-[20px]">
                {!isInDefaultContext &&
                  valueSwitch !==
                    defaultContext?.isOutsideProjectDownloadsAuthorized &&
                  t("applications.application_card.different_default_context")}
              </div>
            </div>
          </div>
          <p
            className="text-xl font-bold text-navy-700 dark:text-white mx-2 my-3"
            data-test-id="applicationConfiguration"
          >
            {t("contexts.context_card.second_section")}
          </p>
          <div className="flex gap-4 flex-wrap mb-5 mx-2">
            {applications.data.map((application: IApplication) => (
              <ApplicationCard
                application={application}
                key={application.id}
                context={context}
                defaultContext={defaultContext}
                isInDefaultContext={isInDefaultContext}
                isModifyAuthorized={
                  isInDefaultContext ? true : isModifyAuthorized
                }
              />
            ))}
          </div>
          <Pagination meta={applications.meta} />
        </div>
      </>
    </Card>
  );
}
