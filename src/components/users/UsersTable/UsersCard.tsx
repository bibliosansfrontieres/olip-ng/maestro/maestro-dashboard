import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import UsersTable from "components/users/UsersTable/UsersTable";
import { UsersContext } from "contexts/users.context";

export default function UsersCard() {
  const { users, isLoading, error } = useContext(UsersContext);

  return (
    <>
      {users && !isLoading && !error && (
        <>
          <UsersTable tableData={users.data} />
          <Pagination meta={users.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !users && <ErrorList translation="users.get_users.error" />}
    </>
  );
}
