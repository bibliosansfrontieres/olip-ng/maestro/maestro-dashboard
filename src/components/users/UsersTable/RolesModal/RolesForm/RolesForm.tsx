import { useContext } from "react";
import { RoleNameEnum } from "enums/roleName.enum";
import { FormikHelpers, useFormik } from "formik";
import { IUser } from "interfaces/user.interface";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { IPatchUserRequest } from "services/users/interfaces/patchUser.interface";
import { patchUser } from "services/users/users.service";
import { UsersContext } from "src/contexts/users.context";

import { FormControl, Select } from "@chakra-ui/react";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function RolesForm({
  user,
  onClose,
}: {
  user: IUser;
  onClose: () => void;
}) {
  const SelectIcon = () => (
    <FontAwesomeIcon
      icon={faChevronDown}
      className="text-sm text-navy-700 dark:text-white"
    />
  );
  const { setReload } = useContext(UsersContext);

  const { t } = useTranslation();
  const formik = useFormik({
    initialValues: {
      roleName: user.role.name,
    } as IPatchUserRequest,
    onSubmit: async (
      values: IPatchUserRequest,
      { setSubmitting }: FormikHelpers<IPatchUserRequest>,
    ) => {
      if (user.role.name === values.roleName) {
        setSubmitting(false);
        toast.error(t("users.patch_users.no_change"));

        return;
      }
      setSubmitting(false);
      const result = await patchUser({ ...values, id: user.id });
      if (result) {
        setReload(true);
        onClose();
      }
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <FormControl isInvalid={!!formik.errors.roleName}></FormControl>
      <FormControl>
        <label
          className="mb-2 block text-sm font-medium text-navy-700 dark:text-white"
          htmlFor="roleName"
        >
          {t("roles.form.role_name")}
        </label>
        <Select
          id="roleName"
          name="roleName"
          className="text-sm font-medium text-navy-700 dark:text-white"
          onChange={formik.handleChange}
          value={formik.values.roleName}
          icon={<SelectIcon />}
        >
          <option value={RoleNameEnum.ADMIN}>{t("enums.role.admin")}</option>
          <option value={RoleNameEnum.PROJECT_MANAGER}>
            {t("enums.role.project_manager")}
          </option>
          <option value={RoleNameEnum.USER}>{t("enums.role.user")}</option>
        </Select>
      </FormControl>
      <button
        type="submit"
        className="btn-primary mt-5 "
        disabled={
          formik.isSubmitting ||
          !formik.isValid ||
          JSON.stringify(formik.initialValues) === JSON.stringify(formik.values)
        }
      >
        {t("button.submit")}
      </button>
    </form>
  );
}
