import { useContext } from "react";
import {
  ColumnHeaderHelper,
  ColumnHeaderHelperWithoutSort,
} from "common/TableHelper/ColumnHeaderHelper";
import GroupsModal from "components/users/UsersTable/GroupsModal/GroupsModal";
import RolesModal from "components/users/UsersTable/RolesModal/RolesModal";
import { UserContext } from "contexts/user.context";
import {
  UserTableColumnEnum,
  UserTableVmTabsColumnEnum,
} from "enums/columns/userTableColumn.enum";
import { UserOrderByEnum } from "enums/orderBy/userOrderBy.enum";
import { TableNamesEnum } from "enums/table.enum";
import { IUser } from "interfaces/user.interface";
import { useTranslation } from "react-i18next";

import { Table, TableContainer, Tbody, Td, Thead, Tr } from "@chakra-ui/react";

export default function UsersTable({
  tableData,
  isInVmTabs = false,
}: {
  tableData: IUser[];
  isInVmTabs?: boolean;
}) {
  const { t } = useTranslation();
  const { user: selfUser } = useContext(UserContext);
  return (
    <TableContainer>
      <Table className="w-full" variant="unstyled">
        <Thead>
          <Tr>
            {isInVmTabs ? (
              <ColumnHeaderHelperWithoutSort<typeof UserTableVmTabsColumnEnum>
                tableName={TableNamesEnum.USER}
                headers={UserTableVmTabsColumnEnum}
              />
            ) : (
              <ColumnHeaderHelper<
                typeof UserTableColumnEnum,
                typeof UserOrderByEnum
              >
                tableName={TableNamesEnum.USER}
                headers={UserTableColumnEnum}
                orderByEnum={UserOrderByEnum}
              />
            )}
          </Tr>
        </Thead>
        <Tbody>
          {tableData.map((user) => {
            return (
              <Tr key={`Tr-${user.id}`} className="h-16">
                <Td>
                  <p className="text-sm font-bold text-navy-700 dark:text-white">
                    {user.name}
                  </p>
                </Td>
                <Td>
                  <p className="text-sm font-bold text-navy-700 dark:text-white">
                    {user.mail}
                  </p>
                </Td>
                <Td>
                  <img
                    className="h-10 w-10 min-h-10 min-w-10 rounded-full"
                    src={user.avatar}
                    alt={`${user.avatar}`}
                  />
                </Td>
                {!isInVmTabs && (
                  <>
                    <Td>
                      <p className="text-sm font-bold text-navy-700 dark:text-white">
                        {t(`enums.role.${user.role.name.toLowerCase()}`)}
                      </p>
                    </Td>
                    <Td className="flex gap-2 border-white/0 py-3 pr-4 items-center">
                      <GroupsModal userId={user.id} />
                      {selfUser && selfUser.id !== user.id && (
                        <RolesModal user={user} />
                      )}
                    </Td>
                  </>
                )}
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
