import { Dispatch, SetStateAction, useState } from "react";
import { ColumnHeaderHelperWithoutSort } from "common/TableHelper/ColumnHeaderHelper";
import { GroupsTableColumnEnum } from "enums/columns/groupsTableColumn.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { TableNamesEnum } from "enums/table.enum";
import { usePermissions } from "hooks/usePermission";
import { IGroupUsers } from "interfaces/group.interface";
import { useTranslation } from "react-i18next";
import {
  deleteUserGroupsDelete,
  postUserGroupsAdd,
} from "services/users/users.service";

import {
  Badge,
  Table,
  TableContainer,
  Tbody,
  Td,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const AddGroupUserButton = ({
  groupId,
  userId,
  isArchived,
  refresh,
}: {
  groupId: string;
  userId: string;
  isArchived: boolean;
  refresh: Dispatch<SetStateAction<boolean>>;
}) => {
  const [disabled, setDisabled] = useState(false);

  const onAddingGroup = async () => {
    setDisabled(true);
    const result = await postUserGroupsAdd({ userId, groupId });
    if (result) {
      setDisabled(true);
      refresh(true);
    } else {
      setDisabled(false);
    }
  };
  return (
    <button
      disabled={
        !usePermissions(PermissionNameEnum.UPDATE_USER) ||
        isArchived ||
        disabled
      }
      onClick={onAddingGroup}
      className="bg-green-500 hover:enabled:bg-green-600 active:bg-green-700 dark:bg-green-400 dark:hover:enabled:bg-green-300 dark:active:bg-green-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
    >
      <FontAwesomeIcon className="h-4 w-4" icon={faPlus} />
    </button>
  );
};

const DeleteGroupUserButton = ({
  groupId,
  userId,
  isArchived,
  refresh,
}: {
  groupId: string;
  userId: string;
  isArchived: boolean;
  refresh: Dispatch<SetStateAction<boolean>>;
}) => {
  const [disabled, setDisabled] = useState(false);

  const onDeletingGroup = async () => {
    setDisabled(true);
    const result = await deleteUserGroupsDelete({ userId, groupId });
    if (result) {
      setDisabled(true);
      refresh(true);
    } else {
      setDisabled(false);
    }
  };
  return (
    <button
      disabled={
        !usePermissions(PermissionNameEnum.UPDATE_USER) ||
        isArchived ||
        disabled
      }
      onClick={onDeletingGroup}
      className="bg-red-500 hover:enabled:bg-red-600 active:bg-red-700 dark:bg-red-400 dark:hover:enabled:bg-red-300 dark:active:bg-red-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
    >
      <FontAwesomeIcon className="h-4 w-4" icon={faMinus} />
    </button>
  );
};

export default function GroupsTableModal({
  tableData,
  refresh,
  userId,
  withUser,
}: {
  tableData: IGroupUsers[];
  refresh: Dispatch<SetStateAction<boolean>>;
  userId: string;
  withUser: boolean;
}) {
  const { t } = useTranslation();
  return (
    <TableContainer overflowY="scroll">
      <Table className="w-full" variant="unstyled">
        <Thead>
          <Tr>
            <ColumnHeaderHelperWithoutSort<typeof GroupsTableColumnEnum>
              tableName={TableNamesEnum.GROUPS}
              headers={GroupsTableColumnEnum}
            />
          </Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {tableData.map((group) => {
            return (
              <Tr key={`Tr-${group.id}`} className="h-16">
                <Td>
                  <p>{group.name}</p>
                  {group.isArchived && (
                    <Badge>{t("groups.table.is_archived")}</Badge>
                  )}
                </Td>
                <Td>
                  <p>{group.description}</p>
                </Td>
                <Td>
                  {withUser ? (
                    <DeleteGroupUserButton
                      groupId={group.id}
                      userId={userId}
                      isArchived={group.isArchived}
                      refresh={refresh}
                    />
                  ) : (
                    <AddGroupUserButton
                      groupId={group.id}
                      userId={userId}
                      isArchived={group.isArchived}
                      refresh={refresh}
                    />
                  )}
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
