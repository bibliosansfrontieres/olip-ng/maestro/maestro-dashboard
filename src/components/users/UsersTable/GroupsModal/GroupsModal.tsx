import { useCallback, useEffect, useState } from "react";
import OutletSkeleton from "common/Feedback/Skeletons/OutletSkeleton";
import PaginationQuery from "common/QueryElements/PaginationQuery";
import Modal from "components/modal/Modal";
import Switch from "components/switch/Switch";
import GroupsTableModal from "components/users/UsersTable/GroupsModal/GroupsTableModal/GroupsTableModal";
import { BooleanEnum } from "enums/boolean.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import useApi from "hooks/useApi";
import { usePermissions } from "hooks/usePermission";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { useTranslation } from "react-i18next";
import { IGetUsersIdGroupResponse } from "services/users/interfaces/getUsersIdGroup.interface";
import { getUsersIdGroups } from "services/users/users.service";

import { useDisclosure } from "@chakra-ui/hooks";
import { Tooltip } from "@chakra-ui/react";
import { faMagnifyingGlass, faUsers } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const GroupsTableHelper = ({
  value,
  userId,
  withUser,
}: {
  value: string;
  userId: string;
  withUser: boolean;
}) => {
  const [params, setParams] = useState({} as IPageRequest);
  const [waitedValue, setWaitedValue] = useState(value);
  useEffect(() => {
    const timeOutId = setTimeout(() => {
      setWaitedValue(value);
      //Your search query and it will run the function after 1sec from user stops typing
    }, 1000);
    return () => clearTimeout(timeOutId);
  }, [value]);

  const {
    result,
    isLoading,
    setReload: refresh,
  } = useApi<IGetUsersIdGroupResponse>(
    useCallback(() => {
      return getUsersIdGroups({
        query: waitedValue,
        id: userId,
        isUserInGroup:
          BooleanEnum[withUser.toString().toUpperCase() as BooleanEnum],
        ...params,
      });
    }, [waitedValue, params, withUser, userId]),
  );

  if (isLoading) {
    return <OutletSkeleton />;
  } else if (!result) {
    return;
  }

  return (
    <>
      <GroupsTableModal
        tableData={result.data}
        userId={userId}
        refresh={refresh}
        withUser={withUser}
      />
      <PaginationQuery meta={result.meta} setParams={setParams} />
    </>
  );
};
export default function GroupsModal({ userId }: { userId: string }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { t } = useTranslation();
  const [value, setValue] = useState("");
  const [withUser, setWithUser] = useState(false);

  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2 pl-3 max-w-32"
        label={t("users.users_modal.title")}
        placement="top"
        openDelay={500}
        aria-label={t("users.users_modal.title")}
      >
        <button
          onClick={() => {
            onOpen();
            setValue("");
            setWithUser(true);
          }}
          disabled={
            !usePermissions([
              PermissionNameEnum.READ_GROUP,
              PermissionNameEnum.READ_USER,
            ])
          }
          className="bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-4 w-4" icon={faUsers} />
        </button>
      </Tooltip>

      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
            {t("users.users_modal.title")}
          </h1>
          <div className="flex items-center rounded-full h-10 bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white mb-2">
            <p className="pl-3 pr-2 text-xl">
              <FontAwesomeIcon
                className="h-4 w-4 text-gray-400 dark:text-white"
                icon={faMagnifyingGlass}
              />
            </p>
            <input
              type="text"
              value={value}
              placeholder={t("navbar.search")}
              onChange={(e) => setValue(e.target.value)}
              className="block h-full !w-full rounded-full bg-lightPrimary text-sm font-medium text-navy-700 outline-none placeholder:!text-gray-400 dark:bg-navy-900 dark:text-white dark:placeholder:!text-white sm:w-fit"
            />
          </div>
          <div className="flex flex-col gap-2 my-2">
            <div className="flex gap-1">
              <Switch
                id="withUser"
                checked={withUser}
                onChange={() => setWithUser((prev) => !prev)}
              />
              <label
                htmlFor="withUser"
                className="text-sm text-navy-700 dark:text-white"
              >
                {t("users.users_modal.checkbox.with_user")}
              </label>
            </div>
          </div>
          <GroupsTableHelper
            userId={userId}
            value={value}
            withUser={withUser}
          />
        </>
      </Modal>
    </>
  );
}
