import { useState } from "react";
import { ColumnHeaderHelperWithoutSort } from "common/TableHelper/ColumnHeaderHelper";
import { EntraUserTableColumnEnum } from "enums/columns/userTableColumn.enum";
import { EntraUserDto } from "interfaces/entraUser.interface";
import { postImportUser } from "services/entra/entra.service";
import { TableNamesEnum } from "src/enums/table.enum";

import { Table, TableContainer, Tbody, Td, Thead, Tr } from "@chakra-ui/react";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const AddEntraUserButton = ({
  isReferenced,
  id,
}: {
  isReferenced: boolean;
  id: string;
}) => {
  const [disabled, setDisabled] = useState(false);
  const onAddEntraUser = async () => {
    setDisabled(true);
    const result = await postImportUser({ id });
    if (result) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  };
  return (
    <button
      disabled={isReferenced || disabled}
      onClick={onAddEntraUser}
      className="bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
    >
      <FontAwesomeIcon className="h-4 w-4" icon={faPlus} />
    </button>
  );
};

export default function EntraUsersTable({
  tableData,
}: {
  tableData: EntraUserDto[];
}) {
  return (
    <TableContainer overflowY="scroll">
      <Table className="w-full" variant="unstyled">
        <Thead>
          <Tr>
            <ColumnHeaderHelperWithoutSort<typeof EntraUserTableColumnEnum>
              tableName={TableNamesEnum.USER}
              headers={EntraUserTableColumnEnum}
            />
          </Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {tableData.map((user) => {
            return (
              <Tr key={`Tr-${user.id}`} className="h-16">
                <Td>
                  <p>{user.displayName}</p>
                </Td>
                <Td>
                  <p>{user.mail}</p>
                </Td>
                <Td className="border-white/0 py-3 pr-4">
                  <AddEntraUserButton
                    isReferenced={user.isReferenced}
                    id={user.id}
                  />
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
