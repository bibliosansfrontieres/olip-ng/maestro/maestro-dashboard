import { useCallback, useContext, useEffect, useState } from "react";
import OutletSkeleton from "common/Feedback/Skeletons/OutletSkeleton";
import Modal from "components/modal/Modal";
import EntraUsersTable from "components/users/UsersModal/EntraUsersTable/EntraUsersTable";
import { UsersContext } from "contexts/users.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import useApi from "hooks/useApi";
import { usePermissions } from "hooks/usePermission";
import { useTranslation } from "react-i18next";
import { getEntraUsers } from "services/entra/entra.service";
import { IGetEntraUsersResponse } from "services/entra/interfaces/getEntraUsers.interface";

import { useDisclosure } from "@chakra-ui/hooks";
import { faMagnifyingGlass, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const EntraUsersTableHelper = ({ value }: { value: string }) => {
  const [waitedValue, setWaitedValue] = useState(value);
  useEffect(() => {
    const timeOutId = setTimeout(() => {
      setWaitedValue(value);
      //Your search query and it will run the function after 1sec from user stops typing
    }, 1000);
    return () => clearTimeout(timeOutId);
  }, [value]);
  const { result, isLoading } = useApi<IGetEntraUsersResponse>(
    useCallback(async () => {
      if (waitedValue.length > 0) {
        return getEntraUsers({ query: waitedValue });
      }
    }, [waitedValue]),
  );

  if (value.length === 0) {
    return;
  }
  if (isLoading) {
    return <OutletSkeleton />;
  } else if (!result) {
    return;
  }
  return <EntraUsersTable tableData={result!.data} />;
};
export default function UsersModal() {
  const { setReload } = useContext(UsersContext);

  const { t } = useTranslation();
  const [value, setValue] = useState("");
  const { isOpen, onOpen, onClose } = useDisclosure();
  const handleClose = () => {
    onClose();
    setValue("");
    setReload(true);
  };

  return (
    <>
      <button
        className="btn-primary"
        onClick={onOpen}
        disabled={!usePermissions(PermissionNameEnum.IMPORT_USER)}
      >
        <FontAwesomeIcon className="mr-2" icon={faPlus} />
        {t("users.page.button")}
      </button>

      <Modal disclosure={{ isOpen, onClose: handleClose }}>
        <>
          <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
            {t("users.page.button")}
          </h1>
          <div className="flex items-center rounded-full h-10 bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white mb-2">
            <p className="pl-3 pr-2 text-xl">
              <FontAwesomeIcon
                className="h-4 w-4 text-gray-400 dark:text-white"
                icon={faMagnifyingGlass}
              />
            </p>
            <input
              type="text"
              value={value}
              placeholder={t("navbar.search")}
              onChange={(e) => setValue(e.target.value)}
              className="block h-full !w-full rounded-full bg-lightPrimary text-sm font-medium text-navy-700 outline-none placeholder:!text-gray-400 dark:bg-navy-900 dark:text-white dark:placeholder:!text-white sm:w-fit"
            />
          </div>
          <EntraUsersTableHelper value={value} />
        </>
      </Modal>
    </>
  );
}
