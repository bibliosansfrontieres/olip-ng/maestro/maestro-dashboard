import { ColumnHeaderHelper } from "common/TableHelper/ColumnHeaderHelper";
import { TooltipCell, TooltipP } from "common/TableHelper/TooltipCell";
import ConnectedDevicesModal from "components/fqdns/fqdnTable/ConnectedDevicesModal";
import { FqdnMaskTableColumEnum } from "enums/columns/fqdnMaskTableColumn.enum";
import { FqdnMaskOrderByEnum } from "enums/orderBy/fqdnMaskOrderBy.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { TableNamesEnum } from "enums/table.enum";
import { usePermissions } from "hooks/usePermission";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import {
  Table,
  TableContainer,
  Tbody,
  Td,
  Thead,
  Tooltip,
  Tr,
} from "@chakra-ui/react";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function FqdnTable({
  fqdns,
}: {
  fqdns: (IFqdnMask & { devices: IDevice[] })[];
}) {
  const hasPermissionReadDevice = usePermissions([
    PermissionNameEnum.READ_DEVICE,
  ]);
  const { t } = useTranslation();
  return (
    <TableContainer pt={5}>
      <Table variant="unstyled">
        <Thead>
          <Tr>
            <ColumnHeaderHelper<
              typeof FqdnMaskTableColumEnum,
              typeof FqdnMaskOrderByEnum
            >
              tableName={TableNamesEnum.FQDN}
              headers={FqdnMaskTableColumEnum}
              orderByEnum={FqdnMaskOrderByEnum}
            />
          </Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {fqdns.map((fqdn) => {
            return (
              <Tr key={`Tr-${fqdn.id}`} className="h-16">
                <Td>
                  {/* because device is displayed in fqdn card */}
                  {hasPermissionReadDevice ? (
                    <Link to={`/fqdns/${fqdn.id}`}>
                      <TooltipP
                        key="label"
                        element={fqdn.label}
                        pStyle="underline"
                      />
                    </Link>
                  ) : (
                    <TooltipP key="label" element={fqdn.label} />
                  )}
                </Td>

                <TooltipCell key="mask" element={fqdn.mask} />
                <Td>
                  <ConnectedDevicesModal
                    devices={fqdn.devices}
                    label={fqdn.label}
                  />
                </Td>
                <Td>
                  {hasPermissionReadDevice && (
                    <Tooltip
                      className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
                      label={t("fqdn.table.open")}
                      placement="top"
                      openDelay={500}
                      aria-label={t("fqdn.table.open")}
                    >
                      <Link
                        to={`/fqdns/${fqdn.id}`}
                        className="h-8 w-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
                      >
                        <FontAwesomeIcon className="h-4 w-4" icon={faEye} />
                      </Link>
                    </Tooltip>
                  )}
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
