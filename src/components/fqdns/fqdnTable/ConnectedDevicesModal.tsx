import DeviceTable from "components/devices/deviceTable/DeviceTable";
import Modal from "components/modal/Modal";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { useTranslation } from "react-i18next";

import { Tooltip, useDisclosure } from "@chakra-ui/react";

export default function ConnectedDevicesModal({
  devices,
  label,
}: {
  devices: IDevice[];
  label: string;
}) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const hasPermissionReadDevice = usePermissions([
    PermissionNameEnum.READ_DEVICE,
  ]);
  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
        label={t("fqdn.devices_modal.open")}
        placement="top"
        openDelay={500}
        aria-label={t("fqdn.devices_modal.open")}
      >
        <button
          onClick={onOpen}
          disabled={!hasPermissionReadDevice}
          className="text-md w-8 h-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-lg text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          {devices.length}
        </button>
      </Tooltip>
      <Modal disclosure={{ isOpen, onClose }}>
        <>
          <h1 className="mb-[20px] font-bold text-2xl dark:text-white text-navy-700">
            {t("fqdn.devices_modal.title") + " " + label}
          </h1>
          <DeviceTable
            devices={
              devices as (IDevice & {
                fqdnMask: IFqdnMask;
                deploys: IDeployWithVmAndLabelsAndUser[] | [];
              })[]
            }
          />
        </>
      </Modal>
    </>
  );
}
