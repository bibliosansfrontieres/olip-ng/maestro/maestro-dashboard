import { useCallback } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import FqdnTable from "components/fqdns/fqdnTable/FqdnTable";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useSearchParams } from "react-router-dom";
import { getFqdns } from "services/fqdnMasks/fqdns.service";
import { IGetFqdnsResponse } from "services/fqdnMasks/interfaces/getFqdns.interface";

export default function FqdnsCard() {
  const [params] = useSearchParams();

  const { result, isLoading, error } = useApi<IGetFqdnsResponse>(
    useCallback(async () => {
      return await getFqdns({
        order: params.get("order") as PageOrderEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        orderBy: params.get("orderBy") || undefined,
        query: params.get("query") || undefined,
      });
    }, [params]),
  );
  return (
    <>
      {result && !isLoading && !error && (
        <>
          <FqdnTable fqdns={result.data} />
          <Pagination meta={result.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !result && <ErrorList translation="fqdn.get_fqdns.error" />}
    </>
  );
}
