import { useEffect, useState } from "react";
import DeviceTable from "components/devices/deviceTable/DeviceTable";
import { IDeployWithVmAndLabelsAndUser } from "interfaces/deploy.interface";
import { IDevice } from "interfaces/device.interface";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { useTranslation } from "react-i18next";
import { getDevicesFromMask } from "src/services/devices/devices.service";

export default function DeviceMaskTable({ mask }: { mask: string }) {
  const { t } = useTranslation();
  const [devices, setDevices] = useState([] as IDevice[]);
  useEffect(() => {
    const setDevicesMasks = async () => {
      const result = await getDevicesFromMask({ mask });
      if (result) {
        setDevices(result);
      }
    };
    setDevicesMasks();
  }, [mask]);
  return (
    <div className="mb-3 ml-3 mt-2">
      {devices.length > 0 ? (
        <>
          <p className="italic">
            {t("fqdn.card.form.num_devices", {
              number: devices.length,
            })}
          </p>
          <DeviceTable
            devices={
              devices as (IDevice & {
                fqdnMask: IFqdnMask;
                deploys: IDeployWithVmAndLabelsAndUser[] | [];
              })[]
            }
          />
        </>
      ) : (
        <p className="italic">{t("fqdn.card.form.no_devices")}</p>
      )}
    </div>
  );
}
