import DeviceMaskTable from "components/fqdns/FqdnCard/DeviceMaskTable";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { FormikHelpers, useFormik } from "formik";
import { useTranslation } from "react-i18next";
import { useRevalidator } from "react-router-dom";
import { toast } from "react-toastify";
import { patchFqdn } from "services/fqdnMasks/fqdns.service";
import { IFqdnMask } from "src/interfaces/fqdnMask.interface";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

interface IProps {
  label: string;
  mask: string;
}

export default function FqdnEditForm({
  fqdn,
  setIsEditting,
}: {
  fqdn: IFqdnMask;
  setIsEditting: (prev: boolean) => void;
}) {
  const { t } = useTranslation();
  const revalidator = useRevalidator();

  const validationSchema = Yup.object({
    label: Yup.string().required(t("fqdn.card.form.validation.label.required")),
    mask: Yup.string().required(t("fqdn.card.form.validation.mask.required")),
  });
  const formik = useFormik({
    initialValues: { label: fqdn.label, mask: fqdn.mask } as IProps,
    validationSchema: validationSchema,
    onSubmit: async (
      values: IProps,
      { setSubmitting }: FormikHelpers<IProps>,
    ) => {
      const initialValues = { label: fqdn.label, mask: fqdn.mask };
      const modifiedValues = {} as IProps;
      const keyValues = Object.keys(values);
      // add only different values
      for (const key of keyValues) {
        if (
          values[key as keyof IProps] !== initialValues[key as keyof IProps]
        ) {
          modifiedValues[key as keyof IProps] = values[key as keyof IProps];
        }
      }
      if (Object.keys(modifiedValues).length === 0) {
        setSubmitting(false);
        toast.error(t("fqdn.patch_fqdn.no_change"));

        return;
      }
      const result = await patchFqdn({ ...modifiedValues, id: fqdn.id });
      setSubmitting(false);
      if (result) {
        revalidator.revalidate();
        setIsEditting(false);
      }
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <FormControl isInvalid={!!formik.errors.label}>
        <InputField
          id="label"
          label={t("fqdn.card.form.label")}
          extra="mt-3"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          name="label"
          value={formik.values.label}
          state={formik.errors.label ? "error" : undefined}
          required={true}
          autofocus={true}
        />
        {formik.errors.label && (
          <FormErrorMessageText text={formik.errors.label} />
        )}
      </FormControl>
      <FormControl isInvalid={!!formik.errors.mask}>
        <InputField
          id="mask"
          label={t("fqdn.card.form.mask")}
          extra="mt-3"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          name="mask"
          value={formik.values.mask}
          state={formik.errors.mask ? "error" : undefined}
          required={true}
        />
      </FormControl>
      <DeviceMaskTable mask={formik.values.mask} />
      <button
        type="submit"
        className="btn-primary mt-5 "
        disabled={
          formik.isSubmitting ||
          !formik.isValid ||
          JSON.stringify(formik.initialValues) === JSON.stringify(formik.values)
        }
      >
        {t("button.submit")}
      </button>
    </form>
  );
}
