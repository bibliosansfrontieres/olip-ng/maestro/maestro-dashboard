import { useState } from "react";
import Card from "components/card/Card";
import { IFqdnMask } from "interfaces/fqdnMask.interface";
import { useTranslation } from "react-i18next";
import { PermissionNameEnum } from "src/enums/permissionName.enum";
import { usePermissions } from "src/hooks/usePermission";

import { faPen, faX } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import DeviceMaskTable from "./DeviceMaskTable";
import FqdnEditForm from "./FqdnEditForm";

export default function FqdnCard({ fqdn }: { fqdn: IFqdnMask }) {
  const { t } = useTranslation();
  const [isEditting, setIsEditting] = useState(false);
  const hasPermissionToEdit = usePermissions(PermissionNameEnum.UPDATE_FQDN);
  return (
    <Card>
      <>
        <div className="flex place-content-between items-center">
          <h1 className="text-2xl font-bold dark:text-white text-navy-700">
            {isEditting ? t("fqdn.card.form.title") : t("fqdn.card.title")}
          </h1>
          <button
            className="border-[1px] border-brand-500 hover:border-brand-600 dark:border-brand-400 dark:hover:text-brand-300 text-brand-500 hover:text-brand-600 dark:text-brand-400 disabled:cursor-not-allowed disabled:opacity-40 p-2 rounded-lg transition duration-200 flex"
            onClick={() => setIsEditting((prev) => !prev)}
            disabled={!hasPermissionToEdit}
          >
            <FontAwesomeIcon
              className="h-4 w-4"
              icon={isEditting ? faX : faPen}
            />
          </button>
        </div>
        {!isEditting && (
          <>
            <div className="flex gap-2 items-center">
              <p className="font-bold">{t("fqdn.card.label")}</p>
              {fqdn.label}
            </div>
            <div className="flex gap-2 items-center">
              <p className="font-bold">{t("fqdn.card.mask")}</p>
              {fqdn.mask}
            </div>
            <DeviceMaskTable mask={fqdn.mask} />
          </>
        )}
        {isEditting && (
          <FqdnEditForm fqdn={fqdn} setIsEditting={setIsEditting} />
        )}
      </>
    </Card>
  );
}
