import { Dispatch, SetStateAction, useContext, useState } from "react";
import { ColumnHeaderQueryHelper } from "common/QueryElements/ColumnHeaderQueryHelper";
import { RedirectButton } from "common/TableHelper/Buttons";
import { ColumnHeaderHelper } from "common/TableHelper/ColumnHeaderHelper";
import GroupDeletionModal from "components/groups/GroupDeletionModal/GroupDeletionModal";
import GroupEditionModal from "components/groups/GroupEditionModal/GroupEditionModal";
import UsersModal from "components/vm/Tabs/GroupsTab/UsersModal/UsersModal";
import { GroupsContext } from "contexts/groups.context";
import { GroupsTableColumnEnum } from "enums/columns/groupsTableColumn.enum";
import { GroupOrderByEnum } from "enums/orderBy/groupOrderBy.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { TableNamesEnum } from "enums/table.enum";
import { useCanPatchVm, usePermissions } from "hooks/usePermission";
import {
  IGroupUsers,
  IGroupUsersVirtualMachines,
} from "interfaces/group.interface";
import { IPageRequest } from "interfaces/pageRequest.interface";
import { useOutletContext, useRevalidator } from "react-router-dom";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import {
  deleteVirtualMachineGroup,
  postVirtualMachineGroup,
} from "services/virtualMachines/virtualMachines.service";

import { Table, TableContainer, Tbody, Td, Thead, Tr } from "@chakra-ui/react";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const DeleteGroupVmButton = ({ groupId }: { groupId: string }) => {
  const vm = useOutletContext() as IGetVirtualMachineResponse;
  const revalidator = useRevalidator();
  const { setReload: refresh } = useContext(GroupsContext);
  const userCanPatch = useCanPatchVm(vm, true);
  const updateVMPermission = usePermissions(
    PermissionNameEnum.UPDATE_VIRTUAL_MACHINE,
  );

  const [disabled, setDisabled] = useState(false);
  const onDeletingGroup = async () => {
    setDisabled(true);
    const result = await deleteVirtualMachineGroup({ id: vm.id, groupId });
    if (result) {
      setDisabled(true);
      // refresh for the group list
      refresh(true);
      // revalidator to revalidate the vm
      revalidator.revalidate();
    } else {
      setDisabled(false);
    }
  };
  return (
    <>
      {userCanPatch && (
        <button
          disabled={!updateVMPermission || disabled || vm.isArchived}
          onClick={onDeletingGroup}
          className="bg-red-500 hover:enabled:bg-red-600 active:bg-red-700 dark:bg-red-400 dark:hover:enabled:bg-red-300 dark:active:bg-red-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-4 w-4" icon={faMinus} />
        </button>
      )}
    </>
  );
};

const AddGroupVmButton = ({ groupId }: { groupId: string }) => {
  const vm = useOutletContext() as IGetVirtualMachineResponse;
  const [disabled, setDisabled] = useState(false);
  const revalidator = useRevalidator();
  const { setReload: refresh } = useContext(GroupsContext);
  const userCanPatch = useCanPatchVm(vm, true);
  const isGroupInVm =
    vm.groups.findIndex((group) => group.id === groupId) !== -1;
  const onAddingGroup = async () => {
    setDisabled(true);
    const result = await postVirtualMachineGroup({ id: vm.id, groupId });
    if (result) {
      setDisabled(true);
      // refresh for the group list
      refresh(true);
      // revalidator to revalidate the vm
      revalidator.revalidate();
    } else {
      setDisabled(false);
    }
  };
  const updateVMPermission = usePermissions(
    PermissionNameEnum.UPDATE_VIRTUAL_MACHINE,
  );
  return (
    <>
      {!isGroupInVm && userCanPatch && (
        <button
          disabled={!updateVMPermission || disabled || vm.isArchived}
          onClick={onAddingGroup}
          className="bg-green-500 hover:enabled:bg-green-600 active:bg-green-700 dark:bg-green-400 dark:hover:enabled:bg-green-300 dark:active:bg-green-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-4 w-4" icon={faPlus} />
        </button>
      )}
    </>
  );
};

export default function GroupsTable<V extends IPageRequest>({
  tableData,
  isInVmTabs = false,
  isInVmModal = false,
  isInHome = false,
  setParams,
  params,
}: {
  tableData: IGroupUsers[] | IGroupUsersVirtualMachines[];
  isInVmTabs?: boolean;
  isInVmModal?: boolean;
  isInHome?: boolean;
  setParams?: Dispatch<SetStateAction<V>>;
  params?: V;
}) {
  return (
    <TableContainer>
      <Table className="w-full" variant="unstyled">
        <Thead>
          <Tr>
            {!isInHome && (
              <ColumnHeaderHelper<
                typeof GroupsTableColumnEnum,
                typeof GroupOrderByEnum
              >
                tableName={TableNamesEnum.GROUPS}
                headers={GroupsTableColumnEnum}
                orderByEnum={GroupOrderByEnum}
              />
            )}
            {isInHome && setParams && params && (
              <ColumnHeaderQueryHelper<
                typeof GroupsTableColumnEnum,
                typeof GroupOrderByEnum,
                V
              >
                tableName={TableNamesEnum.GROUPS}
                headers={GroupsTableColumnEnum}
                orderByEnum={GroupOrderByEnum}
                setQueryParams={setParams}
                queryParams={params}
              />
            )}
          </Tr>
        </Thead>
        <Tbody className="text-sm font-bold text-navy-700 dark:text-white">
          {tableData.map((group) => {
            return (
              <Tr key={`Tr-${group.id}`} className="h-16">
                <Td>
                  <p>{group.name}</p>
                </Td>
                <Td>
                  <p>{group.description}</p>
                </Td>
                <Td className="flex gap-2">
                  {!isInVmTabs && !isInVmModal && (
                    <>
                      <GroupEditionModal group={group} />
                      <GroupDeletionModal group={group} />
                    </>
                  )}
                  {isInVmTabs && <DeleteGroupVmButton groupId={group.id} />}
                  {isInVmModal && <AddGroupVmButton groupId={group.id} />}
                  {isInVmTabs && group.users && (
                    <UsersModal users={group.users} />
                  )}
                  {isInHome && group.virtualMachines && (
                    <RedirectButton
                      href={`/virtual-machines?groups=${group.id}&isUserVmsFiltered=FALSE`}
                      icon={group.virtualMachines.length.toString()}
                      translationKey="home.groups_table.see_vms"
                    />
                  )}
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
