import { useContext } from "react";
import Modal from "components/modal/Modal";
import { GroupsContext } from "contexts/groups.context";
import { HomePageContext } from "contexts/homePage.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IGroupUsers } from "interfaces/group.interface";
import { useTranslation } from "react-i18next";
import { patchGroup } from "services/groups/groups.service";

import { Tooltip, useDisclosure } from "@chakra-ui/react";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function GroupDeletionModal({ group }: { group: IGroupUsers }) {
  const { setReload: refresh } = useContext(GroupsContext);
  const { setGroupsReload: refreshHomePage } = useContext(HomePageContext);
  const { t } = useTranslation();
  const {
    isOpen: isOpenDelete,
    onOpen: onOpenDelete,
    onClose: onCloseDelete,
  } = useDisclosure();
  const onDeleteGroup = async (id: string) => {
    await patchGroup({ id, isArchived: true });
    refresh(true);
    refreshHomePage(true);
    onCloseDelete();
  };

  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
        label={t("groups.table.actions.delete")}
        placement="top"
        openDelay={500}
        aria-label={t("groups.table.actions.delete")}
      >
        <button
          onClick={onOpenDelete}
          disabled={!usePermissions(PermissionNameEnum.DELETE_GROUP)}
          className="h-8 w-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-3 w-3" icon={faTrash} />
        </button>
      </Tooltip>
      <Modal
        disclosure={{
          isOpen: isOpenDelete,
          onClose: onCloseDelete,
        }}
      >
        <>
          <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
            {t("groups.table.actions.delete")}
          </h1>
          <p className=" text-md dark:text-white text-navy-700">
            {t("groups.table.actions.delete_text", { name: group.name })}
          </p>
          <div className="flex flex-row-reverse gap-2 mt-2">
            <button
              onClick={() => onDeleteGroup(group.id)}
              className="rounded-xl border-2 border-red-500 hover:border-red-600 hover:text-red-600 text-red-500 dark:border-red-400 dark:hover:border-red-300 dark:text-red-400 dark:hover:text-red-300 px-5 py-3 font-medium transition duration-200"
            >
              {t("button.delete")}
            </button>
          </div>
        </>
      </Modal>
    </>
  );
}
