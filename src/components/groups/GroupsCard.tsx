import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import TableSkeleton from "common/Feedback/Skeletons/TableSkeleton";
import Pagination from "common/Pagination";
import GroupsTable from "components/groups/GroupsTable";
import { GroupsContext } from "contexts/groups.context";

export default function GroupsCard() {
  const { groups, isLoading, error, isInVmTabs } = useContext(GroupsContext);
  return (
    <>
      {groups && !isLoading && !error && (
        <>
          <GroupsTable tableData={groups.data} isInVmTabs={isInVmTabs} />
          <Pagination meta={groups.meta} />
        </>
      )}
      {isLoading && <TableSkeleton nbLines={10} />}
      {error && !groups && <ErrorList translation="groups.get_groups.error" />}
    </>
  );
}
