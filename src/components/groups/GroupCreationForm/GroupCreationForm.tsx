import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { GroupsContext } from "contexts/groups.context";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import { postGroup } from "services/groups/groups.service";
import { IPostGroupRequest } from "services/groups/interfaces/postGroup.interface";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

export default function GroupCreationForm({
  onClose,
}: {
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const { setReload: refresh } = useContext(GroupsContext);
  const validationSchema = Yup.object({
    name: Yup.string()
      .min(2, t("groups.creation.validation.name.length"))
      .required(t("groups.creation.validation.name.required")),
  });
  const formik = useFormik({
    initialValues: { name: "", description: "" } as IPostGroupRequest,
    validationSchema: validationSchema,
    onSubmit: async (values, { setSubmitting }) => {
      const modifiedValues = { ...values } as IPostGroupRequest;

      for (const key of Object.keys(values)) {
        if (
          key === "description" &&
          values[key as keyof IPostGroupRequest] === ""
        ) {
          modifiedValues[key as "description"] = undefined;
        }
      }
      setSubmitting(false);
      const result = await postGroup(modifiedValues);
      if (result) {
        refresh(true);
        onClose();
      }
    },
  });

  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("groups.creation.title")}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <FormControl isInvalid={!!formik.errors.name}>
          <InputField
            id="name"
            label={t("groups.creation.label.name")}
            extra="mt-3"
            type="text"
            placeholder={t("groups.creation.placeholder.name")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="name"
            value={formik.values.name}
            state={formik.errors.name ? "error" : undefined}
            required={true}
            autofocus={true}
          />
          {formik.errors.name && (
            <FormErrorMessageText text={formik.errors.name} />
          )}
        </FormControl>
        <FormControl>
          <InputField
            label={t("groups.creation.label.description")}
            id="description"
            extra="mt-3"
            type="text"
            placeholder={t("groups.creation.placeholder.description")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="description"
            value={formik.values.description}
          />
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5 "
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
