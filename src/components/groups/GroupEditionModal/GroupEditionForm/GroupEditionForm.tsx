import { useContext } from "react";
import InputField from "components/shared/fields/InputField";
import FormErrorMessageText from "components/shared/texts/FormErrorMessageText";
import { GroupsContext } from "contexts/groups.context";
import { HomePageContext } from "contexts/homePage.context";
import { FormikHelpers, useFormik } from "formik";
import { IGroup } from "interfaces/group.interface";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { patchGroup } from "services/groups/groups.service";
import * as Yup from "yup";

import { FormControl } from "@chakra-ui/react";

interface IProps {
  name?: string;
  description?: string;
}
export default function GroupEditionForm({
  group,
  onClose,
}: {
  group: IGroup;
  onClose: () => void;
}) {
  const { t } = useTranslation();
  const { setReload: refreshGroups } = useContext(GroupsContext);
  const { setGroupsReload: refreshHomePage } = useContext(HomePageContext);

  const validationSchema = Yup.object({
    name: Yup.string()
      .min(2, t("groups.creation.validation.name.length"))
      .required(t("groups.creation.validation.name.required")),
  });
  const formik = useFormik({
    initialValues: {
      name: group.name,
      description: group.description ?? "",
    } as IProps,
    validationSchema: validationSchema,
    onSubmit: async (
      values: IProps,
      { setSubmitting }: FormikHelpers<IProps>,
    ) => {
      const initialValues = {
        name: group.name,
        description: group.description ?? "",
      };
      const modifiedValues = {} as IProps;
      const keyValues = Object.keys(values);
      // add only different values
      for (const key of keyValues) {
        if (
          values[key as keyof IProps] !== initialValues[key as keyof IProps]
        ) {
          modifiedValues[key as keyof IProps] = values[key as keyof IProps];
        }
      }
      if (Object.keys(modifiedValues).length === 0) {
        setSubmitting(false);
        toast.error(t("groups.patch_group.no_change"));

        return;
      }
      const result = await patchGroup({ ...modifiedValues, id: group.id });
      setSubmitting(false);
      if (result) {
        refreshGroups(true);
        refreshHomePage(true);
        onClose();
      }
    },
  });

  return (
    <>
      <h1 className="mb-[20px] text-2xl font-bold dark:text-white text-navy-700">
        {t("groups.table.actions.edit")}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <FormControl isInvalid={!!formik.errors.name}>
          <InputField
            id="name"
            label={t("groups.creation.label.name")}
            extra="mt-3"
            type="text"
            placeholder={t("groups.creation.placeholder.name")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="name"
            value={formik.values.name}
            state={formik.errors.name ? "error" : undefined}
            required={true}
            autofocus={true}
          />
          {formik.errors.name && (
            <FormErrorMessageText text={formik.errors.name} />
          )}
        </FormControl>
        <FormControl>
          <InputField
            label={t("groups.creation.label.description")}
            id="description"
            extra="mt-3"
            type="text"
            placeholder={t("groups.creation.placeholder.description")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="description"
            value={formik.values.description}
          />
        </FormControl>
        <button
          type="submit"
          className="btn-primary mt-5 "
          disabled={
            formik.isSubmitting ||
            !formik.isValid ||
            JSON.stringify(formik.initialValues) ===
              JSON.stringify(formik.values)
          }
        >
          {t("button.submit")}
        </button>
      </form>
    </>
  );
}
