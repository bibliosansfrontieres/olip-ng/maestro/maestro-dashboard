import GroupEditionForm from "components/groups/GroupEditionModal/GroupEditionForm/GroupEditionForm";
import Modal from "components/modal/Modal";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { IGroupUsers } from "interfaces/group.interface";
import { useTranslation } from "react-i18next";

import { Tooltip, useDisclosure } from "@chakra-ui/react";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function GroupEditionModal({ group }: { group: IGroupUsers }) {
  const { t } = useTranslation();
  const {
    isOpen: isOpenEdit,
    onOpen: onOpenEdit,
    onClose: onCloseEdit,
  } = useDisclosure();

  return (
    <>
      <Tooltip
        className="bg-lightPrimary text-navy-700 dark:bg-navy-900 dark:text-white rounded-full text-sm p-2"
        label={t("groups.table.actions.edit")}
        placement="top"
        openDelay={500}
        aria-label={t("groups.table.actions.edit")}
      >
        <button
          onClick={onOpenEdit}
          disabled={!usePermissions(PermissionNameEnum.UPDATE_GROUP)}
          className="h-8 w-8 bg-brand-500 hover:enabled:bg-brand-600 active:bg-brand-700 dark:bg-brand-400 dark:hover:enabled:bg-brand-300 dark:active:bg-brand-200 flex items-center justify-center rounded-full p-2 text-3xl text-white transition duration-200 hover:cursor-pointer dark:text-white disabled:opacity-25 disabled:cursor-not-allowed"
        >
          <FontAwesomeIcon className="h-3 w-3" icon={faPen} />
        </button>
      </Tooltip>
      <Modal disclosure={{ isOpen: isOpenEdit, onClose: onCloseEdit }}>
        <GroupEditionForm group={group} onClose={onCloseEdit} />
      </Modal>
    </>
  );
}
