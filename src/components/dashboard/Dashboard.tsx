import { useState } from "react";
import Progress from "common/Feedback/Progress";
import Widget from "components/card/Widget";
import Switch from "components/switch/Switch";
import {
  IDisksIoData,
  IFsSizeData,
  IMemData,
  INetworkStatsData,
} from "interfaces/monitoring.interface";
import { useTranslation } from "react-i18next";
import useSocketOnEvent from "src/hooks/useSocketOnEvent";
import { IMonitoringValues } from "src/interfaces/monitoringValues.interface";

import {
  faDownload,
  faFile,
  faFileArrowDown,
  faFileArrowUp,
  faGauge,
  faGaugeHigh,
  faHardDrive,
  faHourglassEnd,
  faHourglassHalf,
  faHourglassStart,
  faMemory,
  faUpload,
  IconDefinition,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const LittleCard = ({
  title,
  value,
  icon,
  children,
}: {
  title: string;
  value: string | undefined | number;
  icon: IconDefinition;
  children?: React.ReactNode;
}) => (
  <Widget
    title={title}
    subtitle={value ? `${value}` : "-"}
    icon={<FontAwesomeIcon className="text-2xl" icon={icon} />}
    children={children}
  />
);

const memoryUsage = (memory: IMemData | undefined) => {
  if (!memory) {
    return { used: 0, total: 0 };
  }

  const used = ((memory.total - memory.available) / 1024 ** 3).toFixed(2);
  const total = (memory.total / 1024 ** 3).toFixed(2);
  return { used, total };
};

const memoryPercentage = (memory: IMemData | undefined) => {
  if (!memory) {
    return 0;
  }
  return ((memory.total - memory.available) / memory.total) * 100;
};

const rxStats = (networkStats: INetworkStatsData[] | undefined) => {
  if (!networkStats) {
    return 0;
  }
  return (networkStats[0]?.rx_sec / 1024 ** 2).toFixed(2);
};

const txStats = (networkStats: INetworkStatsData[] | undefined) => {
  if (!networkStats) {
    return 0;
  }
  return (networkStats[0]?.tx_sec / 1024 ** 2).toFixed(2);
};

const iOSec = (
  disksIo: IDisksIoData | undefined,
  type: "rIO_sec" | "wIO_sec" | "tIO_sec",
) => {
  const typedDisksIo = disksIo?.[type];
  if (typedDisksIo === undefined || typedDisksIo === null) {
    return "-";
  }
  return (typedDisksIo / 1000).toFixed(2);
};

const waitTime = (
  disksIo: IDisksIoData | undefined,
  type: "rWaitTime" | "wWaitTime" | "tWaitTime",
) => {
  if (!disksIo) {
    return "-";
  }
  return disksIo[type];
};

const fsSizePercentage = (fsSize: IFsSizeData[] | undefined) => {
  const fsSizeOverlay = fsSize?.find((fs) => fs.mount === "/");
  if (!fsSizeOverlay) {
    return 0;
  }
  return (fsSizeOverlay.used / fsSizeOverlay.size) * 100;
};

const fsSizeUsage = (fsSize: IFsSizeData[] | undefined) => {
  const fsSizeOverlay = fsSize?.find((fs) => fs.mount === "/");
  if (!fsSizeOverlay) {
    return { used: 0, total: 0 };
  }

  const used = (fsSizeOverlay.used / 1024 ** 3).toFixed(2);
  const total = (fsSizeOverlay.size / 1024 ** 3).toFixed(2);
  return { used, total };
};

export default function Dashboard() {
  const { t } = useTranslation();

  const [monitoringValues, setMonitoringValues] = useState<IMonitoringValues>();
  const [isRefreshAutomatic, setIsRefreshAutomatic] = useState(true);

  useSocketOnEvent(
    `monitoring.values`,
    (result: { monitoringValues: IMonitoringValues }) => {
      if (!isRefreshAutomatic) {
        return;
      }
      setMonitoringValues(result.monitoringValues);
    },
  );

  return (
    <>
      <div className="flex gap-2 items-center mb-4">
        <Switch
          id="automaticRefresh"
          checked={isRefreshAutomatic}
          onChange={() => setIsRefreshAutomatic((value) => !value)}
        />
        <label
          htmlFor="automaticRefresh"
          className="text-base text-navy-700 dark:text-white"
        >
          {t("dashboard.switch.label")}
        </label>
      </div>
      <div className="flex flex-col gap-4">
        <div className="flex gap-6 flex-wrap">
          <LittleCard
            title={t("dashboard.load.average_load")}
            value={monitoringValues?.load?.avgLoad.toFixed(2)}
            icon={faGauge}
          />
          <LittleCard
            title={t("dashboard.load.current_load")}
            value={monitoringValues?.load?.currentLoad.toFixed(2)}
            icon={faGaugeHigh}
          />
          <LittleCard
            title={t("dashboard.memory.memory")}
            value={t(
              "dashboard.memory.memory_usage",
              memoryUsage(monitoringValues?.memory),
            )}
            children={
              <Progress value={memoryPercentage(monitoringValues?.memory)} />
            }
            icon={faMemory}
          />
          <LittleCard
            title={t("dashboard.fs_size.size")}
            value={t(
              "dashboard.fs_size.size_usage",
              fsSizeUsage(monitoringValues?.fsSize),
            )}
            children={
              <Progress value={fsSizePercentage(monitoringValues?.fsSize)} />
            }
            icon={faHardDrive}
          />
        </div>
        <div className="flex gap-6 flex-wrap">
          <LittleCard
            title={t("dashboard.network_stats.rx_sec")}
            value={t("dashboard.network_stats.rx_sec_value", {
              rx_sec: rxStats(monitoringValues?.networkStats),
            })}
            icon={faDownload}
          />
          <LittleCard
            title={t("dashboard.network_stats.tx_sec")}
            value={t("dashboard.network_stats.tx_sec_value", {
              tx_sec: txStats(monitoringValues?.networkStats),
            })}
            icon={faUpload}
          />
        </div>
        <div className="flex gap-6 flex-wrap">
          <LittleCard
            title={t("dashboard.disks_io.rio_sec")}
            value={t("dashboard.disks_io.io_sec_value", {
              io_sec: iOSec(monitoringValues?.disksIo, "rIO_sec"),
            })}
            icon={faFileArrowUp}
          />
          <LittleCard
            title={t("dashboard.disks_io.wio_sec")}
            value={t("dashboard.disks_io.io_sec_value", {
              io_sec: iOSec(monitoringValues?.disksIo, "wIO_sec"),
            })}
            icon={faFileArrowDown}
          />
          <LittleCard
            title={t("dashboard.disks_io.tio_sec")}
            value={t("dashboard.disks_io.io_sec_value", {
              io_sec: iOSec(monitoringValues?.disksIo, "tIO_sec"),
            })}
            icon={faFile}
          />
        </div>
        <div className="flex gap-6 flex-wrap">
          <LittleCard
            title={t("dashboard.disks_io.r_wait_time")}
            value={t("dashboard.disks_io.wait_time_value", {
              wait_time: waitTime(monitoringValues?.disksIo, "rWaitTime"),
            })}
            icon={faHourglassStart}
          />
          <LittleCard
            title={t("dashboard.disks_io.w_wait_time")}
            value={t("dashboard.disks_io.wait_time_value", {
              wait_time: waitTime(monitoringValues?.disksIo, "wWaitTime"),
            })}
            icon={faHourglassEnd}
          />
          <LittleCard
            title={t("dashboard.disks_io.t_wait_time")}
            value={t("dashboard.disks_io.wait_time_value", {
              wait_time: waitTime(monitoringValues?.disksIo, "tWaitTime"),
            })}
            icon={faHourglassHalf}
          />
        </div>
      </div>
    </>
  );
}
