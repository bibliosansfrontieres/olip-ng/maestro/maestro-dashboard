import getEnv from "utils/getEnv";

import axios from "@axios";

const instance = axios.create({
  baseURL: getEnv("PUBLIC_API_URL"),
  timeout: 60000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

instance.interceptors.request.use((config) => {
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken) {
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
});

export default instance;
