export const translationsList = [
  "ara",
  "ben",
  "deu",
  "ell",
  "eng",
  "fas",
  "fra",
  "fuc",
  "hau",
  "hin",
  "ita",
  "nld",
  "pol",
  "prs",
  "pus",
  "ron",
  "rus",
  "spa",
  "swa",
  "ukr",
];

//Don't forget to add the translation at language.[name] in fr.json and en.json
