import { PermissionNameEnum } from "enums/permissionName.enum";
import { Page } from "src/types";

import {
  faChartSimple,
  faFolder,
  faHome,
  faListUl,
  faLocationPinLock,
  faNetworkWired,
  faPlus,
  faRocket,
  faServer,
  faTag,
  faTerminal,
  faUser,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const menuListPages: Page[] = [
  {
    name: "sidebar.pages.home",
    link: "/",
    icon: <FontAwesomeIcon icon={faHome} />,
  },
  {
    name: "sidebar.pages.vm_create",
    link: "/create",
    icon: <FontAwesomeIcon icon={faPlus} />,
    permission: PermissionNameEnum.CREATE_VIRTUAL_MACHINE,
  },
  {
    name: "sidebar.pages.vm_list",
    link: "/virtual-machines",
    icon: <FontAwesomeIcon icon={faListUl} />,
    permission: PermissionNameEnum.READ_VIRTUAL_MACHINE,
  },
  {
    name: "sidebar.pages.devices",
    link: "/devices",
    icon: <FontAwesomeIcon icon={faServer} />,
    permission: PermissionNameEnum.READ_DEVICE,
  },
  {
    name: "sidebar.pages.deploys",
    link: "/deploys",
    icon: <FontAwesomeIcon icon={faRocket} />,
    permission: PermissionNameEnum.READ_DEPLOY,
  },
  {
    name: "sidebar.pages.projects",
    link: "/projects",
    icon: <FontAwesomeIcon icon={faFolder} />,
    permission: PermissionNameEnum.READ_PROJECT,
  },
];

export const menuAdminListPages: Page[] = [
  {
    name: "sidebar.pages.dashboard",
    link: "/dashboard",
    icon: <FontAwesomeIcon icon={faChartSimple} />,
    permission: PermissionNameEnum.READ_USER,
  },
  {
    name: "sidebar.pages.users",
    link: "/users",
    icon: <FontAwesomeIcon icon={faUser} />,
    permission: PermissionNameEnum.READ_USER,
  },
  {
    name: "sidebar.pages.groups",
    link: "/groups",
    icon: <FontAwesomeIcon icon={faUsers} />,
    permission: PermissionNameEnum.READ_GROUP,
  },
  {
    name: "sidebar.pages.context",
    link: "/context",
    icon: <FontAwesomeIcon icon={faLocationPinLock} />,
    permission: PermissionNameEnum.UPDATE_DEFAULT_CONTEXT,
  },
  {
    name: "sidebar.pages.fqdns",
    link: "/fqdns",
    icon: <FontAwesomeIcon icon={faNetworkWired} />,
    permission: PermissionNameEnum.READ_FQDN,
  },
  {
    name: "sidebar.pages.labels",
    link: "/labels",
    icon: <FontAwesomeIcon icon={faTag} />,
    permission: PermissionNameEnum.READ_LABEL,
  },
  {
    name: "sidebar.pages.logs",
    link: "/logs",
    icon: <FontAwesomeIcon icon={faTerminal} />,
    permission: PermissionNameEnum.READ_LOGS,
  },
];

export const otherPages: Page[] = [
  {
    name: "sidebar.pages.notifications",
    link: "/notifications",
  },
];
