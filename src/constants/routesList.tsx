import {
  menuAdminListPages,
  menuListPages,
  otherPages,
} from "constants/menuListPages";
import { Route } from "src/types";

export const routesList: Route[] = menuListPages
  .concat(menuAdminListPages)
  .concat(otherPages)
  .map((page) => ({
    name: page.name,
    link: page.link,
  }));
