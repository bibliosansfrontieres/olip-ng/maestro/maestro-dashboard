import { Suspense } from "react";
import ErrorLoaderCard from "common/ErrorLoader/ErrorLoaderCard";
import CardSkeleton from "common/Feedback/Skeletons/CardSkeleton";
import DeployCard from "components/deploys/deployCard/DeployCard";
import { Await, useLoaderData } from "react-router-dom";
import { IGetDeployResponse } from "services/deploys/interfaces/getDeploy.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

export default function DeployPage() {
  const loaderData = useLoaderData() as {
    result: Promise<ApiRequestResponse<IGetDeployResponse>>;
  };

  return (
    <Suspense fallback={<CardSkeleton />}>
      <Await
        resolve={loaderData.result}
        errorElement={
          <ErrorLoaderCard translation="deploy.get_deploy_loader.error" />
        }
      >
        {(response: IGetDeployResponse) => <DeployCard deploy={response} />}
      </Await>
    </Suspense>
  );
}
