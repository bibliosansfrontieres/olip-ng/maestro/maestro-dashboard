import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import VMsCard from "components/vm/VMList/VMs";
import VmTableHelper from "components/vm/VMList/VmTableHelper/VmTableHelper";

export default function VMs() {
  return (
    <Card>
      <>
        <SearchBarTable />
        <VmTableHelper />
        <VMsCard />
      </>
    </Card>
  );
}
