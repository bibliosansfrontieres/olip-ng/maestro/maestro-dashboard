import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import LogsCard from "components/log/LogsCard";
import LogsTableHelper from "components/log/LogsTableHelper";

export default function LogsPage({
  isInVmTabs = false,
}: {
  isInVmTabs?: boolean;
}) {
  return (
    <Card>
      <>
        {isInVmTabs && <SearchBarTable />}
        <LogsTableHelper isInVmTabs={isInVmTabs} />
        <LogsCard />
      </>
    </Card>
  );
}
