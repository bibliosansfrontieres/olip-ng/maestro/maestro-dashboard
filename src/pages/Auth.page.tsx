import { useContext, useEffect } from "react";
import PageSkeleton from "common/Feedback/Skeletons/PageSkeleton";
import { UserContext } from "contexts/user.context";
import { useNavigate, useSearchParams } from "react-router-dom";

export default function AuthPage() {
  const navigate = useNavigate();
  const { login } = useContext(UserContext);
  const [params] = useSearchParams();
  const accessToken = params.get("accessToken");
  const refreshToken = params.get("refreshToken");
  const expiresAt = params.get("expiresAt");
  const redirection = params.get("redirect") || undefined;
  if (accessToken) {
    localStorage.setItem("accessToken", accessToken);
  }
  if (refreshToken) {
    localStorage.setItem("refreshToken", refreshToken);
  }
  if (expiresAt) {
    localStorage.setItem("expiresAt", expiresAt);
  }

  useEffect(() => {
    login().then(() => {
      navigate(redirection || "/");
    });
  }, [navigate, redirection, login]);

  return <PageSkeleton />;
}
