import { Suspense } from "react";
import ErrorLoaderCard from "common/ErrorLoader/ErrorLoaderCard";
import CardSkeleton from "common/Feedback/Skeletons/CardSkeleton";
import ProjectCard from "components/projects/projectCard/ProjectCard";
import ErrorCard from "components/vm/Tabs/InformationTab/ErrorCard";
import { Await, useLoaderData } from "react-router-dom";
import { IGetProjectResponse } from "services/projects/interfaces/getProject.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

export default function ProjectPage() {
  const loaderData = useLoaderData() as {
    result: Promise<ApiRequestResponse<IGetProjectResponse>>;
  };

  return (
    <Suspense fallback={<CardSkeleton />}>
      <Await
        resolve={loaderData.result}
        errorElement={
          <ErrorLoaderCard translation="project.get_project_loader.error" />
        }
      >
        {(response: IGetProjectResponse) => (
          <div className="flex flex-col gap-4">
            <ProjectCard project={response} inProjectPage={true} />
            {response.projectErrors && response.projectErrors.length > 0 && (
              <ErrorCard
                errors={response.projectErrors}
                count={response.projectErrors.length}
              />
            )}
          </div>
        )}
      </Await>
    </Suspense>
  );
}
