import { Suspense } from "react";
import ErrorLoaderCard from "common/ErrorLoader/ErrorLoaderCard";
import CardSkeleton from "common/Feedback/Skeletons/CardSkeleton";
import FqdnCard from "components/fqdns/FqdnCard/FqdnCard";
import { Await, useLoaderData } from "react-router-dom";
import { IGetFqdnResponse } from "services/fqdnMasks/interfaces/getFqdn.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

export default function FqdnPage() {
  const loaderData = useLoaderData() as {
    result: Promise<ApiRequestResponse<IGetFqdnResponse>>;
  };

  return (
    <Suspense fallback={<CardSkeleton />}>
      <Await
        resolve={loaderData.result}
        errorElement={
          <ErrorLoaderCard translation="fqdn.get_fqdn_loader.error" />
        }
      >
        {(response: IGetFqdnResponse) => <FqdnCard fqdn={response} />}
      </Await>
    </Suspense>
  );
}
