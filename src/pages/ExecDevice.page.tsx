import { useCallback, useRef, useState } from "react";
import Card from "components/card/Card";
import ExecDeviceFull from "components/devices/execDevice/ExecDeviceFull";
import useApi from "hooks/useApi";
import { t } from "i18next";
import { IFullConvo } from "interfaces/fullConvo.interface";
import { useLocation } from "react-router-dom";
import { getDevice } from "services/devices/devices.service";

import { Modal, ModalContent, useDisclosure } from "@chakra-ui/react";
import {
  faDownLeftAndUpRightToCenter,
  faUpRightAndDownLeftFromCenter,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ExecDevicePage() {
  const [fullConvo, setFullConvo] = useState<IFullConvo[]>([]);
  const finalFocusRef = useRef(null);
  const initialFocusRef = useRef(null);
  const location = useLocation();
  const { result: device } = useApi(
    useCallback(
      () => getDevice({ id: location.pathname.split("/")[2] }),
      [location.pathname],
    ),
  );

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Card>
      <div className="flex flex-col gap-4">
        <div className="flex justify-between">
          <h3 className="text-xl font-bold text-navy-700 dark:text-white">
            {t("device.exec.title", {
              macAddress: device?.macAddress,
            })}
          </h3>
          <button className="btn-primary self-end" onClick={onOpen}>
            <FontAwesomeIcon icon={faUpRightAndDownLeftFromCenter} />
          </button>
        </div>
        <Modal
          isOpen={isOpen}
          onClose={onClose}
          size="full"
          finalFocusRef={finalFocusRef}
          initialFocusRef={initialFocusRef}
        >
          <ModalContent className="p-2 dark:bg-navy-800">
            <div className="flex justify-between mb-3">
              <h3 className="text-xl font-bold text-navy-700 dark:text-white">
                {t("device.exec.title", {
                  macAddress: device?.macAddress,
                })}
              </h3>
              <button className="btn-primary" onClick={onClose}>
                <FontAwesomeIcon icon={faDownLeftAndUpRightToCenter} />
              </button>
            </div>
            {device && device.isAlive && (
              <ExecDeviceFull
                ref={initialFocusRef}
                fullConvo={fullConvo}
                setFullConvo={setFullConvo}
              />
            )}
          </ModalContent>
        </Modal>
        {device && device.isAlive && (
          <ExecDeviceFull
            fullConvo={fullConvo}
            setFullConvo={setFullConvo}
            ref={finalFocusRef}
          />
        )}
        {!device || (!device.isAlive && <div>{t("device.exec.error")}</div>)}
      </div>
    </Card>
  );
}
