import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import FqdnsCard from "components/fqdns/FqdnsCard";

export default function FqdnsPage() {
  return (
    <Card>
      <>
        <SearchBarTable />
        <FqdnsCard />
      </>
    </Card>
  );
}
