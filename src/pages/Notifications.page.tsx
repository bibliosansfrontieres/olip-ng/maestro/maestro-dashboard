import Card from "components/card/Card";
import NotificationsCard from "components/notifications/NotificationsCard";
import NotificationTableHelper from "components/notifications/NotificationTable/NotificationTableHelper";
import { NotificationsContextProvider } from "src/contexts/notifications.context";

export default function NotificationsPage() {
  return (
    <Card>
      <NotificationsContextProvider>
        <NotificationTableHelper />
        <NotificationsCard />
      </NotificationsContextProvider>
    </Card>
  );
}
