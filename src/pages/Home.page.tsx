import { useContext } from "react";
import ErrorList from "common/Feedback/Errors/ErrorList";
import { SimpleTableCardSkeleton } from "common/Feedback/Skeletons/TableCardSkeleton";
import PaginationQuery from "common/QueryElements/PaginationQuery";
import { SearchBarTableQuery } from "common/QueryElements/SearchBarTableQuery";
import Card from "components/card/Card";
import GroupsTable from "components/groups/GroupsTable";
import VmTable from "components/vm/VMList/VmTable/VmTable";
import VmTableQueryHelper from "components/vm/VMList/VmTableHelper/VmTableQueryHelper";
import { HomePageContext } from "contexts/homePage.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { useTranslation } from "react-i18next";

export default function HomePage() {
  const { t } = useTranslation();
  const hasPermissionVms = usePermissions(
    PermissionNameEnum.READ_VIRTUAL_MACHINE,
  );
  const hasPermissionGroups = usePermissions(PermissionNameEnum.READ_GROUP);

  const {
    groupsResult,
    groupsAreLoading,
    groupsErrors,
    groupsParams,
    setGroupsParams,
    vmsResult,
    vmsAreLoading,
    vmsErrors,
    vmsParams,
    setVmsParams,
  } = useContext(HomePageContext);

  return (
    <div className="flex flex-col gap-8">
      {hasPermissionVms && (
        <Card extraStyle="overflow-x-scroll">
          <div className="flex flex-col gap-4">
            <h1 className="text-xl font-bold text-navy-700 dark:text-white">
              {t("home.virtualMachines")}
            </h1>
            <SearchBarTableQuery setParams={setVmsParams} />
            <VmTableQueryHelper setParams={setVmsParams} />

            <>
              {vmsResult && !vmsAreLoading && !vmsErrors && (
                <>
                  <VmTable
                    tableData={vmsResult!.data}
                    inHomePage={true}
                    setParams={setVmsParams}
                    params={vmsParams}
                  />
                  <PaginationQuery
                    meta={vmsResult!.meta}
                    setParams={setVmsParams}
                  />
                </>
              )}
            </>

            {vmsAreLoading && <SimpleTableCardSkeleton nbLines={5} />}
            {vmsErrors && !vmsResult && (
              <ErrorList translation="vm.get_vms.error" />
            )}
          </div>
        </Card>
      )}
      {hasPermissionGroups && (
        <Card extraStyle="overflow-x-scroll">
          <div className="flex flex-col gap-4">
            <h1 className="text-xl font-bold text-navy-700 dark:text-white">
              {t("home.groups")}
            </h1>
            <SearchBarTableQuery setParams={setGroupsParams} />
            <>
              {groupsResult && !groupsAreLoading && !groupsErrors && (
                <>
                  <GroupsTable
                    tableData={groupsResult.data}
                    isInHome={true}
                    setParams={setGroupsParams}
                    params={groupsParams}
                  />
                  <PaginationQuery
                    meta={groupsResult.meta}
                    setParams={setGroupsParams}
                  />
                </>
              )}
              {groupsAreLoading && <SimpleTableCardSkeleton nbLines={5} />}
              {groupsErrors && !groupsResult && (
                <ErrorList translation="groups.get_groups.error" />
              )}
            </>
          </div>
        </Card>
      )}
    </div>
  );
}
