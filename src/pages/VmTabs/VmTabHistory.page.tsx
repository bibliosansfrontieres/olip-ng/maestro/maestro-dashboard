import LogsPage from "pages/Logs.page";

export default function VmTabHistoryPage() {
  return <LogsPage isInVmTabs={true} />;
}
