import GroupsPage from "pages/Groups.page";

export default function VmTabGroupsPage() {
  return <GroupsPage isInVmTabs={true} />;
}
