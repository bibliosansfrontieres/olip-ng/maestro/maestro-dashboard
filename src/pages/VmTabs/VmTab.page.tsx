import { useContext } from "react";
import ErrorLoaderCard from "common/ErrorLoader/ErrorLoaderCard";
import CardSkeleton from "common/Feedback/Skeletons/CardSkeleton";
import VmTabs from "components/vm/Tabs/VmTabs";
import VmTabsCard from "components/vm/Tabs/VmTabsCard";
import { VmContext } from "contexts/vm.context";

import { SkeletonText } from "@chakra-ui/react";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const TabsSkeleton = () => {
  return (
    <>
      <div className="flex gap-2 flex-wrap sm:flex-nowrap mb-4">
        {[1, 2, 3, 4, 5, 6].map((_, index) => (
          <button
            key={index}
            className="btn-primary-base truncate w-40 bg-gray-500  dark:bg-gray-400 opacity-25 dark:opacity-40 cursor-not-allowed"
            disabled={true}
          >
            <div className="flex center gap-1 items-center">
              <FontAwesomeIcon className="mr-2" icon={faCircle} />
              <SkeletonText width="100px" noOfLines={1} />
            </div>
          </button>
        ))}
      </div>
      <VmTabs />
      <div className="mt-4">
        <CardSkeleton />
      </div>
    </>
  );
};

export default function VmTabPage() {
  const { vm, isLoading, error } = useContext(VmContext);

  return (
    <>
      {isLoading && <TabsSkeleton />}
      {vm && !isLoading && !error && <VmTabsCard vm={vm} />}
      {error && <ErrorLoaderCard translation="vm.get_vm.error" />}
    </>
  );
}
