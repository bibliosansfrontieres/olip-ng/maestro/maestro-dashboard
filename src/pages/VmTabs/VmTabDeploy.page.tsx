import DeploysPage from "pages/Deploys.page";

export default function VmTabDeployPage() {
  return <DeploysPage isInVmTabs={true} />;
}
