import { Suspense } from "react";
import ErrorLoaderCard from "common/ErrorLoader/ErrorLoaderCard";
import CardSkeleton from "common/Feedback/Skeletons/CardSkeleton";
import ContextTab from "components/vm/Tabs/ContextTab/ContextTab";
import { Await, useLoaderData } from "react-router-dom";
import { IGetApplicationsResponse } from "services/applications/interfaces/getApplications.interface";
import { IGetContextsDefaultResponse } from "services/contexts/interfaces/getContextsDefault.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

export default function ContextPage() {
  const loaderData = useLoaderData() as {
    result: Promise<{
      applications: ApiRequestResponse<IGetApplicationsResponse>;
      defaultContext: ApiRequestResponse<IGetContextsDefaultResponse>;
    }>;
  };

  return (
    <Suspense fallback={<CardSkeleton />}>
      <Await
        resolve={loaderData.result}
        errorElement={<ErrorLoaderCard translation="vm.get_vm_loader.error" />}
      >
        {({ applications, defaultContext }) => {
          return (
            <ContextTab
              applications={applications}
              defaultContext={defaultContext}
            />
          );
        }}
      </Await>
    </Suspense>
  );
}
