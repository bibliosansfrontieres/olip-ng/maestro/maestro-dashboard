import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import SaveTab from "components/vm/Tabs/SaveTab/SaveTab";
import SaveTableHelper from "components/vm/Tabs/SaveTab/SaveTableHelper/SaveTableHelper";

export default function VmTabSavePage() {
  return (
    <Card>
      <>
        <SearchBarTable />
        <SaveTableHelper />
        <SaveTab />
      </>
    </Card>
  );
}
