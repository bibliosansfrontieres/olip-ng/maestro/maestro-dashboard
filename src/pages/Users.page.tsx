import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import UsersModal from "components/users/UsersModal/UsersModal";
import UsersCard from "components/users/UsersTable/UsersCard";
import { UsersContextProvider } from "src/contexts/users.context";

export default function UsersPage() {
  return (
    <Card>
      <UsersContextProvider>
        <>
          <header className="relative flex flex-wrap items-center justify-between">
            <SearchBarTable />
            <UsersModal />
          </header>
          <UsersCard />
        </>
      </UsersContextProvider>
    </Card>
  );
}
