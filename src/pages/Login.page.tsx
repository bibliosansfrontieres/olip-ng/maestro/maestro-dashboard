import { useCallback } from "react";
import bsfLogoBlanc from "assets/img/bsf-logo/bsfLogoBlanc.svg";
import ErrorCard from "components/card/error/ErrorCard";
import FixedPlugin from "components/fixedPlugin/FixedPlugin";
import Footer from "components/footer/Footer";
import Login from "components/login/Login";
import useApi from "hooks/useApi";
import { useSearchParams } from "react-router-dom";
import { getLoginUrl } from "services/entra/entra.service";
import { IGetLoginUrlResponse } from "services/entra/interfaces/getLoginUrl.interface";

export default function LoginPage() {
  const [searchParams] = useSearchParams();

  const {
    error: isError,
    isLoading,
    result,
  } = useApi<IGetLoginUrlResponse>(
    useCallback(
      () => getLoginUrl({ redirect: searchParams.get("redirect") || "/" }),
      [searchParams],
    ),
  );

  return (
    <>
      {isError ? (
        <ErrorCard />
      ) : (
        <>
          <div className="relative float-right h-full min-h-screen w-full !bg-white dark:!bg-navy-900">
            <FixedPlugin />
            <main className={`mx-auto min-h-screen`}>
              <div className="relative flex">
                <div className="mx-auto flex min-h-full w-full flex-col justify-start pt-12 lg:h-screen lg:max-w-[1013px] lg:px-8 lg:pt-0 xl:h-[100vh] xl:max-w-[1383px] xl:px-0 xl:pl-[70px]">
                  <div className="mb-auto flex flex-col pl-5 pr-5 lg:max-w-[48%] lg:pl-0 xl:max-w-full">
                    {!isLoading && result ? (
                      <Login o365Url={result.url ?? ""} />
                    ) : null}

                    <div className="flex justify-center items-center absolute right-0 rounded-bl-[120px] lg:min-h-screen lg:w-[45vw] w-0 h-0 bg-brand-500 dark:bg-brand-400">
                      <img
                        className="h-80 w-80 mb-[80px]"
                        src={bsfLogoBlanc}
                        alt=""
                      />
                    </div>
                  </div>
                  <Footer />
                </div>
              </div>
            </main>
          </div>
        </>
      )}
    </>
  );
}
