import { useContext, useEffect, useState } from "react";
import Footer from "components/footer/Footer";
import Navbar from "components/navbar/Navbar";
import Sidebar from "components/sidebar/Sidebar";
import { UserContext } from "contexts/user.context";
import { BooleanEnum } from "enums/boolean.enum";
import useSocketOnEvent from "hooks/useSocketOnEvent";
import { INotification } from "interfaces/notification.interface";
import { useTranslation } from "react-i18next";
import { Outlet, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import { getNotifications } from "services/notifications/notifications.service";
import { socket } from "src/socket.io";

export default function MainLayout(): JSX.Element {
  const { pathname } = useLocation();
  const [open, setOpen] = useState(!(window.innerWidth < 1200));
  const { t } = useTranslation();
  const [numberNotificationsUnread, setNumberNotificationsUnread] = useState(0);
  const { user } = useContext(UserContext);
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);
  useEffect(() => {
    window.addEventListener("resize", () =>
      window.innerWidth < 1200 ? setOpen(false) : setOpen(true),
    );
  }, []);

  useEffect(() => {
    socket.on("connect", () => {});
    socket.on("error", () => toast.error(t("error.page.something_went_wrong")));
    return () => {
      socket.off("connect", () => {});
      socket.off("error", () => {});
    };
  }, [t]);

  useEffect(() => {
    const callNotification = async () => {
      const result = await getNotifications({
        isNotSeenOnly: BooleanEnum.TRUE,
        take: 1,
      });
      if (result) {
        setNumberNotificationsUnread(result.meta.count);
      }
    };
    callNotification();
  }, []);

  useSocketOnEvent(
    `user.${user?.id}.notification`,
    (result: { notification?: INotification; count: number }) => {
      setNumberNotificationsUnread(result.count);
      if (result.notification) {
        toast.info(result.notification.message);
      }
    },
  );
  return (
    <div className="flex h-full w-full overflow-x-hidden">
      <Sidebar open={open} onClose={() => setOpen(false)} />
      <div className="md:pr-2 w-full xl:pl-[240px] bg-lightPrimary dark:!bg-navy-900">
        <main className="mb-auto transition-all">
          <div id="top">
            <Navbar
              onOpenSidenav={() => setOpen((prev) => !prev)}
              numberNotificationsUnread={numberNotificationsUnread}
            />
            <div
              className="mx-5 mt-4"
              style={{ minHeight: "calc(100vh - 172px)" }}
            >
              <Outlet />
            </div>
          </div>
        </main>
        <div>
          <Footer />
        </div>
      </div>
    </div>
  );
}
