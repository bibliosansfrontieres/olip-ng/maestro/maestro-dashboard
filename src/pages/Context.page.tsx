import { Suspense } from "react";
import ErrorLoaderCard from "common/ErrorLoader/ErrorLoaderCard";
import CardSkeleton from "common/Feedback/Skeletons/CardSkeleton";
import ContextCard from "components/context/ContextCard";
import { Await, useLoaderData } from "react-router-dom";
import { IGetApplicationsResponse } from "services/applications/interfaces/getApplications.interface";
import { IGetContextsDefaultResponse } from "services/contexts/interfaces/getContextsDefault.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

export default function ContextPage() {
  const contextData = useLoaderData() as {
    result: Promise<{
      applications: ApiRequestResponse<IGetApplicationsResponse>;
      defaultContext: ApiRequestResponse<IGetContextsDefaultResponse>;
    }>;
  };

  return (
    <Suspense fallback={<CardSkeleton />}>
      <Await
        resolve={contextData.result}
        errorElement={
          <ErrorLoaderCard translation="contexts.get_context_loader.error" />
        }
      >
        {({ applications, defaultContext }) => {
          return (
            <ContextCard
              applications={applications}
              context={defaultContext}
              isInDefaultContext={true}
            />
          );
        }}
      </Await>
    </Suspense>
  );
}
