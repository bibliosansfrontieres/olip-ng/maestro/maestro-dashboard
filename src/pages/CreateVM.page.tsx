import Card from "components/card/Card";
import VerifyProject from "components/projects/projectCreation/VerifyProject";

export default function CreateVMPage() {
  return (
    <Card>
      <VerifyProject inVmPage={true} />
    </Card>
  );
}
