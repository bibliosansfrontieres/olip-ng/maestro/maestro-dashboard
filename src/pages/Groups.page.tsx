import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import GroupCreationForm from "components/groups/GroupCreationForm/GroupCreationForm";
import GroupsCard from "components/groups/GroupsCard";
import Modal from "components/modal/Modal";
import GroupsAddModal from "components/vm/Tabs/GroupsTab/GroupAddModal/GroupAddModal";
import { GroupsContextProvider } from "contexts/groups.context";
import { PermissionNameEnum } from "enums/permissionName.enum";
import { usePermissions } from "hooks/usePermission";
import { useTranslation } from "react-i18next";

import { useDisclosure } from "@chakra-ui/react";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function GroupsPage({
  isInVmTabs = false,
}: {
  isInVmTabs?: boolean;
}) {
  const { t } = useTranslation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const createGroupPermission = usePermissions(PermissionNameEnum.CREATE_GROUP);

  return (
    <Card>
      <GroupsContextProvider isInVmTabs={isInVmTabs}>
        <>
          <header className="relative flex flex-wrap items-center justify-between">
            <SearchBarTable />
            {isInVmTabs ? (
              <GroupsAddModal />
            ) : (
              <>
                <button
                  onClick={onOpen}
                  className="btn-primary"
                  disabled={!createGroupPermission}
                >
                  <FontAwesomeIcon className="mr-2" icon={faPlus} />
                  {t("groups.page.button")}
                </button>
                <Modal disclosure={{ isOpen, onClose }}>
                  <GroupCreationForm onClose={onClose} />
                </Modal>
              </>
            )}
          </header>
          <GroupsCard />
        </>
      </GroupsContextProvider>
    </Card>
  );
}
