import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import DevicesCard from "components/devices/DevicesCard";
import DeviceTableHelper from "components/devices/deviceTable/DeviceTableHelper";
import { DevicesContextProvider } from "src/contexts/devices.context";

export default function DevicesPage() {
  return (
    <Card>
      <DevicesContextProvider>
        <SearchBarTable />
        <DeviceTableHelper />
        <DevicesCard />
      </DevicesContextProvider>
    </Card>
  );
}
