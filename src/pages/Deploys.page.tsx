import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import DeploysCard from "components/deploys/DeploysCard";
import DeployTableHelper from "components/deploys/DeployTableHelper/DeployTableHelper";
import { DeploysContextProvider } from "src/contexts/deploys.context";

export default function DeploysPage({
  isInVmTabs = false,
}: {
  isInVmTabs?: boolean;
}) {
  return (
    <Card>
      <DeploysContextProvider isInVmTabs={isInVmTabs}>
        <SearchBarTable />
        <DeployTableHelper />
        <DeploysCard />
      </DeploysContextProvider>
    </Card>
  );
}
