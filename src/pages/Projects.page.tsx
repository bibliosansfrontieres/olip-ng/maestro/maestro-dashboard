import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import ProjectsCard from "components/projects/projectsCard/ProjectsCard";
import CreateProjectModal from "components/projects/projectTable/CreateProjectModal/CreateProjectModal";
import ProjectTableHelper from "components/projects/projectTable/ProjectTableHelper";
import { ProjectsContextProvider } from "contexts/projects.context";

export default function ProjectsPage() {
  return (
    <Card>
      <ProjectsContextProvider>
        <>
          <header className="relative flex flex-wrap items-center justify-between">
            <SearchBarTable />
            <CreateProjectModal />
          </header>

          <ProjectTableHelper />
          <ProjectsCard />
        </>
      </ProjectsContextProvider>
    </Card>
  );
}
