import { useContext } from "react";
import { UserContext } from "contexts/user.context";
import { useTranslation } from "react-i18next";
import {
  isRouteErrorResponse,
  Link,
  Navigate,
  useLocation,
  useRouteError,
} from "react-router-dom";

import { AxiosError } from "@axios";

export default function ErrorPage() {
  const error = useRouteError() as AxiosError;

  console.error(error);
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const { pathname } = useLocation();
  if (!user) {
    return <Navigate to={`/login?redirect=${pathname}`} replace />;
  }
  if (error.response?.status === 401) {
    return <Navigate to={`/`} replace />;
  }

  const message = isRouteErrorResponse(error)
    ? error.statusText
    : error?.response?.statusText || error?.message || "Unknown error";
  return (
    <div id="error-page" className="ml-2">
      <h1>Oops!</h1>
      <p>{t("error.page.something_went_wrong")}</p>
      <p className="mb-5">
        <i>{message}</i>
      </p>
      <Link
        className="linear bg-brand-500 hover:bg-brand-600 dark:bg-brand-400 dark:hover:bg-brand-300 rounded-xl px-5 py-3 text-base font-medium text-white transition duration-200 dark:text-white"
        to="/"
      >
        {t("error.page.go_back")}
      </Link>
    </div>
  );
}
