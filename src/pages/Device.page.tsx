import { Suspense } from "react";
import ErrorLoaderCard from "common/ErrorLoader/ErrorLoaderCard";
import CardSkeleton from "common/Feedback/Skeletons/CardSkeleton";
import DeviceCard from "components/devices/DeviceCard";
import { Await, useLoaderData } from "react-router-dom";
import { IGetDeviceResponse } from "services/devices/interfaces/getDevice.interface";
import { ApiRequestResponse } from "types/apiRequestResponse.type";

export default function DevicePage() {
  const loaderData = useLoaderData() as {
    result: Promise<ApiRequestResponse<IGetDeviceResponse>>;
  };

  return (
    <Suspense fallback={<CardSkeleton />}>
      <Await
        resolve={loaderData.result}
        errorElement={
          <ErrorLoaderCard translation="device.get_device_loader.error" />
        }
      >
        {(response: IGetDeviceResponse) => <DeviceCard device={response} />}
      </Await>
    </Suspense>
  );
}
