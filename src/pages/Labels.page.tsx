import { SearchBarTable } from "common/SearchBarTable";
import Card from "components/card/Card";
import LabelsCard from "components/labels/LabelsCard";
import LabelsTableHelper from "components/labels/LabelsTableHelper";
import { LabelsContextProvider } from "contexts/labels.context";

export default function LabelsPage() {
  return (
    <Card>
      <LabelsContextProvider>
        <SearchBarTable />
        <LabelsTableHelper />
        <LabelsCard />
      </LabelsContextProvider>
    </Card>
  );
}
