import { createContext, ReactNode, useCallback } from "react";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useLocation, useSearchParams } from "react-router-dom";
import { getDeploys } from "services/deploys/deploys.service";
import { IGetDeploysResponse } from "services/deploys/interfaces/getDeploys.interface";
import { IGetVirtualMachineDeploysResponse } from "services/virtualMachines/interfaces/getVirtualMachineDeploys.interface";
import { getVirtualMachineDeploys } from "services/virtualMachines/virtualMachines.service";
import { VirtualMachineDeployOrderByEnum } from "src/enums/orderBy/virtualMachineDeployOrderBy.enum";

type ContextValue = {
  deploys: IGetDeploysResponse | IGetVirtualMachineDeploysResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
  isInVmTabs?: boolean;
};

const defaultValue: ContextValue = {
  deploys: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
  isInVmTabs: false,
};

export const DeploysContext = createContext<ContextValue>(defaultValue);

export const DeploysContextProvider = ({
  children,
  isInVmTabs = false,
}: {
  children: ReactNode;
  isInVmTabs?: boolean;
}) => {
  const [params] = useSearchParams();
  const location = useLocation();
  const id = isInVmTabs ? location.pathname.split("/")[2] : undefined;

  const {
    result: deploys,
    isLoading,
    error,
    setReload,
  } = useApi<
    IGetDeploysResponse | IGetVirtualMachineDeploysResponse | undefined
  >(
    useCallback(async () => {
      if (id) {
        return await getVirtualMachineDeploys({
          id,
          order: params.get("order") as PageOrderEnum,
          page: Number(params.get("page")) || undefined,
          take: Number(params.get("take")) || undefined,
          query: params.get("query") || undefined,
          orderBy:
            (params.get("orderBy") as VirtualMachineDeployOrderByEnum) ||
            undefined,
          statuses: params.get("statuses") || undefined,
          userIds: params.get("userIds") || undefined,
          labelIds: params.get("labelIds") || undefined,
          createdDateRange: params.get("createdDateRange") || undefined,
        });
      }

      return await getDeploys({
        order: params.get("order") as PageOrderEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        query: params.get("query") || undefined,
        orderBy: params.get("orderBy") || undefined,
        statuses: params.get("statuses") || undefined,
        userIds: params.get("userIds") || undefined,
        labelIds: params.get("labelIds") || undefined,
        createdDateRange: params.get("createdDateRange") || undefined,
      });
    }, [params, id]),
  );
  const contextValue = {
    deploys,
    isLoading,
    error,
    setReload,
    isInVmTabs,
  };
  return (
    <DeploysContext.Provider value={contextValue}>
      {children}
    </DeploysContext.Provider>
  );
};
