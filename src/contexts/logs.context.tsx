import { createContext, ReactNode, useCallback } from "react";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useLocation, useSearchParams } from "react-router-dom";
import { IGetLogsResponse } from "services/log/interfaces/getLogs.interface";
import { getLogs } from "services/log/logs.service";
import { LogOrderByEnum } from "src/enums/orderBy/logOrderBy.enum";
import { getVirtualMachineHistory } from "src/services/virtualMachines/virtualMachines.service";

type ContextValue = {
  logs: IGetLogsResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  logs: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const LogsContext = createContext<ContextValue>(defaultValue);

export const LogsContextProvider = ({ children }: { children: ReactNode }) => {
  const [params] = useSearchParams();
  const location = useLocation();

  const {
    result: logs,
    isLoading,
    error,
    setReload,
  } = useApi<IGetLogsResponse | undefined>(
    useCallback(async () => {
      const isInVmTabs = location.pathname.includes("virtual-machines");
      const isInHistoryTab = location.pathname.includes("history");
      if (isInVmTabs && isInHistoryTab) {
        const id = location.pathname.split("/")[2];
        if (id) {
          return await getVirtualMachineHistory({
            id,
            order: params.get("order") as PageOrderEnum,
            page: Number(params.get("page")) || undefined,
            take: Number(params.get("take")) || undefined,
            dateRange: params.get("dateRange") || undefined,
            userIds: params.get("userIds") || undefined,
            actions: params.get("actions") || undefined,
            entities: params.get("entities") || undefined,
            levels: params.get("levels") || undefined,
            orderBy: (params.get("orderBy") as LogOrderByEnum) || undefined,
            query: params.get("query") || undefined,
          });
        }
      } else if (!isInVmTabs) {
        return await getLogs({
          order: params.get("order") as PageOrderEnum,
          page: Number(params.get("page")) || undefined,
          take: Number(params.get("take")) || undefined,
          orderBy: params.get("orderBy") || undefined,
          actions: params.get("actions") || undefined,
          entities: params.get("entities") || undefined,
          levels: params.get("levels") || undefined,
          userIds: params.get("userIds") || undefined,
          dateRange: params.get("dateRange") || undefined,
        });
      }
    }, [params, location]),
  );
  const contextValue = {
    logs,
    isLoading,
    error,
    setReload,
  };
  return (
    <LogsContext.Provider value={contextValue}>{children}</LogsContext.Provider>
  );
};
