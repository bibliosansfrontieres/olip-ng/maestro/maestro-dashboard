import { createContext, ReactNode, useCallback } from "react";
import { useSearchParams } from "react-router-dom";
import { PageOrderEnum } from "src/enums/pageOrder.enum";
import { RoleNameEnum } from "src/enums/roleName.enum";
import { UsersOrderFieldsEnum } from "src/enums/usersOrderFields.enum";
import useApi from "src/hooks/useApi";
import { IGetUsersResponse } from "src/services/users/interfaces/getUsers.interface";
import { getUsers } from "src/services/users/users.service";

type ContextValue = {
  users: IGetUsersResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  users: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const UsersContext = createContext<ContextValue>(defaultValue);

export const UsersContextProvider = ({ children }: { children: ReactNode }) => {
  const [params] = useSearchParams();

  const {
    result: users,
    isLoading,
    error,
    setReload,
  } = useApi<IGetUsersResponse>(
    useCallback(async () => {
      return await getUsers({
        order: params.get("order") as PageOrderEnum,
        orderField:
          // @TODO: fix userfieldenum to not need to uppercase => see with back
          params.get("orderBy")?.toUpperCase() as UsersOrderFieldsEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        query: params.get("query") || undefined,
        roleName:
          (params.get("roleName")?.toUpperCase() as RoleNameEnum) || undefined,
      });
    }, [params]),
  );
  const contextValue = {
    users,
    isLoading,
    error,
    setReload,
  };
  return (
    <UsersContext.Provider value={contextValue}>
      {children}
    </UsersContext.Provider>
  );
};
