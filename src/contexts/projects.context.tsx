import { createContext, ReactNode, useCallback } from "react";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useSearchParams } from "react-router-dom";
import { IGetProjectsResponse } from "services/projects/interfaces/getProjects.interface";
import { getProjects } from "services/projects/projects.service";

type ContextValue = {
  projects: IGetProjectsResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  projects: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const ProjectsContext = createContext<ContextValue>(defaultValue);

export const ProjectsContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [params] = useSearchParams();

  const {
    result: projects,
    isLoading,
    error,
    setReload,
  } = useApi<IGetProjectsResponse | undefined>(
    useCallback(async () => {
      return await getProjects({
        order: params.get("order") as PageOrderEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        query: params.get("query") || undefined,
        orderBy: params.get("orderBy") || undefined,
        updatedDateRange: params.get("updatedDateRange") || undefined,
        statuses: params.get("statuses") || undefined,
      });
    }, [params]),
  );
  const contextValue = {
    projects,
    isLoading,
    error,
    setReload,
  };
  return (
    <ProjectsContext.Provider value={contextValue}>
      {children}
    </ProjectsContext.Provider>
  );
};
