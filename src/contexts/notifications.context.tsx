import { createContext, ReactNode, useCallback } from "react";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useSearchParams } from "react-router-dom";
import { IGetNotificationsResponse } from "services/notifications/interfaces/getNotifications.interface";
import { getNotifications } from "services/notifications/notifications.service";
import { BooleanEnum } from "src/enums/boolean.enum";

type ContextValue = {
  notifications: IGetNotificationsResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  notifications: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const NotificationsContext = createContext<ContextValue>(defaultValue);

export const NotificationsContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [params] = useSearchParams();

  const {
    result: notifications,
    isLoading,
    error,
    setReload,
  } = useApi<IGetNotificationsResponse | undefined>(
    useCallback(async () => {
      return await getNotifications({
        order: params.get("order") as PageOrderEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        orderBy: params.get("orderBy") || undefined,
        isNotSeenOnly:
          params.get("isNotSeenOnly") === "FALSE"
            ? BooleanEnum.FALSE
            : BooleanEnum.TRUE,
      });
    }, [params]),
  );
  const contextValue = {
    notifications,
    isLoading,
    error,
    setReload,
  };
  return (
    <NotificationsContext.Provider value={contextValue}>
      {children}
    </NotificationsContext.Provider>
  );
};
