import { createContext, ReactNode, useCallback } from "react";
import { VirtualMachineSaveOrderByEnum } from "enums/orderBy/virtualMachineSaveOrderBy.enum";
import { PageOrderEnum } from "enums/pageOrder.enum";
import { VirtualMachineSaveTypeEnum } from "enums/virtualMachineSaveType.enum";
import useApi from "hooks/useApi";
import { useLocation, useSearchParams } from "react-router-dom";
import { IGetVirtualMachineSavesResponse } from "services/virtualMachineSaves/interfaces/getVirtualMachineSaves.interface";
import { getVirtualMachineSaves } from "services/virtualMachineSaves/virtualMachineSaves.service";

type ContextValue = {
  saves: IGetVirtualMachineSavesResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  saves: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const VmSavesContext = createContext<ContextValue>(defaultValue);

export const VmSavesContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [params] = useSearchParams();
  const location = useLocation();

  const {
    result: saves,
    isLoading,
    error,
    setReload,
  } = useApi<IGetVirtualMachineSavesResponse>(
    useCallback(async () => {
      const vmId = location.pathname.split("/")[2];
      const inSavesTab = location.pathname.includes("save");
      if (vmId && inSavesTab) {
        return await getVirtualMachineSaves({
          vmId,
          order: params.get("order") as PageOrderEnum,
          page: Number(params.get("page")) || undefined,
          take: Number(params.get("take")) || undefined,
          query: params.get("query") || undefined,
          orderBy:
            (params.get("orderBy") as VirtualMachineSaveOrderByEnum) ||
            undefined,
          dateRange: params.get("dateRange") || undefined,
          types:
            (params.get("types") as VirtualMachineSaveTypeEnum) || undefined,
          userIds: params.get("userIds") || undefined,
        });
      }
    }, [params, location]),
  );

  const contextValue = {
    saves,
    isLoading,
    error,
    setReload,
  };
  return (
    <VmSavesContext.Provider value={contextValue}>
      {children}
    </VmSavesContext.Provider>
  );
};
