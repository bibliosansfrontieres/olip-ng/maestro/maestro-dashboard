import { createContext, ReactNode, useCallback } from "react";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useSearchParams } from "react-router-dom";
import { IGetLabelsResponse } from "services/labels/interfaces/getLabels.interface";
import { getLabels } from "services/labels/labels.service";

type ContextValue = {
  labels: IGetLabelsResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  labels: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const LabelsContext = createContext<ContextValue>(defaultValue);

export const LabelsContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [params] = useSearchParams();

  const {
    result: labels,
    isLoading,
    error,
    setReload,
  } = useApi<IGetLabelsResponse | undefined>(
    useCallback(async () => {
      return await getLabels({
        order: params.get("order") as PageOrderEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        orderBy: params.get("orderBy") || undefined,
        query: params.get("query") || undefined,
        createdDateRange: params.get("createdDateRange") || undefined,
        updatedDateRange: params.get("updatedDateRange") || undefined,
      });
    }, [params]),
  );
  const contextValue = {
    labels,
    isLoading,
    error,
    setReload,
  };
  return (
    <LabelsContext.Provider value={contextValue}>
      {children}
    </LabelsContext.Provider>
  );
};
