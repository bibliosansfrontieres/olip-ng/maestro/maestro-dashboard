import { createContext, ReactNode, useCallback } from "react";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useSearchParams } from "react-router-dom";
import { getDevices } from "services/devices/devices.service";
import { IGetDevicesResponse } from "services/devices/interfaces/getDevices.interface";
import { BooleanEnum } from "src/enums/boolean.enum";

type ContextValue = {
  devices: IGetDevicesResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  devices: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const DevicesContext = createContext<ContextValue>(defaultValue);

export const DevicesContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [params] = useSearchParams();

  const {
    result: devices,
    isLoading,
    error,
    setReload,
  } = useApi<IGetDevicesResponse | undefined>(
    useCallback(async () => {
      return await getDevices({
        order: params.get("order") as PageOrderEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
        orderBy: params.get("orderBy") || undefined,
        isAliveOnly:
          params.get("isAliveOnly") === "FALSE"
            ? BooleanEnum.FALSE
            : BooleanEnum.TRUE,
        query: params.get("query") || undefined,
        lastSeenDateRange: params.get("lastSeenDateRange") || undefined,
        fqdnMaskIds: params.get("fqdnMaskIds") || undefined,
        labelIds: params.get("labelIds") || undefined,
        statuses: params.get("statuses") || undefined,
      });
    }, [params]),
  );
  const contextValue = {
    devices,
    isLoading,
    error,
    setReload,
  };
  return (
    <DevicesContext.Provider value={contextValue}>
      {children}
    </DevicesContext.Provider>
  );
};
