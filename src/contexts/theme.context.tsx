import { createContext, ReactNode, useEffect, useState } from "react";

type ContextValue = {
  setDarkMode: (value: boolean) => void;
  darkMode: boolean;
};

const defaultValue: ContextValue = {
  darkMode: false,
  setDarkMode: () => {},
};

export const ThemeContext = createContext<ContextValue>(defaultValue);

export const ThemeContextProvider = ({ children }: { children: ReactNode }) => {
  const [darkMode, setDarkMode] = useState(
    localStorage.getItem("darkMode") === "true"
  );
  useEffect(() => {
    if (darkMode) {
      document.body.classList.add("dark");
    } else {
      document.body.classList.remove("dark");
    }
  }, [darkMode]);
  const setDarkModeLocalStorage = (value: boolean) => {
    localStorage.setItem("darkMode", `${value}`);
    setDarkMode(value);
  };
  const contextValue = {
    darkMode,
    setDarkMode: setDarkModeLocalStorage,
  };

  return (
    <ThemeContext.Provider value={contextValue}>
      {children}
    </ThemeContext.Provider>
  );
};
