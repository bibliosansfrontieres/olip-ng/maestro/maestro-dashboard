import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useCallback,
  useState,
} from "react";
import { BooleanEnum } from "enums/boolean.enum";
import { PermissionNameEnum } from "enums/permissionName.enum";
import useApi from "hooks/useApi";
import { usePermissions } from "hooks/usePermission";
import { getGroups } from "services/groups/groups.service";
import {
  IGetGroupsParams,
  IGetGroupsResponse,
} from "services/groups/interfaces/getGroups.interface";
import {
  IGetVirtualMachinesParams,
  IGetVirtualMachinesResponse,
} from "services/virtualMachines/interfaces/getVirtualMachines.interface";
import { getVirtualMachines } from "services/virtualMachines/virtualMachines.service";

type ContextValue = {
  groupsResult: IGetGroupsResponse | undefined;
  groupsAreLoading: boolean;
  groupsErrors: boolean;
  groupsParams: IGetGroupsParams;
  setGroupsParams: Dispatch<SetStateAction<IGetGroupsParams>>;
  setGroupsReload: (value: boolean) => void;
  vmsResult: IGetVirtualMachinesResponse | undefined;
  vmsAreLoading: boolean;
  vmsErrors: boolean;
  vmsParams: IGetVirtualMachinesParams;
  setVmsParams: Dispatch<SetStateAction<IGetVirtualMachinesParams>>;
};

const defaultValue: ContextValue = {
  groupsResult: undefined,
  groupsAreLoading: true,
  groupsErrors: false,
  groupsParams: {} as IGetGroupsParams,
  setGroupsParams: () => {},
  setGroupsReload: () => {},
  vmsResult: undefined,
  vmsAreLoading: true,
  vmsErrors: false,
  vmsParams: {} as IGetVirtualMachinesParams,
  setVmsParams: () => {},
};

export const HomePageContext = createContext<ContextValue>(defaultValue);

export const HomePageContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [vmsParams, setVmsParams] = useState({} as IGetVirtualMachinesParams);
  const [groupsParams, setGroupsParams] = useState({} as IGetGroupsParams);
  const hasPermissionVms = usePermissions(
    PermissionNameEnum.READ_VIRTUAL_MACHINE,
  );
  const hasPermissionGroups = usePermissions(PermissionNameEnum.READ_GROUP);

  const {
    result: vmsResult,
    isLoading: vmsAreLoading,
    error: vmsErrors,
  } = useApi<IGetVirtualMachinesResponse | undefined>(
    useCallback(async () => {
      if (hasPermissionVms) {
        return await getVirtualMachines({
          ...vmsParams,
          isUserVmsFiltered: BooleanEnum.TRUE,
        });
      }
    }, [vmsParams, hasPermissionVms]),
  );

  const {
    result: groupsResult,
    isLoading: groupsAreLoading,
    error: groupsErrors,
    setReload: setGroupsReload,
  } = useApi<IGetGroupsResponse | undefined>(
    useCallback(async () => {
      if (hasPermissionGroups) {
        return await getGroups({
          ...groupsParams,
          isOnlyTakingUserGroups: BooleanEnum.TRUE,
        });
      }
    }, [groupsParams, hasPermissionGroups]),
  );

  const contextValue = {
    groupsResult,
    groupsAreLoading,
    groupsErrors,
    groupsParams,
    setGroupsParams,
    setGroupsReload,
    vmsResult,
    vmsAreLoading,
    vmsErrors,
    vmsParams,
    setVmsParams,
  };
  return (
    <HomePageContext.Provider value={contextValue}>
      {children}
    </HomePageContext.Provider>
  );
};
