import { createContext, ReactNode, useCallback } from "react";
import useApi from "hooks/useApi";
import { useParams } from "react-router-dom";
import { IGetVirtualMachineResponse } from "services/virtualMachines/interfaces/getVirtualMachine.interface";
import { getVirtualMachine } from "services/virtualMachines/virtualMachines.service";

type ContextValue = {
  vm: IGetVirtualMachineResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
};

const defaultValue: ContextValue = {
  vm: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
};

export const VmContext = createContext<ContextValue>(defaultValue);

export const VmContextProvider = ({ children }: { children: ReactNode }) => {
  const params = useParams();

  const {
    result: vm,
    isLoading,
    error,
    setReload,
  } = useApi<IGetVirtualMachineResponse | undefined>(
    useCallback(async () => {
      if (params.id) {
        return await getVirtualMachine({
          id: params.id,
        });
      }
    }, [params.id]),
  );
  const contextValue = {
    vm,
    isLoading,
    error,
    setReload,
  };
  return (
    <VmContext.Provider value={contextValue}>{children}</VmContext.Provider>
  );
};
