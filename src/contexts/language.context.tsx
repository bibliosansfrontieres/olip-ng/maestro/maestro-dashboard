import { createContext, ReactNode, useEffect, useState } from "react";
import { enGB } from "date-fns/locale/en-GB";
import { fr } from "date-fns/locale/fr";
import { MaestroLanguagesEnum } from "enums/maestroLanguages.enum";
import { registerLocale, setDefaultLocale } from "react-datepicker";
import { useTranslation } from "react-i18next";
import { convertLanguagesIso6393ToIso6391 } from "utils/languages";
registerLocale("FRA", fr);
registerLocale("ENG", enGB);

type ContextValue = {
  setLanguage: (value: MaestroLanguagesEnum) => void;
  language: MaestroLanguagesEnum | string;
};

const defaultValue: ContextValue = {
  language: MaestroLanguagesEnum.FRA,
  setLanguage: () => {},
};

export const LanguageContext = createContext<ContextValue>(defaultValue);

export const LanguageContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const { i18n } = useTranslation();
  const [language, setLanguage] = useState(
    (localStorage.getItem("language") as MaestroLanguagesEnum) ??
      MaestroLanguagesEnum.FRA,
  );

  useEffect(() => {
    i18n.changeLanguage(
      convertLanguagesIso6393ToIso6391(language ?? MaestroLanguagesEnum.FRA),
    );
    // for datepicker
    setDefaultLocale(language ?? MaestroLanguagesEnum.FRA);
    localStorage.setItem("language", `${language}`);
  }, [language, i18n]);

  const setLanguageLocalStorage = (value: MaestroLanguagesEnum) => {
    setLanguage(value);
  };

  const contextValue = {
    language: language ?? MaestroLanguagesEnum.FRA,
    setLanguage: setLanguageLocalStorage,
  };

  return (
    <LanguageContext.Provider value={contextValue}>
      {children}
    </LanguageContext.Provider>
  );
};
