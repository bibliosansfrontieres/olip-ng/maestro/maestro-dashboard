import { createContext, ReactNode, useCallback } from "react";
import { PageOrderEnum } from "enums/pageOrder.enum";
import useApi from "hooks/useApi";
import { useLocation, useSearchParams } from "react-router-dom";
import { getGroups } from "services/groups/groups.service";
import { IGetGroupsResponse } from "services/groups/interfaces/getGroups.interface";
import { IGetVirtualMachineGroupsResponse } from "services/virtualMachines/interfaces/getVirtualMachineGroups.interface";
import { getVirtualMachineGroups } from "services/virtualMachines/virtualMachines.service";

type ContextValue = {
  groups: IGetGroupsResponse | IGetVirtualMachineGroupsResponse | undefined;
  isLoading: boolean;
  error: boolean;
  setReload: (value: boolean) => void;
  isInVmTabs?: boolean;
};

const defaultValue: ContextValue = {
  groups: undefined,
  isLoading: true,
  error: false,
  setReload: () => {},
  isInVmTabs: false,
};

export const GroupsContext = createContext<ContextValue>(defaultValue);

export const GroupsContextProvider = ({
  children,
  isInVmTabs = false,
}: {
  children: ReactNode;
  isInVmTabs?: boolean;
}) => {
  const [params] = useSearchParams();
  const location = useLocation();

  const {
    result: groups,
    isLoading,
    error,
    setReload,
  } = useApi<IGetGroupsResponse | IGetVirtualMachineGroupsResponse | undefined>(
    useCallback(async () => {
      if (isInVmTabs) {
        const id = location.pathname.split("/")[2];
        if (id) {
          return await getVirtualMachineGroups({
            id: id,
            query: params.get("query") || undefined,
            order: params.get("order") as PageOrderEnum,
            page: Number(params.get("page")) || undefined,
            take: Number(params.get("take")) || undefined,
          });
        }
      }
      return await getGroups({
        query: params.get("query") || undefined,
        order: params.get("order") as PageOrderEnum,
        page: Number(params.get("page")) || undefined,
        take: Number(params.get("take")) || undefined,
      });
    }, [params, isInVmTabs, location]),
  );
  const contextValue = {
    groups,
    isLoading,
    error,
    setReload,
    isInVmTabs,
  };
  return (
    <GroupsContext.Provider value={contextValue}>
      {children}
    </GroupsContext.Provider>
  );
};
