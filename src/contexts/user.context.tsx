import { createContext, ReactNode, useCallback, useState } from "react";
import { IUser } from "interfaces/user.interface";
import { postToken } from "services/auth/auth.service";
import { getProfile } from "services/users/users.service";
import clearLocalStorage from "utils/clearLocalStorage";

type ContextValue = {
  checkToken: () => boolean | Promise<boolean>;
  login: () => Promise<void>;
  user: IUser | null;
  isLogged: boolean;
};

const defaultValue: ContextValue = {
  checkToken: () => false,
  login: async () => {},
  user: null,
  isLogged: false,
};

export const UserContext = createContext<ContextValue>(defaultValue);

export const UserContextProvider = ({ children }: { children: ReactNode }) => {
  const localUser = localStorage.getItem("user") || null;
  const [user, setUser] = useState<IUser | null>(
    localUser ? JSON.parse(localUser) : null,
  );
  const [isLogged, setIsLogged] = useState(false);

  /**
   * Checks the validity of the current access token and refreshes it if necessary.
   *
   * @returns {boolean | Promise<boolean>} True if the access token is valid.
   */
  const checkToken = useCallback((): boolean | Promise<boolean> => {
    /**
     * Attempts to obtain a new access token using the refresh token stored in local storage.
     *
     * @returns {Promise<boolean>} True if a new access token was successfully obtained.
     */
    const getNewToken = async (): Promise<boolean> => {
      const refreshToken = localStorage.getItem("refreshToken");
      if (refreshToken) {
        const result = await postToken({ refreshToken });
        if (result) {
          const { accessToken: newAccessToken, expiresAt: newExpiresAt } =
            result;
          localStorage.setItem("accessToken", newAccessToken);
          localStorage.setItem("expiresAt", `${newExpiresAt}`);
          setIsLogged(true);
          return true;
        }
      }
      setIsLogged(false);
      setUser(null);
      clearLocalStorage();
      return false;
    };

    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");
    const expiresAt = localStorage.getItem("expiresAt");
    if (accessToken && refreshToken && expiresAt) {
      if (parseFloat(expiresAt) - 60 * 1000 > Date.now()) {
        setIsLogged(true);
        if (!user) {
          getProfile().then((userProfile) => {
            setUser(userProfile);
            localStorage.setItem("user", JSON.stringify(userProfile));
          });
        }
        return true;
      }
      return getNewToken();
    }
    setIsLogged(false);
    return false;
  }, [user]);

  /**
   * Set the user and refreshes the access token if necessary.
   * Logout if the access token is invalid.
   *
   * @returns {Promise<void>} A promise that resolves when the login operation is complete.
   */
  const login = async (): Promise<void> => {
    const check = await checkToken();
    if (!check) {
      setUser(null);
      clearLocalStorage();
      setIsLogged(false);
      return;
    }
    const userProfile = await getProfile();
    setUser(userProfile ?? null);
  };

  const contextValue = {
    login,
    checkToken,
    user,
    isLogged,
  };

  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  );
};
