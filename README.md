# Maestro Dashboard

## Usage for developement environment

### Steps

Clone repository (SSH) :

```shell
git clone git@gitlab.com:bibliosansfrontieres/maestro/maestro-dashboard.git
```

Clone repository (HTTPS) :

```shell
git clone https://gitlab.com/bibliosansfrontieres/maestro/maestro-dashboard.git
```

Install node dependencies :

```shell
npm install
```

Copy environment variables :

```shell
cp .env.example .env
```

Run for development :

```shell
npm run dev
```

## Usage with Docker

Copy environment variables :

```shell
cp .env.example .env
```

Create a docker compose :

```yaml
---
version: "3.8"
services:
  maestro-api:
    image: registry.gitlab.com/bibliosansfrontieres/maestro/maestro-dashboard:latest
    env_file:
      - ".env"
    ports:
      - "3001:80"
```

Run docker compose :

```shell
docker compose up -d
```

## Test with cypress

Install node dependencies :

```shell
npm install
```

Copy environment variables :

```shell
cp .env.example .env
```

### Run cypress :

#### Open cypress :

```shell
npm run cy:open
```

or

```shell
npx cypress open
```

#### Run in CLI :

```shell
npx cypress run
```

Possible options :

```shell
npx cypress run --browser firefox --spec "cypress/e2e/init/homePage.cy.ts"
```

See https://docs.cypress.io/guides/guides/command-line#Options for all options
